<?php
namespace Tests\unit;
use CDE\Actors\Admin\UseCases\CreateTuile\Boundaries\RepositoryInterface;
use CDE\Actors\Admin\UseCases\CreateTuile\CreateTuile;
use CDE\Actors\Admin\UseCases\CreateTuile\RequestData;
use CDE\Actors\Admin\UseCases\CreateTuile\ResponseData;
use CDE\Actors\BoundaryDataFactory;
use CDE\Entities\Tuiles\TuileFactory;
use CDE\TestApi\DataModelHelper;
use Codeception\Test\Unit;
use RuntimeException;

class AdminCreateTuileTest extends Unit
{
    /**
     * @var CreateTuile
     */
    protected $useCase;

    /**
     * @var RequestData
     */
    protected $requestData;

    /**
     * @var ResponseData
     */
    protected $responseData;

    /**
     * @var  RepositoryInterface
     */
    protected $repository;

    /** @var BoundaryDataFactory */
    protected $boundaryFactory;

    /**
     * @var array
     */
    protected $requestDataStruct;

    /**
     * @var DataModelHelper
     */
    protected $dataStructHelper;

    /**
     * @var array
     */
    protected $responseDataStruct;


    public function setUp()
    {
        # set up objects
        $this->repository = $this->getMockForAbstractClass(RepositoryInterface::class);

        # setup data structure
        $this->boundaryFactory = new BoundaryDataFactory();
        $this->dataStructHelper = new DataModelHelper($this->boundaryFactory, 'AdminCreateTuile');
    }


    public function testCreateTuileSimpleRegular()
    {
        #prepare request data
        /** @var RequestData $request */
        $request = $this->dataStructHelper->createFakeRequestData("TuileLink");

        # fix type data
        $request->typeData = "TuileLink";


        # prepare internal components behaviour
        $tuileReturned = TuileFactory::createTuile("TuileLink");
        $tuileReturned->fillFromDataStructure($request);
        $tuileReturned->setId(time());

        $this->repository->expects($this->exactly(1))
            ->method('persist')
            ->will($this->returnValue($tuileReturned));

        # invoke use case
        $this->useCase      = new CreateTuile($this->repository);
        /** @var ResponseData $response */
        $responseActual = $this->useCase->doCreateTuile($request);

        # prepare response expected
        $responseExpected =  $this->boundaryFactory::createResponseDataFrom("TuileLink", $tuileReturned);

        # assert response has same mandatory properties then request
        $assertable = $request->getMandatoryFields();
        foreach ($assertable as $field) {
            $this->assertEquals($responseExpected->$field, $responseActual->$field);
        }

        # assert response mandatory fields are present
        $assertable = $responseActual->getMandatoryFields();
        foreach ($assertable as $field) {
            $this->assertEquals($responseExpected->$field, $responseActual->$field);
        }

        var_dump($responseActual);

    }

    public function testCreateTuileBadClassName_1()
    {
        $this->expectException(\InvalidArgumentException::class);
        #prepare request data
        /** @var RequestData $request */
        $request = $this->dataStructHelper->createFakeRequestData("TuileLinkss");
    }

    public function testCreateTuileBadClassName_2()
    {
        $this->expectException(\InvalidArgumentException::class);
        #prepare request data
        /** @var RequestData $request */
        $request = $this->dataStructHelper->createFakeRequestData("TuileLinks");

        # fix type data
        $request->typeData = "TuileLink";

        # prepare internal components behaviour
        $tuileReturned = TuileFactory::createTuile("TuileLinksss");
    }

    public function testCreateTuileBadClassName_3()
    {
        $this->expectException(\InvalidArgumentException::class);
        #prepare request data
        /** @var RequestData $request */
        $request = $this->dataStructHelper->createFakeRequestData("TuileLink");

        # fix type data
        $request->typeData = "TuileLink";

        # prepare internal components behaviour
        $tuileReturned = TuileFactory::createTuile("TuileLink");
        $tuileReturned->fillFromDataStructure($request);
        $tuileReturned->setId(time());

        $this->repository->expects($this->exactly(1))
            ->method('persist')
            ->will($this->returnValue($tuileReturned));

        # invoke use case
        $this->useCase      = new CreateTuile($this->repository);
        /** @var ResponseData $response */
        $responseActual = $this->useCase->doCreateTuile($request);

        # prepare response expected
        $responseExpected =  $this->boundaryFactory::createResponseDataFrom("TuileLinkss", $tuileReturned);
    }

    public function testCreateTuilePersistThrowException()
    {
        $this->expectException(\RuntimeException::class);
        #prepare request data
        /** @var RequestData $request */
        $request = $this->dataStructHelper->createFakeRequestData("TuileLink");

        # fix type data
        $request->typeData = "TuileLink";

        # prepare internal components behaviour
        $tuileReturned = TuileFactory::createTuile("TuileLink");
        $tuileReturned->fillFromDataStructure($request);
        $tuileReturned->setId(time());

        $this->repository->expects($this->exactly(1))
            ->method('persist')
            ->will($this->throwException(new RuntimeException()));

        # invoke use case
        $this->useCase      = new CreateTuile($this->repository);
        /** @var ResponseData $response */
        $responseActual = $this->useCase->doCreateTuile($request);
    }
}