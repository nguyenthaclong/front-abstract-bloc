<?php

/**
 * Class Admin_Gestion_Page_GRU_User_Context
 */
class Admin_Gestion_Page_GRU_User_Context
{
    private $token;

    private $account_info;

    /**
     * @return bool
     */
    public function is_authenticated()
    {
        return is_user_logged_in();
    }

    /**
     * @return bool|int
     */
    public function is_admin()
    {
        return current_user_can('administrator');
    }

    /**
     * @return WP_User Current WP_User instance.
     */
    public function get_user()
    {
        return wp_get_current_user();
    }

    /**
     * @return string
     */
    public function get_token()
    {
        return $this->token;
    }

    /**
     * @param $token
     */
    public function set_token($token)
    {
        $this->token = $token;
    }


    /**
     * @return string|null
     */
    public function get_access_token()
    {
        if ($this->is_authenticated()) {
            $user = $this->get_user();
            $userId = $user instanceof WP_User ? $user->data->ID : $user;

            return get_user_meta($userId, 'access_token')[0];
        }

        return isset($this->token->access_token) && !empty($this->token->access_token) ? $this->token->access_token : null;
    }

    /**
     * @param string $access_token
     */
    public function set_access_token($access_token)
    {
        $this->token->access_token = $access_token;
    }

    /**
     * @return WP_User
     */
    public function create_user()
    {
        return new WP_User();
    }

    /**
     * @param WP_User $user
     * @param string  $data_item
     * @param mixed   $value
     */
    public function set_data($user, $data_item, $value)
    {
        $user->data->$data_item = $value;
    }

    /**
     * @param WP_User $user
     * @param         $data_item
     *
     * @return mixed
     */
    public function get_data($user, $data_item)
    {
        return $user->data->$data_item;
    }

    /**
     * @param WP_User|int $user
     * @param string      $meta_key
     * @param bool        $single
     *
     * @return mixed
     */
    public function get_user_meta($user, $meta_key, $single = false)
    {
        $userId = $user instanceof WP_User ? $user->data->ID : $user;

        return get_user_meta($userId, $meta_key, $single);
    }

    /**
     * @param WP_User|int $user
     * @param string      $meta_key
     * @param mixed       $meta_value
     * @param string      $prev_value
     */
    public function update_user_meta($user, $meta_key, $meta_value, $prev_value = '')
    {
        $userId = $user instanceof WP_User ? $user->data->ID : $user;
        update_user_meta($userId, $meta_key, $meta_value, $prev_value);
    }

    /**
     * @param WP_User|int $user
     * @param string      $meta_key
     * @param mixed       $meta_value
     * @param bool        $unique
     */
    public function add_user_meta($user, $meta_key, $meta_value, $unique = false)
    {
        $userId = $user instanceof WP_User ? $user->data->ID : $user;
        add_user_meta($userId, $meta_key, $meta_value, $unique);
    }

    /**
     * @param string $field
     * @param mixed  $value
     *
     * @return bool|WP_User
     */
    public function get_user_by($field, $value)
    {
        return get_user_by($field, $value);
    }

    /**
     * @param array $data
     *
     * @return int|WP_Error
     */
    public function insert_user($data)
    {
        return wp_insert_user($data);
    }

    /**
     * @param array $data
     *
     * @return int|WP_Error
     */
    public function update_user($data)
    {
        return wp_update_user($data);
    }

    public function get_gru_id($single = false)
    {
        if ($this->is_authenticated()) {
            $user = $this->get_user();
            return get_user_meta($user->data->ID, 'gru_id', $single)[0];
        }

        return null;
    }

    public function get_account_info()
    {
        return $this->account_info;
    }

    public function set_account_info($info)
    {
        $this->account_info = $info;
    }

    public function get_display_name()
    {
        return $this->account_info->data->prenom . ' ' . $this->account_info->data->nom_usage;
    }

    /**
     * @param        $user
     * @param        $access_token
     * @param string $prev_value
     */
    public function update_access_token($user, $access_token, $prev_value = '')
    {
        $userId = $user instanceof WP_User ? $user->data->ID : $user;
        update_user_meta($userId, 'access_token', $access_token, $prev_value);
    }

    /**
     * @param        $user
     * @param        $expires_in
     * @param string $prev_value
     */
    public function update_expires_in($user, $expires_in, $prev_value = '')
    {
        $userId = $user instanceof WP_User ? $user->data->ID : $user;
        update_user_meta($userId, 'expires_in', $expires_in, $prev_value);
    }

    /**
     * @param        $user
     * @param        $refresh_token
     * @param string $prev_value
     */
    public function update_refresh_token($user, $refresh_token, $prev_value = '')
    {
        $userId = $user instanceof WP_User ? $user->data->ID : $user;
        update_user_meta($userId, 'refresh_token', $refresh_token, $prev_value);
    }
}
