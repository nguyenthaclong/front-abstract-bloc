<?php

/**
 * 1997-2018 Quadra Informatique
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 *
 *  @author    Quadra Informatique <modules@quadra-informatique.fr>
 *  @copyright 1997-2018 Quadra Informatique
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * 
 *  Admin_Gestion_Page_GRU_Tuile
 */
class Admin_Gestion_Page_GRU_Import_Export {

    /**
     * Répertoire d'upload pour les imports de configuration
     * 
     * @var string 
     */
    private $import_dir;

    /**
     * Bootstraps the class and hooks required actions & filters.
     */
    public function __construct() {
        add_action('wp_ajax_export_module_configuration', array($this, 'export_module_configuration'), 50);
        add_action('wp_ajax_import_configuration', array($this, 'import_configuration'), 50);
        add_action('wp_ajax_validate_import_configuration', array($this, 'validate_import_configuration'), 50);

        $plugin_base_name = explode('/', plugin_basename(__FILE__))[0];
        $absolute_base_name = explode($plugin_base_name, plugin_dir_path(__FILE__))[0] . $plugin_base_name;
        $this->import_dir = "$absolute_base_name/assets/data/import/";
    }

    /**
     * Ajax
     * Exporte la configuration présent dans les tables du modules :
     *  Grilles / Menus / Tuiles
     * 
     * @global type $wpdb
     * @return void
     */
    public function export_module_configuration() {
        global $wpdb;
        
        $plugin_base_name = explode('/', plugin_basename(__FILE__))[0];
        $plugin_dir_path = explode($plugin_base_name, plugin_dir_path(__FILE__))[0];
        $path_download = plugins_url("$plugin_base_name");

        $export = array(
            "pages" => array(),
            "config" => array(),
            "grids" => array(),
            "menus" => array(),
            "tuiles" => array()
        );        
        
        $pages = get_pages();
        foreach ($pages as $page) {
            $page->post_content = htmlentities($page->post_content);
            $export["pages"][$page->ID] = json_decode(json_encode($page), true);
        }
        
        $link_options = get_option(Admin_Gestion_Page_GRU_Tools::links_pages_option);        
        foreach($link_options as $key => $value) {
            if($value === "") {
                continue;
            }            
            $export['config'][$key] = $export['pages'][$value]['post_name'];
        }
        
        $grids = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}gestion_page_gru_page_correspondance_grid`");
        foreach ($grids as $grid) {
            $page = get_page($grid->page_correspondance_grid_id);
            $grid->page_title = $page->post_title;
            $export["grids"][$grid->page_correspondance_grid_id] = json_decode(json_encode($grid), true);
        }

        $menus = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}gestion_page_gru_page_menu`");
        foreach ($menus as $menu) {
            $export["menus"][$menu->page_menu_id] = json_decode(json_encode($menu), true);
        }

        $tuiles = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}gestion_page_gru_tuile` WHERE tuile_type NOT IN ('1','2')");
        foreach ($tuiles as $tuile) {
            $result_style = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}gestion_page_gru_tuile_style` WHERE tuile_style_tuile_id = {$tuile->tuile_id}");
            
            $test = '../assets/images/tuiles/'.$tuile->tuile_image;
            if($tuile->tuile_image !== "" && is_file(__DIR__ . "/../../assets/images/tuiles/" .$tuile->tuile_image)) {                
                $image = file_get_contents(__DIR__ . "/../../assets/images/tuiles/" .$tuile->tuile_image);
                $tuile->tuile_image_base64 = base64_encode($image);
            }
            $export["tuiles"][$tuile->tuile_id] = json_decode(json_encode($tuile), true);
            $export["tuiles"][$tuile->tuile_id]['styles'] = json_decode(json_encode($result_style), true);
            if ($tuile->tuile_type == "2") {
                $result_correspondance = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}gestion_page_gru_tuile_ctg_correspondance` WHERE tuile_ctg_correspondance_parent_id = {$tuile->tuile_id}");
                $export['tuiles'][$tuile->tuile_id]['correspondances'] = json_decode(json_encode($result_correspondance), true);
            }
        }               

        $xml_export = new SimpleXMLElement("<?xml version=\"1.0\"?><export_infos></export_infos>");
        Admin_Gestion_Page_GRU_Tools::array_to_xml($export, $xml_export);

        $path_file = "/assets/data/export/" . uniqid() . ".xml";
        file_put_contents($plugin_dir_path . $plugin_base_name . $path_file, $xml_export->asXML());
        echo $path_download . $path_file;
        wp_die();
    }

    /**
     * Ajax
     * Importe la configuration dans les tables du modules :
     *  Grilles / Menus / Tuiles
     * 
     * @global type $wpdb
     * @return void
     */
    public function import_configuration() {

        $tmp_name = $_FILES["file"]["tmp_name"];
        $name = $_FILES["file"]["name"];
        $check_upload = move_uploaded_file($tmp_name, $this->import_dir . $name);


        if (file_exists($this->import_dir . $name)) {
            $export_infos = simplexml_load_file($this->import_dir . $name);

            $html = "<div id='import-popup-content'><input type='hidden' id='import-file' value='$name' />";
            
            $pages = $export_infos->{"item-pages"};            
            $pages_correspondances = array();
            $select_page = $select_page_menu = '<select class="select-page-correspondance">';
            $select_page_menu .= "<option value='-1'> --- Pas de lien --- </option>";
            foreach ($pages->children() as $page) {
                $pages_correspondances["".$page->ID.""] =  "".$page->post_title."";
                $select_page .= '<option value="' . $page->post_name . '">' . $page->post_title . '</option>';
                $select_page_menu .= '<option value="' . $page->post_name . '">' . $page->post_title . '</option>';
            }
            $select_page .= "</select>";
            $select_page_menu .= "</select>";

            foreach ($export_infos as $key => $value) {
                switch ($key) {
                    
                    case "item-pages" :
                        $html .= <<<HTML
                        <div class="row">
                            <div class="col-lg-12">
                               <h3>Pages importées</h3>
                            </div>
                        </div>
                        <div id="page-import">
                            <div data-grid='{$page->guid}'>
                                <div>
                                    <ul class="row" >
HTML;
                        foreach ($value as $page) {
                            $html .= <<<HTML
                                        <li class="col-lg-3">
                                            $page->post_title
                                        </li>
HTML;
                        }
                            $html .= <<<HTML
                                    </ul>
                                </div> 
                            </div>
                        </div><br/><nr/>
                                    
                        <div class="row">
                            <div class="col-lg-12">
                               <h3>Définir la page d'accueil</h3>
                            </div>
                        </div>
                        <div id="home-page">
                            <div class="row">
                                <div class="col-lg-2">
                                    Page d'accueil
                                </div>
                                <div class="col-lg-1">
                                    <i class="fa fa-arrow-alt-circle-right"></i>
                                </div>
                                <div class="col-lg-3">
                                    $select_page
                                </div>
                            </div>
                        </div>
HTML;
                        break;
                        
                    case "item-config" : 
                        $html .= <<<HTML
                        <div class="row">
                            <div class="col-lg-12">
                               <h3>Configuration à importer</h3>
                            </div>
                        </div>
                        <div id="config-import">
                            <div class="row">
HTML;
                        foreach ($value as $key => $config) {
                            $html .= <<<HTML
                                <div class="col-lg-6 config-data" data-key='$key'>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            $config
                                        </div> 
                                        <div class="col-lg-2">
                                            <i class="fa fa-arrow-alt-circle-right"></i>
                                        </div>
                                        <div class="col-lg-6">
HTML;
                            $select_page_config = str_replace('value="'.$config.'"', 'value="'.$config.'" selected', $select_page);
                            $html .= <<<HTML
                                        $select_page_config
                                        </div>
                                    </div>
                                </div>
HTML;
                        }
                        $html .= "</div>";
                        break;
                    case "item-grids" :
                        $html .= <<<HTML
                        <div class="row">
                            <div class="col-lg-12">
                               <h3>Grilles à importer</h3>
                            </div>
                        </div>
                        <div id="grid-import" class="row">         
HTML;
                        foreach ($value as $grid) {
                            $html .= <<<HTML
                            <div class="col-lg-6 grid-data" data-grid='{$grid->page_correspondance_grid}'>
                                <div class="row">
                                    <div class="col-lg-4">
                                         <i class="fa fa-toggle-on toggle-import"></i>
                                        <span class="page-title">$grid->page_title</span>
                                    </div> 
                                    <div class="col-lg-2">
                                        <i class="fa fa-arrow-alt-circle-right"></i>
                                    </div>
                                    <div class="col-lg-6">
HTML;
                            $select_page_grid = str_replace(">" . $grid->page_title . "</option>", " selected>" . $grid->page_title . "</option>", $select_page);
                            $html .= <<<HTML
                                    $select_page_grid
                                    </div>
                                </div>
                            </div>
HTML;
                        }
                        $html .= "</div><br/><nr/>";
                        break;
                    case "item-menus" :
                        $html .= <<<HTML
                        <div class="row">
                            <div class="col-lg-12">
                               <h3>Menus à importer</h3>
                            </div>
                        </div>
                        <div id="menu-import">
HTML;

                        foreach ($value as $menu) {
                            $menu_content = unserialize($menu->page_menu_content);
                            $html .= <<<HTML
                            <div id="menu-id-{$menu->page_menu_id}" class="main-menu">
                                <div class="row">
                                    <div class="col-lg-1 text-center">
                                        <i class="fa fa-toggle-on toggle-import menu-import main-menu"></i>
                                    </div>
                                    <div class="col-lg-2">
                                        Libellé du menu : 
                                    </div>
                                    <div class="col-lg-9">
                                        <input type="text" value="{$menu->page_menu_libelle}" name="menu-libelle"/>
                                    </div>
                                </div> 
                                <div class="row">
                                    <div class="offset-lg-1 col-lg-11">
HTML;
                            foreach ($menu_content as $menu_n1) {

                                $select_page_menu_selected = str_replace(">" . $menu_n1['libelle'] . "</option>", " selected>" . $menu_n1['libelle'] . "</option>", $select_page_menu);
                                
                                $html .= <<<HTML
                                        <div name="{$menu_n1['sub-menu-id']}" class="sub-menu">
                                            <div class="row">        
                                                <div class="col-lg-1 text-center">
                                                    <i class="fa fa-toggle-on toggle-import menu-import sub-menu"></i>
                                                </div>                
                                                <div class="col-lg-2">
                                                    Sous-menu :
                                                </div>
                                                <div class="col-lg-4">
                                                    <input type="text" value="{$menu_n1['libelle']}" name="sub-menu-libelle"/>
                                                </div>
                                                <div class="col-lg-1">
                                                    <i class="fa fa-arrow-alt-circle-right"></i>
                                                </div>
                                                <div class="col-lg-4">
                                                    $select_page_menu_selected
                                                </div> 
                                            </div>                        
HTML;
                                if (array_key_exists('childs', $menu_n1)) {
                                    foreach ($menu_n1['childs'] as $child) {
                                        
                                        $replace = (isset($pages_correspondances[$child['id-page-link']]))? $pages_correspondances[$child['id-page-link']] : "";                                        
                                        $select_page_menu_selected = str_replace(">" . $replace . "</option>", " selected>" . $replace . "</option>", $select_page_menu);
                                      
                                        $html .= <<<HTML
                                                <div class="row link" name="{$child['link-id']}">
                                                    <div class="offset-lg-1 col-lg-1 text-center">
                                                        <i class="fa fa-toggle-on toggle-import link"></i>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        Lien :
                                                    </div>                                                    
                                                    <div class="col-lg-3">
                                                        <input type="text" value="{$child['libelle']}" name="link-libelle"/>
                                                    </div>
                                                    <div class="col-lg-1">
                                                        <i class="fa fa-arrow-alt-circle-right"></i>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        $select_page_menu_selected
                                                    </div> 
                                                </div>
HTML;
                                    }
                                }

                                $html .= <<<HTML
                                        </div> 
HTML;
                            }

                            $html .= <<<HTML
                                        </div>
                                    </div>
                                </div> 
HTML;
                        }
                        $html .= "</div>";
                        break;
                    case "item-tuiles" :
                        $html .= <<<HTML
                        <div class="row">
                            <div class="col-lg-12">
                               <h3>Tuiles importées</h3>
                            </div>
                        </div>
                        <div id="tuile-import">
HTML;
                        $html_type_txt = $html_type_inf = $html_type_lien = '';
                        foreach ($value as $tuile) {
                            switch ($tuile->tuile_type[0]) {
                                case "0" :
                                    $html_type_txt .= "<li>" . $tuile->tuile_title . '</li>';
                                    break;
                                case "3" :
                                    $html_type_inf .= "<li>" . $tuile->tuile_title . '</li>';
                                    break;
                                case "4" :
                                    $html_type_lien .= "<li>" . $tuile->tuile_title . '</li>';
                                    break;
                            }
                        }
                        $html .= <<<HTML
                            <div class="row">
                                <div class="col-lg-4">
                                    <h5>Tuiles de type textuel</h5>
                                    <ol>$html_type_txt</ol>
                                </div>
                                <div class="col-lg-4">
                                    <h5>Tuiles de type information</h5>
                                    <ol>$html_type_inf</ol>  
                                </div>
                                <div class="col-lg-4">
                                    <h5>Tuiles de type lien</h5>
                                    <ol>$html_type_lien</ol> 
                                </div>
                            </div> 
                        </div>
HTML;
                        break;
                }
            }
            $html .= "</div>";

            $return = array(
                "code" => 2,
                "message" => "Fichier " . $name . " uploadé, saisir les correspondances avant de valider l'import",
                "html" => $html
            );
        } else {
            $return = array(
                "code" => 0,
                "message" => "Echec lors de l'ouverture du fichier " . $name
            );
        }

        echo json_encode($return);
        wp_die();
    }

    /**
     * Valide l'import de la configuration
     * 
     * @global type $wpdb
     * @return void
     */
    public function validate_import_configuration() {
        global $wpdb;
        $is_ok = true;
        $error_msg = "";
        if (file_exists($this->import_dir . $_POST['import_data']['import_file'])) {
            $export_infos = simplexml_load_file($this->import_dir . $_POST['import_data']['import_file']);
            
            $wpdb->query("DELETE FROM `{$wpdb->prefix}posts` WHERE post_type = 'page'");
            $pages = $export_infos->{"item-pages"};
            foreach ($pages->children() as $page) {
                $insert = $wpdb->insert("{$wpdb->prefix}posts", array(
                    'post_author' => $page->post_author,
                    'post_date' => $page->post_date,
                    'post_date_gmt' => $page->post_date_gmt,
                    'post_content' => html_entity_decode($page->post_content),
                    'post_title' => $page->post_title,
                    'post_excerpt' => $page->post_excerpt,
                    'post_status' => $page->post_status,
                    'comment_status' => $page->comment_status,
                    'ping_status' => $page->ping_status,
                    'post_password' => $page->post_password,
                    'post_name' => $page->post_name,
                    'to_ping' => $page->to_ping,
                    'pinged' => $page->pinged,
                    'post_modified' => $page->post_modified,
                    'post_modified_gmt' => $page->post_modified_gmt,
                    'post_content_filtered' => $page->post_content_filtered,
                    'post_parent' => $page->post_parent,
                    'guid' => $page->guid,
                    'menu_order' => $page->menu_order,
                    'post_type' => $page->post_type,
                    'post_mime_type' => $page->post_mime_type,
                    'comment_count' => $page->comment_count,
                ));
                if ($insert === false) {
                    $error_msg .= $wpdb->last_error;
                    if ($is_ok === true) {
                        $is_ok = false;
                    }
                }
            }
            
            $page_correspondance = array();
            $page_list = $wpdb->get_results("SELECT ID, post_name FROM {$wpdb->prefix}posts WHERE post_type = 'page'");     
            foreach($page_list as $page) {
                $page_correspondance[$page->post_name] = $page->ID;
            }
            
            $array_config = array();
            foreach($_POST['import_data']['config'] as $config) {
                $array_config[$config['key']] = $page_correspondance[$config['value']];
            }
            update_option(Admin_Gestion_Page_GRU_Tools::links_pages_option, $array_config);
            
            update_option('show_on_front', 'page');
            update_option('page_on_front', $page_correspondance[$_POST['import_data']['homepage']]);
            
            $wpdb->query("TRUNCATE TABLE `{$wpdb->prefix}gestion_page_gru_page_correspondance_grid`");
            foreach ($_POST['import_data']['grid'] as $grid) {
                
                $id_page = $page_correspondance[$grid['page_correspondance_grid_id']];   
                $insert = $wpdb->insert("{$wpdb->prefix}gestion_page_gru_page_correspondance_grid", array(
                    'page_correspondance_grid_id' => $id_page,
                    'page_correspondance_grid' => stripslashes($grid['page_correspondance_grid'])
                ));
                if ($insert === false) {
                    $error_msg .= $wpdb->last_error;
                    if ($is_ok === true) {
                        $is_ok = false;
                    }
                }
            }

            $wpdb->query("TRUNCATE TABLE `{$wpdb->prefix}gestion_page_gru_page_correspondance_menu`");
            $wpdb->query("TRUNCATE TABLE `{$wpdb->prefix}gestion_page_gru_page_menu`");
            foreach ($_POST['import_data']['menu'] as $menu) {
                foreach ($menu['childs'] as $key=>$child) {                    
                    $id_page = $page_correspondance[$child['id-page-link']];   
                    $child["id-page-link"] = $id_page;
                    foreach ($child["childs"] as $k=>$children) {                        
                        $id_page_child = $page_correspondance[$children["id-page-link"]];   
                        $children["id-page-link"] = $id_page_child;
                        $child["childs"][$k]["id-page-link"] = $id_page_child;
                    } 
                    $menu['childs'][$key] = $child;
                }
                
                $insert = $wpdb->insert("{$wpdb->prefix}gestion_page_gru_page_menu", array(
                    'page_menu_libelle' => $menu['menu-libelle'],
                    'page_menu_content' => serialize($menu['childs'])
                ));
                if ($insert === false) {
                    $error_msg .= $wpdb->last_error;
                    if ($is_ok === true) {
                        $is_ok = false;
                    }
                }
            }
            foreach($page_correspondance as $page) {
                $wpdb->insert("{$wpdb->prefix}gestion_page_gru_page_correspondance_menu", array(
                    'page_correspondance_menu_page_id' => $page,
                    'page_correspondance_menu_menu_id' => '1'
                ));
            }
            
            $images = glob(__DIR__ . "/../../assets/images/tuiles/*");
            foreach ($images as $img) { 
                if (is_file($img)) {
                    unlink($img);                    
                }
            }
            
            $wpdb->query("TRUNCATE TABLE `{$wpdb->prefix}gestion_page_gru_tuile`");
            $wpdb->query("TRUNCATE TABLE `{$wpdb->prefix}gestion_page_gru_tuile_ctg_correspondance`");
            $wpdb->query("TRUNCATE TABLE `{$wpdb->prefix}gestion_page_gru_tuile_style`");
            
            $tuiles = $export_infos->{"item-tuiles"};

            foreach ($tuiles->children() as $tuile) {
                if($tuile->tuile_type == "1" || $tuile->tuile_type == "2") {
                    continue;
                }
                $insert = $wpdb->insert("{$wpdb->prefix}gestion_page_gru_tuile", array(
                    'tuile_id' => $tuile->tuile_id,
                    'tuile_type' => $tuile->tuile_type,
                    'tuile_title' => $tuile->tuile_title,
                    'tuile_description' => $tuile->tuile_description,
                    'tuile_description_plus' => $tuile->tuile_description_plus,
                    'tuile_icon' => $tuile->tuile_icon,
                    'tuile_image' => $tuile->tuile_image,
                    'tuile_teleservice_id' => $tuile->tuile_teleservice_id,
                    'tuile_function_id' => $tuile->tuile_function_id,
                ));
                
                if(isset($tuile->tuile_image_base64)) {
                    $content = base64_decode($tuile->tuile_image_base64);
                    $image = fopen(__DIR__ . "/../../assets/images/tuiles/" . $tuile->tuile_image, "wb");
                    fwrite($image, $content);
                    fclose($image);
                }
                
                if ($insert === false) {
                    $error_msg .= $wpdb->last_error;
                    if ($is_ok === true) {
                        $is_ok = false;
                    }
                }

                if ($tuile->tuile_type == "2") {
                    $correspondances = $tuile->{'item-correspondances'};
                    foreach ($correspondances->children() as $correspondance) {
                        $insert = $wpdb->insert("{$wpdb->prefix}gestion_page_gru_tuile_ctg_correspondance", array(
                            'tuile_ctg_correspondance_child_id' => $correspondance->tuile_ctg_correspondance_child_id,
                            'tuile_ctg_correspondance_parent_id' => $correspondance->tuile_ctg_correspondance_parent_id
                        ));
                        
                        if ($insert === false) {
                            $error_msg .= $wpdb->last_error;
                            if ($is_ok === true) {
                                $is_ok = false;
                            }
                        }
                    }
                }

                $styles = $tuile->{'item-styles'};
                foreach ($styles->children() as $style) {
                    $insert = $wpdb->insert("{$wpdb->prefix}gestion_page_gru_tuile_style", array(
                        'tuile_style_tuile_id' => $tuile->tuile_id,
                        'tuile_style_css_libelle' => $style->tuile_style_css_libelle,
                        'tuile_style_css_value' => $style->tuile_style_css_value,
                    ));
                    if ($insert === false) {
                        $error_msg .= $wpdb->last_error;
                        if ($is_ok === true) {
                            $is_ok = false;
                        }
                    }
                }
            }            
            
            unlink($this->import_dir . $_POST['import_data']['import_file']);
        }
        if ($is_ok !== false) {
            $return = array(
                "code" => 1,
                "message" => "L'import s'est finalisé sans problème",
            );
        } else {
            $return = array(
                "code" => 0,
                "message" => "Erreur lors de l'import du fichier : <br/>" . $error_msg
            );
        }

        echo json_encode($return);
        wp_die();
    }

}

new Admin_Gestion_Page_GRU_Import_Export();
