<?php

use CDE\Application\Actors\Admin\UseCases\CreateTuile\Views\CreateTuileViewFactory;

/**
 * 1997-2018 Quadra Informatique
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 *
 *  @author    Quadra Informatique <modules@quadra-informatique.fr>
 *  @copyright 1997-2018 Quadra Informatique
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * 
 *  Admin_Gestion_Page_GRU_Tuile
 */
class Admin_Gestion_Page_GRU_Tuile {

    /**
     * Répertoire d'upload pour les images des tuiles
     * 
     * @var string 
     */
    private $uploads_dir;

    /**
     * Url vers le dossier image
     * 
     * @var string 
     */
    private $image_url_path;

    /**
     * Constructeur
     */
    public function __construct() {
        $plugin_base_name = explode('/', plugin_basename(__FILE__))[0];
        $absolute_base_name = explode($plugin_base_name, plugin_dir_path(__FILE__))[0] . $plugin_base_name;
        $this->uploads_dir = "$absolute_base_name/assets/images/tuiles/";
        $this->image_url_path = plugins_url("$plugin_base_name/assets/images/tuiles/");

        add_action('gestion_page_gru_get_tuile_list_management', array($this, 'get_tuile_list_management'), 50);
        add_action('wp_ajax_get_tuile_data', array($this, 'get_tuile_data'), 50);
        add_action('wp_ajax_get_icon_list', array($this, 'get_icon_list'), 50);
        add_action('wp_ajax_save_tuile_content', array($this, 'save_tuile_content'), 50);
        add_action('wp_ajax_upload_tuile_image', array($this, 'upload_tuile_image'), 50);

        # trigger use case admin create tuile here
        add_action('wp_ajax_admin_create_tuile_post', array($this, 'handlePostCreateTuile'), 50);
        add_action('wp_ajax_admin_create_tuile_new', array($this, 'handleGetCreateTuile'), 50);
    }

    /**
     * Récupère la liste des tuiles pour modification
     * 
     * @global object $wpdb
     * @return void
     */
    public function get_tuile_list_management() {
        global $wpdb;
        $results = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}gestion_page_gru_tuile` ORDER BY tuile_type");
        $last_type = "";
        $html = "";
        foreach ($results as $element_gru) {
            if ($last_type == "" || $last_type !== $element_gru->tuile_type) {
                if ($last_type !== $element_gru->tuile_type && $last_type == "0") {
                    $html .= <<<HTML
                    <div id="add_block_gru_type_text" class="row">
                        <span class="offset-lg-1 col-lg-3"><i class="fa fa-plus"></i></span>
                        <span class="col-lg-6"><p> Ajouter une tuile</p></span>
                    </div>
HTML;
                }
                $libelle_type = "Tuiles de type ";
                switch ($element_gru->tuile_type) {
                    case "0" :
                        $libelle_type .= "textuel";
                        break;
                    case "1" :
                        $libelle_type .= "téléservice";
                        break;
                    case "2" :
                        $libelle_type .= "catégorie";
                        break;
                    case "3" :
                        $libelle_type .= "information";
                        break;
                    case "4" :
                        $libelle_type .= "lien";
                        break;
                }
                if ($last_type != "") {
                    $html .= "</div></div>";
                }
                $html .= "<div class='row' id='type{$element_gru->tuile_type}'><div class='col-lg-12'><h3>$libelle_type</h3></div><div class='col-lg-12 content-list' >";
            }
            $html .= $this->get_tuile_html($element_gru);
            $last_type = $element_gru->tuile_type;
        }
        
        $html .= <<<HTML
                <div id="add_block_gru_type_link" class="row">
                    <span class="offset-lg-1 col-lg-3"><i class="fa fa-plus"></i></span>
                    <span class="col-lg-6"><p> Ajouter une tuile</p></span>
                </div>  
            </div>
        </div>
HTML;
        $html = apply_filters("get_dynamic_css", $html);
        echo $html;
    }

    /**
     * Génère le rendu html d'une tuile
     * 
     * @param object $element_gru
     * @return string
     */
    public function get_tuile_html($element_gru) {
        $plugin_base_name = explode('/', plugin_basename(__FILE__))[0];
        $path_image = plugins_url("$plugin_base_name/assets/images/tuiles/");
        $tuile_image = (isset($element_gru->tuile_image)) ? $path_image . $element_gru->tuile_image : "";
        $tuile_icon = ($element_gru->tuile_icon !== "") ? "<i class='fa fa-{$element_gru->tuile_icon}'></i>" : "";
        $tuile_title = stripslashes($element_gru->tuile_title);
        $tuile_description = stripslashes($element_gru->tuile_description);

        switch ($element_gru->tuile_type) {
            case "0" :
                $html = <<<HTML
                <div class="element-gru element-gru-text box-shadow-gru" style="background-image:url($tuile_image)" data-tuile-id="{$element_gru->tuile_id}" name="element-gru-{$element_gru->tuile_id}">
                    <div class="row content-text">
                        <div class="col-lg-6">       
                            <div class="element-gru-header">
                                <div class="title">
                                    <span class="gestion-page-gru-element-title">$tuile_title</span>
                                </div>
                            </div>
                            <div class="grid-element-gru-content">                                                
                                $tuile_icon
                                <p>$tuile_description</p>
                            </div>
                        </div>
                    </div>
                </div>  
HTML;
                break;
            case "1" :  case "2" :  case "4" : 
                $html = <<<HTML
                <div class="element-gru box-shadow-gru" data-tuile-id="{$element_gru->tuile_id}" name="element-gru-{$element_gru->tuile_id}" style="background-image:url($tuile_image)">
                    <div class="element-gru-header">
                        <div class="title">
                            <span class="gestion-page-gru-element-title">$tuile_title</span>
                        </div>
                    </div>
                    <div class="grid-element-gru-content">                                                
                        $tuile_icon
                        <p>$tuile_description</p>
                    </div>
                </div>
HTML;
                break;
            case "3" :
                $html = <<<HTML
                <div class="element-gru box-shadow-gru" data-tuile-id="{$element_gru->tuile_id}" name="element-gru-{$element_gru->tuile_id}">
                    <div class="element-gru-header">
                        <div class="title">
                            <span class="gestion-page-gru-element-title">$tuile_title</span>
                        </div>
                    </div>
                    <div class="grid-element-gru-content">
                        <div class="multi-content">
                            <div class='desc-dyn'>#</div>
                            $tuile_icon
                        </div>
                        <div class="desc-plus">{$element_gru->tuile_description_plus}</div>
                        <p>$tuile_description</p>
                    </div>
                </div>
HTML;
                        
                break;
        }

        return $html;
    }

    /**
     * Récupère une liste "ol" des tuiles 
     * 
     * @global object $wpdb
     * @return string
     */
    public function get_select_tuile_list($selected_child) {
        global $wpdb;
        $results = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}gestion_page_gru_tuile` WHERE tuile_type = 1 ");
        $html = '<ol id="select-list-tuile">';
        foreach ($results as $tuile) {
            $title = stripslashes($tuile->tuile_title);
            $selected = (in_array($tuile->tuile_id, $selected_child)) ? "selected" : "";
            $html .= <<<HTML
            <li class="$selected" data-tuile-title="{$tuile->tuile_title}" data-tuile-id="{$tuile->tuile_id}" data-tuile-icon="{$tuile->tuile_icon}" data-tuile-type="{$tuile->tuile_type}">
                <i class="fa fa-{$tuile->tuile_icon}"></i>$title
            </li>
                
HTML;
        }

        $html .= '</ol>';
        return $html;
    }

    /**
     * Ajax
     * Récupère la liste des icones sélectionnables
     * 
     * @return string
     */
    public function get_icon_list() {
        $html_icon = <<<HTML
            <div class="select-icon">
                <div class="col-lg-9">
                    <input type="text" id="filter_icon_list" placeholder="Filtrer par libellé des icônes"/>
                </div>
            </div>
            <br/>
            <div class="select-icon">
HTML;
        $Admin_Gestion_Page_GRU_Tools = new Admin_Gestion_Page_GRU_Tools();
        $list_icons = $Admin_Gestion_Page_GRU_Tools->get_list_icons();
        foreach ($list_icons as $i => $value) {
            $html_icon .= <<<HTML
                <div class="form-modif-tuile" data-icon="$value" id="$i">
                    <i class="$value"></i>
                </div>
HTML;
        }
        $html_icon .= "</div>";
        echo $html_icon;
        wp_die();
    }

    /**
     * Récupère le style d'une tuile
     * 
     * @global object $wpdb
     * @param string $tuile_id
     * @return array
     */
    public function get_tuile_style($tuile_id) {
        global $wpdb;
        $return_style = array(
            "title-background-color" => "#FFFFFF",
            "title-color" => "#000000",
            "title-font-family" => "Arial, sans-serif",
            "title-font-size" => "15",
            "body-background-color" => "#FFFFFF",
            "body-color" => "#000000",
            "body-font-family" => "Arial, sans-serif",
            "body-font-size" => "15",
            "icon-color" => "#000000",
            "icon-font-size" => "50"
        );

        $styles_tuile = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}gestion_page_gru_tuile_style` WHERE 	tuile_style_tuile_id = $tuile_id");
        if (count($styles_tuile) > 0) {
            foreach ($styles_tuile as $style) {
                $return_style[$style->tuile_style_css_libelle] = $style->tuile_style_css_value;
            }
        }

        return $return_style;
    }

    /**
     * Handle GET route, return edit form
     */
    public function handleGetCreateTuile()
    {
        try {
            $tuileType = filter_var($_POST['tuile_type'], FILTER_SANITIZE_STRING);
            $view = CreateTuileViewFactory::getView($tuileType);
            echo $view->showCreateTuileInputForm();
        } catch (InvalidArgumentException $e) {
            echo  $e->getMessage();
        } finally {
            wp_die();
        }
    }
    
    
    
    /**
     * Ajax
     * Génère le formulaire de modification de la tuile
     * 
     * @global object $wpdb
     * @return void
     */
    public function get_tuile_data() {
        global $wpdb;

        $input_ctg = "";
        $tuile = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}gestion_page_gru_tuile` WHERE tuile_id = {$_POST['tuile_id']}")[0];
        $tuile_id = $_POST['tuile_id'];
        $tuile_title = (isset($tuile->tuile_title)) ? stripcslashes($tuile->tuile_title) : "";
        $tuile_description = (isset($tuile->tuile_description)) ? stripcslashes($tuile->tuile_description) : "";
        $tuile_description_plus = (isset($tuile->tuile_description_plus)) ? stripcslashes($tuile->tuile_description_plus) : "";
        $tuile_teleservice_id = (isset($tuile->tuile_teleservice_id)) ? $tuile->tuile_teleservice_id : "";

        $styles = $this->get_tuile_style($tuile_id);
        $Admin_Gestion_Page_GRU_Tools = new Admin_Gestion_Page_GRU_Tools();
        $list_font_family = $Admin_Gestion_Page_GRU_Tools->get_list_font_family();
        $list_font_family_options_title = $list_font_family_options_desc = "";
        foreach ($list_font_family as $font) {
            $selected_title = ($styles['title-font-family'] === $font) ? "selected" : "";
            $selected_desc = ($styles['body-font-family'] === $font) ? "selected" : "";
            $list_font_family_options_title .= "<option value='$font' $selected_title style='font-family: $font'>$font</option>";
            $list_font_family_options_desc .= "<option value='$font' $selected_desc style='font-family: $font'>$font</option>";
        }
        if (substr($_POST['tuile_id'], 0, 3) === "new") {
            $tuile->tuile_type = end(explode("_", $_POST['tuile_id']));
            $tuile_title = "Libellé tuile";
            $tuile_description = "Description";
        }

        $tuile_html = $this->get_tuile_html($tuile);
        $visual_width = ($tuile->tuile_type == "0") ? "col-lg-11" : "col-lg-6";
        $visual = "<div class='$visual_width'>$tuile_html</div>";
        switch ($tuile->tuile_type) {
            case "1" :
                $input_ctg .= <<<HTML
                <div class="row" id="select-page-tuile-content"> 
                    <div class="col-lg-3">
                        Id du téléservice de la tuile
                    </div>
                    <div class="col-lg-9">
                        <input disabled id="teleservice-id" class="gestion-page-gru-select" value="$tuile_teleservice_id"/>
                    </div>
                </div>
HTML;
                break;
            case "2" :
                $list_child = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}gestion_page_gru_tuile_ctg_correspondance` WHERE tuile_ctg_correspondance_parent_id = {$_POST['tuile_id']}");
                $selected_child = array();
                foreach ($list_child as $child) {
                    array_push($selected_child, $child->tuile_ctg_correspondance_child_id);
                }
                $tuile_list = $this->get_select_tuile_list($selected_child);
                $input_ctg .= <<<HTML
                <div id="select-tuile-list-content">
                   <div class="row">
                       <div class="col-lg-12">Liste des tuiles associées</div>
                   </div>
                   <div class="row">
                       <div class="col-lg-12">
                           $tuile_list
                       </div>
                    </div>
                </div>
HTML;
                break;
            case "3" :

                $results = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}gestion_page_gru_function` WHERE function_name LIKE '%liste%'");
                $select_function = "<select id='tuile_function_id'><option value='-1'> Choisir ... </option>";
                foreach ($results as $function) {
                    $selected_function = (isset($tuile->tuile_function_id) && $tuile->tuile_function_id === $function->function_id) ? "selected" : "";
                    $select_function .= "<option value='{$function->function_id}' $selected_function> {$function->function_libelle} </option>";
                }

                $select_function .= "</select>";

                $input_ctg .= <<<HTML
                <div class="row"> 
                    <div class="col-lg-3">
                        Texte interne
                    </div>
                    <div class="col-lg-9">
                        <textarea rows='5' id="tuile_description_plus" name="tuile_description_plus">$tuile_description_plus</textarea>
                    </div>
                </div>
                <div class="row"> 
                    <div class="col-lg-3">
                        Fonction liée
                    </div>
                    <div class="col-lg-9">
                        $select_function
                    </div>
                </div>
HTML;
                break;
                
            case "4" :               
                
                $tuile_page_id = $display_select = "";
                $display_external = "display:none;";
                $checked = "";
                if(!is_numeric($tuile->tuile_page_id)) {                    
                    $display_external = "";
                    $display_select = "display:none;";
                    $checked = "checked";
                    $tuile_page_id = $tuile->tuile_page_id;
                }
                
                $pages = get_pages(array('sort_column' => 'post_title'));
                $select_pages = "<select id='tuile_page_id' name='tuile_page_id' class='gestion-page-gru-select'><option value='-1'>...</option>";
                foreach ($pages as $page) {
                    $page_link = get_page_link($page->ID);
                    $selected_page = (isset($tuile->tuile_page_id) && $tuile->tuile_page_id == $page->ID) ? "selected" : "";
                    $select_pages .= '<option value="'.$page->ID.'" '.$selected_page.'>'.$page->post_title.' : '.str_replace(get_home_url(), "", $page_link).'</option>';
                }
                $select_pages .= "</select>";            
                $input_ctg .= <<<HTML
                <div class="row"> 
                    <div class="col-lg-3">
                        Page liée
                    </div>
                    <div class="col-lg-3 mt-1">
                        <input type="checkbox" id="switch_external_link" $checked/>
                        Lien externe
                    </div>
                    <div class="col-lg-6 content-external-link" style="$display_external">
                        <input type="text" id="tuile_page_id_external" name="tuile_page_id_external" value="$tuile_page_id" />
                    </div>
                    <div class="col-lg-6 content-internal-link" style="$display_select">
                        $select_pages
                    </div>
                </div>    
                <div class="row"> 
                    <div class="col-lg-3">
                        Texte au survol
                    </div>
                    <div class="col-lg-9">
                        <textarea rows='5' id="tuile_description_plus" name="tuile_description_plus">$tuile_description_plus</textarea>
                    </div>
                </div>                
HTML;
            break;            
        }

        $form_html = <<<HTML
            <div id="infos-tuile-content" class="row">
                <div id="tuile-visuel" class="col-lg-6">
                    <div class="row">
                        $visual
                    </div>
                </div>               
                <div id="infos-tuile-form" class="col-lg-6">
                   <input type="hidden" name="tuile_id" id="tuile_id" value="$tuile_id"/>
                   <input type="hidden" name="tuile_type" id="tuile_type" value="{$tuile->tuile_type}"/>
                   <input type="hidden" name="tuile_icon" id="tuile_icon" value="{$tuile->tuile_icon}" />
                   <input type="hidden" name="tuile_image_sup" id="tuile_image_sup" value="" />
                   <input type="hidden" name="tuile_teleservice_id" id="tuile_teleservice_id" />
                    <div class="row">
                        <div class="col-lg-12">
                           <h5>Formulaire d'édition</h5>
                        </div>
                    </div>
                   <div class="row">
                       <div class="col-lg-3">
                           Nom de la tuile
                       </div>
                       <div class="col-lg-9">
                           <input type="text" id="tuile_title" name="tuile_title" value="$tuile_title" />
                       </div> 
                   </div>      
                   <div class="row">
                       <div class="col-lg-3">
                           Description de la tuile
                       </div>
                       <div class="col-lg-9">
                           <textarea rows='5' id="tuile_description" name="tuile_description">$tuile_description</textarea>
                       </div>
                   </div>   
                    <div class="row" id="content-upload-image"> 
                       <div class="col-lg-3">
                           Image de fond
                           <i class="fa fa-trash" id="remove_image" title="Retirer l'image" />
                       </div>
                       <div class="col-lg-9">
                           <input type="file" id="tuile_image" name="tuile_image" accept="image/x-png,image/gif,image/jpeg"/>
                       </div> 
                       <div id="gestion-gru-ajax-msg-upload" class="col-lg-12">
                           <div class="updated notice" style="display:none">
                               <p class="msg"></p>
                           </div>
                           <div class="error notice" style="display:none;">
                               <p class="msg"></p>
                           </div>
                           <div class="infos notice" style="display:none;">
                               <p class="msg"></p>
                           </div>
                       </div>
                   </div>      
                   <div class="row" id="show_icon_list_content" > 
                       <div class="col-lg-3 highligth-target-off">
                           Icone de la tuile
                           <i class="fa fa-trash" id="remove_icon" title="Retirer l'icone" />
                       </div> 
                       <div class="col-lg-9">
                           <button id="show_icon_list" class="gestion-page-gru-button"> Afficher formulaire de sélection d'icones </button>
                       </div>  
                       <br/>   
                   </div>                   
                    $input_ctg                   
                    <div id="select-page-tuile-style-content">
                        <div class="row"> 
                           <div class="col-lg-3">
                               Police du titre
                           </div>
                           <div class="col-lg-9">                                    
                               <select id="title-font-family" name="font-family">
                                    $list_font_family_options_title
                               </select>
                           </div>
                       </div>
                       <div class="row"> 
                           <div class="col-lg-3">
                               Police de la desc.
                           </div>
                           <div class="col-lg-9">                                    
                               <select id="body-font-family" name="font-family">
                                    $list_font_family_options_desc
                               </select>
                           </div>
                       </div>
                       <div class="row"> 
                           <div class="col-lg-3">
                               Taille titre (px)
                           </div>
                           <div class="col-lg-3">
                               <input id="title-font-size" type="text" value="{$styles['title-font-size']}"/>
                           </div>
                           <div class="col-lg-3">
                               Taille desc. (px)
                           </div>
                           <div class="col-lg-3">
                               <input id="body-font-size" type="text" value="{$styles['body-font-size']}"/>
                           </div>
                       </div>
                       <div class="row"> 
                           <div class="col-lg-3">
                               Couleur texte titre
                           </div>
                           <div class="col-lg-3">
                               <input id="title-color" value="{$styles['title-color']}" type="text" autocomplete="off" class="color-picker gestion-page-gru-select"/>
                           </div>
                           <div class="col-lg-3">
                               Couleur texte desc.
                           </div>
                           <div class="col-lg-3">
                               <input id="body-color" value="{$styles['body-color']}" type="text" autocomplete="off" class="color-picker gestion-page-gru-select"/>
                           </div>
                       </div>
                       <div class="row"> 
                           <div class="col-lg-3">
                               Couleur fond titre
                           </div>
                           <div class="col-lg-3">
                               <input id="title-background-color" value="{$styles['title-background-color']}" type="text" autocomplete="off" class="color-picker gestion-page-gru-select"/>
                           </div>
                           <div class="col-lg-3">
                               Couleur fond desc.
                           </div>
                           <div class="col-lg-3">
                               <input id="body-background-color" value="{$styles['body-background-color']}" type="text" autocomplete="off" class="color-picker gestion-page-gru-select"/>
                           </div>
                       </div>
                       <div class="row">
                           <div class="col-lg-3">
                               Taille icône (px)
                           </div>
                           <div class="col-lg-3">
                               <input id="icon-font-size" value="{$styles['icon-font-size']}" type="text" />
                           </div>
                           <div class="col-lg-3">
                               Couleur icône
                           </div>
                           <div class="col-lg-3">
                               <input id="icon-color" value="{$styles['icon-color']}" type="text" class="color-picker gestion-page-gru-select"/>
                           </div>
                       </div>
                   </div>
               </div> 
            </div> 
HTML;

        echo $form_html;
        wp_die();
    }

    /**
     * Route POST
     * controller receiving data from browser when new tuile form is submit
     */
    public function handlePostCreateTuile()
    {

    }



    /**
     * Ajax
     * Sauvegarde le contenu d'une tuile
     * 
     * @global object $wpdb
     * @return void
     */
    public function save_tuile_content() {
        global $wpdb;
        
        if ($_POST['tuile_delete'] == 'delete') {
            $tuile_id = $_POST['tuile_id'];
            $wpdb->delete($wpdb->prefix . "gestion_page_gru_tuile_style", array('tuile_style_tuile_id' => $tuile_id));
            $wpdb->delete($wpdb->prefix . "gestion_page_gru_tuile", array('tuile_id' => $tuile_id ));
            
            echo json_encode(array('code' => 1, 'message' => 'La tuile a bien été supprimée', 'lastid' => $tuile_id));
            wp_die();
        }
        
        if ($_POST['tuile_type'] == "2") {

            $result = $wpdb->delete(
                $wpdb->prefix . "gestion_page_gru_tuile_ctg_correspondance", array(
                    'tuile_ctg_correspondance_parent_id' => $_POST['tuile_id']
                )
            );
            foreach ($_POST['tuile_list'] as $value) {
                $result = $wpdb->insert(
                    $wpdb->prefix . "gestion_page_gru_tuile_ctg_correspondance", array(
                        'tuile_ctg_correspondance_child_id' => $value,
                        'tuile_ctg_correspondance_parent_id' => $_POST['tuile_id']
                    )
                );
            }
        }

        $lastid = "";
        if (substr($_POST['tuile_id'], 0, 3) === "new") {
            $result = $wpdb->insert(
                $wpdb->prefix . "gestion_page_gru_tuile", array(
                    'tuile_title' => $_POST['tuile_title'],
                    'tuile_type' => $_POST['tuile_type'],
                    'tuile_description' => $_POST['tuile_description'],
                    'tuile_description_plus' => (!empty($_POST['tuile_description_plus'])) ? $_POST['tuile_description_plus'] : "",
                    'tuile_icon' => (!empty($_POST['tuile_icon'])) ? $_POST['tuile_icon'] : null,
                    'tuile_image' => (!empty($_POST['tuile_image'])) ? $_POST['tuile_image'] : null
                )
            );
            $lastid = $wpdb->insert_id;
        } else {

            $data_to_update = array(
                'tuile_title' => $_POST['tuile_title'],
                'tuile_description' => $_POST['tuile_description'],
                'tuile_description_plus' => (!empty($_POST['tuile_description_plus'])) ? $_POST['tuile_description_plus'] : "",
                'tuile_icon' => (!empty($_POST['tuile_icon'])) ? $_POST['tuile_icon'] : null,
                'tuile_function_id' => (!empty($_POST['tuile_function_id'])) ? $_POST['tuile_function_id'] : null,
                'tuile_page_id' => (!empty($_POST['tuile_page_id'])) ? $_POST['tuile_page_id'] : null,
            );

            if (!empty($_POST['tuile_image']) && $_POST['tuile_image'] !== "sup") {
                $data_to_update['tuile_image'] = $_POST['tuile_image'];
            } else if ($_POST['tuile_image_sup'] === "sup") {
                $data_to_update['tuile_image'] = "";
            }


            if ($_POST['tuile_id'] == 'duplicate') {
                $data_to_update['tuile_type'] = (!empty($_POST['tuile_type'])) ? $_POST['tuile_type'] : "";
                $result = $wpdb->insert(
                        $wpdb->prefix . "gestion_page_gru_tuile", $data_to_update
                );
                $lastid = $wpdb->insert_id;
            } else {
                $result = $wpdb->update(
                        $wpdb->prefix . "gestion_page_gru_tuile", $data_to_update, array(
                    'tuile_id' => $_POST['tuile_id']
                        )
                );
                $lastid = $_POST['tuile_id'];
            }
        }

        if (isset($_POST['style'])) {
            $tuile_id = ($lastid !== "") ? $lastid : $_POST['tuile_id'];
            $result = $wpdb->delete(
                    $wpdb->prefix . "gestion_page_gru_tuile_style", array(
                'tuile_style_tuile_id' => $tuile_id
                    )
            );

            foreach ($_POST['style'] as $key => $value) {
                $wpdb->insert(
                        $wpdb->prefix . "gestion_page_gru_tuile_style", array(
                    'tuile_style_tuile_id' => $tuile_id,
                    'tuile_style_css_libelle' => $key,
                    'tuile_style_css_value' => $value
                        )
                );
            }
        }

        if ($result !== false) {
            $code = 1;
            $message = "La tuile a été mise à jour";
        } else {
            $code = 0;
            $message = "Erreur lors de la mise à jour de la tuile";
        }

        $return = array(
            "code" => $code,
            "message" => $message,
            "lastid" => $lastid
        );
        if ($_POST['tuile_id'] == 'duplicate') {
            $updated_tuile = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}gestion_page_gru_tuile` WHERE tuile_id = $lastid");
        } else {
            $updated_tuile = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}gestion_page_gru_tuile` WHERE tuile_id = $lastid");
            $return['lastid'] = $_POST['tuile_id'];
        }

        $html = apply_filters("get_dynamic_css", $this->get_tuile_html($updated_tuile[0]), $lastid);
        $return['tuile_html'] = $html;
        echo json_encode($return);
        
        wp_die();
    }

    /**
     * Ajax
     * Gère l'upload des images
     * @return void
     */
    public function upload_tuile_image() {

        $tmp_name = $_FILES["file"]["tmp_name"];
        $name = $_FILES["file"]["name"];
        $check_upload = move_uploaded_file($tmp_name, $this->uploads_dir . $name);

        if ($check_upload) {
            $return = array(
                "code" => 1,
                "message" => "Image uploadé",
                "path_image" => $this->image_url_path . $name
            );
        } else {
            $return = array(
                "code" => 0,
                "message" => "Erreur lors de l'upload de l'image"
            );
        }

        echo json_encode($return);
        wp_die();
    }

}

new Admin_Gestion_Page_GRU_Tuile();
