<?php

/**
 * 1997-2018 Quadra Informatique
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 *
 *  @author    Quadra Informatique <modules@quadra-informatique.fr>
 *  @copyright 1997-2018 Quadra Informatique
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * 
 *  Admin_Gestion_Page_GRU_Menu
 */
class Admin_Gestion_Page_GRU_Menu {


    /**
     * Constructeur
     */
    public function __construct() {
        add_action('wp_ajax_save_menu_list', array($this, 'save_menu_list'), 50);
        add_action('gestion_page_gru_get_menus_content', array($this, 'get_menus_content'), 50);  
    }

    /**
     * Génère le contenu des menus récupérés en BDD
     * 
     * @global string $wpdb
     * @return void
     */
    public function get_menus_content() {
        global $wpdb;
        $this->cpt_element = 0;
        $result = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}gestion_page_gru_page_menu`");

        $html = "";
        if (count($result) > 0) {
            foreach ($result as $menu) {
                $links = unserialize($menu->page_menu_content);
                $html_child = "";
                if(!empty($links)) {
                    foreach ($links as $key => $link) {
                        $html_child .= $this->generate_menu_item($key, $link, true);
                    }
                }
                
                $list_page = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}gestion_page_gru_page_correspondance_menu` WHERE page_correspondance_menu_menu_id = '{$menu->page_menu_id}'");
                $pages = "";
                if(count($list_page) > 0) {
                    foreach($list_page as $page) {
                        $pages .= "{$page->page_correspondance_menu_page_id},";
                    }
                    $pages = substr($pages,0,-1);
                }
                $html .= <<<HTML
                    <ul>
                        <li class="libelle border-bottom" data-list-page="[$pages]" data-menu-id="{$menu->page_menu_id}" data-libelle="{$menu->page_menu_libelle}" data-default="{$menu->page_menu_is_default}">
                            <i class="fa fa-chevron-circle-right"></i>
                            <span class="libelle">{$menu->page_menu_libelle}</span>
                            <i class="fa fa-trash"></i>
                        </li>
                        <li class="menu-content" style="display:none;">
                            <ul>
                                $html_child
                                <li class="add-element menu"> <i class="fa fa-plus-circle"></i> Ajouter un élément </li>
                            </ul>
                        </li>                                
                    </ul>
HTML;
            }
        }
        
        $html .= <<<HTML
            <ul>                        
                <li class="add-element menu"> <i class="fa fa-plus-circle"></i> Ajouter un élément </li>
            </ul>
HTML;
        echo $html;
    }

    /**
     * Génère un item du menu
     * 
     * @param string $_key
     * @param string $_item
     * @param boolean $_is_parent
     * @return string
     */
    public function generate_menu_item($_key, $_item, $_is_parent = false) {

        $html_child = "";
        if (isset($_item['childs'])) {
            foreach ($_item['childs'] as $key => $child) {
                $html_child .= $this->generate_menu_item($key, $child);
            }
        }

        if ($html_child === "" && !$_is_parent) {
            $html .= <<<HTML
            <li class="link" data-link-id="$_key" data-id-page-link="{$_item['id-page-link']}" data-libelle="{$_item['libelle']}">
                <i class="fa fa-link"></i>
                <span class="libelle">{$_item['libelle']}</span>
                <i class="fa fa-trash"></i>
            </li>
HTML;
        } else {
            $html .= <<<HTML
                <li class="sub-libelle" data-sub-menu-id="$_key" data-id-page-link="{$_item['id-page-link']}"  data-libelle="{$_item['libelle']}">
                    <i class="fa fa-chevron-circle-right"></i>
                    <span class="libelle">{$_item['libelle']}</span>
                    <i class="fa fa-trash"></i>
                </li>
                <li class="menu-content" style="display:none;">
                    <ul>
                        $html_child
                        <li class="add-element sub-menu"><i class="fa fa-plus-circle"></i> Ajouter un élément </li>
                    </ul>
                </li>                
HTML;
        }
        return $html;
    }

    /**
     * Ajax
     * Sauvegarde le contenu des menus générés
     * 
     * @global object $wpdb
     * @return void
     */
    public function save_menu_list() {
        global $wpdb;
        $list_menus = $_POST['data'];
        if (count($list_menus) > 0) {
            $list_return = array();      
            $list_existing_menu =  wp_get_nav_menus();
            foreach($list_existing_menu as $menu) {
                wp_delete_nav_menu($menu->slug);
            }            
            
            $wpdb->query("TRUNCATE TABLE `{$wpdb->prefix}gestion_page_gru_page_menu`");                
            foreach ($list_menus as $menu) {          
                $result = $wpdb->insert("{$wpdb->prefix}gestion_page_gru_page_menu", array(     
                    'page_menu_id' => $menu['menu-id'],
                    'page_menu_libelle' => $menu['libelle'],
                    'page_menu_content' => serialize($menu['childs'])
                ));               
                
                if ($result === false) {
                    array_push($list_return, array(
                        "code" => 0,
                        "message" => "Erreur lors de la mise à jour du menu \"{$menu['libelle']}\""
                    ));
                } else {
                    array_push($list_return, array(
                        "code" => 1,
                        "message" => "Mise à jour du menu \"{$menu['libelle']}\" ok"
                    ));
                    $wpdb->delete("{$wpdb->prefix}gestion_page_gru_page_correspondance_menu", array(
                        "page_correspondance_menu_menu_id" => $menu['menu-id']
                    ));                    
                    if(!empty($menu['list-page'])) {                        
                        foreach($menu['list-page'] as $id_page) {
                            $wpdb->insert("{$wpdb->prefix}gestion_page_gru_page_correspondance_menu", array(                            
                                "page_correspondance_menu_page_id" => $id_page,                            
                                "page_correspondance_menu_menu_id" => $menu['menu-id']
                            ));
                        }
                    }
                }
            }
        }

        echo json_encode($list_return);
        wp_die();
    }

}

new Admin_Gestion_Page_GRU_Menu();
