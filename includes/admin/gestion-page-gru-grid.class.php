<?php

/**
 * 1997-2018 Quadra Informatique
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 *
 *  @author    Quadra Informatique <modules@quadra-informatique.fr>
 *  @copyright 1997-2018 Quadra Informatique
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * 
 *  Admin_Gestion_Page_GRU_Grid
 */
class Admin_Gestion_Page_GRU_Grid {

    protected $cpt_element;

    /**
     * Constructor
     */
    public function __construct() {
        add_action('wp_ajax_get_grid_content', array($this, 'get_grid_content'), 50);
        add_action('wp_ajax_add_new_page', array($this, 'add_new_page'), 50);
        add_action('wp_ajax_remove_page', array($this, 'remove_page'), 50);
        add_action('wp_ajax_save_grid_page', array($this, 'save_grid_page'), 50);
        add_action('gestion_page_gru_get_bootstrap_element_list', array($this, 'get_bootstrap_element_list'), 50);
        add_action('gestion_page_gru_get_tuile_list', array($this, 'get_tuile_list'), 50);
        add_action('gestion_page_gru_get_cde_function_list', array($this, 'get_cde_function_list'), 50);
    }

    /**
     * Ajax
     * Récupère et génère le contenu d'une grille lié a une page
     * 
     * @global object $wpdb
     * @return void
     */
    public function get_grid_content() {
        global $wpdb;       
        
        $this->cpt_element = 0;
        $result = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}gestion_page_gru_page_correspondance_grid` WHERE page_correspondance_grid_id = {$_POST['id-page']}");
        $html = "";
        if (count($result) > 0) {
            $grid = unserialize($result[0]->page_correspondance_grid);
            foreach ($grid as $block) {
                $html .= $this->generate_block($block);
            }
        }

        $data = array(
            "content" => $html
        );

        sleep(0.8);
        echo json_encode($data);
        wp_die();
    }

    /**
     * Ajax
     * Créer une nouvelle page wordpress
     * 
     * @return void
     */
    public function add_new_page() {
        $my_post = array(
            'post_title' => wp_strip_all_tags($_POST['libelle']),
            'post_content' => "",
            'post_status' => 'publish',
            'post_type' => "page",
            'post_author' => 1
        );

        // Insert the post into the database
        $result = wp_insert_post($my_post);
        $id_post = "";
        if ($result > 0) {
            $code = 1;
            $message = "La page a été créée";
            $id_post = $result;
        } else {
            $code = 0;
            $message = "Erreur lors de la création de la page";
        }

        echo json_encode(array(
            "code" => $code,
            "message" => $message,
            "id_post" => $id_post,
            "data_link" => get_page_link($id_post)
        ));
        wp_die();
    }
    
        /**
     * Ajax
     * Supprimer page wordpress et données associés tables gestion_page_gru_page_correspondance et gestion_page_gru_page_correspondance_grid 
     * 
     * @return void
     */
    public function remove_page() {
        $post_id = $_POST['id-page'];

        // Insert the post into the database
        $result = wp_delete_post($post_id);
        $this->remove_gestion_page_gru_conrespondance($post_id);
        $this->remove_gestion_page_gru_page_correspondance_grid($post_id);
        if ($result > 0) {
            $code = 1;
            $message = "La page a été suprimée";
            $id_post = $result;
        } else {
            $code = 0;
            $message = "Erreur lors de la supression de la page";
        }

        echo json_encode(array(
            "code" => $code,
            "message" => $message,
            "id_post" => $id_post,
        ));
        wp_die();
    }
    
    

    /**
     * Ajax
     * Sauvegarde le contenu d'une grille lié à une page
     * 
     * @global object $wpdb
     * @return void
     */
    public function save_grid_page() {
        global $wpdb;

        $check = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}gestion_page_gru_page_correspondance_grid` WHERE page_correspondance_grid_id = {$_POST['id-page']}");

        if (count($check) > 0) {

            if (empty($_POST['grid'])) {
                $result = $wpdb->delete("{$wpdb->prefix}gestion_page_gru_page_correspondance_grid", array("page_correspondance_grid_id" => $_POST['id-page']));

                if (is_int($result)) {
                    $code = 1;
                    $message = "La grille a été supprimé";
                } else {
                    $code = 0;
                    $message = "Erreur lors de la suppression de la grille";
                }
            } else {
                $result = $wpdb->update(
                        $wpdb->prefix . "gestion_page_gru_page_correspondance_grid", array(
                    'page_correspondance_grid' => serialize($_POST['grid'])
                        ), array(
                    'page_correspondance_grid_id' => $_POST['id-page']
                        )
                );


                if (is_int($result)) {
                    $code = 1;
                    $message = "La grille a été mise à jour";
                } else {
                    $code = 0;
                    $message = "Erreur lors de la mise à jour de la grille";
                }
            }
        } else {
            if (!empty($_POST['grid'])) {
                $result = $wpdb->insert(
                        $wpdb->prefix . "gestion_page_gru_page_correspondance_grid", array(
                    'page_correspondance_grid_id' => $_POST['id-page'],
                    'page_correspondance_grid' => serialize($_POST['grid'])
                        )
                );

                if (is_int($result)) {
                    $code = 1;
                    $message = "La grille a été insérer en base";
                } else {
                    $code = 0;
                    $message = "Erreur lors de l'insertion de la grille en base";
                }
            } else {
                $code = 2;
                $message = "Veuillez générer une grille avant de l'enregistrer";
            }
        }

        echo json_encode(array(
            "code" => $code,
            "message" => $message
        ));
        wp_die();
    }

    /**
     * Génère le contenu d'un block de la grille
     * 
     * @param array $_block
     * @return string
     */
    public function generate_block($_block) {

        $html_child = "";
        if (isset($_block['items'])) {
            foreach ($_block['items'] as $block) {
                switch ($block['name']) {
                    case "element-gru":
                        $html_child .= $this->generate_block_gru($block);
                        break;
                    case "element-cde" :
                        $html_child .= $this->generate_block_cde($block);
                        break;
                    default:
                        $html_child .= $this->generate_block($block);
                        break;
                }
            }
        }


        $sizeup = ($_block['columns'] === "row") ? "" : '<div class="sizeup" style="display:none;"><i class="fa fa-chevron-right"></i></div>';
        $title = $_block['columns'];
        $action_section = $data_section = $style_section = "";
        if ($_block['columns'] === "section") {
            $action_section = "<i class='edit-section fa fa-pencil-alt' title='Edition section'></i>";
            $data_section = "data-section_title='{$_block['title']}' data-section_colorpicker='{$_block['color']}' data-section_title_colorpicker='{$_block['titlecolor']}'";
            $style_section = "style='background-color:{$_block['color']}; border-color:{$_block['color']};'";
            $title = stripslashes($_block['title']);
        }

        $html = <<<HTML
            <div class='grid-element {$_block['columns']}' name='{$_block['columns']}' id="grid-element-{$this->cpt_element}" $data_section>
                <div class='row'>
                    <div class='col-lg-12'>
                        <div class='row grid-element-header' $style_section>
                            <div class='actions'>
                                <i class='delete-block fa fa-trash' title='Supprimer le block'></i>
                                <i class='clone-block fa fa-clone' title='Duppliquer le block'></i>
                                $action_section
                            </div>                            
                            <div class='title-header'>
                                <span class='gestion-page-gru-element-title'>$title</span>
                            </div>
                        </div>
                        $sizeup
                        <div class='row grid-element-content'>
                            $html_child
                        </div>
                    </div>
                </div>
            </div>
HTML;
        $this->cpt_element++;
        return $html;
    }

    /**
     * Génère le contenu d'un block GRU de la grille
     * 
     * @global object $wpdb
     * @param string $id
     * @return string
     */
    public function generate_block_gru($block) {
        global $wpdb;
        $result = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}gestion_page_gru_tuile` WHERE tuile_id = {$block['key']}");
        $gru_function = $wpdb->get_row("SELECT * FROM `{$wpdb->prefix}gestion_page_gru_function` WHERE function_id = {$block['function']['id']}");  
        $html = "";
        if (count($result) > 0) {
            $plugin_base_name = explode('/', plugin_basename(__FILE__))[0];
            $path_image = plugins_url("$plugin_base_name/assets/images/tuiles/");

            $tuile_title = stripslashes($result[0]->tuile_title);
            $tuile_desc = stripslashes($result[0]->tuile_description);
            switch ($result[0]->tuile_type) {
                case "0" : case "4" :
                    $html .= <<<HTML
                        <div class='element-gru element-gru-text  box-shadow-gru' name='element-gru-{$result[0]->tuile_id}' style="background-image:url($path_image{$result[0]->tuile_image})">
                            <div class="row content-text">
                                <div class="col-lg-6">       
                                    <div class='element-gru-header'>
                                        <div class='actions float-left'>
                                            <i class='delete-block fa fa-trash' title='Supprimer le block'></i>
                                        </div>
                                        <div class='title'>
                                            <span class='gestion-page-gru-element-title'>$tuile_title</span>
                                        </div>
                                    </div>
                                    <div class='grid-element-gru-content'>                                                
                                        <i class='fa fa-{$result[0]->tuile_icon}'></i>
                                        <p>$tuile_desc</p>
                                    </div>
                                </div>
                            </div>
                        </div>
HTML;
                    break;
                case "1" : case "2" : case "3" :
                    $params_functions = "";
//                    echo "<pre>" . print_r($block, true) . "</Pre>";
//                    echo "<pre>" . print_r($result[0], true) . "</Pre>";
//                     echo "<pre>" . print_r($block, true) . "</Pre>";
                    if($result[0]->tuile_function_id !== NULL && (isset($gru_function->function_params_canvas))) {
                        $params_functions = json_encode($block['function']);
                        $params_canvas_html = <<<HTML
                            <div class="grid-element-gru-content row">
                                <div class="offset-lg-1 col-lg-5">                              
                                    <i class='fa fa-{$result[0]->tuile_icon}'></i>
                                </div>
                                <div class="col-lg-6 cde-function-params">
                                    <div>
                                        <i class="fa fa-cogs"></i> Paramètres
                                    </div><br/>
                                    <button>Modifier</button>
                                </div><br/>
                                <p>$tuile_desc</p>  
                            </div>          
HTML;
                    } else {                        
                        $params_canvas_html = <<<HTML
                            <div class='grid-element-gru-content'>                                                
                                <i class='fa fa-{$result[0]->tuile_icon}'></i>
                                <p>$tuile_desc</p>
                            </div>
HTML;
                    }
                    
                    $html .= <<<HTML
                        <div class='element-gru box-shadow-gru' name='element-gru-{$result[0]->tuile_id}' style="background-image:url($path_image{$result[0]->tuile_image})" data-function-id="{$result[0]->tuile_function_id}" data-function-params='$params_functions'>
                            <div class='element-gru-header'>
                                <div class='actions float-left'>
                                    <i class='delete-block fa fa-trash' title='Supprimer le block'></i>
                                </div>
                                <div class='title'>
                                    <span class='gestion-page-gru-element-title'>$tuile_title</span>
                                </div>
                            </div>
                            $params_canvas_html
                        </div>
HTML;
                    break;
            }
        }

        return $html;
    }

    /**
     * Génère un bloc fonction CDE
     * 
     * @global object $wpdb
     * @param array $block
     * @return string
     */
    public function generate_block_cde($block) {
        global $wpdb;
        
        $result = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}gestion_page_gru_function` WHERE function_id = {$block['key']}");
        
        if(array_key_exists('function', $block) && (array_key_exists('param', $block['function']) || array_key_exists('childs', $block['function']))) {
            $param_canvas_html = <<<HTML
            <div class="row">
                <div class="col-lg-12 cde-function-params text-center">
                    <div>
                        <i class="fa fa-cogs"></i>Paramètres
                    </div><br>
                    <button>Modifier</button>
                </div>
            </div>
HTML;
        } else {
            $param_canvas_html = <<<HTML
            <div class="row">
                <div class="col-lg-12 cde-function-params text-center">
                    <p> Aucun paramètres à renseigner </p>
                </div>
            </div>
HTML;
        }
        
        $params_functions = (array_key_exists('function', $block)) ? "data-function-params='".json_encode($block['function'])."'" : "";
        $html = <<<HTML
        <div class="element-cde-function element-gru box-shadow-gru" name="element-cde-{$result[0]->function_id}" data-function-id="{$result[0]->function_id}" $params_functions>
            <div class="element-gru-header">
                <div class="actions float-left">
                    <i class="delete-block fa fa-trash" title="Supprimer le block"></i>
                </div>
                <p class="text-center">{$result[0]->function_libelle}</p>
            </div>
            <div class="col-lg-12 content-text">
                $param_canvas_html
            </div>
        </div>
HTML;
        return $html;
    }
    
    /**
     * Génère les éléments drag an drop bootstrap
     * 
     * @return void
     */
    public function get_bootstrap_element_list() {
        $html = <<<HTML
        <div class="gestion-page-gru-block-column section" name="section">     
            <div class="header-libelle">Section</div>
        </div>           
        <div class="gestion-page-gru-block-column img-col-row" name="row">     
            <div class="header-libelle">Ligne</div>
        </div>           
        <div class="gestion-page-gru-block-column" name="multi-3">
            <div class="header-libelle">3 / 3 / 3 / 3 colonne(s)</div>
        </div>   
        <div class="gestion-page-gru-block-column" name="multi-4">
            <div class="header-libelle"> 4 / 4 / 4 colonne(s)</div>
        </div>   
        <div class="gestion-page-gru-block-column" name="multi-6">
            <div class="header-libelle"> 6 / 6 colonne(s)</div>
        </div> 
HTML;
        for ($i = 1; $i <= 12; $i++) {
            $html .= <<<HTML
                <div class="gestion-page-gru-block-column" name="col-lg-$i">
                    <div class="header-libelle">$i colonne(s)</div>
                </div>
HTML;
        }
        
        echo $html;
    }

    /**
     * Récupère la liste des tuiles GRU en BDD
     * 
     * @global object $wpdb
     * @return void
     */
    public function get_tuile_list() {
        global $wpdb;
        $results = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}gestion_page_gru_tuile` ORDER BY tuile_type");

        $html = "";
        $plugin_base_name = explode('/', plugin_basename(__FILE__))[0];
        foreach ($results as $element_gru) {
            $img_href = ($element_gru->tuile_image !== "") ? plugins_url("$plugin_base_name/assets/images/tuiles/") . $element_gru->tuile_image : "";

            $icon_context = "";
             if($element_gru->tuile_function_id !== NULL) {
                $result_function = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}gestion_page_gru_function` WHERE function_id = {$element_gru->tuile_function_id}");
                
                if($result_function[0]->function_need_get_params == "1") {
                    $icon_context = "<i class='fa fa-link cde-get-param'></i>";
                }
             }
            
            $title = stripslashes($element_gru->tuile_title);
            $html .= <<<HTML
                <div class="gestion-page-gru-block-gru" name="element-gru-{$element_gru->tuile_id}" 
                                data-tuile-type="{$element_gru->tuile_type}" 
                                data-tuile-title="$title" 
                                data-tuile-description="{$element_gru->tuile_description}"
                                data-tuile-icon="{$element_gru->tuile_icon}"
                                data-tuile-function-id="{$element_gru->tuile_function_id}"
                                data-tuile-image-href="$img_href"
                        style="background: url($img_href);background-size:contain">   
                    <div class="content-block">
                        <i class="fa fa-{$element_gru->tuile_icon}"></i>
                        $icon_context
                    </div>        
                    <div class="header-libelle">$title</div>    
                </div>
HTML;
        }

        $html .= <<<HTML
                <div class="clear"></div><br/>
                <div class='row legend'>
                    <div class='col-lg-12'>                       
                        <i class="fa fa-square type-0"></i> Tuile textuelle &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <i class="fa fa-square type-1"></i> Tuile téléservice &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <i class="fa fa-square type-2"></i> Tuile catégorie &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <i class="fa fa-square type-3"></i> Tuile information &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <i class='fa fa-link cde-get-param'></i> Nécessite un contexte (Foyers, individus, etc.)
                    </div>
                </div>
HTML;



        $html = apply_filters("get_dynamic_css", $html);
        echo $html;
    }

    /**
     * Retourn l'html de la liste des fonctions CDE
     * 
     * @global object $wpdb
     * @return void
     */
    public function get_cde_function_list() {
        global $wpdb;

        $results_all_canvas = $wpdb->get_results(<<<SQL
            SELECT * 
            FROM `{$wpdb->prefix}gestion_page_gru_function`
SQL
        );
        $canvas_params = array();
        foreach ($results_all_canvas as $element_cde) {
            $canvas_params[$element_cde->function_id] = array(
                'data' => unserialize($element_cde->function_params_canvas),
                'childs' => array()
            );            
        }
        
        $results = $wpdb->get_results(<<<SQL
            SELECT * 
            FROM `{$wpdb->prefix}gestion_page_gru_function` 
            ORDER BY function_libelle, function_need_get_params, function_params_canvas
SQL
        );
        $html = "";
        foreach ($results as $element_cde) {
            switch ($element_cde->function_name) {
                case "cde_type_et_id" : case "cde_lien_deconnexion" : case "cde_url_site" : case "cde_url_site_https" :
                case "cde_url_uploads" : case "cde_url_wordpress" : case "cde_gestion_abonnements" :
                    break;
                default:
                    $title = stripslashes($element_cde->function_libelle);
                    $icon = ($element_cde->function_params_canvas != NULL || $element_cde->function_params_canvas != "" || 
                            (array_key_exists($element_cde->function_id, $canvas_params) && count($canvas_params[$element_cde->function_id]['childs']) > 0)) ? "<i class='fa fa-cogs'></i>" : "";
                    $icon_get = ($element_cde->function_need_get_params == "1") ? "<i class='fa fa-link'></i>" : "";
                    $html .= <<<HTML
                        <div class="gestion-page-gru-cde-function" name="element-cde-{$element_cde->function_id}" 
                                        data-function-libelle="{$element_cde->function_libelle}"
                                        data-function-id="{$element_cde->function_id}">
                            <div class="header-libelle">$title $icon $icon_get </div>
                        </div>
HTML;
                    break;
            }
        }

        $json_encode = json_encode($canvas_params);
        $html .= <<<HTML
                <div class="clear"></div><br/>
                <div class='row legend'>
                    <div class='col-lg-12'>                       
                        <i class='fa fa-cogs'></i> - Nécessite des paramètres &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;         
                        <i class='fa fa-link'></i> - Nécessite un contexte (Foyers, individus, etc.)
                    </div>
                </div>
                <script> general_admin_settings.function_canvas_param =  $json_encode </script>
HTML;

        $html .= "";
        echo $html;
    }


public function remove_gestion_page_gru_conrespondance($id) {
        global $wpdb;
        
            $results_correspondance = $wpdb->get_results(<<<SQL
            SELECT page_correspondance_menu_page_id 
            FROM `{$wpdb->prefix}gestion_page_gru_page_correspondance_menu` WHERE page_correspondance_menu_page_id = {$id}
SQL
        );
            
         if ($results_correspondance) {
         $result_delete = $wpdb->delete("{$wpdb->prefix}gestion_page_gru_page_correspondance_menu" , array( "page_correspondance_menu_page_id" => $id ) );
            }

}

public function remove_gestion_page_gru_page_correspondance_grid($id) {
        global $wpdb;
        
            $results_correspondance_grid = $wpdb->get_results(<<<SQL
            SELECT page_correspondance_grid_id 
            FROM `{$wpdb->prefix}gestion_page_gru_page_correspondance_grid` WHERE page_correspondance_grid_id = {$id}
SQL
        );
            
         if ($results_correspondance_grid) {
         $result_delete = $wpdb->delete("{$wpdb->prefix}gestion_page_gru_page_correspondance_grid" , array( "page_correspondance_grid_id" => $id ) );
            }     
}

}

new Admin_Gestion_Page_GRU_Grid();
