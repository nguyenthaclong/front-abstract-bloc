<?php

/**
 * 1997-2018 Quadra Informatique
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 *
 *  @author    Quadra Informatique <modules@quadra-informatique.fr>
 *  @copyright 1997-2018 Quadra Informatique
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * 
 *  Gestion_Page_GRU_General_Admin
 */
class Gestion_Page_GRU_General_Admin {

    // Url absolu du module
    private $plugin_base_name;

    /**
     * Bootstraps the class and hooks required actions & filters.
     */
    public function __construct() {

        $plugin_base_name = explode('/', plugin_basename(__FILE__))[0];
        $this->plugin_base_name = $plugin_base_name;

        add_action('admin_menu', array($this, 'add_menu_bo_element'), 50);
        add_action('wp_ajax_check_connexion_gru', array($this, 'check_connexion_gru'), 50);
    }

    /**
     * Ajoute un nouveau menu sur la gauche en BO pour la gestion du module
     *
     * @param array
     * @return array
     */
    public function add_menu_bo_element() {
        Admin_Gestion_Page_GRU_Tools::load_jquery_lib();
        wp_register_script('gestion-page-gru-general-admin-settings', plugins_url("$this->plugin_base_name/assets/js/admin/prototype/general-settings.class.js"));
        wp_enqueue_script('gestion-page-gru-general-admin-settings');

        add_menu_page('Gestion page GRU', 'Configuration GRU', 'edit_posts', 'gestion_page_gru', array($this, 'config_page'), null, 100);
        add_submenu_page('gestion_page_gru', 'Gestion grilles GRU', 'Gestion grilles GRU', 'edit_posts', 'gestion_page_gru_manage_gru_config', array($this, 'manage_gestion_page_gru_options_display'));
        add_submenu_page('gestion_page_gru', 'Gestion menus', 'Gestion menus', 'edit_posts', 'gestion_page_gru_manage_menu', array($this, "manage_gestion_page_gru_menu_display"));
        add_submenu_page('gestion_page_gru', 'Gestion tuiles GRU', 'Gestion tuiles GRU', 'edit_posts', 'gestion_page_gru_manage_tuile_gru', array($this, "manage_gestion_page_gru_tuile_gru_display"));
    }

    /**
     * Affiche le templete de modification de grille en BO
     */
    public function manage_gestion_page_gru_options_display() {
        if (!current_user_can('manage_options')) {
            wp_die('Unauthorized user');
        }

        wp_register_script('general-grid-bo', plugins_url("$this->plugin_base_name/assets/js/admin/general-grid-bo.js"));
        wp_register_script('general-grid-manager', plugins_url("$this->plugin_base_name/assets/js/admin/prototype/grid-manager.js"));
        wp_register_script('general-tuile-color-picker', plugins_url("$this->plugin_base_name/assets/js/jquery/jquery-colorpicker.min.js"));
        wp_enqueue_script('general-grid-bo');
        wp_enqueue_script('general-grid-manager');
        wp_enqueue_script('general-tuile-color-picker');
        include_once plugin_dir_path(__FILE__) . '../../templates/admin/general-settings-grid.php';
    }

    /**
     * Affiche le templete de modification de menu en BO
     */
    public function manage_gestion_page_gru_menu_display() {
        if (!current_user_can('manage_options')) {
            wp_die('Unauthorized user');
        }

        wp_register_script('general-menu-bo', plugins_url("$this->plugin_base_name/assets/js/admin/general-menu-bo.js"));
        wp_register_script('general-menu-manager', plugins_url("$this->plugin_base_name/assets/js/admin/prototype/menu-manager.js"));
        wp_enqueue_script('general-menu-bo');
        wp_enqueue_script('general-menu-manager');
        include_once plugin_dir_path(__FILE__) . '../../templates/admin/general-settings-menu.php';
    }

    /**
     * Affiche le templete de tuile de menu en BO
     */
    public function manage_gestion_page_gru_tuile_gru_display() {
        if (!current_user_can('manage_options')) {
            wp_die('Unauthorized user');
        }

        wp_register_script('general-tuile-bo', plugins_url("$this->plugin_base_name/assets/js/admin/general-tuile-bo.js"));
        wp_register_script('general-tuile-manager', plugins_url("$this->plugin_base_name/assets/js/admin/prototype/tuile-manager.js"));
        wp_register_script('general-tuile-color-picker', plugins_url("$this->plugin_base_name/assets/js/jquery/jquery-colorpicker.min.js"));
        wp_enqueue_script('general-tuile-bo');
        wp_enqueue_script('general-tuile-manager');
        wp_enqueue_script('general-tuile-color-picker');

        include_once plugin_dir_path(__FILE__) . '../../templates/admin/general-settings-tuile-gru.php';
    }

    /**
     * Affiche la page de configuration du plugin
     */
    public function config_page() {

        $dev_options = get_option(Gestion_Page_GRU_Api::rest_param_option);
        $link_options = get_option(Admin_Gestion_Page_GRU_Tools::links_pages_option);

        if (isset($_POST['update_gru_parameters'])) {
            if (substr($_POST['gru_url'], -1) != '/') {
                $_POST['gru_url'] .= '/';
            }
            $dev_options['gru_url'] = $_POST['gru_url'];
            $dev_options['crm_client_id'] = $_POST['crm_client_id'];
            $dev_options['crm_client_secret'] = $_POST['crm_client_secret'];
            $dev_options['sugar_session_id'] = '';

            // Compte
            $link_options['crm_link_forgotten_password'] = $_POST['crm_link_forgotten_password'];
            $link_options['crm_link_my_account'] = $_POST['crm_link_my_account'];
            $link_options['crm_update_my_account'] = $_POST['crm_update_my_account'];
            $link_options['crm_link_create_account'] = $_POST['crm_link_create_account'];
            $link_options['crm_link_connexion'] = $_POST['crm_link_connexion'];

            // Foyer
            $link_options['crm_link_home_foyer'] = $_POST['crm_link_home_foyer'];
            $link_options['crm_link_create_foyer'] = $_POST['crm_link_create_foyer'];
            $link_options['crm_link_update_foyer'] = $_POST['crm_link_update_foyer'];

            // Association
            $link_options['crm_link_home_association'] = $_POST['crm_link_home_association'];
            $link_options['crm_link_create_association'] = $_POST['crm_link_create_association'];
            $link_options['crm_link_update_association'] = $_POST['crm_link_update_association'];

            // Société
            $link_options['crm_link_home_societe'] = $_POST['crm_link_home_societe'];
            $link_options['crm_link_create_societe'] = $_POST['crm_link_create_societe'];
            $link_options['crm_link_update_societe'] = $_POST['crm_link_update_societe'];

            // Demande
            $link_options['crm_link_show_request'] = $_POST['crm_link_show_request'];
            $link_options['crm_link_show_request_chat'] = $_POST['crm_link_show_request_chat'];
            $link_options['crm_link_create_request'] = $_POST['crm_link_create_request'];            
            $link_options['crm_link_update_request'] = $_POST['crm_link_update_request'];            

            // Individu
            $link_options['crm_link_create_individual'] = $_POST['crm_link_create_individual'];
            $link_options['crm_link_update_individual'] = $_POST['crm_link_update_individual'];

            // Document
            $link_options['crm_link_show_document'] = $_POST['crm_link_show_document'];


            // MAJ des options de configuration du plugin
            update_option(Gestion_Page_GRU_Api::rest_param_option, $dev_options);
            update_option(Admin_Gestion_Page_GRU_Tools::links_pages_option, $link_options);
            echo ( '<div class="updated"><p><strong>Configuration mise à jour.</strong></p></div>' );
        }

        wp_register_script('general-function-bo', plugins_url("$this->plugin_base_name/assets/js/admin/general-function-bo.js"));
        wp_register_script('general-settings-bo', plugins_url("$this->plugin_base_name/assets/js/admin/prototype/general-settings.class.js"));
        wp_enqueue_script('general-function-bo');
        wp_enqueue_script('general-settings-bo');
        include_once plugin_dir_path(__FILE__) . '../../templates/admin/general-settings-config-rest.php';
    }

    public function check_connexion_gru() {
        $rest_api_client = new Gestion_Page_GRU_Api();
        $result = $rest_api_client->check_access_oauth_client();

        switch ($result['http_code']) {
            case "200":
                $result = array(
                    'code' => 1,
                    'message' => 'La connexion avec la gru est fonctionnelle'
                );
                break;
            default:
                $result = array(
                    'code' => 0,
                    'message' => "[" . $result['http_code'] . '] La connexion avec la gru à échouée <br/><br/><pre>' . print_r($result,true) . '</pre>'
                );
                break;
        }

        echo json_encode($result);
        wp_die();
    }

}

new Gestion_Page_GRU_General_Admin();
