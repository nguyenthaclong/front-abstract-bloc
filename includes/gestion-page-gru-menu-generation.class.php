<?php

/**
 * 1997-2018 Quadra Informatique
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 *
 *  @author    Quadra Informatique <modules@quadra-informatique.fr>
 *  @copyright 1997-2018 Quadra Informatique
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * 
 *  Gestion_Page_GRU_Menu_Generation
 */
class Gestion_Page_GRU_Menu_Generation {

    /**
     * Constructeur
     */
    public function __construct() {
        add_action('wp_head', array($this, 'init_nav_menu'));
    }
    
    /**
     * Initialise le menu pour les pages FO
     */
    public function init_nav_menu() {
        if (!is_admin()) {
            $plugin_base_name = explode('/', plugin_basename(__FILE__))[0];
            wp_register_script('gestion-page-gru-main', plugins_url("$plugin_base_name/assets/js/main.js"));
            wp_enqueue_script('gestion-page-gru-main');
            wp_nav_menu(array(
                'fallback_cb' =>  array($this, "generate_menu_on_page"), //function de substitution à utiliser si le menu n'existe 
            ));
        }
    }
    
    /**
     * Génère le menu associé à chaque page
     * 
     * @global object $wpdb
     */
    public function generate_menu_on_page() {
        global $wpdb;
        $page_id = get_the_ID();
        $result = $wpdb->get_results(<<<SQL
            SELECT * FROM {$wpdb->prefix}gestion_page_gru_page_correspondance_menu 
            INNER JOIN {$wpdb->prefix}gestion_page_gru_page_menu ON {$wpdb->prefix}gestion_page_gru_page_correspondance_menu.page_correspondance_menu_menu_id = {$wpdb->prefix}gestion_page_gru_page_menu.page_menu_id
            WHERE page_correspondance_menu_page_id = '$page_id'
SQL
        );

        if (count($result) > 0) {
            $html_menu = "<nav class='main_menu'><div id='gestion-page-gru-content' class='gru-content nav-menu-gru'>";
            $infos_menu = unserialize($result[0]->page_menu_content);
            foreach ($infos_menu as $ctg) {
                $html_menu .= $this->generate_category_element($ctg);
            }
            $right_menu = $this->get_right_menu();
            $html_menu .= "$right_menu</div></nav>";
        }
        echo $html_menu;
    }

    /**
     * Génère les éléments niveau 1 du menu
     * 
     * @param array $ctg
     * @return string
     */
    public function generate_category_element($ctg) {
        $html_child = isset($ctg['childs']) ? $this->generate_category_childs_elements($ctg['childs']) : "";
        $link_ctg = (!empty($ctg['id-page-link'])) ? get_page_link($ctg['id-page-link']) : "#";
        $is_selected = ($link_ctg == get_permalink()) ?  "selected" : "";
        $html = <<<HTML
            <span class="gestion-page-gru-ctg-menu">
                <a href="$link_ctg" class="$is_selected">
                    <span class="menu-libelle">{$ctg['libelle']}</span>
                </a>
                <div class="menu-content" style="display:none;">
                    $html_child
                </div>
            </span>
HTML;
        return $html;
    }

    /**
     * Génère les éléments niveau 2 du menu
     * 
     * @param array $childs
     * @return string
     */
    public function generate_category_childs_elements($childs) {
        $html = "<ul class='ctg-childs'>";
        foreach ($childs as $child) {
            $link = get_page_link($child['id-page-link']);
            $is_selected = "";
            //$is_selected = ($link == get_permalink()) ?  "selected" : "";
            $html .= <<<HTML
                <li class='child'>
                    <a href="$link" class="$is_selected">
                        {$child['libelle']}
                    </a>
                </li>
HTML;
        }
        $html .= "</ul>";
        return $html;
    }
    
    /**
     * Génère l'élément du menu a droite (Recherche / Connexion)
     * 
     * @return string
     */
    public function get_right_menu() {
        
        $link_options = get_option(Admin_Gestion_Page_GRU_Tools::links_pages_option);
        if(is_user_logged_in()) {          
            $link_account = wp_logout_url(get_page_link($link_options['crm_link_connexion']));            
            $html_account = <<<HTML
            <span>
                <a href="$link_account">
                    <i class="fas fa-sign-out-alt"></i>
                     <br/>
                    Déconnexion
                </a>
            </span>
                    
HTML;
        } else {            
            $link_account = get_page_uri($link_options['crm_link_connexion']);
            $html_account = <<<HTML
            <span>
                <a href="$link_account">
                    <i class="fa fa-power-off"></i>
                     <br/>
                    Connexion
                </a>
            </span>
                    
HTML;
        }
        
        $html = <<<HTML
        <div class="menu-right">
            <!--<span>
                <i class="fa fa-search"></i>
                 <br/>
                Recherche
            </span>-->
            $html_account
        </div>     
HTML;
        return $html;
    }
}

new Gestion_Page_GRU_Menu_Generation();
