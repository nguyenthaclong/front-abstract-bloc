<?php

/**
 * 1997-2018 Quadra Informatique
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 *
 *  @author    Quadra Informatique <modules@quadra-informatique.fr>
 *  @copyright 1997-2018 Quadra Informatique
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * 
 *  Gestion_Page_GRU_Grid_Generation
 */
class Gestion_Page_GRU_Grid_Generation {

    private $css_id_loaded = array();
    private $nb_block_gru = 0;

    /**
     * Constructeur
     */
    public function __construct() {
        add_filter('the_content', array($this, 'generate_grid_on_page'), 50);
        add_action('wp_ajax_set_context', array($this, 'set_context'), 50);
        add_action('wp_ajax_add_to_context', array($this, 'add_to_context'), 50);
    }

    /**
     * Génère le contenu de la grille sur la page associée
     * 
     * @global object $wpdb
     * @param string $content
     * @return string
     */
    public function generate_grid_on_page($content) {
        global $wpdb;
        Admin_Gestion_Page_GRU_Tools::load_jquery_lib();
        $page_id = get_the_ID();

        $result = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}gestion_page_gru_page_correspondance_grid` WHERE page_correspondance_grid_id = '$page_id'");

        $html_grid = "";
        $base_url = get_home_url();
        $html_grid .= "<script type='text/javascript'> var base_url = '$base_url'; </script>";
        $link_options = get_option(Admin_Gestion_Page_GRU_Tools::links_pages_option);
        switch ($page_id) {
            // Compte
            case $link_options['crm_link_forgotten_password'] :
            case $link_options['crm_link_my_account'] :
            case $link_options['crm_link_create_account'] :
            case $link_options['crm_link_connexion'] :
                Admin_Gestion_Page_GRU_Tools::remove_context();
                Admin_Gestion_Page_GRU_Tools::show_context_debug("Compte context");
                break;
            case $link_options['crm_update_my_account'] :
                Admin_Gestion_Page_GRU_Tools::remove_context_request_without_link();
                Admin_Gestion_Page_GRU_Tools::show_context_debug("Compte context");
                break;

            // Foyer
            case $link_options['crm_link_home_foyer'] :
                Admin_Gestion_Page_GRU_Tools::remove_context_request();
                Admin_Gestion_Page_GRU_Tools::show_context_debug("Foyer context");
                break;
            case $link_options['crm_link_create_foyer'] :
            case $link_options['crm_link_update_foyer'] :
                Admin_Gestion_Page_GRU_Tools::remove_context_request_without_link();
                Admin_Gestion_Page_GRU_Tools::show_context_debug("Foyer context - retour link");
                break;


            // Association
            case $link_options['crm_link_home_association'] :
                Admin_Gestion_Page_GRU_Tools::remove_context_request();
                Admin_Gestion_Page_GRU_Tools::show_context_debug("Association context");
                break;
            case $link_options['crm_link_create_association'] :
            case $link_options['crm_link_update_association'] :
                Admin_Gestion_Page_GRU_Tools::remove_context_request_without_link();
                Admin_Gestion_Page_GRU_Tools::show_context_debug("Association context - retour link");
                break;

            // Societe
            case $link_options['crm_link_home_societe'] :
                Admin_Gestion_Page_GRU_Tools::remove_context_request();
                Admin_Gestion_Page_GRU_Tools::show_context_debug("Société context");
                break;
            case $link_options['crm_link_create_societe'] :
            case $link_options['crm_link_update_societe'] :
                Admin_Gestion_Page_GRU_Tools::remove_context_request_without_link();
                Admin_Gestion_Page_GRU_Tools::show_context_debug("Association context - retour link");
                break;

            // Demande
            case $link_options['crm_link_show_request'] :
            case $link_options['crm_link_show_request_chat'] :
            case $link_options['crm_link_create_request'] :
            case $link_options['crm_link_update_request'] :
                Admin_Gestion_Page_GRU_Tools::show_context_debug("Demande context");
                break;

            // Individu
            case $link_options['crm_link_create_individual'] :
            case $link_options['crm_link_update_individual'] :
                Admin_Gestion_Page_GRU_Tools::show_context_debug("Individu context");
                break;

            default :
                if (count($result) > 0) {
                    Admin_Gestion_Page_GRU_Tools::remove_context();
                    Admin_Gestion_Page_GRU_Tools::show_context_debug("Default remove context");
                }
                break;
        }

        if (count($result) > 0) {

            $grid = unserialize($result[0]->page_correspondance_grid);
            foreach ($grid as $block) {
                $html_grid .= $this->generate_block($block);
            }

            $plugin_base_name = explode('/', plugin_basename(__FILE__))[0];
            wp_register_script('gestion-page-gru-main', plugins_url("$plugin_base_name/assets/js/main.js"));
            wp_register_script('gestion-page-gru-main-entity', plugins_url("$plugin_base_name/assets/js/front/entity.js"));
            wp_register_script('gestion-page-gru-main-individual', plugins_url("$plugin_base_name/assets/js/front/individual.js"));
            wp_register_script('gestion-page-gru-main-request', plugins_url("$plugin_base_name/assets/js/front/request.js"));
            wp_register_script('gestion-page-gru-main-document', plugins_url("$plugin_base_name/assets/js/front/document.js"));
            wp_register_script('gestion-page-gru-main-message', plugins_url("$plugin_base_name/assets/js/front/message.js"));
            wp_enqueue_script('gestion-page-gru-main');
            wp_enqueue_script('gestion-page-gru-main-entity');
            wp_enqueue_script('gestion-page-gru-main-individual');
            wp_enqueue_script('gestion-page-gru-main-request');
            wp_enqueue_script('gestion-page-gru-main-document');
            wp_enqueue_script('gestion-page-gru-main-message');
        }

        $html_updated = Admin_Gestion_Page_GRU_Tools::get_gru_msg_html('updated');
        $html_errors = Admin_Gestion_Page_GRU_Tools::get_gru_msg_html('error');
        $html_infos = Admin_Gestion_Page_GRU_Tools::get_gru_msg_html('infos');
        $html_grid .= <<<HTML
            <div id="gestion-gru-ajax-msg" class="row">
                $html_updated
                $html_errors
                $html_infos
            </div>           
HTML;

        $content .= <<<HTML
        <div id="gestion-page-gru-content" class="gru-content">
            $html_grid
        </div>
HTML;

        return $content;
    }

    /**
     * Génère les éléments de la grille
     * 
     * @param array $_block
     * @return string
     */
    public function generate_block($_block) {
        $html_child = "";
        if (isset($_block['items'])) {
            foreach ($_block['items'] as $block) {
                switch ($block['name']) {
                    case "element-gru":
                        $html_child .= $this->generate_block_gru($block);
                        break;
                    case "element-cde" :
                        $html_child .= $this->generate_block_cde($block);
                        break;
                    default:
                        $html_child .= $this->generate_block($block);
                        break;
                }
            }
        }

        if ($_block['columns'] === "section") {
            $title = ($_block['title'] !== "") ?
                    "<h2 style='color:{$_block['titlecolor']}'>" . stripslashes($_block['title']) . "</h2>" : "";

            $html = <<<HTML
            <section class="{$_block['columns']}" style="background-color:{$_block['color']};">
                $title
                <div class="container">                        
                    $html_child
                </div>
            </section>
HTML;
        } else {
            $html = <<<HTML
            <div class="{$_block['columns']}">
                $html_child
            </div>
HTML;
        }

        return $html;
    }

    /**
     * Génère les tuiles GRU
     * 
     * @global object $wpdb
     * @param string $id
     * @return string
     */
    public function generate_block_gru($block) {
        global $wpdb;
        $result = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}gestion_page_gru_tuile` WHERE tuile_id = {$block['key']}");
        $html = "";
        if (count($result) > 0) {
            $link_options = get_option(Admin_Gestion_Page_GRU_Tools::links_pages_option);
            if (is_user_logged_in()) {
                $connection_link = '';
            } else {
                $connection_link = 'window.location = \'' . get_page_link($link_options['crm_link_connexion']) . '\'';
            }
            $href_create_request = get_page_link($link_options['crm_link_create_request']);
            $plugin_base_name = explode('/', plugin_basename(__FILE__))[0];
            $path_image = plugins_url("$plugin_base_name/assets/images/tuiles/");
            $tuile_title = stripslashes($result[0]->tuile_title);
            $tuile_desc = stripslashes($result[0]->tuile_description);
            $this->nb_block_gru++;

            $style_image_background = "";
            if (is_file(__DIR__ . '/../assets/images/tuiles/' . $result[0]->tuile_image)) {
                $style_image_background = "style='background-image:url($path_image{$result[0]->tuile_image})'";
            }


            switch ($result[0]->tuile_type) {
                case "0" : case "4" :
                    $tuile_content = <<<HTML
                        <div class="element-gru element-gru-text box-shadow-gru" name="element-gru-{$result[0]->tuile_id}" $style_image_background>                  
                                    <div class="element-gru-header">
                                        <div class="title">
                                            <span class="gestion-page-gru-element-title">$tuile_title</span>
                                        </div>
                            </div>
                           <div class="grid-element-gru-content">
                                        <i class="fa fa-{$result[0]->tuile_icon}"></i>
                                        <p>$tuile_desc</p>
                                    </div>
                        </div>
HTML;

                    if (!empty($result[0]->tuile_page_id)) {
                        $link = (is_numeric($result[0]->tuile_page_id)) ? get_page_link($result[0]->tuile_page_id) : $result[0]->tuile_page_id;
                        $hover = "";
                        if (!empty($result[0]->tuile_description_plus)) {
                            $hover = <<<HTML
                            <div class="gru-link-hover">
                                <p> {$result[0]->tuile_description_plus} </p>
                            </div>
HTML;
                        }
                        $tuile_content = <<<HTML
                            <a href="$link" class="gru-link">
                                $tuile_content
                                $hover
                            </a>
HTML;
                    }
                    $html .= $tuile_content;
                    break;
                case "1" :
                    $function = ("aucun" == $result[0]->tuile_type_beneficiaire || empty($result[0]->tuile_type_beneficiaire)) ? "$.grufront.set_context" : "$.grufront.add_to_context";

                    $html .= <<<HTML
                      <span class="gru-icon-button" onclick="$function({id_service: '{$result[0]->tuile_teleservice_id}',type_beneficiaire: '{$result[0]->tuile_type_beneficiaire}', retour_link:window.location.href}, function() { window.location = '$href_create_request'; });">
                          <div class="element-gru box-shadow-gru" name="element-gru-{$result[0]->tuile_id}" $style_image_background>
                                <div class="element-gru-header">
                                    <div class="title">
                                        <span class="gestion-page-gru-element-title">$tuile_title</span>
                                    </div>
                                </div>
                                <div class="grid-element-gru-content">
                                    <i class="fa fa-{$result[0]->tuile_icon}"></i>
                                    <p>$tuile_desc</p>
                                </div>
                            </div>
                        </span>
HTML;
                    break;

                case "3" :

                    $desc_plus = "<div class='desc-plus'>" . stripcslashes($result[0]->tuile_description_plus) . "</div>";
                    $data = (array_key_exists('data', $block['function'])) ? $block['function']['data'] : array();
                    $result_function = ($data === array()) ?
                            $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}gestion_page_gru_function` WHERE function_id = {$result[0]->tuile_function_id}") :
                            $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}gestion_page_gru_function` WHERE function_id = {$block['function']['id']}");
                    $content_list = call_user_func_array($result_function[0]->function_name, array($data));

                    $html .= <<<HTML
                        <div class="element-gru element-gru-notif box-shadow-gru" name="element-gru-{$result[0]->tuile_id}" $style_image_background>
                            <div class="element-gru-header">
                                <div class="title">
                                    <span class="gestion-page-gru-element-title">$tuile_title</span>
                                </div>
                            </div>
                            <div class="grid-element-gru-content">
                                <div class="multi-content">
                                    <div class="desc-dyn"></div>
                                    <i class="fa fa-{$result[0]->tuile_icon}"></i>
                                </div>
                                $desc_plus
                                <p>$tuile_desc</p>
                            </div>
                            <div class="content-link-notif" name="content-notif-{$result[0]->tuile_id}-{$this->nb_block_gru}" style="display:none;">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="content-notif box-shadow-gru">
                                            <h2> Liste $tuile_title </h2>
                                            $content_list
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
HTML;
                    break;
                case "2" :
                    $links = $wpdb->get_results(<<<SQL
                        SELECT * 
                        FROM `{$wpdb->prefix}gestion_page_gru_tuile_ctg_correspondance`
                        WHERE tuile_ctg_correspondance_parent_id = {$result[0]->tuile_id}
SQL
                    );
                    $links_ctg = "";
                    if (count($links) > 0) {
                        foreach ($links as $link) {
                            $child = $wpdb->get_results(<<<SQL
                                SELECT * 
                                FROM `{$wpdb->prefix}gestion_page_gru_tuile`
                                WHERE tuile_id = {$link->tuile_ctg_correspondance_child_id}
SQL
                            );

                            if (!empty($child[0]->tuile_teleservice_id) && $child[0]->tuile_teleservice_id !== "0") {
                                $img = ($child[0]->tuile_image != "") ? $style_image_background : '';
                                $title = stripcslashes($child[0]->tuile_title);
                                if (!in_array($child[0]->tuile_id, $this->css_id_loaded)) {
                                    $html = apply_filters("get_dynamic_css", $html, $child[0]->tuile_id);
                                }

                                $links_ctg .= <<<HTML
                                        <li class='link-ctg' name='element-gru-{$child[0]->tuile_id}'>
                                            <a href='#' onclick="$.grufront.add_to_context({id_service: '{$child[0]->tuile_teleservice_id}',type_beneficiaire: '{$child[0]->tuile_type_beneficiaire}','retour_link':window.location.href}, function() { window.location = '$href_create_request'; })">
                                                <div class='circle-link' $img>
                                                    <i class='fa fa-{$child[0]->tuile_icon}'></i>
                                                    <p class='desc-link'>$title</p>
                                                </div>
                                            </a>   
                                        </li>
HTML;
                            }
                        }
                    }
                    $html .= <<<HTML
                        <div class="element-gru element-ctg box-shadow-gru" name="element-gru-{$result[0]->tuile_id}" $style_image_background>
                            <div class="element-gru-header">
                                <div class="title">
                                    <span class="gestion-page-gru-element-title">{$result[0]->tuile_title}</span>
                                </div>
                            </div>
                            <div class="grid-element-gru-content">
                                <i class="fa fa-{$result[0]->tuile_icon}"></i>
                                <p>{$result[0]->tuile_description}</p>
                            </div>
                            <div class="content-link-ctg box-shadow-gru" style="display:none;">
                                <h2>{$result[0]->tuile_title}</h2>
                                <ul>$links_ctg</ul>      
                            </div>
                        </div>
HTML;
                    break;
            }
        }

        if (!in_array($result[0]->tuile_id, $this->css_id_loaded)) {
            $html = apply_filters("get_dynamic_css", $html, $result[0]->tuile_id);
            $this->css_id_loaded . push[$result[0]->tuile_id];
        }
        return $html;
    }

    /**
     * Génère les blocs de fonctions CDE
     * 
     * @global object $wpdb
     * @param array $block
     * 
     * @return string
     */
    public function generate_block_cde($block) {
        global $wpdb;
        $content_child = "";
        $data = $childs = array();
        if ((array_key_exists('function', $block))) {
            if (count($block['function']['childs']) > 0) {
                $result_child_function = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}gestion_page_gru_function`");
                if (array_key_exists('childs', $block['function'])) {
                    $childs = $block['function']['childs'];
                }
            }

            $data = $block['function']['data'];
        }

        $result_function = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}gestion_page_gru_function` WHERE function_id = {$block['key']}");
        $cde_function_html = call_user_func_array($result_function[0]->function_name, array($data, $childs));
        return $cde_function_html;
    }

    /**
     * Réinitialise du context en ajax
     */
    public function set_context() {
        Admin_Gestion_Page_GRU_Tools::set_context($_POST['data']);
        wp_die();
    }

    /**
     * Ajoute du context en ajax
     */
    public function add_to_context() {
        Admin_Gestion_Page_GRU_Tools::add_to_context($_POST['data']);
        wp_die();
    }

}

new Gestion_Page_GRU_Grid_Generation();
