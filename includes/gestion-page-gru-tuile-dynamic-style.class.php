<?php

/**
 * 1997-2018 Quadra Informatique
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 *
 *  @author    Quadra Informatique <modules@quadra-informatique.fr>
 *  @copyright 1997-2018 Quadra Informatique
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * 
 *  Admin_Gestion_Page_GRU_Menu
 */
class Admin_Gestion_Page_GRU_Tuile_Dynamic_Style {

    /**
     * Constructeur
     */
    public function __construct() {
        add_filter('get_dynamic_css', array($this, 'get_dynamic_css'), 10,2);
    }

    /**
     * Génère le style d'une tuile
     * 
     * @global type $wpdb
     * @param string $html 
     * @param string $id_tuile
     * @return type
     */
    public function get_dynamic_css($html, $id_tuile = null) {
        global $wpdb;
        $list_style_tuile = ($id_tuile !== null) ?
                $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}gestion_page_gru_tuile_style` WHERE tuile_style_tuile_id = $id_tuile") :
                $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}gestion_page_gru_tuile_style`");

        $css_style_tuile = "";
        if (!empty($list_style_tuile)) {
            foreach ($list_style_tuile as $style) {
                if ($style->tuile_style_css_value === "")
                    continue;

                switch ($style->tuile_style_css_libelle) {
                    case "title-background-color" :
                        $css_style_tuile .= <<<HTML
                .element-gru[name=element-gru-{$style->tuile_style_tuile_id}] .element-gru-header,
                .element-gru[name=element-gru-{$style->tuile_style_tuile_id}] .content-link-ctg,
                li.link-ctg[name=element-gru-{$style->tuile_style_tuile_id}] .circle-link .desc-link {
                    background-color : {$style->tuile_style_css_value} !important;
                }
HTML;
                        break;
                    case "title-color":
                        $css_style_tuile .= <<<HTML
                .element-gru[name=element-gru-{$style->tuile_style_tuile_id}] .element-gru-header,
                .element-gru[name=element-gru-{$style->tuile_style_tuile_id}] .element-gru-header .gestion-page-gru-element-title,
                .element-gru[name=element-gru-{$style->tuile_style_tuile_id}] .content-link-ctg .gestion-page-gru-element-title,
                .element-gru[name=element-gru-{$style->tuile_style_tuile_id}] .content-link-ctg ul li a,
                li.link-ctg[name=element-gru-{$style->tuile_style_tuile_id}] .circle-link .desc-link {
                    color : {$style->tuile_style_css_value} !important;
                }
HTML;
                        break;
                    case "title-font-family":
                        $css_style_tuile .= <<<HTML
                li.link-ctg[name=element-gru-{$style->tuile_style_tuile_id}] .circle-link .desc-link,
                .element-gru[name=element-gru-{$style->tuile_style_tuile_id}] .element-gru-header,
                .element-gru[name=element-gru-{$style->tuile_style_tuile_id}] .element-gru-header .gestion-page-gru-element-title {
                    font-family : {$style->tuile_style_css_value} !important;
                }
HTML;
                        break;
                    case "title-font-size":
                        $css_style_tuile .= <<<HTML
                .element-gru[name=element-gru-{$style->tuile_style_tuile_id}] .element-gru-header,
                .element-gru[name=element-gru-{$style->tuile_style_tuile_id}] .element-gru-header .gestion-page-gru-element-title {
                    font-size : {$style->tuile_style_css_value}px !important;
                }
HTML;
                        break;
                    case "body-background-color":
                        $css_style_tuile .= <<<HTML
                .element-gru[name=element-gru-{$style->tuile_style_tuile_id}] .element-tuile-description,
                .element-gru[name=element-gru-{$style->tuile_style_tuile_id}] .grid-element-gru-content p {
                    background-color: {$style->tuile_style_css_value} !important;
                }
HTML;
                        break;
                    case "body-color":
                        $css_style_tuile .= <<<HTML
                .element-gru[name=element-gru-{$style->tuile_style_tuile_id}] .element-tuile-description,
                .element-gru[name=element-gru-{$style->tuile_style_tuile_id}] .grid-element-gru-content p {
                    color : {$style->tuile_style_css_value} !important;
                }
HTML;
                        break;
                    case "body-font-family":
                        $css_style_tuile .= <<<HTML
                .element-gru[name=element-gru-{$style->tuile_style_tuile_id}] .element-tuile-description,
                .element-gru[name=element-gru-{$style->tuile_style_tuile_id}] .grid-element-gru-content p {
                    font-family : {$style->tuile_style_css_value} !important;
                }
HTML;
                        break;
                    case "body-font-size":
                        $css_style_tuile .= <<<HTML
                .element-gru[name=element-gru-{$style->tuile_style_tuile_id}] .element-tuile-description,
                .element-gru[name=element-gru-{$style->tuile_style_tuile_id}] .grid-element-gru-content p {
                    font-size : {$style->tuile_style_css_value}px !important
                }
HTML;
                        break;
                    case "icon-color":
                        $css_style_tuile .= <<<HTML
                .element-gru[name=element-gru-{$style->tuile_style_tuile_id}] .grid-element-gru-content i,
                li.link-ctg[name=element-gru-{$style->tuile_style_tuile_id}] .circle-link i {
                    color : {$style->tuile_style_css_value} !important
                }
HTML;
                        break;
                    case "icon-font-size":
                        $css_style_tuile .= <<<HTML
                .element-gru[name=element-gru-{$style->tuile_style_tuile_id}] .grid-element-gru-content i {
                    font-size : {$style->tuile_style_css_value}px !important
                }
HTML;
                        break;
                }
            }
        }
        return "<style type='text/css' id='style-tag-$id_tuile'>$css_style_tuile</style>" . $html;
    }
}

new Admin_Gestion_Page_GRU_Tuile_Dynamic_Style();