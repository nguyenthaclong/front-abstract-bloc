<?php

class liaisons_individus_HTML {

    var $infos_individu_a;
    var $liste_individus_b;
    var $liste_roles;
    var $liaisons;

    function __construct($infos_individu_a, $liste_individus_b, $liste_roles, $liaisons) {

        $this->infos_individu_a = $infos_individu_a;
        $this->liste_individus_b = $liste_individus_b;
        $this->liste_roles = $liste_roles;
        $this->liaisons = $liaisons;
    }

    function genere_HTML() {


        $individu_a = $this->infos_individu_a;
        $liste_individus_b = $this->liste_individus_b;
        $liste_roles = $this->liste_roles;

        if (empty($this->liaisons)) { # CREATION DES LIAISONS
            $id_individu_a = $individu_a['id']['value'];

            $html_genere .= '<div class="msg-liaisons-inter-individus">Veuillez définir les liens de parenté entre <strong>' . $individu_a['first_name']['value'] . ' ' . $individu_a['last_name']['value'] . '</strong> et les autres membres du foyer.</div>';
            $html_genere .= '<form name="form_liaisons_individus" class="relations-inter-ind" method="POST" action="">';
            $html_genere .= '<input type="hidden" name="id_individu_a" id="id_individu_a" value="' . $individu_a['id']['value'] . '">'; // à masquer

            foreach ($liste_individus_b as $key => $individu_b) {

                $html_genere .= '<fieldset><legend>Lien de parenté avec ' . $individu_b['individu_name'] . '</legend>';

                $roles = '';
                foreach ($liste_roles as $code_role => $libelle_role) {
                    if ($code_role == $role_courant_a) {
                        $roles .= '<option selected value=' . $code_role . '>' . $libelle_role . '</option>';
                    } else {
                        $roles .= '<option value=' . $code_role . '>' . $libelle_role . '</option>';
                    }
                }

                $html_genere .= '<div class="ligne">';
                $html_genere .= '<div class="label_txt"><span>' . $individu_a['first_name']['value'] . ' ' . $individu_a['last_name']['value'] . '</span> est la ou le </div>';
                $html_genere .= '<select name="role_a_' . $individu_b['individu_id'] . '">';
                foreach ($liste_roles as $code_role => $libelle_role) {
                    if ($role_courant_a == $code_role) {
                        $html_genere .= '<option value="' . $code_role . '" selected>' . $libelle_role . '</option>';
                    } else {
                        $html_genere .= '<option value="' . $code_role . '">' . $libelle_role . '</option>';
                    }
                }

                $html_genere .= '</select>';

                $html_genere .= '<div class="label_txt"> de ';
                $html_genere .= '<span>' . $individu_b['individu_name'] . '</span>';
                $html_genere .= '<input type="hidden" name="id_individu_b_' . $individu_b['individu_id'] . '" id="id_individu_b_' . $individu_b['individu_id'] . '" value="' . $individu_b['individu_id'] . '">'; // à masquer
                $html_genere .= ' qui est sa ou son </div>';
                $html_genere .= '<select name="role_b_' . $individu_b['individu_id'] . '">';
                //$html_genere .= $roles;
                foreach ($liste_roles as $code_role => $libelle_role) {
                    if ($role_courant_b == $code_role) {
                        $html_genere .= '<option value="' . $code_role . '" selected>' . $libelle_role . '</option>';
                    } else {
                        $html_genere .= '<option value="' . $code_role . '">' . $libelle_role . '</option>';
                    }
                }

                $html_genere .= '</select>';
                $html_genere .= '</fieldset>';
            }
            $html_genere .= '<input class="button" type="submit" name="submit_liaisons_individus" value="Valider">';
            $html_genere .= '</form>';
        } else { # MODIFICATION DES LIAISONS
            $liaisons = $this->liaisons['relations'];
            echo '<PRE>';
            print_r($liaisons);
            echo '</PRE>';
            $id_individu_a = $individu_a['id']['value'];

            $html_genere .= '<div class="msg-liaisons-inter-individus">Veuillez définir les liens de parenté entre ' . $individu_a['first_name']['value'] . ' ' . $individu_a['last_name']['value'] . ' et les autres membres du foyer.</div>';
            $html_genere .= '<form name="form_liaisons_individus" class="relations-inter-ind" method="POST" action="">';
            $html_genere .= '<input type="hidden" name="id_individu_a" id="id_individu_a" value="' . $individu_a['id']['value'] . '">'; // à masquer

            foreach ($liste_individus_b as $key => $individu_b) {
                $role_courant_a = '';
                $role_courant_b = '';
                $id_liaison = '';
                foreach ($liaisons as $liaison) {
                    if ($liaison['individu_id'] == $individu_b['individu_id']) { // si on trouve dans le tableau de liaisons, une liaison concernant l'individu courant
                        if (!empty($liaison['role_1']) && !empty($liaison['role_2'])) {
                            $role_courant_a = $liaison['role_1'];
                            $role_courant_b = $liaison['role_2'];
                        }
                        $id_liaison = $liaison['relation_id'];
                    }
                }

                $html_genere .= '<fieldset><legend>Lien de parenté avec ' . $individu_b['individu_name'] . '</legend>';
                //$id_individu_b = $individu_b['id'];
                $html_genere .= '<div class="ligne">';
                $html_genere .= '<div class="label_txt"><span>' . $individu_a['first_name']['value'] . ' ' . $individu_a['last_name']['value'] . '</span> est la ou le </div>';
                $html_genere .= '<select name="role_a_' . $individu_b['individu_id'] . '">';
                //$html_genere .= $roles;
                foreach ($liste_roles as $code_role => $libelle_role) {
                    if ($role_courant_a == $code_role) {
                        $html_genere .= '<option value="' . $code_role . '" selected>' . $libelle_role . '</option>';
                    } else {
                        $html_genere .= '<option value="' . $code_role . '">' . $libelle_role . '</option>';
                    }
                }

                $html_genere .= '</select>';


                $html_genere .= '<div class="label_txt"> de ';
                $html_genere .= '<span>' . $individu_b['individu_name'] . '</span>';
                $html_genere .= '<input type="hidden" name="id_individu_b_' . $individu_b['individu_id'] . '" id="id_individu_b_' . $individu_b['individu_id'] . '" value="' . $individu_b['individu_id'] . '">'; // à masquer
                $html_genere .= '<input type="hidden" name="id_liaison_avec_' . $individu_b['individu_id'] . '" id="id_liaison_avec_' . $individu_b['individu_id'] . '" value="' . $id_liaison . '">'; // à masquer
                $html_genere .= ' qui est sa ou son </div>';
                $html_genere .= '<select name="role_b_' . $individu_b['individu_id'] . '">';
                //$html_genere .= $roles;
                foreach ($liste_roles as $code_role => $libelle_role) {
                    if ($role_courant_b == $code_role) {
                        $html_genere .= '<option value="' . $code_role . '" selected>' . $libelle_role . '</option>';
                    } else {
                        $html_genere .= '<option value="' . $code_role . '">' . $libelle_role . '</option>';
                    }
                }

                $html_genere .= '</select>';
                $html_genere .= '</fieldset>';
            }

            $html_genere .= '<input class="button" type="submit" name="submit_liaisons_individus" value="Valider">';
            $html_genere .= '</form>';
        }

        return $html_genere;
    }

}
