<?php

class liaisons_individus_HTML {

    var $infos_individu_a;
    var $liste_individus_b;
    var $liste_roles;
    var $liaisons;

    function __construct($infos_individu_a, $liste_individus_b, $liste_roles, $liaisons) {

        $this->infos_individu_a = $infos_individu_a;
        $this->liste_individus_b = $liste_individus_b;
        $this->liste_roles = $liste_roles;
        $this->liaisons = $liaisons;
    }

    function genere_HTML() {

        # L'individu dont l'ID est dans l'URL
        $individu_a = $this->infos_individu_a;

        # La liste des autres individus de l'entité
        $liste_individus_b = $this->liste_individus_b;

        # La liste des rôles possibles dans les liens inter-individus
        $liste_roles = $this->liste_roles;

        # Aucune liaison récupérée donc on est dans un contexte de création
        if (empty($this->liaisons)) { # CREATION DES LIAISONS
            $id_individu_a = $individu_a['id']['value'];

            $html_genere .= '<div class="msg-liaisons-inter-individus">Veuillez définir les liens de parenté entre <strong>' . $individu_a['first_name']['value'] . ' ' . $individu_a['last_name']['value'] . '</strong> et les autres membres du foyer.</div>';
            $html_genere .= '<form name="form_liaisons_individus" class="relations-inter-ind" method="POST" action="">';
            $html_genere .= '<input type="hidden" name="id_individu_a" id="id_individu_a" value="' . $individu_a['id']['value'] . '">'; // à masquer

            foreach ($liste_individus_b as $key => $individu_b) {

                $html_genere .= '<fieldset><legend>Lien de parenté avec ' . $individu_b['individu_name'] . '</legend>';
                $roles = '';
                foreach ($liste_roles as $code_role => $libelle_role) {
                    if ($code_role == $role_courant_a) {
                        $roles .= '<option selected value=' . $code_role . '>' . $libelle_role . '</option>';
                    } else {
                        $roles .= '<option value=' . $code_role . '>' . $libelle_role . '</option>';
                    }
                }

                $html_genere .= '<div class="ligne">';
                $html_genere .= '<div class="label_txt"><span>' . $individu_a['first_name']['value'] . ' ' . $individu_a['last_name']['value'] . '</span> est la ou le </div>';
                $html_genere .= '<select name="role_a_' . $individu_b['individu_id'] . '">';
                foreach ($liste_roles as $code_role => $libelle_role) {
                    if ($role_courant_a == $code_role) {
                        $html_genere .= '<option value="' . $code_role . '" selected>' . $libelle_role . '</option>';
                    } else {
                        $html_genere .= '<option value="' . $code_role . '">' . $libelle_role . '</option>';
                    }
                }

                $html_genere .= '</select>';

                $html_genere .= '<div class="label_txt"> de ';
                $html_genere .= '<span>' . $individu_b['individu_name'] . '</span>';
                $html_genere .= '<input type="hidden" name="id_individu_b_' . $individu_b['individu_id'] . '" id="id_individu_b_' . $individu_b['individu_id'] . '" value="' . $individu_b['individu_id'] . '">'; // à masquer
                $html_genere .= ' qui est sa ou son </div>';
                $html_genere .= '<select name="role_b_' . $individu_b['individu_id'] . '">';
                foreach ($liste_roles as $code_role => $libelle_role) {
                    if ($role_courant_b == $code_role) {
                        $html_genere .= '<option value="' . $code_role . '" selected>' . $libelle_role . '</option>';
                    } else {
                        $html_genere .= '<option value="' . $code_role . '">' . $libelle_role . '</option>';
                    }
                }

                $html_genere .= '</select>';
                $html_genere .= '</fieldset>';
            }
            $type_et_id = do_shortcode('[type_et_id]');
            $url_retour = get_permalink(wp_get_post_parent_id(get_the_ID())) . '?' . $type_et_id;
            $html_genere .= '<a href="' . $url_retour . '" class="button btn-secondaire" >Annuler</a>';
            $html_genere .= '<input class="button" type="submit" name="submit_liaisons_individus" value="Valider">';
            $html_genere .= '</form>';
        } else {

            if (!empty($liste_individus_b)) {
                # Des liaisons ont été récupérées donc on est dans un contexte de modification
                $liaisons = $this->liaisons['relations'];

                # ID de l'individu A passé dans l'URL
                $id_individu_a = $individu_a['id']['value'];

                $html_genere .= '<div class="msg-liaisons-inter-individus">Veuillez définir les liens de parenté entre ' . $individu_a['first_name']['value'] . ' ' . $individu_a['last_name']['value'] . ' et les autres membres du foyer.</div>';
                $html_genere .= '<form name="form_liaisons_individus" class="relations-inter-ind" method="POST" action="">';
                $html_genere .= '<input type="hidden" name="id_individu_a" id="id_individu_a" value="' . $individu_a['id']['value'] . '">'; // à masquer
                # Pour chaque individu de l'entité courante
                foreach ($liste_individus_b as $key => $individu_b) {

                    $role_courant_a = '';
                    $role_courant_b = '';
                    $id_liaison = '';

                    # Et pour chaque liaison récupérée	
                    foreach ($liaisons as $liaison) {

                        # Si dans le tableau de liaisons, il y a une liaison concernant l'individu courant
                        if ($liaison['individu_id'] == $individu_b['individu_id']) {
                            # Si les rôles ne sont pas vides alors on récupère leurs valeurs
                            if (!empty($liaison['role_1']) && !empty($liaison['role_2'])) {
                                $role_courant_a = $liaison['role_1'];
                                $role_courant_b = $liaison['role_2'];
                            }
                            $id_liaison = $liaison['relation_id'];
                        }
                    }

                    $html_genere .= '<fieldset><legend>Lien de parenté avec ' . $individu_b['individu_name'] . '</legend>';
                    $html_genere .= '<div class="ligne">';
                    $html_genere .= '<div class="label_txt"><span>' . $individu_a['first_name']['value'] . ' ' . $individu_a['last_name']['value'] . '</span> est la ou le </div>';
                    $html_genere .= '<select name="role_a_' . $individu_b['individu_id'] . '">';
                    foreach ($liste_roles as $code_role => $libelle_role) {
                        if ($role_courant_a == $code_role) {
                            $html_genere .= '<option value="' . $code_role . '" selected>' . $libelle_role . '</option>';
                        } else {
                            $html_genere .= '<option value="' . $code_role . '">' . $libelle_role . '</option>';
                        }
                    }

                    $html_genere .= '</select>';


                    $html_genere .= '<div class="label_txt"> de ';
                    $html_genere .= '<span>' . $individu_b['individu_name'] . '</span>';
                    $html_genere .= '<input type="hidden" name="id_individu_b_' . $individu_b['individu_id'] . '" id="id_individu_b_' . $individu_b['individu_id'] . '" value="' . $individu_b['individu_id'] . '">'; // à masquer
                    $html_genere .= '<input type="hidden" name="id_liaison_avec_' . $individu_b['individu_id'] . '" id="id_liaison_avec_' . $individu_b['individu_id'] . '" value="' . $id_liaison . '">'; // à masquer
                    $html_genere .= ' qui est sa ou son </div>';
                    $html_genere .= '<select name="role_b_' . $individu_b['individu_id'] . '">';
                    foreach ($liste_roles as $code_role => $libelle_role) {
                        if ($role_courant_b == $code_role) {
                            $html_genere .= '<option value="' . $code_role . '" selected>' . $libelle_role . '</option>';
                        } else {
                            $html_genere .= '<option value="' . $code_role . '">' . $libelle_role . '</option>';
                        }
                    }

                    $html_genere .= '</select>';
                    $html_genere .= '</fieldset>';
                }
                $type_et_id = do_shortcode('[type_et_id]');
                $url_retour = get_permalink(wp_get_post_parent_id(get_the_ID())) . '?' . $type_et_id;
                $html_genere .= '<a href="' . $url_retour . '" class="button btn-secondaire" >Annuler</a>';
                $html_genere .= '<input class="button" type="submit" name="submit_liaisons_individus" value="Valider">';
                $html_genere .= '</form>';
            } else {
                $url = get_permalink(wp_get_post_parent_id(get_the_ID())) . '?type_entite=' . $_GET['type_entite'] . '&id_entite=' . $_GET['id_entite'];
                $html_genere = '<h4>Individu modifié, veuillez patienter...</h4>';
                $html_genere .= '<script>';
                $html_genere .= 'setTimeout ( function() {
                                        document.location="' . $url . '"; }, 1000);'; # équivalent sleep(5) en JS
                $html_genere .= '</script>';
            }
        }

        return $html_genere;
    }

}
