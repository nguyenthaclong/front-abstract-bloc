<?php

require_once __DIR__ . "/../../templates/front/document/gestion-page-gru-document-create.php";
require_once __DIR__ . "/../../templates/front/document/gestion-page-gru-document-details.php";
require_once __DIR__ . "/../../templates/front/document/gestion-page-gru-document-list.php";
require_once __DIR__ . "/../../templates/front/document/gestion-page-gru-safety-box-infos.php";

function cde_coffre_fort_infos() {

    $html_genere = "";
    if (!is_user_logged_in()) {
        Admin_Gestion_Page_GRU_Tools::logout_and_redirect();
    } else {
        $context = Admin_Gestion_Page_GRU_Tools::get_context();
        $id_entite = (isset($context['id_entite'])) ? $context['id_entite'] : '';
        $type_entite = (isset($context['type_entite'])) ? $context['type_entite'] : '';

        $api_document = new Gestion_Page_GRU_Api_Document(array(
            'parent_id' => $id_entite,
            'parent_type' => $type_entite
        ));
        $infos_coffre = $api_document->get_infos_portedocs();

        if (isset($infos_coffre->data->parent_id)) {
            $infos_coffre_html = new infos_coffre_HTML($infos_coffre);
            $html_genere = $infos_coffre_html->genere_HTML();
            $html_genere .= "<div><p>Porte Document</p></div>";
        } else {
            $html_genere = '<div class="msg-erreur">Impossible de récupérer les informations du coffre</div>';
        }
    }

    return $html_genere;
}

function cde_coffre_fort_erreur_download() {

    $msgCodeErreur = '<div class="msg-erreur">Une erreur est survenue lors du téléchargement, vous allez être redirigé pour vous reconnecter...</div>';
    wp_logout();
    $link_options = get_option(Admin_Gestion_Page_GRU_Tools::links_pages_option);
    $link_connexion = get_page_uri($link_options['crm_link_connexion']);
    $msgCodeErreur .= '<script>';
    $msgCodeErreur .= 'setTimeout ( function() {
		document.location="' . $link_connexion . '"; }, 3000);';
    $msgCodeErreur .= '</script>';
    return $msgCodeErreur;
}

function cde_coffre_fort_document_details() {
    $context = Admin_Gestion_Page_GRU_Tools::get_context();
    $doc_id = (isset($context['doc_id'])) ? $context['doc_id'] : '';
    $api_document = new Gestion_Page_GRU_Api_Document(array(
        'doc_id' => $doc_id,
    ));
    $document_details = $api_document->get_infos_document();

    if (isset($document_details->data->doc_id)) {
        $document_details_html = new document_details_HTML($document_details);
        $html_genere = $document_details_html->genere_HTML();
    } else {
        $html_genere = '<div class="msg-erreur">Impossible de visualiser ce document</div>';
    }
    return $html_genere;
}

function cde_entite_liste_documents() {

    $html_genere = "";
    if (!is_user_logged_in()) {
        Admin_Gestion_Page_GRU_Tools::logout_and_redirect();
    } else {
        $context = Admin_Gestion_Page_GRU_Tools::get_context();
        $user_wp = wp_get_current_user();
        $id_individu = get_user_meta($user_wp->data->ID, 'gru_id')[0];
        $id_entite = (isset($context['id_entite'])) ? $context['id_entite'] : $id_individu;
        $type_entite = (isset($context['type_entite'])) ? $context['type_entite'] : 'CDE_INDIVIDUS';

        $api_document = new Gestion_Page_GRU_Api_Document(array(
            'parent_id' => $id_entite,
            'parent_type' => $type_entite
        ));

        $document_list = $api_document->get_list_documents();
//        echo "<pre>" . print_r($api_document, true) . "</pre>";
//        echo "<pre>" . print_r($document_list, true) . "</pre>";
//        die("fsefe");
        if (isset($document_list->data->parent_id)) {
            if (isset($document_list->data->documents)) {
                $tab_documents = array();
                foreach ($document_list->data->documents as $key => $document) {
                    $api_document->set_datas(array("doc_id" => $document_list->data->documents->id));
                    $fichier_document = $api_document->get_infos_document();
                    $tab_documents[] = $fichier_document;
                }
                $documents_liste_html = new documents_liste_HTML($document_list->data->documents, $type_entite, $tab_documents);
                $html_genere = $documents_liste_html->genere_HTML();
            } else {
                $html_genere = '<div class="msg-erreur">Actuellement votre porte-documents sécurisé ne contient aucune pièce à afficher.</div>';
                return $html_genere;
            }
        } else {
            $html_genere = '<div class="msg-erreur">Impossible de récupérer la liste des pièces du porte-documents.</div>';
        }
    }
    return $html_genere;
}

function cde_entite_ajouter_document() {

    if (!is_user_logged_in()) {
        Admin_Gestion_Page_GRU_Tools::logout_and_redirect();
    } else {
        $context = Admin_Gestion_Page_GRU_Tools::get_context();
        $user_wp = wp_get_current_user();
        $id_individu = get_user_meta($user_wp->data->ID, 'gru_id')[0];
        $doc_id = (isset($context['doc_id'])) ? $context['doc_id'] : '';
        $id_entite = (isset($context['id_entite'])) ? $context['id_entite'] : $id_individu;
        $type_entite = (isset($context['type_entite'])) ? $context['type_entite'] : 'CDE_INDIVIDUS';
        $file = (isset($context['file'])) ? $context['file'] : '';

        $api_document = new Gestion_Page_GRU_Api_Document(array(
            'doc_id' => $doc_id,
            'parent_id' => $id_entite,
            'parent_type' => $type_entite,
            'file' => $file,
        ));

        if (isset($_POST)) { // le formulaire a été validé
            $add_document = $api_document->create_document();
            if (isset($add_document->data->id)) {
                $html_genere = '<div class="msg-succes">Document ajouté au porte-documents sécurisé</div>';
                $liste_types_piece = $api_document->get_list_type_pieces();
                if (isset($liste_types_piece->data->id)) {
                    $document_ajouter_html = new document_ajouter_HTML($liste_types_piece);
                    $html_genere .= $document_ajouter_html->genere_HTML();
                } else {
                    $html_genere = '<div class="msg-erreur">Impossible d\'ajouter un document car aucun type pièce n\'est paramétré dans le module GRU pour ce type de porte-documents sécurisé.</div>';
                }
            } else {
                $html_genere = '<div class="msg-erreur">Impossible d\'afficher la liste des pièces du porte-documents sécurisé.</div>';
            }
        } else { // affichage du formulaire d'upload
            $infos_coffre = $api_document->get_infos_portedocs();
            $liste_types_piece = $api_document->get_list_type_pieces();
            if (isset($liste_types_piece->data->id)) {
                $document_ajouter_html = new document_ajouter_HTML($liste_types_piece, $infos_coffre);
                $html_genere = $document_ajouter_html->genere_HTML();
            } else {
                $html_genere = '<div class="msg-erreur">Erreur : type de piece justificative incorrect ou inexistante</div>';
            }
        }
    }

    return $html_genere;
}
