<?php

require_once __DIR__ . "/../../templates/front/message/gestion-page-gru-message-display.php";
require_once __DIR__ . "/../../templates/front/message/gestion-page-gru-message-display-list-non-lu.php";
require_once __DIR__ . "/../../templates/front/message/gestion-page-gru-message-display-list-thread.php";


/**
 * Retourne la liste des fils de discussion pour une entité
 * @param $params
 * @return string
 */
function cde_entite_liste_fils_discussion($params)
{
    if (!is_user_logged_in()) {
        Admin_Gestion_Page_GRU_Tools::logout_and_redirect();
    }
    else {
        $context = Admin_Gestion_Page_GRU_Tools::get_context();
        $rest_api_request = new Gestion_Page_GRU_Api_Message(['id_entite' => (isset($context['id_entite'])) ? $context['id_entite'] : "", 'type_entite' => $context['type_entite']]);

        $request_list = $rest_api_request->get_thread_messages_list();
        if (false === $request_list) {
            Admin_Gestion_Page_GRU_Tools::set_gru_msg("Paramètres de contexte invalides", "error");
            return '';
        }

        if (isset($request_list->data)) {
            $liste_demandes_html = new message_display_list_thread_HTML($request_list->data, $params);
            return $liste_demandes_html->genere_HTML();
        }
        else {
            Admin_Gestion_Page_GRU_Tools::set_gru_msg("Impossible de récupérer les fils de discussion", "error");
            return '';
        }
    }
}

/**
 * Retourne la liste des messages pour une entité
 * @param $params
 * @return string
 */
function cde_entite_liste_messages_recents($params)
{
    if (!is_user_logged_in()) {
        Admin_Gestion_Page_GRU_Tools::logout_and_redirect();
    }
    else {
        $context = Admin_Gestion_Page_GRU_Tools::get_context();
        $rest_api_request = new Gestion_Page_GRU_Api_Message(['id_entite' => (isset($context['id_entite'])) ? $context['id_entite'] : "", 'type_entite' => $context['type_entite']]);

        $request_list = $rest_api_request->get_last_messages_list();
        if (false === $request_list) {
            Admin_Gestion_Page_GRU_Tools::set_gru_msg("Paramètres de contexte invalides", "error");
            return '';
        }

        if (isset($request_list->data)) {
            $liste_demandes_html = new message_display_list_non_lu_HTML($request_list->data, $params);
            return $liste_demandes_html->genere_HTML();
        }
        else {
            Admin_Gestion_Page_GRU_Tools::set_gru_msg("Impossible de récupérer les messages", "error");
            return '';
        }
    }
}

function cde_entite_visualiser_messages_demande($params)
{
    if (!is_user_logged_in()) {
        Admin_Gestion_Page_GRU_Tools::logout_and_redirect();
    }
    else
    {
        $link_options = get_option(Admin_Gestion_Page_GRU_Tools::links_pages_option);
        $display_url = get_page_link($link_options['crm_link_show_request_chat']);

        $context = Admin_Gestion_Page_GRU_Tools::get_context();
        $paramsContext = ['id_entite' => (isset($context['id_entite'])) ? $context['id_entite'] : "",
            'type_entite' => $context['type_entite'],
            'parent_id' => $context['id_demande']];
        $rest_api_request = new Gestion_Page_GRU_Api_Message($paramsContext);

        /**
         * Manage message sent
         */
        if (isset($_POST['reponse'])) {

            if (isset($_FILES) && $_FILES['piece_jointe']['tmp_name'] != '') {
                $upload_file_name   = $_FILES['piece_jointe']['name'];
                $upload_file_type   = $_FILES['piece_jointe']['type'];
                $upload_file_base64 = base64_encode(file_get_contents($_FILES['piece_jointe']['tmp_name']));
            } else {
                $upload_file_name   = "";
                $upload_file_type   = "";
                $upload_file_base64 = "";
            }

            $data['upload_file_name'] = $upload_file_name;
            $data['upload_file_type'] = $upload_file_type;
            $data['upload_file_base64'] = $upload_file_base64;
            $data['message'] = sanitize_textarea_field($_POST['reponse']);
            $data['parent_id'] = $_POST['id_demande'];
            $data['parent_name'] = $_POST['nom_demande'];

            $rest_api_request->set_datas($data);
            $sent = $rest_api_request->envoyer_message();
            if (false === $sent) {
                error_log("cde_entite_visualiser_messages_demande:: envoyer_message() failed. Context issue : context = ".var_export($context, true)." | data = " . var_export($data, true));
                Admin_Gestion_Page_GRU_Tools::set_gru_msg("Envoi de message a échoué", "error");
                return '';
            }

            Admin_Gestion_Page_GRU_Tools::set_context($context);
            Admin_Gestion_Page_GRU_Tools::set_gru_msg("Votre message a bien été envoyé.", "updated");
            return '<script>setTimeout ( function() { document.location="' . $display_url. '" }, 2000) </script>';
        }


        /**
         * show messages thread
         */
        $request_list = $rest_api_request->get_messages_demand();
        if (false === $request_list) {
            error_log("cde_entite_visualiser_messages_demande:: get_messages_demand() failed. Context issue : context = ".var_export($context, true)." | paramsContext = " . var_export($paramsContext, true));
            Admin_Gestion_Page_GRU_Tools::set_gru_msg("Paramètres de contexte invalides", "error");
            return '';
        }

        if (isset($request_list->data)) {
            $liste_demandes_html = new message_display_messages_demande_HTML($request_list->data, $params);
            return $liste_demandes_html->genere_HTML();
        }
        else {
            Admin_Gestion_Page_GRU_Tools::set_gru_msg("Impossible de récupérer les messages de la demande", "error");
            return '';
        }
    }
}