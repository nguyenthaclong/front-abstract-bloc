<?php

require_once __DIR__ . "/../../templates/front/request/gestion-page-gru-request-display.php";
require_once __DIR__ . "/../../templates/front/request/gestion-page-gru-request-display-list.php";
require_once __DIR__ . "/../../templates/front/request/gestion-page-gru-request-form-step-display.php";

function cde_entite_creer_demande() {
    $html_genere = "";
    if (!is_user_logged_in()) {
        Admin_Gestion_Page_GRU_Tools::logout_and_redirect();
    } else {
        $current_user = wp_get_current_user();
        $context = Admin_Gestion_Page_GRU_Tools::get_context();
        $id_demande = (isset($context['id_demande'])) ? $context['id_demande'] : '';
        $id_etape = (isset($context['id_etape'])) ? $context['id_etape'] : '';
        $id_entite = (isset($context['id_entite'])) ? $context['id_entite'] : '';
        $type_entite = (isset($context['type_entite'])) ? $context['type_entite'] : '';
        $id_service = (isset($context['id_service'])) ? $context['id_service'] : '';
        $beneficiaire_id = get_user_meta($current_user->data->ID, 'gru_id')[0];
        if ((isset($_POST['id_beneficiaire']))) {
            $context['id_beneficiaire'] = $_POST['id_beneficiaire'];
            Admin_Gestion_Page_GRU_Tools::set_context($context);
        }

        if (isset($context['id_beneficiaire'])) {
            $beneficiaire_id = $context['id_beneficiaire'];
        }

        $rest_api_request = new Gestion_Page_GRU_Api_Request(array(
            'id_demande' => $id_demande,
            'service_id' => $id_service,
            'entite_id' => $id_entite,
            'entite_type' => $type_entite,
            'beneficiaire_id' => $beneficiaire_id
        ));

        if (isset($context['type_beneficiaire']) && $context['type_beneficiaire'] != "aucun") {
            $check_access = $rest_api_request->check_access_service();
            if (!isset($check_access->data->id)) {
//                if (isset($check_access->errors)) {
//                    switch ($check_access->errors->status) {
//                        case '401':
//                            Admin_Gestion_Page_GRU_Tools::set_gru_msg($check_access->errors->details, "error");
//                            return false;
//                        break;
//                        case '404':
//                            Admin_Gestion_Page_GRU_Tools::set_gru_msg($check_access->errors->details, "error");
//                            return false;
//                        break;
//                    }
//                }
                // TODO Rajouter la gestion d'erreurs cf doc API -> saisonnalité
                Admin_Gestion_Page_GRU_Tools::set_gru_msg("Vous n'avez pas accès à ce type de demande", "error");
                return false;
            }
        }

        # On vérifie qu'on dispose de toutes les informations nécessaires
        if (( '' == $id_entite && '' == $type_entite && '' == $beneficiaire_id && '' == $id_service)) {
            Admin_Gestion_Page_GRU_Tools::set_gru_msg("Toutes les informations nécessaires n'ont pas été initialisées correctement", "error");
            return false;
        }


        if (isset($context['type_beneficiaire']) && $context['type_beneficiaire'] == "individu" && empty($context['id_beneficiaire'])) {
            $site_url = site_url();
            if (isset($context['retour_link'])) {
                $referrer_location = $context['retour_link'];
            } else {
                $referrer_location = '';
            }
            $html_genere .= '<script>';
            $html_genere .= 'function attente_chargement () {
                                $.grufront.add_to_context({id_beneficiaire: $("#select_beneficaire").val()}, function() { location.reload(); });
                                jQuery ( "#btn_submit_beneficiaire" ).append ( "<img id=\"img_chargement\" alt=\"GIF chargement\" src=\"' . $site_url . '/wp-includes/js/tinymce/skins/lightgray/img/loader.gif\" srcset=\"' . $site_url . '/wp-includes/js/tinymce/skins/lightgray/img/loader.gif\" />" );
                                jQuery ( "#img_chargement" ).after("<span style=\"margin-left:1%; font-weight:bold;\">Veuillez patienter pendant la création de la demande...</span>");
                            }';
            $html_genere .= '</script>';
            $html_genere .=<<<HTML
            <h4 class="return_link">
                <span class="gru-icon-button" onclick="$.grufront.history_back('$referrer_location')">
                <i class="fas fa-long-arrow-alt-left"></i>&nbsp;&nbsp;  Retour</span>
            </h4><br/>
            <H3>Veuillez choisir le bénéficiaire de cette demande </H3>
HTML;
            $rest_api_entity = new Gestion_Page_GRU_Api_Entity(array('id_entite' => $rest_api_request->get_entite_id(), 'type_entite' => $rest_api_request->get_entite_type()));
            $liste_beneficiaires = $rest_api_entity->get_entity_beneficiary_list($id_service);
            if (isset($liste_beneficiaires->data->beneficiaires)) {

                if (!empty($liste_beneficiaires->texte_beneficiaire)) {
                    $html_genere .= '<div>' . htmlspecialchars_decode($liste_beneficiaires->texte_beneficiaire) . '</div>';
                }
                $nb_beneficiaires = count($liste_beneficiaires->data->beneficiaires);

                if (1 <= $nb_beneficiaires) {

                    $html_genere .= '<form action="" method="POST" id="choix_beneficiaire">';
                    $html_genere .= '<select id="select_beneficaire" name="id_beneficiaire">';

                    foreach ($liste_beneficiaires->data->beneficiaires as $individu) {
                        $date_naissance = empty($individu->date_naissance) ? '' : $individu->date_naissance;
                        $html_genere .= '<option value="' . $individu->individu_id . '">' . $individu->civilite . ' ' . $individu->nom_usage . ' ' . $individu->prenom . ' - Né(e) le ' . $date_naissance . '</option>';
                    }

                    $html_genere .= '</select>';
                    $url_retour = $referrer_location;
                    $html_genere .= '<div id="btn_submit_beneficiaire">';
                    $html_genere .= '<a href="' . $url_retour . '" class="button btn-secondaire">Annuler</a>';
                    $html_genere .= '<input type="button" name="btn_submit_beneficiaire" value="Valider" class="button" onclick="attente_chargement();"></div>';
                    $html_genere .= '</form>';
                } else {
                    Admin_Gestion_Page_GRU_Tools::set_gru_msg("Aucun bénéficiaire correspondant aux critères de ce téléservice", "error");
                    if (isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER'])) {
                        $page_services = $_SERVER['HTTP_REFERER'];
                    }
                    $html_genere .= '<a href="' . $page_services . '">Revenir à la liste des services</a>';
                }
            } else {
                Admin_Gestion_Page_GRU_Tools::set_gru_msg("Impossible de récupérer la liste des bénéficiaires", "error");
            }

            return $html_genere;
        }

        if ($id_demande == '') {
            $create_demande = $rest_api_request->create_demande();
            if (isset($create_demande->data->id_demande)) {
                $id_demande = $create_demande->data->id_demande;
                $context['id_demande'] = $id_demande;
                Admin_Gestion_Page_GRU_Tools::set_context($context);
                $rest_api_request->set_datas($create_demande->data);
            } else {
                Admin_Gestion_Page_GRU_Tools::set_gru_msg("Impossible d'initialiser la demande", "error");
                return false;
            }
        }

        if ('1' == $_REQUEST['save']) {
            $custom_form_params = array(
                'champs' => $_REQUEST,
                'fichiers' => $_FILES
            );
            $update_request = $rest_api_request->update_step_demande($id_etape, $custom_form_params);
            if (isset($update_request->data->demande_id)) {
                $current_step = $rest_api_request->get_current_step_demande($id_etape, $_POST['cde_sens'], $_POST['cde_etape_num']);
                Admin_Gestion_Page_GRU_Tools::load_custom_js($current_step->data->include_js->include_js);
                $context['id_etape'] = $current_step->data->id_etape;
                Admin_Gestion_Page_GRU_Tools::set_context($context);
                if (isset($current_step->data->id_etape)) {

                    switch ($current_step->data->nom_etape) {
                        case '__FIN__':
                            Admin_Gestion_Page_GRU_Tools::set_gru_msg("Votre demande est prête à être transmise. Veuillez vérifier le récapitulatif !", "updated");
                            $link_options = get_option(Admin_Gestion_Page_GRU_Tools::links_pages_option);
                            $html_genere .= '<script> setTimeout ( function() { document.location="' . get_page_link($link_options['crm_link_show_request']) . '"; }, 3000);</script>';
                            return $html_genere;
                        default:
                            $form_demande_html = new request_form_display_step_HTML($current_step, 'creation');
                            $html_genere = $form_demande_html->genere_HTML();
                            break;
                    }

                } else {
                    Admin_Gestion_Page_GRU_Tools::set_gru_msg("Impossible de générer le formulaire de la demande", "error");
                }
            } else {
                Admin_Gestion_Page_GRU_Tools::set_gru_msg("Erreur lors de la sauvegarde de l'étape", "error");
            }
        } else {
            # Pas d'id_etape donc première étape
            $first_step = $rest_api_request->get_first_step_demande();
            Admin_Gestion_Page_GRU_Tools::load_custom_js($first_step->data->include_js->include_js);
            $rest_api_request->set_datas($first_step->data);

            if (isset($first_step->data->id_etape)) {
                $context['id_etape'] = $first_step->data->id_etape;
                Admin_Gestion_Page_GRU_Tools::set_context($context);

                $form_demande_html = new request_form_display_step_HTML($first_step, 'creation');
                $html_genere = $form_demande_html->genere_HTML();
            } else {
                Admin_Gestion_Page_GRU_Tools::set_gru_msg("Impossible de générer le formulaire de la demande", "error");
            }
        }
    }
    return $html_genere;
}

function cde_entite_visualiser_demande() {
    if (!is_user_logged_in()) {
        Admin_Gestion_Page_GRU_Tools::logout_and_redirect();
    } else {
        $html_genere = '';
        $context = Admin_Gestion_Page_GRU_Tools::get_context();
        $id_demande = $context['id_demande'];
        if ('' != $id_demande) {

            $rest_api_request = new Gestion_Page_GRU_Api_Request($context);

            # manage transmit demand
            if ('Transmit' == $_REQUEST['action']) {
                $transmit_request = $rest_api_request->transmit_demande();
                if (isset($transmit_request->data->demande_id) && ($transmit_request->data->status === '__TRANSMITTED__')) {
                    Admin_Gestion_Page_GRU_Tools::set_gru_msg("Votre demande a été transmise !", "updated");
                    $link_options = get_option(Admin_Gestion_Page_GRU_Tools::links_pages_option);
                    $html_genere .= '<script> setTimeout ( function() { document.location="' . get_page_link($link_options['crm_link_my_account']) . '"; }, 3000);</script>';
                    return $html_genere;
                } else {
                    Admin_Gestion_Page_GRU_Tools::set_gru_msg("Transmission de la demande a échouée.", "error");
                }
            }

            $visualiser_demande = $rest_api_request->get_demande();
            if (is_object($visualiser_demande)) {
                $visualiser_demande_html = new request_display_HTML($visualiser_demande->data);
                $html_genere = $visualiser_demande_html->genere_HTML();
            }
        } else {
            Admin_Gestion_Page_GRU_Tools::set_gru_msg("Impossible d'identifier la demande à afficher.", "error");
        }
    }

    return $html_genere;
}

function cde_entite_modifier_demande() {

    if (!is_user_logged_in()) {
        Admin_Gestion_Page_GRU_Tools::logout_and_redirect();
    } else {
        $context = Admin_Gestion_Page_GRU_Tools::get_context();
        $id_etape = (isset($context['id_etape'])) ? $context['id_etape'] : '';
        $html_genere = '';
        if (isset($context['id_demande']) && '' != $context['id_demande']) {
            // Récupération des infos de la demande à modifier
            $rest_api_request = new Gestion_Page_GRU_Api_Request($context);
            $update_request = $rest_api_request->get_demande();

            if ('1' == $_REQUEST['save']) {
                $custom_form_params = array(
                    'champs' => $_REQUEST,
                    'fichiers' => $_FILES
                );
                $update_request = $rest_api_request->update_step_demande($id_etape, $custom_form_params);

                if (isset($update_request->data->demande_id)) {
                    $current_step = $rest_api_request->get_current_step_demande($id_etape, $_POST['cde_sens'], $_POST['cde_etape_num']);
                    Admin_Gestion_Page_GRU_Tools::load_custom_js($current_step->data->include_js->include_js);
                    $context['id_etape'] = $current_step->data->id_etape;
                    Admin_Gestion_Page_GRU_Tools::set_context($context);

                    if (isset($current_step->data->id_etape)) {

                        switch ($current_step->data->nom_etape) {
                            case '__FIN__':
                                Admin_Gestion_Page_GRU_Tools::set_gru_msg("Votre demande est prête à être transmise. Veuillez vérifier le récapitulatif !", "updated");
                                $link_options = get_option(Admin_Gestion_Page_GRU_Tools::links_pages_option);
                                $html_genere .= '<script> setTimeout ( function() { document.location="' . get_page_link($link_options['crm_link_show_request']) . '"; }, 3000);</script>';
                                return $html_genere;
                                break;
                            default:
                                $form_demande_html = new request_form_display_step_HTML($current_step, 'creation');
                                $html_genere = $form_demande_html->genere_HTML();
                                break;
                        }

                    } else {
                        Admin_Gestion_Page_GRU_Tools::set_gru_msg("Impossible de générer le formulaire de la demande", "error");
                    }
                } else {
                    Admin_Gestion_Page_GRU_Tools::set_gru_msg("Erreur lors de la sauvegarde de l'étape", "error");
                }
            } else {
                $first_step = $rest_api_request->get_first_step_demande();
                if (isset($first_step->data->id_etape)) {
                    $context['id_etape'] = $first_step->data->id_etape;
                    Admin_Gestion_Page_GRU_Tools::set_context($context);
                    Admin_Gestion_Page_GRU_Tools::load_custom_js($first_step->data->include_js->include_js);
                    $form_demande_html = new request_form_display_step_HTML($first_step, 'creation');
                    $html_genere = $form_demande_html->genere_HTML();
                } else {
                    Admin_Gestion_Page_GRU_Tools::set_gru_msg("Impossible de générer le formulaire de la demande", "error");
                }
            }
        } else {
            Admin_Gestion_Page_GRU_Tools::set_gru_msg("Toutes les informations nécessaires n'ont pas été initialisées correctement", "error");
            $html_genere = "";
        }
    }

    return $html_genere;
}

function cde_entite_liste_demandes($params) {
    if (!is_user_logged_in()) {
        Admin_Gestion_Page_GRU_Tools::logout_and_redirect();
    } else {
        $context = Admin_Gestion_Page_GRU_Tools::get_context();
        $rest_api_request = new Gestion_Page_GRU_Api_Request(array('entite_id' => (isset($context['id_entite'])) ? $context['id_entite'] : "", 'entite_type' => $params['type_entite']));

        $request_list = $rest_api_request->get_demandes();
        if (isset($request_list->data)) {
            $liste_demandes_html = new request_display_list_HTML($request_list->data, $params);
            $html_genere .= $liste_demandes_html->genere_HTML();
        } else {
            Admin_Gestion_Page_GRU_Tools::set_gru_msg("Impossible de récupérer les demandes", "error");
            $html_genere = '';
        }
    }

    return $html_genere;
}
