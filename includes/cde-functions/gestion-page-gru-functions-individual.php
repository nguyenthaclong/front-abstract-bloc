<?php

require_once __DIR__ . "/../../templates/front/individual/gestion-page-gru-individual-create.php";
require_once __DIR__ . "/../../templates/front/individual/gestion-page-gru-individual-display.php";
require_once __DIR__ . "/../../templates/front/individual/gestion-page-gru-individual-list-display.php";

function cde_individu_infos() {
    $html = '';
    if (!is_user_logged_in()) {
        Admin_Gestion_Page_GRU_Tools::logout_and_redirect();
    } else {
        $current_user = wp_get_current_user();

        // si le WP_User a bien été récupéré
        if (is_a($current_user, 'WP_User') && 0 !== $current_user->data->ID) {
            $gru_id = get_user_meta($current_user->data->ID, 'gru_id')[0];
            $rest_api_individual = new Gestion_Page_GRU_Api_Individual(array('individual_id' => $gru_id));
            $current_user_infos = $rest_api_individual->get_individual();

            if (isset($current_user_infos->data->id)) {
                $individuInfosHTML = new individual_display_HTML($current_user_infos->data);
                $html = $individuInfosHTML->genere_HTML();
            } else {
                Admin_Gestion_Page_GRU_Tools::set_gru_msg("Impossible de récupérér les informations de l'individu.", "error");
            }
        } else {
            Admin_Gestion_Page_GRU_Tools::set_gru_msg("Impossible de récupérer l'individu courant.", "error");
        }
    }
    return $html;
}

function cde_entite_liste_individus() {

    $html = '';
    if (!is_user_logged_in()) {
        Admin_Gestion_Page_GRU_Tools::logout_and_redirect();
    } else {

        $context = Admin_Gestion_Page_GRU_Tools::get_context();
        $type_entite = (isset($context['type_entite'])) ? $context['type_entite'] : "";
        $id_entite = (isset($context['id_entite'])) ? $context['id_entite'] : "";
        if ($type_entite != "" && $id_entite != "") {
            $rest_api_individual = new Gestion_Page_GRU_Api_Individual(array('entite_type' => $type_entite, 'entite_id' => $id_entite));
            $individual_list = $rest_api_individual->get_individual_list();
            $individual_list_html = array();
            foreach ($individual_list->data as $individual) {
                $api_individual = new Gestion_Page_GRU_Api_Individual(array('individual_id' => $individual->objet_id));
                $datas = $api_individual->get_individual();
                $individual_list_html[] = $datas->data;
            }
            $entite_liste_individus_html = new individual_list_display_HTML($individual_list_html);
            $html = $entite_liste_individus_html->genere_HTML();
        } else {
            Admin_Gestion_Page_GRU_Tools::set_gru_msg("Impossible de récupérer les individus de l'entité", "error");
        }
    }
    return $html;
}

function cde_entite_creer_individu() {

    $html = '';
    if (!is_user_logged_in()) {
        Admin_Gestion_Page_GRU_Tools::logout_and_redirect();
    } else {

        $context = Admin_Gestion_Page_GRU_Tools::get_context();
        $type_entite = (isset($context['type_entite'])) ? $context['type_entite'] : "";
        $id_entite = (isset($context['id_entite'])) ? $context['id_entite'] : "";
        $rest_api_individual = new Gestion_Page_GRU_Api_Individual(array('entite_type' => $type_entite, 'entite_id' => $id_entite));

        if (isset($_REQUEST['sauver_individu'])) {
            $rest_api_individual->set_datas($_REQUEST);
            $create_individual = $rest_api_individual->create_individual_record();
            if (isset($create_individual->data->id)) {
                $link_options = get_option(Admin_Gestion_Page_GRU_Tools::links_pages_option);
                $link = get_page_link($link_options['crm_link_my_account']);
                Admin_Gestion_Page_GRU_Tools::set_gru_msg("Individu créé, vous allez être redirigé...", "updated");
                $html .= '<script> setTimeout ( function() {  document.location="' . $link . '"; }, 3000); </script>';
            } else {
                Admin_Gestion_Page_GRU_Tools::set_gru_msg("Impossible de créer l'individu.", "error");
            }
            return $html;
        }

        if ('' != $id_entite && '' != $type_entite) {
            $creer_individu_html = new individual_create_HTML($rest_api_individual->get_fields(), $id_entite);
            $html .= $creer_individu_html->genere_HTML();
        } else {
            Admin_Gestion_Page_GRU_Tools::set_gru_msg("CREATION D'UNE ENTITE : toutes les informations nécessaires n'ont pas été initialisées correctement", "error");
        }
    }
    return $html;
}

function cde_entite_modifier_individu() {

    $html = '';
    if (!is_user_logged_in()) {
        Admin_Gestion_Page_GRU_Tools::logout_and_redirect();
    } else {

        $context = Admin_Gestion_Page_GRU_Tools::get_context();
        $rest_api_individual = new Gestion_Page_GRU_Api_Individual(array('individual_id' => $context['individual_id']));        
        if (isset($_REQUEST['sauver_individu'])) {
            $rest_api_individual->set_datas($_REQUEST);
            $create_individual = $rest_api_individual->update_individual_record();        
            if (isset($create_individual->data->id)) {
                Admin_Gestion_Page_GRU_Tools::set_gru_msg("Votre modification a été enregistrée, vous allez être redirigé...", "updated");
                $link_options = get_option(Admin_Gestion_Page_GRU_Tools::links_pages_option);
                $link = get_page_link($link_options['crm_link_my_account']);
                $html .= '<script> setTimeout ( function() {  document.location="' . $link . '"; }, 3000); </script>';
            } else {
                Admin_Gestion_Page_GRU_Tools::set_gru_msg("Impossible de mettre à jour l'individu.", "error");
            }
            
            return $html;            
        }
        
        $individual = $rest_api_individual->get_individual();
        if ($context['individual_id']) {
            $creer_individu_html = new individual_create_HTML($rest_api_individual->get_fields($individual->data), null);
            $html .= $creer_individu_html->genere_HTML();
        } else {
            Admin_Gestion_Page_GRU_Tools::set_gru_msg(">CREATION D'UNE ENTITE : toutes les informations nécessaires n'ont pas été initialisées correctement", "error");
        } 
    }
    return $html;
}