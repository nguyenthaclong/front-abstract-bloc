<?php

require_once __DIR__ . "/../../templates/front/account/gestion-page-gru-account-activation.php";
require_once __DIR__ . "/../../templates/front/account/gestion-page-gru-account-create.php";
require_once __DIR__ . "/../../templates/front/account/gestion-page-gru-account-update.php";
require_once __DIR__ . "/../../templates/front/account/gestion-page-gru-account-update-password.php";
require_once __DIR__ . "/../../templates/front/account/gestion-page-gru-account-validation.php";
require_once __DIR__ . "/../../templates/front/account/gestion-page-gru-account-lost-password.php";

function cde_formulaire_connexion() {
    $html_genere = '';

    $link_options = get_option(Admin_Gestion_Page_GRU_Tools::links_pages_option);
    $html_genere .= '<article class="iconbox iconbox_top main_color"  itemscope="itemscope" itemtype="https://schema.org/CreativeWork" >';
    $html_genere .= '<div class="iconbox_content"><header class="entry-content-header"><div class="iconbox_icon heading-color"><i class="fa fa-user"></i></div>';
    $html_genere .= '<h3 class=\'iconbox_content_title\'  itemprop="headline"  ></h3></header><div class=\'iconbox_content_container \'  itemprop="text"  >';

    if (!is_user_logged_in()) {
        $html_genere .= '<h3>J\'ai déjà un compte</h3>';
        $html_genere .= '<p>Les champs marqués d\'un astérisque (*) sont obligatoires</p>';

        if (isset($_GET['login']) && $_GET['login'] == 'failed') {
            Admin_Gestion_Page_GRU_Tools::set_gru_msg("Identifiant ou mot de passe invalide", "error");
        }

        $html_genere .= wp_login_form(array(
            'echo' => false,
            'redirect' => get_page_link($link_options['crm_link_my_account']),
            'form_id' => 'loginform',
            'label_username' => __('Adresse électronique *'),
            'label_password' => __('Mot de passe *'),
            'label_remember' => __('Remember Me'),
            'label_log_in' => __('SE CONNECTER'),
            'id_username' => 'user_login',
            'id_password' => 'user_pass',
            'id_remember' => 'rememberme',
            'id_submit' => 'wp-submit',
            'remember' => 0,
            'value_username' => '',
            'value_remember' => FALSE
        ));

        $html_genere .= '<a class="lien_mdp_oublie" href="' . get_page_uri($link_options['crm_link_forgotten_password']) . '">Mot de passe oublié ?</a>';
        $html_genere .= ' | <a class="lien_creer_compte" href="' . get_page_uri($link_options['crm_link_create_account']) . '">Pas encore de compte ?</a>';
    } else {
        $html_genere .= '<a href="' . get_page_uri($link_options['crm_link_my_account']) . '" target="_self" class="button">VOIR MON COMPTE</a>';
    }

    $html_genere .= '</div></div><footer class="entry-footer"></footer></article>';
    return $html_genere;
}

function cde_formulaire_creation_compte() {

    $html_genere = '';
    if (!is_user_logged_in()) {
        $creer_compte_html = new account_create_HTML ( );
        if ('POST' == $_SERVER['REQUEST_METHOD'] && 'Valider' == $_POST['creation_compte']) { # le formulaire a été validé
            $rest_data = array(
                'code_externe' => 'INTERNET',
                'id_externe' => '0',
            );

            $error_msg = array();
            if (empty($_POST['mentions_legales'])) {
                array_push($error_msg, "Veuillez accepter les mentions légales.");
            }

            # Civilité
            if (!empty($_POST['civilite'])) {
                $rest_data['civilite'] = htmlspecialchars(stripslashes(trim($_POST['civilite'])));
            } else {
                array_push($error_msg, "Veuillez renseigner une civilité");
            }
            # Prénom
            if (!empty($_POST['prenom'])) {
                $rest_data['prenom'] = htmlspecialchars(stripslashes(trim($_POST['prenom'])));
            } else {
                array_push($error_msg, "Veuillez renseigner un prénom");
            }
            # Nom
            if (!empty($_POST['nom_usage'])) {
                $rest_data['nom_usage'] = htmlspecialchars(stripslashes(trim($_POST['nom_usage'])));
            } else {
                array_push($error_msg, "Veuillez renseigner un nom");
            }
            # Date de naissance
            if (!empty($_POST['date_naissance'])) {
                if (1 !== preg_match("/^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$/", $_POST['date_naissance'])) {
                    array_push($error_msg, "La date renseignée ne respecte pas le format attendu.");
                }
                $tab_date = explode('/', $_POST['date_naissance']);
                if (!checkdate($tab_date[1], $tab_date[0], $tab_date[2])) {
                    array_push($error_msg, "La date renseignée n'est pas valide.");
                }

                $rest_data['date_naissance'] = trim($_POST['date_naissance']);
            } else {
                array_push($error_msg, "Veuillez renseigner une date de naissance");
            }
            # Sexe
            if (!empty($_POST['sexe'])) {
                $rest_data['sexe'] = htmlspecialchars(stripslashes(trim($_POST['sexe'])));
            } else {
                array_push($error_msg, "Veuillez renseigner un sexe");
            }

            # Vérification du format et de l'équivalence des adresses mail
            if (!empty($_POST['email'])) {
                $rest_data['email'] = htmlspecialchars(stripslashes(trim($_POST['email'])));
            } else {
                array_push($error_msg, "Veuillez renseigner un email");
            }

            if (!empty($_POST['email2'])) {
                if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === FALSE || filter_var($_POST['email2'], FILTER_VALIDATE_EMAIL) === FALSE) {
                    array_push($error_msg, "Les adresses mail renseignées ne sont pas valides.");
                }

                if ($_POST['email'] !== $_POST['email2']) {
                    array_push($error_msg, "Les adresses mail renseignées ne sont pas identiques");
                }

                $rest_data['email2'] = htmlspecialchars(stripslashes(trim($_POST['email2'])));
            } else {
                array_push($error_msg, "Veuillez renseigner la vadation de email");
            }

            if ($_POST['email'] == $_POST['email2']) {
                $rest_data['email'] = $_POST['email'];
            } else {
                array_push($error_msg, "Les emails ne sont pas identiques");
            }

            # Vérification de l'équivalence de mots de passe...
            if (!empty($_POST['pwd1']) && !empty($_POST['pwd2'])) {
                $password1 = $_POST['pwd1'];
                $password2 = $_POST['pwd2'];
            } else {
                array_push($error_msg, "Veuillez renseigner les mots de passe");
            }

            # et qu'ils contiennent au moins 8 caractères, 1 minuscule, 1 capitale et 1 chiffre
            if (0 == strcmp($password1, $password2)) {
                $regex = '/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/';
                if (!preg_match($regex, $password1)) {
                    array_push($error_msg, "Les mots de passe ne sont pas valides");
                }

                if ($password1 != $password2) {
                    array_push($error_msg, "Les mots de passe renseignés ne sont pas identiques.");
                }
            } else {
                array_push($error_msg, "Les mots de passe ne sont pas identiques");
            }

            # Pas de champ vide
            if (count($error_msg) == 0) {
                if (!empty($_POST['champ_sup_hp'])) {# Si input de honeypot renseigné, message de confirmation fictif affiché sans lancer la création du compte.
                    $html_genere .= '<h3>CRÉER VOTRE COMPTE PERSONNEL</h3>';
                    $html_genere .= '<div>Un mail de confrimation vous a été envoyé à l\'adresse <strong>' . $rest_data['email'] . '</strong>';
                    $html_genere .= ' Si vous ne le recevez pas, merci de vérifier qu’il n’ait pas été classé en spam.</div>';
                } else {
                    $rest_data['mdp'] = $_POST['pwd1'];
                    $rest_api_comptes = new Gestion_Page_GRU_Api_Account($rest_data);
                    $creer_compte = $rest_api_comptes->create_account();
                    if (isset($creer_compte->data->id)) { # La création a fonctionné
                        $html_genere .= '<h3>CRÉER VOTRE COMPTE PERSONNEL</h3>';
                        $html_genere .= '<div>Un mail d\'activation vous a été envoyé à l\'adresse <strong>' . $rest_data['email'] . '</strong>, veuillez ';
                        $html_genere .= 'cliquer sur le lien qu\'il contient afin de pouvoir vous connecter à la plateforme avec votre compte.';
                        $html_genere .= ' Si vous ne le recevez pas, merci de vérifier qu’il n’ait pas été classé en spam.</div>';
                    } else { # La création a rencontré un problème, affichage d'un message d'erreur
                        if (isset($creer_compte->detail) && strpos($creer_compte->detail, 'doublon') !== false) {
                            Admin_Gestion_Page_GRU_Tools::set_gru_msg("La création du compte dans le portail a échoué (doublon potentiel).", "error");
                        } else {
                            Admin_Gestion_Page_GRU_Tools::set_gru_msg("La création du compte a échouée.", "error");
                        }
                        $html_genere .= $creer_compte_html->genere_HTML();
                    }
                }
            } else {
                Admin_Gestion_Page_GRU_Tools::set_gru_msg($error_msg, "error");
                $html_genere .= $creer_compte_html->genere_HTML();
            }
        } else {
            $html_genere .= $creer_compte_html->genere_HTML();
        }
    } else { # l'usager est déjà connecté
        $link_options = get_option(Admin_Gestion_Page_GRU_Tools::links_pages_option);
        $link = get_page_link($link_options['crm_link_my_account']);
        $html_genere = '<div class="msg-erreur">Vous êtes déjà authentifié, veuillez patienter...</div>';
        $html_genere .= '<script>';
        $html_genere .= 'setTimeout ( function() {
                                document.location="' . $link . '"; }, 2000);'; # équivalent sleep(3) en JS
        $html_genere .= '</script>';
    }
    return $html_genere; # contient le HTML à afficher sur la page
}

function cde_activation_compte() {
    $html_genere = '';

    if (!empty($_GET['data'])) { # activation classique qui passe par GRU
        # Traitement des paramètres passés dans l'URL pour les décoder dans un tableau de valeurs
        $data = urldecode(base64_decode($_GET['data']));

        $explode_data = explode('&', $data);
        $infos_compte = array();

        foreach ($explode_data as $k => $value) {
            list ( $k, $v ) = explode('=', $value);
            $infos_compte [$k] = $v;
        }

        # les infos provenant de GRU (passées dans l'URL encodées en base64), dans tous les cas on doit avoir l'adresse mail et l'ID du CDE_INDIVIDUS
        if (!empty($infos_compte['email']) && !empty($infos_compte['id_gru'])) {

            $rest_api_comptes = new Gestion_Page_GRU_Api_Account(array('email' => $infos_compte['email']));
            $activer_compte = $rest_api_comptes->activate_account();

            if (isset($activer_compte->data->id)) {
                $link_options = get_option(Admin_Gestion_Page_GRU_Tools::links_pages_option);
                $link_login = get_page_uri($link_options['crm_link_connexion']);
                Admin_Gestion_Page_GRU_Tools::set_gru_msg("Compte activé, vous allez être redirigé vers la page de connexion...", "updated");
                $html_genere .= '<script> setTimeout ( function() {document.location="' . $link_login . '"; }, 3000); </script>';
            } else {
                Admin_Gestion_Page_GRU_Tools::set_gru_msg("L'activation du compte a échoué.", "error");
            }

        } else {
            Admin_Gestion_Page_GRU_Tools::set_gru_msg("Les paramètres disponibles ne permettent pas de poursuivre l'activation du compte.", "error");
        }
    } else {
        Admin_Gestion_Page_GRU_Tools::set_gru_msg("Aucun paramètre disponible pour poursuivre l'activation du compte.", "error");
    }

    return $html_genere;
}

function cde_formulaire_modification_compte() {

    if (!is_user_logged_in()) {
        Admin_Gestion_Page_GRU_Tools::logout_and_redirect();
    }

    $current_user = wp_get_current_user();
    $id_individu = get_user_meta($current_user->data->ID, 'gru_id')[0];

    $rest_api_comptes = new Gestion_Page_GRU_Api_Account(array('id_gru' => $id_individu));
    $html_genere = '';
    if (!is_user_logged_in()) {
        $html_genere = cde_redirect_to();
    } else {

        if ('POST' == $_SERVER['REQUEST_METHOD'] && 'Valider' == $_POST['modifier_compte']) { # le formulaire a été validé
            $rest_api_comptes->set_datas($_POST);
            $modifier_compte = $rest_api_comptes->update_account();
            if (isset($modifier_compte->data->id)) {
                Admin_Gestion_Page_GRU_Tools::set_gru_msg("Modification du compte effectuée", "updated");
            } else {
                Admin_Gestion_Page_GRU_Tools::set_gru_msg("Erreur lors de la modification", "error");
            }
        }

        $infos_compte = $rest_api_comptes->get_account();
        $fields = array(
            'salutation' => $infos_compte->data->civilite,
            'prenom' => $infos_compte->data->prenom,
            'prenom2' => $infos_compte->data->prenom2,
            'prenom3' => $infos_compte->data->prenom3,
            'nom_usage' => $infos_compte->data->nom_usage,
            'nom_famille' => $infos_compte->data->nom_famille,
            'date_naissance' => $infos_compte->data->date_naissance,
            'situation_familiale' => $infos_compte->data->situation_familiale,
            'mobile' => $infos_compte->data->mobile,
            'email' => $infos_compte->data->email,
        );
        $account_update_HTML = new account_update_HTML($fields);
        $html_genere .= $account_update_HTML->genere_HTML();
    }

    return $html_genere;
}

function cde_modification_mot_de_passe() {

    $html_genere = '';
    $user_context = Admin_Gestion_Page_GRU_Tools::get_user_context();
    if ($user_context->is_authenticated()) {

        if ('POST' == $_SERVER['REQUEST_METHOD'] && 'Valider' == $_POST['modifier_mot_de_passe']) { // traitement du formulaire
            $user = $user_context->get_user();
            $pwd = $_POST['pwd'];
            $new_pwd_1 = $_POST['new-pwd-1'];
            $new_pwd_2 = $_POST['new-pwd-2'];

            if ($user instanceof WP_User) {
                $id_individu = $user_context->get_gru_id();
            } else {
                Admin_Gestion_Page_GRU_Tools::logout_and_redirect();
                return false;
            }

            $rest_api_comptes = new Gestion_Page_GRU_Api_Account(array('id_gru' => $id_individu));
            if (0 == strcmp($new_pwd_1, $new_pwd_2)) {

                $regex = '/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/';

                if (preg_match($regex, $new_pwd_1)) {
                    $maj_mdp = $rest_api_comptes->update_password_account($pwd, $new_pwd_1);
                    if (isset($maj_mdp->data->id)) {
                        Admin_Gestion_Page_GRU_Tools::set_gru_msg("Modification effectuée, vous allez être redirigé vers la page de connexion...", "updated");
                        $link_options = get_option(Admin_Gestion_Page_GRU_Tools::links_pages_option);
                        $link_login = get_page_uri($link_options['crm_link_connexion']);
                        wp_logout();
                        $html_genere .= '<script> setTimeout ( function() { document.location="' . $link_login . '"; }, 4000); </script>';
                    } else {
                        Admin_Gestion_Page_GRU_Tools::set_gru_msg("Une erreur est survenue lors de la modification du mot de passe.", "error");
                        $modifier_mot_de_passe_html = new account_update_password_HTML ( );
                        $html_genere .= $modifier_mot_de_passe_html->genere_HTML();
                    }
                } else {
                    Admin_Gestion_Page_GRU_Tools::set_gru_msg("Le nouveau mot de passe ne respecte pas les règles de sécurité.", "error");
                    $html_genere = '<div class="msg-erreur"></div>';
                    $modifier_mot_de_passe_html = new account_update_password_HTML ( );
                    $html_genere .= $modifier_mot_de_passe_html->genere_HTML();
                }
            } else {
                Admin_Gestion_Page_GRU_Tools::set_gru_msg("Les deux mots de passe ne sont pas identiques.", "error");
                $modifier_mot_de_passe_html = new account_update_password_HTML ( );
                $html_genere .= $modifier_mot_de_passe_html->genere_HTML();
            }
        } else { // affichage du formulaire
            $modifier_mot_de_passe_html = new account_update_password_HTML($_REQUEST);
            $html_genere .= $modifier_mot_de_passe_html->genere_HTML();
        }
    }
    return $html_genere;
}

function cde_mot_de_passe_oublie()
{
    $html = '';
    $user_context = Admin_Gestion_Page_GRU_Tools::get_user_context();
    if (!$user_context->is_authenticated()) {
        # il y a un ID dans l'URL
        if (isset($_GET['data']) && !empty($_GET['data'])) {

        # Traitement des paramètres passés dans l'URL pour les décoder dans un tableau de valeurs	
        $data = urldecode(base64_decode($_GET['data']));
        $explode_data = explode('&', $data);
        $infosCompte  = [];

        foreach ($explode_data as $value) {
            list ( $k, $v ) = explode('=', $value);
            $infosCompte [$k] = $v;
        }

        $dateCourante = new DateTime(null, new DateTimeZone('Europe/Paris'));

        $dateMail = new DateTime($infosCompte['date'], new DateTimeZone('Europe/Paris'));
        $dateMail->add(new DateInterval('P2D')); // le mail pour changer de mot de passe est valide 2 jours

        if ($dateCourante < $dateMail) {

                $individuUUID = $infosCompte['id_gru'];

                if ('POST' == $_SERVER['REQUEST_METHOD'] && 'Valider' == $_POST['modifier_mot_de_passe']) {

                    if (0 == strcmp($_POST['pwd1'], $_POST['pwd2'])) {

                        $regex = '/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/';

                        if (1 === preg_match($regex, $_POST['pwd1'])) {

                            $rest_api_comptes = new Gestion_Page_GRU_Api_Account(array('id_gru' => $individuUUID));
                            $maj_mdp          = $rest_api_comptes->update_password_lost_account($_POST['pwd1']);

                            if (isset($maj_mdp->data->id)) {
                                Admin_Gestion_Page_GRU_Tools::set_gru_msg("Modification effectuée, vous allez être redirigé vers la page de connexion...", "updated");
                                $link_options = get_option(Admin_Gestion_Page_GRU_Tools::links_pages_option);
                                $login_url = get_page_link($link_options['crm_link_connexion']);
                                $html .= '<script> setTimeout ( function() { document.location="' . $login_url . '"; }, 4000); </script>';
                            } else {
                                Admin_Gestion_Page_GRU_Tools::set_gru_msg("Une erreur est survenue lors de la modification du mot de passe.", "error");
                                $mot_de_passe_oublie_html = new account_lost_password_HTML ();
                                $html .= $mot_de_passe_oublie_html->genere_HTML();
                            }
                        } else {
                            Admin_Gestion_Page_GRU_Tools::set_gru_msg("Le mot de passe renseigné ne respecte pas les règles de sécurité.", "error");
                            $mot_de_passe_oublie_html = new account_lost_password_HTML();
                            $html .= $mot_de_passe_oublie_html->genere_HTML();
                        }
                    } else {
                        Admin_Gestion_Page_GRU_Tools::set_gru_msg("Les deux mots de passe ne sont pas identiques.", "error");
                        $mot_de_passe_oublie_html = new account_lost_password_HTML();
                        $html .= $mot_de_passe_oublie_html->genere_HTML();
                    }
                } else {
                    $mot_de_passe_oublie_html = new account_lost_password_HTML();
                    $html .= $mot_de_passe_oublie_html->genere_HTML();
                }
            } else {
                Admin_Gestion_Page_GRU_Tools::set_gru_msg("Ce lien n'est plus valide.", "error");
            }
        } else { // $_GET['data'] n'existe pas donc affichage du formulaire vide

            if ('POST' == $_SERVER['REQUEST_METHOD'] && 'Valider' == $_POST['mot_de_passe_oublie']) {

            if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false) {

                $rest_api_comptes = new Gestion_Page_GRU_Api_Account(array('email' => $_POST['email']));
                $maj_mdp = $rest_api_comptes->lost_password_account();

                    if (isset($maj_mdp->data->id)) {
                        Admin_Gestion_Page_GRU_Tools::set_gru_msg("Un email vous permettant de changer de mot de passe vous a été envoyé. Vous allez être redirigé...", "updated");
                        $link_options = get_option(Admin_Gestion_Page_GRU_Tools::links_pages_option);
                        $login_url = get_page_link($link_options['crm_link_connexion']);
                        $html .= '<script> setTimeout ( function() { document.location="' . $login_url . '"; }, 3000) </script>';
                    } else {
                        Admin_Gestion_Page_GRU_Tools::set_gru_msg("Aucun compte ne correspond à cette adresse mail", "error");
                    }
                } else {
                    Admin_Gestion_Page_GRU_Tools::set_gru_msg("L'adresse email saisie est incorrecte.", "error");
                    $mot_de_passe_oublie_html = new account_lost_password_HTML();
                    $html .= $mot_de_passe_oublie_html->genere_HTML();
                }
            } else {
                $mot_de_passe_oublie_html = new account_lost_password_HTML();
                $html .= $mot_de_passe_oublie_html->genere_HTML();
            }
        }
    }
    return $html;
}

function cde_lien_creation_compte() {

    $html_genere = '';
    if (!is_user_logged_in()) {
        $link_options = get_option(Admin_Gestion_Page_GRU_Tools::links_pages_option);
        $link_account = get_page_uri($link_options['crm_link_create_account']);
        $html_genere .= <<<HTML
            <article class="iconbox iconbox_top main_color"> 
                <div class="iconbox_content">
                    <header class="entry-content-header">
                        <div class="iconbox_icon heading-color">                            
                            <i class="fa fa-user"></i>
                        </div>
                    </header>
                    <div class"iconbox_content_container">
                        <p style="text-align: center;">
                            <h3>Pas encore de compte ?</h3><br>
                            <a href="$link_account" class="button">CRÉER VOTRE COMPTE PERSONNEL</a>
                        </p>
                    </div>
                </div>
            </article>
HTML;
    }

    return $html_genere;
}
