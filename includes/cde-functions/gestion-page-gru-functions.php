<?php


function cde_entite_modifier_liaisons_individus($atts) {

    if (is_user_logged_in()) {

        extract(shortcode_atts(array(
            'type' => '',
                        ), $atts));

        $html_genere = '';
        $user_wp = wp_get_current_user();
        $id_individu = $_GET['id_individu'];
        $id_entite = $_GET['id_entite'];
        $type_entite = $type;
        $sugarcrm_access = new Arno_Sugar_Access();

        if ('POST' == $_SERVER['REQUEST_METHOD'] && 'Valider' == $_POST['submit_liaisons_individus']) { # le formulaire à été validé
            $parametres = array();
            $parametres['id_individu_a'] = $_POST['id_individu_a'];
            foreach ($_POST as $key => $value) {
                if ('id_individu_b_' == substr($key, 0, 14)) {
                    # On ne récupère les valeurs des rôles que les les deux rôles ont été renseignés
                    if (!empty($_POST['role_a_' . $value]) && '_empty_' !== $_POST['role_a_' . $value] && !empty($_POST['role_b_' . $value]) && '_empty_' !== $_POST['role_b_' . $value]) {
                        $parametres['individus_b'][$value] = array(
                            'id_liaison' => $_POST['id_liaison_avec_' . $value],
                            'id_individu_b' => $_POST['id_individu_b_' . $value],
                            'role_a' => $_POST['role_a_' . $value],
                            'role_b' => $_POST['role_b_' . $value],
                        );
                    }
                }
            }
            # S'il y a des liaisons à créer entre l'individu créé et les autres individus de l'entité	
            if (isset($parametres['individus_b'])) {
                $modifier_liaisons = $sugarcrm_access->modifierLiaisonsInterIndividus($parametres);
                if ('0' == $modifier_liaisons->getErrorNb()) {
                    $url = get_permalink(wp_get_post_parent_id(get_the_ID())) . '?type_entite=' . $type_entite . '&id_entite=' . $id_entite;
                    $html_genere = '<h4>Individu modifié, veuillez patienter...</h4>';
                    $html_genere .= '<script>';
                    $html_genere .= 'setTimeout ( function() {
						document.location="' . $url . '"; }, 2000);'; # équivalent sleep(5) en JS
                    $html_genere .= '</script>';
                }
            } else {
                $url = get_permalink(wp_get_post_parent_id(get_the_ID())) . '?type_entite=' . $type_entite . '&id_entite=' . $id_entite;
                $html_genere = '<h4>Individu modifié, veuillez patienter...</h4>';
                $html_genere .= '<script>';
                $html_genere .= 'setTimeout ( function() {
					document.location="' . $url . '"; }, 2000);'; # équivalent sleep(5) en JS
                $html_genere .= '</script>';
            }
        } else {

            # Récupération de TOUTES les liaisons entre l'individu passé en paramètre et n'importe quel autre individu existant dans GRU
            $liaisons_individu = $sugarcrm_access->listeLiaisonsIndividus($id_individu);

            # Récupération de TOUS les individus appartenant à l'entité courante
            $liste_individus_b = $sugarcrm_access->listeIndividusEntite($type_entite, $id_entite);
            if (0 !== $liaisons_individu->getErrorNb() && 0 !== $liste_individus_b->getErrorNb()) {
                $message_erreur = traiterCodeErreur($liaisons_individu->getErrorNb());
                $html_genere = '<div class="msg-erreur">Impossible de récupérer les informations nécessaires.</div>';
                $html_genere .= $message_erreur;
            } else {

                $liaisons_individu = $liaisons_individu->getResultArray();
                $liste_individus_b = $liste_individus_b->getResultArray();

                # Construction d'un tableau qui contient les ID des individus de l'entité	
                $liste_individus = array();
                foreach ($liste_individus_b['relations'] as $k_ind => $individu) {
                    $liste_individus[] = $individu['individu_id'];
                }

                # Pour chaque liaison inter-individu trouvée sur l'individu A
                foreach ($liaisons_individu['relations'] as $key => $liaison) {
                    # Si l'individu B qui se trouve à l'extrémité de la liaison n'appartient pas à l'entité courante 
                    if (!in_array($liaison['individu_id'], $liste_individus)) {
                        # On supprime la relation correspondante dans le tableau des relations
                        unset($liaisons_individu['relations'][$key]);
                    }
                }

                # Récupération des informations de l'individu A (celui passé en paramètre)
                $infos_individu_a = $sugarcrm_access->infosEntite($id_individu, 'CDE_INDIVIDUS');
                $infos_individu_a = $infos_individu_a['entry_list'][0]['name_value_list'];

                # Dans le tableau des individus de l'entité courante, on supprimer l'individu A 
                $liste_individus_b = $liste_individus_b['relations'];
                foreach ($liste_individus_b as $key => $individu_b) {
                    if ($id_individu == $individu_b['individu_id']) {
                        unset($liste_individus_b [$key]);
                    }
                }

                # Récupération des rôles disponibles pour les liens inter-individus
                $liste_roles = $sugarcrm_access->GetAppListes();
                $liste_roles = $liste_roles->getResultArray();
                $liste_roles = $liste_roles['app_list_strings']['role_individu_list'];

                # Contruction du HTML
                $liaisons_individus_html = new liaisons_individus_HTML($infos_individu_a, $liste_individus_b, $liste_roles, $liaisons_individu);
                $html_genere = $liaisons_individus_html->genere_HTML();
            }
        }
    } else {
        $html_genere = '<div class="msg-erreur">Vous devez être authentifié pour accéder à cette page</div>';
    }
    return $html_genere;
}

function cde_entite_creer_liaisons_individus($atts) {

    extract(shortcode_atts(array(
        'type' => '',
                    ), $atts));

    $html_genere = '';
    $user_wp = wp_get_current_user();
    $id_individu = $_GET['id_individu'];
    $id_entite = $_GET['id_entite'];
    $type_entite = $type;

    $sugarcrm_access = new Arno_Sugar_Access();

    if ('POST' == $_SERVER['REQUEST_METHOD'] && 'Valider' == $_POST['submit_liaisons_individus']) { # le formulaire à été validé
        $parametres = array();
        $parametres['id_individu_a'] = $_POST['id_individu_a'];

        foreach ($_POST as $key => $value) {
            if ('id_individu_b_' == substr($key, 0, 14)) {
                # On ne récupère les valeurs des rôles que les les deux rôles ont été renseignés
                if (!empty($_POST['role_a_' . $value]) && '_empty_' !== $_POST['role_a_' . $value] && !empty($_POST['role_b_' . $value]) && '_empty_' !== $_POST['role_b_' . $value]) {
                    $parametres['individus_b'][$value] = array(
                        'id_individu_b' => $_POST['id_individu_b_' . $value],
                        'role_a' => $_POST['role_a_' . $value],
                        'role_b' => $_POST['role_b_' . $value],
                    );
                }
            }
        }

        # S'il y a des liaisons à créer entre l'individu créé et les autres individus de l'entité	
        if (isset($parametres['individus_b'])) {

            $creer_liaisons = $sugarcrm_access->creerLiaisonsInterIndividus($parametres);

            if ('0' == $creer_liaisons->getErrorNb()) {
                $url = get_permalink(wp_get_post_parent_id(get_the_ID())) . '?type_entite=' . $type_entite . '&id_entite=' . $id_entite;
                $html_genere = '<h4>Individu créé, veuillez patienter...</h4>';
                $html_genere .= '<script>';
                $html_genere .= 'setTimeout ( function() {
					document.location="' . $url . '"; }, 2000);'; # équivalent sleep(5) en JS
                $html_genere .= '</script>';
            }
        } else {
            $url = get_permalink(wp_get_post_parent_id(get_the_ID())) . '?type_entite=' . $type_entite . '&id_entite=' . $id_entite;
            $html_genere = '<h4>Individu créé, veuillez patienter...</h4>';
            $html_genere .= '<script>';
            $html_genere .= 'setTimeout ( function() {
					document.location="' . $url . '"; }, 2000);'; # équivalent sleep(5) en JS
            $html_genere .= '</script>';
        }
    } else {
        $filtre = '';

        # Récupération des informations de l'individu qui vient d'être créé
        $infos_individu_a = $sugarcrm_access->infosEntite($id_individu, 'CDE_INDIVIDUS');
        $infos_individu_a = $infos_individu_a['entry_list'][0]['name_value_list'];

        # Récupération des rôles	
        $liste_roles = $sugarcrm_access->GetAppListes();
        $liste_roles = $liste_roles->getResultArray();
        $liste_roles = $liste_roles['app_list_strings']['role_individu_list'];

        # 12-03-2018 : plante à cause de cde_liaisons_objet_individu	
        $liste_individus_b = $sugarcrm_access->listeIndividusEntite($type_entite, $id_entite, $filtre);
        if ('0' == $liste_individus_b->getErrorNb()) {
            $liste_individus_b = $liste_individus_b->getResultArray();
            $liste_individus_b = $liste_individus_b['relations'];
            if (0 < count($liste_individus_b)) {
                foreach ($liste_individus_b as $key => $individu_b) {
                    if ($id_individu == $individu_b['individu_id']) {
                        unset($liste_individus_b [$key]);
                    }
                }
            }

            # Contruction du HTML
            $liaisons_individus_html = new liaisons_individus_HTML($infos_individu_a, $liste_individus_b, $liste_roles);
            $html_genere = $liaisons_individus_html->genere_HTML();
        } else {
            $message_erreur = traiterCodeErreur($liste_individus->getErrorNb());
            $html_genere = '<div class="msg-erreur">Impossible de récupérer les informations du coffre</div>';
            $html_genere .= $message_erreur;
        }
    }
    return $html_genere;
}



