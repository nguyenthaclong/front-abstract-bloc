<?php

require_once __DIR__ . "/../../templates/front/entity/gestion-page-gru-entity-create.php";
require_once __DIR__ . "/../../templates/front/entity/gestion-page-gru-entity-display.php";
require_once __DIR__ . "/../../templates/front/entity/gestion-page-gru-entity-list.php";

function cde_entite_creer($params) {
    $html = '';

    if (!is_user_logged_in()) {
        Admin_Gestion_Page_GRU_Tools::logout_and_redirect();
    } else {
        $link_options = get_option(Admin_Gestion_Page_GRU_Tools::links_pages_option);

        $sauver_entite = false;
        if (isset($_REQUEST['sauver_entite']) && 'sauver' == $_REQUEST['sauver_entite']) {
            $sauver_entite = true;
        }

        if ('' != $params['type_entite'] || '' != $params['page_redirection']) {

            $rest_api_entity = new Gestion_Page_GRU_Api_Entity(array('type_entite' => $params['type_entite']));
            $fields = $rest_api_entity->get_fields_from_type();
            if (true == $sauver_entite) {

                # Le formulaire a été validé
                # Récupération des valeurs du formulaire (uniquement des champs commençant par "cde_"
                # $valeurs contient les valeurs selon la structure standard des "name_value_list" de SugarCRM/SuiteCRM
                $valeurs = array();
                foreach ($_REQUEST as $nomChamps => $valeurChamps) {
                    if ('cde_' == substr($nomChamps, 0, 4)) {
                        $valeurs[] = array(
                            'name' => substr($nomChamps, 4), // on retire le préfix "cde_"
                            'value' => $valeurChamps,
                        );
                    }
                }

                $role = 'membre';
                if ('CDE_FOYERS' == $params['type_entite']) {
                    $role = 'membre';
                } else {
                    $role = $_REQUEST['cde_role'];
                }

                $rest_api_entity->set_datas($_POST);
                $create_entity = $rest_api_entity->create_entite_record();

                if (isset($create_entity->data->id)) {

                    $context = array(
                        'type_entite' => $params['type_entite'],
                        'id_entite' => $create_entity->data->id,
                    );
                    Admin_Gestion_Page_GRU_Tools::set_context($context);
                    Admin_Gestion_Page_GRU_Tools::set_gru_msg("Votre entité a été enregistrée, vous allez être redirigé...", "updated");
                    $html .= '<script>setTimeout ( function() { document.location="' . get_page_link($link_options['crm_link_home_foyer']) . '" }, 3000) </script>';
                } else {
                    Admin_Gestion_Page_GRU_Tools::set_gru_msg("Une erreur est survenue lors de la création.", "error");

                    # Génération du HTML
                    $creer_entite_html = new entity_create_HTML($params, $fields);
                    $html .= $creer_entite_html->genere_HTML();
                }
            } else { # Affichage du formulaire
                # Génération du HTML				
                $creer_entite_html = new entity_create_HTML($params, $fields);
                $html = $creer_entite_html->genere_HTML();
            }
        } else { # On ne sait pas sur quel type d'entité on se trouve
            Admin_Gestion_Page_GRU_Tools::set_gru_msg("CREATION D'UNE ENTITE : Toutes les informations nécessaires n\'ont pas été initialisées correctement, vous allez être redirigé...", "error");
            $html .= '<script> setTimeout ( function() { document.location="' . home_url() . '" }, 3000 )</script>';
        }
    }
    return $html;
}

function cde_entite_liste_instances($params) {
    if (!is_user_logged_in()) {
        Admin_Gestion_Page_GRU_Tools::logout_and_redirect();
    } else {

        $rest_api_entites = new Gestion_Page_GRU_Api_Entity(array('type_entite' => $params['type_entite']));
        $entites = $rest_api_entites->get_entites();
        $html_genere = '';
        if (isset($entites->data)) {
            $liste_instances = $entites->data[0];

            if (1 <= count($liste_instances->relations)) {
                foreach ($liste_instances->relations as $key => $instance) {
                    if ('0' === $instance->droit_connexion) {
                        unset($liste_instances->relations[$key]);
                    }
                }
            }

            $liste_entites_html = new entity_list_HTML($liste_instances, $params);
            $html_genere = $liste_entites_html->genere_HTML();
        } else {
            Admin_Gestion_Page_GRU_Tools::set_gru_msg("Impossible d'accéder au module " . $params['type_entite'], "error");
        }
    }
    return $html_genere;
}

function cde_entite_infos() {

    $html = '';
    if (!is_user_logged_in()) {
        Admin_Gestion_Page_GRU_Tools::logout_and_redirect();
    } else {

        $context = Admin_Gestion_Page_GRU_Tools::get_context();
        $id_entite = $context['id_entite'];
        $type_entite = $context['type_entite'];
        $rest_api_entites = new Gestion_Page_GRU_Api_Entity(array('id_entite' => $id_entite, 'type_entite' => $type_entite));
        $currentUser = wp_get_current_user();
        if ('' != $id_entite && '' != $type_entite && is_a($currentUser, 'WP_User') && 0 !== $currentUser->ID) {
            $entites = $rest_api_entites->get_entite_record();
            if (isset($entites->data->id)) {
                $entite_infos_html = new entity_display_HTML($entites->data);
                $html = $entite_infos_html->genere_HTML();
            } else {
                Admin_Gestion_Page_GRU_Tools::set_gru_msg("Impossible  de récupérér les informations de l'entité.", "error");
            }
        } else {
            Admin_Gestion_Page_GRU_Tools::set_gru_msg("INFOS ENTITES : toutes les informations nécessaire n'ont pas été initialisées correctement", "error");
        }
    }
    return $html;
}

function cde_entite_modifier($params, $fields) {

    $html = '';

    if (!is_user_logged_in()) {
        Admin_Gestion_Page_GRU_Tools::logout_and_redirect();
    } else {

        $entiteUUID = '';
        $entiteType = '';
        $sauver_entite = false;
        if (isset($_REQUEST['sauver_entite']) && 'sauver' == $_REQUEST['sauver_entite']) {
            $sauver_entite = TRUE;
        }


        $context = Admin_Gestion_Page_GRU_Tools::get_context();
        $id_entite = $context['id_entite'];
        $type_entite = $context['type_entite'];
        if ('' != $type_entite) {

            $rest_api_entity = new Gestion_Page_GRU_Api_Entity(array('id_entite' => $id_entite, 'type_entite' => $type_entite));
            $fields = $rest_api_entity->get_fields_from_type();
            $entity = $rest_api_entity->get_entite_record();
            foreach ($fields as $i => $field) {
                if (isset($entity->data->{$field['name']})) {
                    $fields[$i]['valeur'] = $entity->data->{$field['name']};
                }
            }

            if ($sauver_entite) {
                $role = 'membre';
                if ('CDE_FOYERS' == $entitType) {
                    $role = 'membre';
                } else {
                    $role = $_REQUEST['cde_role'];
                }


                $rest_api_entity->set_datas($_POST);
                $update_entity = $rest_api_entity->update_entite_record();
                if (isset($update_entity->data->id)) {
                    $link_options = get_option(Admin_Gestion_Page_GRU_Tools::links_pages_option);
                    Admin_Gestion_Page_GRU_Tools::set_gru_msg("Modification enregistrée, vous allez être redirigé...", "updated");

                    $html .= '<script>setTimeout ( function() { document.location="' . get_page_link($link_options['crm_link_home_foyer']) . '" }, 3000) </script>';
                } else {
                    Admin_Gestion_Page_GRU_Tools::set_gru_msg("Une erreur est survenue lors de la modification.", "error");
                    $entity_create_html = new entity_create_HTML($params, $fields);
                    $html .= $entity_create_html->genere_HTML();
                }
            } else {

                $entity_create_html = new entity_create_HTML($params, $fields);
                $html = $entity_create_html->genere_HTML();
            }
        } else {
            Admin_Gestion_Page_GRU_Tools::set_gru_msg("CREATION D'UNE ENTITE : toutes les informations nécessaires n'ont pas été initialisées correctement", "error");
        }
    }
    return $html;
}
