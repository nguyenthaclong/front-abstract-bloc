<?php

/**
 * 1997-2018 Quadra Informatique
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 *
 *  @author    Quadra Informatique <modules@quadra-informatique.fr>
 *  @copyright 1997-2018 Quadra Informatique
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * 
 *  Gestion_Page_GRU_Api_Account
 */
class Gestion_Page_GRU_Api_Account {
    
    private $rest_api_client;
    private $id_gru = "";
    private $civilite = "";
    private $nom = "";
    private $prenom = "";
    private $prenom1 = "";
    private $prenom2 = "";
    private $prenom3 = "";
    private $nom_usage = "";
    private $nom_famille = "";
    private $email = "";
    private $date_naissance = "";
    private $nom_naissance = "";
    private $mobile = "";
    private $situation_familiale = "";
    private $sexe = "";
    private $cp_naissance = "";
    private $ville_naissance = "";
    private $profession = "";
    private $tel_bureau = "";
    private $mdp = "";

    public function __construct($fields = array()) {
        $this->rest_api_client =  new Gestion_Page_GRU_Api();
        $this->set_datas($fields);
    }
    
    public function get_datas() {
        $datas = array();
        
        if($this->id_gru !== "") {              $datas['id_gru'] = $this->id_gru; }
        if($this->civilite !== "") {            $datas['civilite'] = $this->civilite; }
        if($this->nom !== "") {                 $datas['nom'] = $this->nom; }
        if($this->prenom !== "") {              $datas['prenom'] = $this->prenom; }
        if($this->prenom1 !== "") {             $datas['prenom1'] = $this->prenom1; }
        ($this->prenom2 !== "") ? $datas['prenom2'] = $this->prenom2 : $datas['prenom2'] = "";    
        ($this->prenom3 !== "") ? $datas['prenom3'] = $this->prenom3 : $datas['prenom3'] = "";
        if($this->nom_usage !== "") {           $datas['nom_usage'] = $this->nom_usage; }
        ($this->nom_famille !== "") ? $datas['nom_famille'] = $this->nom_famille : $datas['nom_famille'] = "";
        if($this->email !== "") {               $datas['email'] = $this->email; }
        if($this->date_naissance !== "") {      $datas['date_naissance'] = $this->date_naissance; }
        ($this->nom_naissance !== "") ? $datas['nom_naissance'] = $this->nom_naissance : $datas['nom_naissance'] = "";
        ($this->mobile !== "") ? $datas['mobile'] = $this->mobile : $datas['mobile'] = "";
        ($this->situation_familiale !== "") ? $datas['situation_familiale'] = $this->situation_familiale : $datas['situation_familiale'] = "";
        ($this->cp_naissance !== "") ? $datas['cp_naissance'] = $this->cp_naissance : $datas['cp_naissance'] = "";
        ($this->ville_naissance !== "") ? $datas['ville_naissance'] = $this->ville_naissance : $datas['ville_naissance'] = "";
        ($this->profession !== "") ? $datas['profession'] = $this->profession : $datas['profession'] = "";
        ($this->tel_bureau !== "") ? $datas['tel_bureau'] = $this->tel_bureau : $datas['tel_bureau'] = "";
        if($this->sexe !== "") {                $datas['sexe'] = $this->sexe; }
        if($this->mdp !== "") {                 $datas['mdp'] = $this->mdp; }
        
        return $datas;
    }    
    
    public function set_datas($fields) {
        foreach ($fields as $id => $value) {
            if (isset($this->$id)) {
                $this->$id = $value;
            }
        }
    }
    
    /*
     * Récupère les informations du compte
     * GET
     * 
     * @return array
     */
    public function get_account() {        
        $result = $this->rest_api_client->call("comptes/" . $this->id_gru); 
        return $result;
    }
    
    /**
     * Crée un compte
     * POST
     * 
     * @return array
     */
    public function create_account() {
        $result = $this->rest_api_client->call("comptes","POST", $this->get_datas(), false); 
        return $result;
    }
    
    /**
     * Met à jour un compte
     * PUT
     * 
     * @return array
     */
    public function update_account() {
        //echo '<pre>'.print_r($this->get_datas(),true).'</pre>';die();
        $result = $this->rest_api_client->call("comptes/" . $this->id_gru,"PUT", $this->get_datas()); 
        
        return $result;        
    }
    
    /**
     * Activer un compte
     * POST
     * 
     * @return array
     */
    public function activate_account() {        
        $result = $this->rest_api_client->call("comptes/activer/" . $this->email,"POST"); 
        
        return $result;
    }
    
    /**
     * Active le processus de récupération de mot de passe
     * POST
     * 
     * @return array
     */
    public function lost_password_account() {
        $result = $this->rest_api_client->call("comptes/motdepasse-perdu/" . $this->email,"POST"); 
        
        return $result;
    }
    
    /**
     * Modifie le mot de passe
     * POST
     * 
     * @return array
     */
    public function update_password_account($old_password, $new_password) {
        $result = $this->rest_api_client->call("comptes/motdepasse-modif/" . $this->id_gru, "POST", array('ancien_motdepasse' => $old_password, 'nouveau_motdepasse' => $new_password), true, false);

        return $result;
    }

    /**
     * Enregistre le nouveau mot de passe depuis formulaire mot de passe perdu
     * @param $new_password string
     *
     * @return array
     */
    public function update_password_lost_account($new_password) {
        $result = $this->rest_api_client->call("comptes/motdepasse-perdu-modif/" . $this->id_gru, "POST", array('nouveau_motdepasse' => $new_password), true, false);

        return $result;
    }

    /**
     * Récupère l'id du compte
     * GET
     *
     * @return array
     */
    public function get_account_id() {
        return $this->rest_api_client->call("comptes/get/id/compte");
    }
}
