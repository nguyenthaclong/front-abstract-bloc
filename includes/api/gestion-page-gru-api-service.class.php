<?php

/**
 * 1997-2018 Quadra Informatique
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 *
 *  @author    Quadra Informatique <modules@quadra-informatique.fr>
 *  @copyright 1997-2018 Quadra Informatique
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * 
 *  Gestion_Page_GRU_Api_Service
 */
class Gestion_Page_GRU_Api_Service {

    
    public function __construct($fields = array()) {
        $this->rest_api_client = new Gestion_Page_GRU_Api();
        $this->set_datas($fields);
    }

    public function get_datas() {
        $datas = array();
        return $datas;
    }

    public function set_datas($fields) {
        foreach ($fields as $id => $value) {
            if (isset($this->$id) && $value !== "") {
                $this->$id = $value;
            }
        }
    }
    
    public function get_services_list() {
        $result = $this->rest_api_client->call("services");
        return $result;
    }    
}

