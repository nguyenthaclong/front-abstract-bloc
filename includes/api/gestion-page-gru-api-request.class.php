<?php

/**
 * 1997-2018 Quadra Informatique
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 *
 *  @author    Quadra Informatique <modules@quadra-informatique.fr>
 *  @copyright 1997-2018 Quadra Informatique
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * 
 *  Gestion_Page_GRU_Api_Request
 */
class Gestion_Page_GRU_Api_Request {
    
    private $rest_api_client;
    
    private $id_demande = "";
    private $service_id = "";
    private $beneficiaire_id = "";
    private $entite_id = "";
    private $entite_type = "";
    private $num_demande = "";
    private $nom_demande = "";
    private $nom_service = "";
    private $nom_demandeur = "";
    private $nom_beneficiaire = "";
    private $nom_entite = "";
    private $id_etape = "";
    private $nom_etape = "";
    private $num_etape = "";
    private $nb_etapes = "";
    private $brouillon = "";
    
    
    private $nature = "";
    private $pourcentage_brouillon = "";
    private $pourcentage_avancement = "";
    private $date_creation = "";
    private $date_modification = "";
    private $date_transmission = "";
    private $date_statut = "";
    private $provenance = "";
    private $cloture = "";
    private $statut_id = "";
    private $statut_name = "";
    private $commentaire_usager = "";
    private $action_possible = "";
    private $editable = "";
    
    public function __construct($fields = array()) {
        $this->rest_api_client =  new Gestion_Page_GRU_Api();
        $this->set_datas($fields);
    }
        
    public function get_datas() {
        $datas = array();
        
        if($this->id_demande !== "") {              $datas['id_demande'] = $this->id_demande; }  
        if($this->service_id !== "") {              $datas['service_id'] = $this->service_id; }
        if($this->beneficiaire_id !== "") {         $datas['beneficiaire_id'] = $this->beneficiaire_id; }
        if($this->entite_id !== "") {               $datas['entite_id'] = $this->entite_id; }
        if($this->entite_type !== "") {             $datas['entite_type'] = $this->entite_type; }
        if($this->num_demande !== "") {             $datas['num_demande'] = $this->num_demande; }
        if($this->nom_demande !== "") {             $datas['nom_demande'] = $this->nom_demande; }
        if($this->nom_service !== "") {             $datas['nom_service'] = $this->nom_service; }
        if($this->nom_demandeur !== "") {           $datas['nom_demandeur'] = $this->nom_demandeur; }
        if($this->nom_beneficiaire !== "") {        $datas['nom_beneficiaire'] = $this->nom_beneficiaire; }
        if($this->nom_entite!== "") {               $datas['nom_entite'] = $this->nom_entite; }
        if($this->id_etape !== "") {                $datas['id_etape'] = $this->id_etape; }
        if($this->nom_etape !== "") {               $datas['nom_etape'] = $this->nom_etape; }
        if($this->num_etape!== "") {                $datas['num_etape'] = $this->num_etape; }
        if($this->nb_etapes !== "") {               $datas['nb_etapes'] = $this->nb_etapes; }
        
        if($this->nature !== "") {                  $datas['nature'] = $this->nature; }
        if($this->pourcentage_brouillon !== "") {   $datas['pourcentage_brouillon'] = $this->pourcentage_brouillon; }
        if($this->pourcentage_avancement !== "") {  $datas['pourcentage_avancement'] = $this->pourcentage_avancement; }
        if($this->date_creation !== "") {           $datas['date_creation'] = $this->date_creation; }
        if($this->date_modification !== "") {       $datas['date_modification'] = $this->date_modification; }
        if($this->date_transmission !== "") {       $datas['date_transmission'] = $this->date_transmission; }
        if($this->date_statut !== "") {             $datas['date_statut'] = $this->date_statut; }
        if($this->provenance !== "") {              $datas['provenance'] = $this->provenance; }
        if($this->brouillon !== "") {               $datas['brouillon'] = $this->brouillon; }
        if($this->cloture !== "") {                 $datas['cloture'] = $this->cloture; }
        if($this->statut_id !== "") {               $datas['statut_id'] = $this->statut_id; }
        if($this->statut_name !== "") {             $datas['statut_name'] = $this->statut_name; }
        if($this->commentaire_usager !== "") {      $datas['commentaire_usager'] = $this->commentaire_usager; }
        if($this->action_possible !== "") {         $datas['action_possible'] = $this->action_possible; }
        if($this->editable !== "") {                $datas['editable'] = $this->editable; }
        
        return $datas;
    }
    
    public function set_datas($fields) {
        foreach ($fields as $id => $value) {
            if (isset($this->$id) && $value !== "") {
                $this->$id = $value;
            }
        }
    }
    
    public function get_entite_id() {
        return $this->entite_id;
    }
    
    public function get_entite_type() {
        return $this->entite_type;
    }
    
    public function get_service_id() {
        return $this->service_id;
    }
    
    /*
     * Récupère les informations de toute les demandes
     * GET
     * 
     * @return array
     */
    public function get_demandes() { 
        $url = (!empty($this->entite_type)) ? "demandes?entite_id=" . $this->entite_id . "&type_entite=" . $this->entite_type : "demandes";
        $result = $this->rest_api_client->call($url); 
        return $result;
        
    }
    
    /*
     * Récupère les informations d'une demande
     * GET
     * 
     * @return array
     */
    public function get_demande() {
        $result = $this->rest_api_client->call("demandes/" . $this->id_demande); 
        return $result;       
    }

    public function transmit_demande() {
        $result = $this->rest_api_client->call("demandes/transmit/".$this->id_demande, "POST");
        return $result;
    }
    
    /**
     * Crée une demande
     * POST
     * 
     * @return array
     */
    public function create_demande() {             
        $result = $this->rest_api_client->call("demandes","POST",$this->get_datas());      
        return $result;
    }

    
    /**
     * Supprime une demande
     * DELETE
     * 
     * @return array
     */
    public function delete_demande() {
        $result = $this->rest_api_client->call("demandes/" . $this->id_demande,"DELETE");   
        return $result;     
    }

    /**
     * Récupère la premier étape d'une demande
     * GET
     * 
     * @return array
     */
    public function get_first_step_demande() {
        $result = $this->rest_api_client->call("demandes/" . $this->id_demande . "/etapes");           
        return $result;          
    }

    /**
     * Récupère l'étape d'une demande en fonction d'une autre autre étape
     * GET
     * 
     * @return array
     */
    public function  get_current_step_demande($id_etape, $sens, $etape_num) {
        $result = $this->rest_api_client->call("demandes/" . $this->id_demande . "/etapes/$id_etape/$sens/$etape_num");
        return $result;           
    }
    
    
    /**
     * Met à jour une demande en focntion d'une étape
     * POST
     * 
     * @return array
     */
     public function update_step_demande($id_etape, $custom_form_params) {
        
         if(count($custom_form_params['fichiers']) > 0) {
            foreach($custom_form_params['fichiers'] as $i => $file) {
                # we have to deal with a single or multiple file uploaded
                if (is_array($file['tmp_name'])) {
                    # multiple files upload
                    $dataFiles = [];
                    foreach ($file['tmp_name'] as $fileTemp) {
                        $data = file_get_contents($fileTemp);
                        $dataFiles[] = base64_encode($data);
                    }
                    $custom_form_params['fichiers'][$i]['content'] = $dataFiles;
                } else {
                    # single file upload
                    $data = file_get_contents($file['tmp_name']);
                    $custom_form_params['fichiers'][$i]['content'] = base64_encode($data);
                }
                unset($data);
            }
         } else {
             unset($custom_form_params['fichiers']);
         }
        $result = $this->rest_api_client->call("demandes/" . $this->id_demande . "/etapes/$id_etape","POST", $custom_form_params);   
        
        return $result;     
     }
     
     public function check_access_service() {
        $result = $this->rest_api_client->call("services/" . $this->service_id . "/" . $this->entite_type . "/" . $this->entite_id);   
        return $result;              
     }
}