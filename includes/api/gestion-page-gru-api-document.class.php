<?php

/**
 * 1997-2018 Quadra Informatique
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 *
 *  @author    Quadra Informatique <modules@quadra-informatique.fr>
 *  @copyright 1997-2018 Quadra Informatique
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * 
 *  Gestion_Page_GRU_Api_Document
 */
class Gestion_Page_GRU_Api_Document {

    private $rest_api_client= "";
    private $doc_id= "";
    private $parent_id= "";
    private $parent_type= "";
    private $taille_max_coffre= "";
    private $taille_max_fichier= "";
    private $liste_mimes= "";
    private $nb_docs= "";
    private $taille_coffre= "";
    private $type_piece_id= "";
    private $type_piece_name= "";
    private $documents= "";
    private $nom_doc= "";
    private $filename= "";
    private $file= "";
    private $date_peremption= "";
    private $date_depot= "";
    private $type_mime= "";
    private $file_size= "";

    public function __construct($fields = array()) {
        $this->rest_api_client = new Gestion_Page_GRU_Api();
        $this->set_datas($fields);
    }

    public function get_datas() {
        $datas = array();

        if ($this->doc_id !== "") {
            $datas['doc_id'] = $this->doc_id;
        }
        if ($this->parent_id !== "") {
            $datas['parent_id'] = $this->parent_id;
        }
        if ($this->parent_type !== "") {
            $datas['parent_type'] = $this->parent_type;
        }
        if ($this->taille_max_coffre !== "") {
            $datas['taille_max_coffre'] = $this->taille_max_coffre;
        }
        if ($this->taille_max_fichier !== "") {
            $datas['taille_max_fichier'] = $this->taille_max_fichier;
        }
        if ($this->liste_mimes !== "") {
            $datas['liste_mimes'] = $this->liste_mimes;
        }
        if ($this->nb_docs !== "") {
            $datas['nb_docs'] = $this->nb_docs;
        }
        if ($this->taille_coffre !== "") {
            $datas['taille_coffre'] = $this->taille_coffre;
        }
        if ($this->type_piece_id !== "") {
            $datas['type_piece_id'] = $this->type_piece_id;
        }
        if ($this->type_piece_name !== "") {
            $datas['type_piece_name'] = $this->type_piece_name;
        }
        if ($this->documents !== "") {
            $datas['documents'] = $this->documents;
        }
        if ($this->nom_doc !== "") {
            $datas['nom_doc'] = $this->nom_doc;
        }
        if ($this->filename !== "") {
            $datas['filename'] = $this->filename;
        }
        if ($this->file !== "") {
            $datas['file'] = $this->file;
        }
        if ($this->date_peremption !== "") {
            $datas['date_peremption'] = $this->date_peremption;
        }
        if ($this->date_depot !== "") {
            $datas['date_depot'] = $this->date_depot;
        }
        if ($this->type_mime !== "") {
            $datas['type_mime'] = $this->type_mime;
        }
        if ($this->file_size !== "") {
            $datas['file_size'] = $this->file_size;
        }


        return $datas;
    }

    public function set_datas($fields) {
        foreach ($fields as $id => $value) {
            if (isset($this->$id) && $value !== "") {
                $this->$id = $value;
            }
        }
    }


    /*
     * Récupère les informations du portedocument
     * GET
     * 
     * @return array
     */

    public function get_infos_portedocs() {
       $result = $this->rest_api_client->call("infos-portedocs/" . $this->parent_type . "/" . $this->parent_id );
        return $result;
    }

    /*
     * Récupère la liste des pièces justificatives
     * GET
     * 
     * @return array
     */

    public function get_list_type_pieces() {
        $result = $this->rest_api_client->call("types-pieces-justificatives");
        return $result;
    }

    /**
     * Récupère la liste des documents
     * GET
     * 
     * @return array
     */
    public function get_list_documents() {
        $result = $this->rest_api_client->call("documents/" . $this->parent_type . "/" . $this->parent_id );
        return $result;
    }

    /**
     * Création d'un document
     * POST
     * 
     * @return array
     */
    public function create_document() {
         $result = $this->rest_api_client->call("documents/" . $this->parent_type . "/" . $this->parent_id ,"POST",$this->get_datas());
        return $result;
    }

    /**
     * Récupère les infos d'un document
     * GET
     * 
     * @return array
     */
    public function get_infos_document() {
        $result = $this->rest_api_client->call("documents/" . $this->doc_id );
        return $result;
    }

    /**
     * Mise à jour d'un document
     * PUT
     * 
     * @return array
     */
    public function update_document() {
        $result = $this->rest_api_client->call("documents/" . $this->doc_id,"PUT", $this->get_datas());
        return $result;
    }

    /**
     * Supprime un document
     * DELETE
     * 
     * @return array
     */
    public function delete_document() {
        $result = $this->rest_api_client->call("documents/" . $this->doc_id,"DELETE");
        return $result;
    }

}
