<?php

/**
 * 1997-2018 Quadra Informatique
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 *
 *  @author    Quadra Informatique <modules@quadra-informatique.fr>
 *  @copyright 1997-2018 Quadra Informatique
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * 
 *  Gestion_Page_GRU_Api_Individual
 */
class Gestion_Page_GRU_Api_Individual {

    private $rest_api_client;
    private $individual_id = "";
    private $entite_id = "";
    private $entite_type = "";
    private $civilite = "";
    private $nom_usage = "";
    private $prenom = "";
    private $prenom2 = "";
    private $prenom3 = "";
    private $nom_famille = "";
    private $email = "";
    private $date_naissance = "";
    private $mobile = "";
    private $situation_familiale = "";
    private $sexe = "";
    private $cp_naissance = "";
    private $ville_naissance = "";
    private $pays_naissance = "";
    private $profession = "";
    private $tel_bureau = "";
    private $roles = [];
    private $role = "";
    private $etat_individu ="";
    private $role_1 = "";
    private $role_2 = "";

    public function __construct($fields = array()) {
        $this->rest_api_client = new Gestion_Page_GRU_Api();
        $this->set_datas($fields);
    }

    public function get_datas() {
        $datas = array();

        if ($this->civilite !== "") {
            $datas['civilite'] = $this->civilite;
        }
        if ($this->nom_usage !== "") {
            $datas['nom_usage'] = $this->nom_usage;
        }
        if ($this->prenom !== "") {
            $datas['prenom'] = $this->prenom;
        }
        $datas['prenom2'] = ($this->prenom2 !== "") ? $this->prenom2 : "";
        $datas['prenom3'] = ($this->prenom3 !== "") ? $this->prenom3 : "";
        $datas['nom_famille'] = ($this->nom_famille !== "") ? $this->nom_famille : "";

        if ($this->email !== "") {
            $datas['email'] = $this->email;
        }
        if ($this->date_naissance !== "") {
            $datas['date_naissance'] = $this->date_naissance;
        }
        if ($this->mobile !== "") {
            $datas['mobile'] = $this->mobile;
        }
        if ($this->situation_familiale !== "") {
            $datas['situation_familiale'] = $this->situation_familiale;
        }
        if ($this->sexe !== "") {
            $datas['sexe'] = $this->sexe;
        }
        $datas['cp_naissance'] = ($this->cp_naissance !== "") ? $this->cp_naissance : "";
        $datas['ville_naissance'] = ($this->ville_naissance !== "") ? $this->ville_naissance : "";
        $datas['pays_naissance'] = ($this->pays_naissance !== "") ? $this->pays_naissance : "";

        $datas['profession'] = ($this->profession !== "") ? $this->profession : "";
        $datas['tel_bureau'] = ($this->tel_bureau !== "") ? $this->tel_bureau : "";
        $datas['etat_individu'] = ($this->etat_individu !== "") ? $this->etat_individu : "";
        if ($this->role !== "") {
            $datas['role'] = $this->role;
        }
        if ($this->role_1 !== "") {
            $datas['role_1'] = $this->role_1;
        }
        if ($this->role_2 !== "") {
            $datas['role_2'] = $this->role_2;
        }

        return $datas;
    }

    public function set_datas($fields) {
        foreach ($fields as $id => $value) {

            if (substr($id, 0, 5) === "role_") {
                $informations = explode("_", $id);


                $indvidualId = $informations[2];
                if (!is_array($this->roles[$indvidualId])) {
                    $this->roles[$indvidualId] = [];
                }
                $this->roles[$indvidualId][$informations[1]] = $value;
            }

            if (isset($this->$id) && $value !== "") {
                $this->$id = $value;
            }
        }
    }

    public function get_fields($values = array()) {

        $fields = array(
            array(
                'type' => 'enum',
                'name' => 'civilite',
                'label' => 'Civilité',
                'options' => array(
                    array('name' => 'M.', 'value' => 'M.'),
                    array('name' => 'Mme', 'value' => 'Mme'),
                ),
                'valeur' => (isset($values->civilite)) ? $values->civilite : ""
            ),
            array(
                'type' => 'varchar',
                'name' => 'nom_usage',
                'label' => 'Nom d\'usage *',
                'valeur' => (isset($values->nom_usage)) ? $values->nom_usage : ""
            ),
            array(
                'type' => 'varchar',
                'name' => 'prenom',
                'label' => 'Prénom *',
                'valeur' => (isset($values->prenom)) ? $values->prenom : ""
            ),
            array(
                'type' => 'varchar',
                'name' => 'prenom2',
                'label' => 'Prénom 2',
                'valeur' => (isset($values->prenom2)) ? $values->prenom2 : ""
            ),
            array(
                'type' => 'varchar',
                'name' => 'prenom3',
                'label' => 'Prénom 3',
                'valeur' => (isset($values->prenom3)) ? $values->prenom3 : ""
            ),
            array(
                'type' => 'varchar',
                'name' => 'nom_famille',
                'label' => 'Nom de Famille',
                'valeur' => (isset($values->nom_famille)) ? $values->nom_famille : ""
            ),
            array(
                'type' => 'varchar',
                'name' => 'email',
                'label' => 'Email',
                'valeur' => (isset($values->email)) ? $values->email : ""
            ),
            array(
                'type' => 'date',
                'name' => 'date_naissance',
                'label' => 'Date de naissance *',
                'valeur' => (isset($values->date_naissance)) ? $values->date_naissance : ""
            ),
            array(
                'type' => 'varchar',
                'name' => 'mobile',
                'label' => 'Téléphone mobile',
                'valeur' => (isset($values->mobile)) ? $values->mobile : ""
            ),
            array(
                'type' => 'enum',
                'name' => 'situation_familiale',
                'label' => 'Situation familiale',
                'options' => array(
                    array('name' => 'celibataire', 'value' => 'Célibataire'),
                    array('name' => 'concubinage', 'value' => 'Concubinage'),
                    array('name' => 'pacse', 'value' => 'Pacsé'),
                    array('name' => 'marie', 'value' => 'Marié'),
                    array('name' => 'divorce', 'value' => 'Divorcé'),
                    array('name' => 'veuf', 'value' => 'Veuf'),
                ),
                'valeur' => (isset($values->situation_familiale)) ? $values->situation_familiale : ""
            ),
            array(
                'type' => 'enum',
                'name' => 'sexe',
                'label' => 'Sexe *',
                'options' => array(
                    array('name' => 'M', 'value' => 'Masculin'),
                    array('name' => 'F', 'value' => 'Féminin')
                ),
                'valeur' => (isset($values->sexe)) ? $values->sexe : ""
            ),
            array(
                'type' => 'varchar',
                'name' => 'cp_naissance',
                'label' => 'Code postal de naissance',
                'valeur' => (isset($values->cp_naissance)) ? $values->cp_naissance : ""
            ),
            array(
                'type' => 'varchar',
                'name' => 'ville_naissance',
                'label' => 'Ville de naissance',
                'valeur' => (isset($values->ville_naissance)) ? $values->ville_naissance : ""
            ),
            array(
                'type' => 'varchar',
                'name' => 'pays_naissance',
                'label' => 'Pays de naissance',
                'valeur' => (isset($values->pays_naissance)) ? $values->pays_naissance : ""
            ),
            array(
                'type' => 'enum',
                'name' => 'etat_individu',
                'label' => 'Etat de l\'individu',
                'options' => array(
                    array('name' => 'vivant', 'value' => 'Vivant(e)'),
                    array('name' => 'decede', 'value' => 'Décédé(e)'),
                    array('name' => 'a naitre', 'value' => 'A Naître'),
                ),
                'valeur' => (isset($values->etat_individu)) ? $values->etat_individu : ""
            ),
            array(
                'type' => 'varchar',
                'name' => 'profession',
                'label' => 'Profession',
                'valeur' => (isset($values->profession)) ? $values->profession : ""
            ),
            array(
                'type' => 'varchar',
                'name' => 'tel_bureau',
                'label' => 'Téléphone',
                'valeur' => (isset($values->tel_bureau)) ? $values->tel_bureau : ""
            ),
//            array(
//                'type' => 'enum',
//                'name' => 'role_1',
//                'label' => 'Rôle utilisateur',
//                'options' => array(
//                    array('name' => '-1', 'value' => '...'),
//                    array('name' => 'Père', 'value' => 'Père'),
//                    array('name' => 'Mère', 'value' => 'Mère')
//                ),
//                'valeur' => (isset($values->role_1)) ? $values->role_1 : ""
//            ),        
//            array(
//                'type' => 'enum',
//                'name' => 'role_2',
//                'label' => 'Rôle individu',
//                'options' => array(
//                    array('name' => '-1', 'value' => '...'),
//                    array('name' => 'Fils', 'value' => 'Fils'),
//                    array('name' => 'Fille', 'value' => 'Fille')
//                ),
//                'valeur' => (isset($values->role_2)) ? $values->role_2 : ""
//            )
        );

        return $fields;
    }

    public function get_individual() {
        $result = $this->rest_api_client->call("individus/" . $this->individual_id);
        return $result;
    }

    public function get_individual_list() {
        $id = ($this->entite_type == "CDE_INDIVIDUS") ? $this->individual_id : $this->entite_id;
        $result = $this->rest_api_client->call("individus/" . $this->entite_type . "/$id");

        return $result;
    }

    public function create_individual_record() {
        $id = ($this->entite_type == "CDE_INDIVIDUS") ? $this->individual_id : $this->entite_id;
        $role_1 = $role_2 = "";
        if ($this->entite_type == "CDE_FOYERS") {
            $role_1 = $this->role_1;
            $role_2 = $this->role_2;
            $this->role_1 = "membre";
            $this->role_2 = "";
        } else if ($this->entite_type != "CDE_FOYERS" && $this->entite_type != "CDE_INDIVIDUS") {
            $this->role_1 = "membre";
            $this->role_2 = "";
        }

        $result = $this->rest_api_client->call("individus/" . $this->entite_type . "/$id", "POST", $this->get_datas());

        if ($role_1 !== "" && $role_2 !== "") {
            $this->role_1 = $role_1;
            $this->role_2 = $role_2;
        }

        $datas = array();
   
        foreach ($this->roles as $key => $value) {

            $id_responsable = $key;        
            foreach ($value as $cle => $valeur) {
                if($cle == '1') {
                    $datas['role_1'] = $valeur;
                }
                if($cle == '2') {
                    $datas['role_2'] = $valeur;
                }
                
            }
            if ($this->entite_type == "CDE_FOYERS" || $this->entite_type == "CDE_INDIVIDUS") {  
                if($datas['role_1'] != "" && $datas['role_2'] != ""){
                    $request = $this->rest_api_client->call("liaisons/" . $id_responsable . "/CDE_INDIVIDUS/" . $result->data->id . "", "POST", $datas);
                }           
            }
            
        }

        return $result;
    }

    public function update_individual_record() {        
        $result = $this->rest_api_client->call("individus/" . $this->individual_id, "PUT", $this->get_datas());
        return $result;
    }

    public function delete_individual_record() {
        $result = $this->rest_api_client->call("individus/" . $this->individual_id, "DELETE");
        return $result;
    }

}
