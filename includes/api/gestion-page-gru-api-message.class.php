<?php

class Gestion_Page_GRU_Api_Message
{
    private $rest_api_client;

    private $message = "";
    private $upload_file_type = "";
    private $upload_file_base64 = "";
    private $upload_file_name = "";

    // demand id
    private $parent_name = "";
    private $parent_id = "";

    // entity in which demand has been created
    private $type_entite = "";
    private $id_entite = "";

    // uploaded file in message
    private $message_id = "";
    private $document_id = "";
    
    public function __construct($fields = array()) {
        $this->rest_api_client = new Gestion_Page_GRU_Api();
        $this->set_datas($fields);
    }

    public function get_datas() {
        $datas = array();
        $datas['id_demande']  = (!isset($this->parent_id) ? "" : $this->parent_id);
        $datas['parent_name'] = (!isset($this->parent_name) ? "" : $this->parent_name);
        $datas['message']     = (!isset($this->message) ? "" : $this->message);
        $datas['upload_file_type']   = (!isset($this->upload_file_type) ? "" : $this->upload_file_type);
        $datas['upload_file_base64'] = (!isset($this->upload_file_base64) ? "" : $this->upload_file_base64);
        $datas['upload_file_name']   = (!isset($this->upload_file_name) ? "" : $this->upload_file_name);
        $datas['flag_type'] = 1; // message from user
        return $datas;
    }

    public function set_datas($fields) {
        foreach ($fields as $id => $value) {
            if (property_exists($this, $id) && $value !== "") {
                $this->$id = $value;
            }
        }
    }

    /**
     * @return array|false if parameter is invalid
     */
    public function get_thread_messages_list() {
        if (empty($this->id_entite) || empty($this->type_entite)) {
            error_log("Gestion_Page_GRU_Api_Message::get_thread_messages_list : Parameters invalid.");
            return false;
        }
        $url = "messages/threads/{$this->id_entite}/{$this->type_entite}";
        $result = $this->rest_api_client->call($url);
        return $result;
    }

    /**
     * @return array|false if parameter is invalid
     */
    public function get_last_messages_list() {
        if (empty($this->id_entite) || empty($this->type_entite)) {
            error_log("Gestion_Page_GRU_Api_Message::get_last_messages_list : Parameters invalid.");
            return false;
        }
        $url = "messages/{$this->id_entite}/{$this->type_entite}";
        $result = $this->rest_api_client->call($url);
        return $result;
    }

    /**
     * @return array|false if parameter is invalid
     */
    public function get_messages_demand() {
        if (empty($this->parent_id) || empty($this->id_entite) || empty($this->type_entite)) {
            error_log("Gestion_Page_GRU_Api_Message::get_messages_demand : Parameters invalid");
            return false;
        }
        $url = "messages/{$this->parent_id}/{$this->id_entite}/{$this->type_entite}";
        $result = $this->rest_api_client->call($url);
        return $result;
    }

    public function envoyer_message() {
        $result = $this->rest_api_client->call("messages", 'POST', $this->get_datas());
        return $result;
    }

    public function get_document_message() {
        $url = "messages-getfile/{$this->parent_id}/{$this->message_id}/{$this->document_id}";
        $result = $this->rest_api_client->call($url);
        return $result;
    }

}