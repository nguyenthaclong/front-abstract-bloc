<?php

/**
 * 1997-2018 Quadra Informatique
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 *
 *  @author    Quadra Informatique <modules@quadra-informatique.fr>
 *  @copyright 1997-2018 Quadra Informatique
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * 
 *  Gestion_Page_GRU_Api_Entity
 */
class Gestion_Page_GRU_Api_Entity {

    private $id_entite = "";
    private $type_entite = "";
    private $nom = "";
    private $rue1 = "";
    private $rue2 = "";
    private $rue3 = "";
    private $rue4 = "";
    private $code_postal = "";
    private $ville = "";
    private $pays = "";
    private $telephone = "";
    private $fax = "";
    private $label_foyer = "";
    private $site_web = "";
    private $forme_juridique = "";
    private $siret = "";
    private $rna = "";
    private $secteur_activite = "";
    private $code_ape = "";
    private $rne = "";

    public function __construct($fields = array()) {
        $this->rest_api_client = new Gestion_Page_GRU_Api();
        $this->set_datas($fields);
    }

    public function get_datas() {
        $datas = array();
        if ($this->type_entite !== "") {
            $datas['type_entite'] = $this->type_entite;
        }
        if ($this->nom !== "") {
            $datas['nom'] = $this->nom;
        }
        if ($this->rue1 !== "") {
            $datas['rue1'] = $this->rue1;
        }
        if ($this->rue2 !== "") {
            $datas['rue2'] = $this->rue2;
        }
        if ($this->rue3 !== "") {
            $datas['rue3'] = $this->rue3;
        }
        if ($this->rue4 !== "") {
            $datas['rue4'] = $this->rue4;
        }
        if ($this->code_postal !== "") {
            $datas['code_postal'] = $this->code_postal;
        }
        if ($this->ville !== "") {
            $datas['ville'] = $this->ville;
        }
        if ($this->pays !== "") {
            $datas['pays'] = $this->pays;
        }
        if ($this->telephone !== "") {
            $datas['telephone'] = $this->telephone;
        }
        if ($this->fax !== "") {
            $datas['pays'] = $this->fax;
        }
        if ($this->label_foyer !== "") {
            $datas['label_foyer'] = $this->label_foyer;
        }
        if ($this->site_web !== "") {
            $datas['site_web'] = $this->site_web;
        }
        if ($this->forme_juridique !== "") {
            $datas['forme_juridique'] = $this->forme_juridique;
        }
        if ($this->siret !== "") {
            $datas['siret'] = $this->siret;
        }
        if ($this->rna !== "") {
            $datas['rna'] = $this->rna;
        }
        if ($this->secteur_activite !== "") {
            $datas['secteur_activite'] = $this->secteur_activite;
        }
        if ($this->code_ape !== "") {
            $datas['code_ape'] = $this->code_ape;
        }
        if ($this->rne !== "") {
            $datas['rne'] = $this->rne;
        }
        return $datas;
    }

    public function set_datas($fields) {
        foreach ($fields as $id => $value) {
            if (isset($this->$id) && $value !== "") {
                $this->$id = $value;
            }
        }
    }

    public function get_fields_from_type() {

        $fields = array(
            array(
                'type' => 'varchar',
                'name' => 'rue1',
                'label' => 'Rue 1',
            ),
            array(
                'type' => 'varchar',
                'name' => 'rue2',
                'label' => 'Rue 2',
            ),
            array(
                'type' => 'varchar',
                'name' => 'rue3',
                'label' => 'Rue 3',
            ),
            array(
                'type' => 'varchar',
                'name' => 'rue4',
                'label' => 'Rue 4',
            ),
            array(
                'type' => 'varchar',
                'name' => 'code_postal',
                'label' => 'Code postal',
            ),
            array(
                'type' => 'varchar',
                'name' => 'ville',
                'label' => 'Ville',
            ),
            array(
                'type' => 'varchar',
                'name' => 'pays',
                'label' => 'Pays',
            ),
            array(
                'type' => 'varchar',
                'name' => 'telephone',
                'label' => 'Téléphone fixe',
            ),
            array(
                'type' => 'varchar',
                'name' => 'fax',
                'label' => 'Fax',
            )
        );

        switch ($this->type_entite) {
            case "CDE_FOYERS":
                array_push($fields, array(
                    'type' => 'enum',
                    'name' => 'label_foyer',
                    'label' => 'Rôle',
                    'options' => array(
                        array('name' => 'Foyer de la famille', 'value' => 'Foyer de la famille'),
                        array('name' => 'Foyer des parents', 'value' => 'Foyer des parents'),
                    )
                ));
                break;
            case "CDE_ASSOCIATIONS":
                array_push($fields, array(
                    'type' => 'varchar',
                    'name' => 'nom',
                    'label' => 'Raison Sociale *',
                ));
                array_push($fields, array(
                    'type' => 'varchar',
                    'name' => 'site_web',
                    'label' => 'Site Web',
                ));
                array_push($fields, array(
                    'type' => 'enum',
                    'name' => 'forme_juridique',
                    'label' => 'Forme Juridique',
                    'options' => array(
                        array('name' => 'asso_declaree', 'value' => 'Association déclarée'),
                        array('name' => 'asso_agreee', 'value' => 'Association Agréée'),
                        array('name' => 'asso_utilite_publique', 'value' => "Association d'Utilité Publique"),
                        array('name' => 'asso_droit_local', 'value' => 'Association de Droit Local'),
                        array('name' => 'autre', 'value' => 'Autre Association'),
                    )
                ));
                array_push($fields, array(
                    'type' => 'varchar',
                    'name' => 'siret',
                    'label' => 'N° de Siret',
                ));
                array_push($fields, array(
                    'type' => 'varchar',
                    'name' => 'rna',
                    'label' => 'N° RNA',
                ));
                break;
            case "CDE_SOCIETES":
                array_push($fields, array(
                    'type' => 'varchar',
                    'name' => 'nom',
                    'label' => 'Raison Sociale *',
                ));
                array_push($fields, array(
                    'type' => 'varchar',
                    'name' => 'site_web',
                    'label' => 'Site Web',
                ));
                array_push($fields, array(
                    'type' => 'enum',
                    'name' => 'secteur_activite',
                    'label' => "Secteur d'Activité",
                    'options' => array(
                        array('name' => 'agriculture_agroalimentaire', 'value' => 'Agriculture Agroalimentaire'),
                        array('name' => 'industrie', 'value' => 'Industrie'),
                        array('name' => 'energie', 'value' => "Énergie"),
                        array('name' => 'commerce_artisanat', 'value' => 'Commerce - Artisanat'),
                        array('name' => 'tourisme', 'value' => 'Tourisme'),
                        array('name' => 'telecoms_internet', 'value' => 'Télécome - Internet'),
                        array('name' => 'recherche', 'value' => 'Recherche'),
                        array('name' => 'finance_assurance', 'value' => 'Finance - Assurance'),
                        array('name' => 'autre', 'value' => 'Autre'),
                    )
                ));
                array_push($fields, array(
                    'type' => 'enum',
                    'name' => 'forme_juridique',
                    'label' => 'Forme Juridique',
                    'options' => array(
                        array('name' => 'SA', 'value' => 'SA'),
                        array('name' => 'SAS', 'value' => 'SAS'),
                        array('name' => 'SASU', 'value' => 'SASU'),
                        array('name' => 'SARL', 'value' => 'SARL'),
                        array('name' => 'EURL', 'value' => 'EURL'),
                        array('name' => 'SNC', 'value' => 'SNC'),
                        array('name' => 'entreprise_individuelle', 'value' => 'Entreprise Individuelle'),
                        array('name' => 'auto_entrepreneur', 'value' => 'Auto-Entrepreneur'),
                        array('name' => 'autre', 'value' => 'Autre'),
                    )
                ));
                array_push($fields, array(
                    'type' => 'varchar',
                    'name' => 'siret',
                    'label' => 'N° de Siret',
                ));
                array_push($fields, array(
                    'type' => 'varchar',
                    'name' => 'cde_ape',
                    'label' => 'Code APE',
                ));
                break;
            case "CDE_COLLEGES":
                array_push($fields, array(
                    'type' => 'varchar',
                    'name' => 'nom',
                    'label' => 'Raison Sociale *',
                ));
                array_push($fields, array(
                    'type' => 'varchar',
                    'name' => 'site_web',
                    'label' => 'Site Web',
                ));
                array_push($fields, array(
                    'type' => 'enum',
                    'name' => 'forme_juridique',
                    'label' => 'Forme Juridique',
                    'options' => array(
                        array('name' => 'SA', 'value' => 'SA'),
                        array('name' => 'SAS', 'value' => 'SAS'),
                        array('name' => 'SASU', 'value' => 'SASU'),
                        array('name' => 'SARL', 'value' => 'SARL'),
                        array('name' => 'EURL', 'value' => 'EURL'),
                        array('name' => 'SNC', 'value' => 'SNC'),
                        array('name' => 'entreprise_individuelle', 'value' => 'Entreprise Individuelle'),
                        array('name' => 'auto_entrepreneur', 'value' => 'Auto-Entrepreneur'),
                        array('name' => 'autre', 'value' => 'Autre'),
                    )
                ));
                array_push($fields, array(
                    'type' => 'varchar',
                    'name' => 'siret',
                    'label' => 'N° de Siret',
                ));
                array_push($fields, array(
                    'type' => 'varchar',
                    'name' => 'rne',
                    'label' => 'N° RNE',
                ));
                break;
            case "CDE_COMMUNES":
            case "CDE_INTERCOMMUNALITES":
            case "CDE_AUTREORGPUBS":
                array_push($fields, array(
                    'type' => 'varchar',
                    'name' => 'nom',
                    'label' => 'Raison Sociale *',
                ));
                array_push($fields, array(
                    'type' => 'varchar',
                    'name' => 'site_web',
                    'label' => 'Site Web',
                ));
                array_push($fields, array(
                    'type' => 'enum',
                    'name' => 'forme_juridique',
                    'label' => 'Forme Juridique',
                    'options' => array(
                        array('name' => 'SA', 'value' => 'SA'),
                        array('name' => 'SAS', 'value' => 'SAS'),
                        array('name' => 'SASU', 'value' => 'SASU'),
                        array('name' => 'SARL', 'value' => 'SARL'),
                        array('name' => 'EURL', 'value' => 'EURL'),
                        array('name' => 'SNC', 'value' => 'SNC'),
                        array('name' => 'entreprise_individuelle', 'value' => 'Entreprise Individuelle'),
                        array('name' => 'auto_entrepreneur', 'value' => 'Auto-Entrepreneur'),
                        array('name' => 'autre', 'value' => 'Autre'),
                    )
                ));
                array_push($fields, array(
                    'type' => 'varchar',
                    'name' => 'siret',
                    'label' => 'N° de Siret',
                ));
                break;
        }
        return $fields;
    }

    public function get_type_entites() {
        $result = $this->rest_api_client->call("entites");
        return $result;
    }

    public function get_entites() {
        $result = $this->rest_api_client->call("entites/" . $this->type_entite);
        return $result;
    }

    public function get_entite_record() {
        $result = $this->rest_api_client->call("entites/" . $this->type_entite . "/" . $this->id_entite);
        return $result;
    }

    public function create_entite_record() {
        $result = $this->rest_api_client->call("entites/" . $this->type_entite, 'POST', $this->get_datas());
        return $result;
    }

    public function update_entite_record() {
        $result = $this->rest_api_client->call("entites/" . $this->type_entite . "/" . $this->id_entite, 'PUT', $this->get_datas());
        return $result;
    }

    public function delete_entity_record() {
        $result = $this->rest_api_client->call("entites/" . $this->type_entite . "/" . $this->id_entite, 'DELETE');
        return $result;
    }

    public function get_entity_beneficiary_list($id_service) {
        $result = $this->rest_api_client->call("entites/" . $this->type_entite . "/" . $this->id_entite . "/services/" . $id_service . "/beneficiaires");
        return $result;
    }

}

abstract class Gestion_Page_GRU_Api_Entity_Relation {

    public $relation_id;
    public $objet_id;
    public $objet_nom;
    public $role_1;
    public $role_1_label;
    public $droit_connexion;
    public $responsable;

}
