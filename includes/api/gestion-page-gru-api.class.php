<?php

/**
 * 1997-2018 Quadra Informatique
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 *
 *  @author    Quadra Informatique <modules@quadra-informatique.fr>
 *  @copyright 1997-2018 Quadra Informatique
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * 
 *  Gestion_Page_GRU_Api
 */
class Gestion_Page_GRU_Api {

    CONST session_libelle_token = "access_token_oauth2";
    CONST rest_param_option = 'sugarcrm_rest_parameters';
    CONST links_pages_option = 'links_pages_option';

    private $token_path = "CDE_API/access_token";
    private $rest_method_path = "CDE_API/V3/";
    private $rest_params;
    private $rest_access_token_url;
    private $rest_url;

    public function __construct() {
        $this->rest_params = get_option(Gestion_Page_GRU_Api::rest_param_option);
        $this->rest_access_token_url = $this->rest_params['gru_url'] . $this->token_path;
        $this->rest_url = $this->rest_params['gru_url'] . $this->rest_method_path;

        add_action('wp_ajax_delete_request', array($this, 'ajax_delete_request_record'), 50);
        add_action('wp_ajax_delete_entity', array($this, 'ajax_delete_entity_record'), 50);
        add_action('wp_ajax_delete_individual', array($this, 'ajax_delete_individual_record'), 50);
        add_action('wp_ajax_synchronize_services_list', array($this, 'ajax_synchronize_services_list'), 50);
        add_action('wp_ajax_upload_gru_document', array($this, 'ajax_upload_gru_document'), 50);
        add_action('wp_ajax_create_document', array($this, 'ajax_create_document'), 50);
        add_action('wp_ajax_download_document', array($this, 'ajax_download_document'), 50);
        add_action('wp_ajax_delete_document', array($this, 'ajax_delete_document'), 50);
        add_action('wp_ajax_show_document', array($this, 'ajax_show_document'), 50);
        add_action('wp_ajax_download_document_message', array($this, 'ajax_download_document_message'), 50);
        
    }

    /**
     * @throws Exception
     */
    public function init_usager($user) {
        $user_context = Admin_Gestion_Page_GRU_Tools::get_user_context();
        $token = $user_context->get_token();

        if (!empty($token)) {
            $expires_at = new \DateTime('+' . $token->expires_in . ' seconds');
            $user_context->update_access_token($user,$token->access_token);
            $user_context->update_expires_in($user,$expires_at->getTimestamp());
            if (!empty($token->refresh_token)) {
                $user_context->update_refresh_token($user,$token->refresh_token);
            }
        }

        // Récupérer les infos du compte.
        $rest_api_comptes = new Gestion_Page_GRU_Api_Account(array('id_gru' => $user->user_login));
        $infos_compte = $rest_api_comptes->get_account();
        $user_context->set_account_info($infos_compte);
     }

    /**
     * Utiliser uniquement pour le test d'accès GRU du front
     * @return mixed
     */
    public function check_access_oauth_client()
    {
        $token_url = $this->rest_access_token_url;
        $ch = curl_init($token_url);
        $params = array(
            "grant_type"    => "client_credentials",
            "client_id"     => $this->rest_params['crm_client_id'],
            "client_secret" => $this->rest_params['crm_client_secret'],
        );

        $this->setup_generic_curl_options($ch, $params);
        curl_exec($ch);
        return curl_getinfo($ch);
    }

    /**
     * Utiliser uniquement pour récupérer le token OAuth avec un client valide (cas de user admin)
     * @return mixed
     */
    public function get_token_oauth_client()
    {
        $token_url = $this->rest_access_token_url;
        $ch = curl_init($token_url);
        $params = array(
            "grant_type"    => "client_credentials",
            "client_id"     => $this->rest_params['crm_client_id'],
            "client_secret" => $this->rest_params['crm_client_secret'],
        );

        $this->setup_generic_curl_options($ch, $params);
        $result_curl = curl_exec($ch);
        $token = json_decode($result_curl);

        return $token;
    }

    /**
     * Utilisé pour demander un token utilisateur en mode login/password
     * @param string $email
     * @param string $password
     * @return mixed
     */
    public function get_token_usager($email = null, $password = null) {
        $user_context = Admin_Gestion_Page_GRU_Tools::get_user_context();
        $token_url = $this->rest_access_token_url;
        $ch = curl_init($token_url);
        $params = [];
        if ($email !== null && $password !== null) {
            $params["grant_type"]    = "password";
            $params["client_id"]     = $this->rest_params['crm_client_id'];
            $params["client_secret"] = $this->rest_params['crm_client_secret'];
            $params["username"]      = $email;
            $params["password"]      = $password;
        }

        $this->setup_generic_curl_options($ch, $params);

        $json_result_call = curl_exec($ch);
        $token = json_decode($json_result_call);

        $user_context->set_token($token);

        return $token;
    }

    /**
     * @param resource $ch context curl
     * @param array $params
     */
    protected function setup_generic_curl_options($ch, array $params)
    {
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLPROTO_HTTPS, 1);
        curl_setopt($ch, CURLOPT_HEADER, false);
    }

    /**
     * Utilisé avant chaque appel à l'API par curl
     * @return string
     */
    public function get_access_token() {

        $user_context = Admin_Gestion_Page_GRU_Tools::get_user_context();
        if ($user_context->is_admin()) {
            return $this->get_token_oauth_client()->access_token;
        } else {
            return $user_context->get_access_token() ?: $this->get_token_oauth_client()->access_token;
        }
    }

    /**
     * Utilisé dans le callback de l'évenement Authenticate de Wordpress
     * @param $email
     * @param $password
     *
     * @return string
     */
    public function get_access_token_usager($email, $password) {
        $user_context = Admin_Gestion_Page_GRU_Tools::get_user_context();
        if ($user_context->is_authenticated()) {
            return $user_context->get_access_token();
        }

        $user_context->create_user();

        //set token
        $token = $this->get_token_usager($email, $password);
        $user_context->set_token($token);

        return $token->access_token;
    }

    public function call($method, $type = "GET", $params = array(), $catch_error = true, $embed_body = true) {

        $access_token = $this->get_access_token();
        $curl = curl_init();
        $params_setopt = array(
            CURLOPT_URL => $this->rest_url . "$method",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $type,
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Authorization: Bearer $access_token",
                "Cache-Control: no-cache",
                "Connection: keep-alive",
                "Content-Type: application/json",
                "accept-encoding: gzip, deflate",
                "cache-control: no-cache"
            )
        );

        if (!empty($params)) {
            switch ($type) {
                case "POST":
                case "PUT":
                    $body = $embed_body ? json_encode(array('data' => $params)) : json_encode($params);
                    $params_setopt[CURLOPT_POSTFIELDS] = $body;
                    break;
                case "DELETE": break;
                default: break;
            }
        }

        curl_setopt_array($curl, $params_setopt);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        $result_call = json_decode($response);

        if (!empty($err) || !empty($result_call->errors) || $result_call->error === "access_denied") {
            if($catch_error) {
                Admin_Gestion_Page_GRU_Tools::logout_and_redirect();
            }
            return false;
        }
        return( $result_call);
    }

    public function ajax_delete_entity_record() {
        $api_entity = new Gestion_Page_GRU_Api_Entity(array('id_entite' => $_POST['id_entity'], 'type_entite' => $_POST['type_entity']));
        $result = $api_entity->delete_entity_record();
        $code = 0;
        $message = "Une erreur est survenue lors de la suppression de l'entité";
        if (isset($result->data->id)) {
            $message = "L'entité à bien été supprimée";
            $code = 1;
        }

        echo json_encode(array(
            "code" => $code,
            "message" => $message,
        ));
        wp_die();
    }

    public function ajax_synchronize_services_list() {
        global $wpdb;
        $api_services = new Gestion_Page_GRU_Api_Service();
        $services_list = $api_services->get_services_list();

        $list_teleservice = array();
        $list_category = array();
        $code = 1;
        $message = "La synchronisation des téléservices a bien été effectuée";
        
        foreach ($services_list->data as $service) {
            $result = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}gestion_page_gru_tuile` WHERE tuile_teleservice_id = '$service->service_id' ORDER BY tuile_type");

            if (!array_key_exists($service->categorie_id, $list_category)) {
                $list_category[$service->categorie_id] = array(
                    "name" => $service->categorie_name,
                    "childs" => array()
                );
            }
            $description = (strlen($service->description) > 100) ? "" : $service->description;
            $data = array(
                'tuile_title' => $service->nom,
                'tuile_description' => $description,
                'tuile_teleservice_id' => $service->service_id,
                'tuile_type' => '1',
                'tuile_type_beneficiaire' => $service->type_beneficiaire
            );
            
            if (count($result) <= 0) {
                $wpdb->insert($wpdb->prefix . "gestion_page_gru_tuile", $data);
                $last_id = $wpdb->insert_id;
            } else {
                $wpdb->update(
                    $wpdb->prefix . "gestion_page_gru_tuile", $data, array(
                        'tuile_teleservice_id' => $service->service_id
                    )
                );
                $last_id = $result[0]->tuile_id;
            }
            $list_teleservice[] = $last_id;
            $list_category[$service->categorie_id]['childs'][] = $last_id;
        }

        $wpdb->delete("{$wpdb->prefix}gestion_page_gru_tuile_ctg_correspondance", []);
        $result = $wpdb->query(<<<SQL
            DELETE t, s
            FROM `{$wpdb->prefix}gestion_page_gru_tuile` t       
            LEFT JOIN `{$wpdb->prefix}gestion_page_gru_tuile_style` s ON t.tuile_id = s.tuile_style_tuile_id 
            WHERE t.tuile_type = '2'
SQL
        );
            
        if ($result === false) {
            $code = 0;
            $message = "Une erreur est survenue lors de la syncrhonisation des téléservices";
        }
        
        $str_tuile_id = '"' . implode('","', $list_teleservice) . '"';

        $result = $wpdb->query(<<<SQL
            DELETE t, s
            FROM `{$wpdb->prefix}gestion_page_gru_tuile` t       
            LEFT JOIN `{$wpdb->prefix}gestion_page_gru_tuile_style` s ON t.tuile_id = s.tuile_style_tuile_id 
            WHERE t.tuile_type = '1'
            AND t.tuile_id NOT IN ($str_tuile_id)
SQL
        );

        foreach ($list_category as $category) {
            $wpdb->insert($wpdb->prefix . "gestion_page_gru_tuile", array(
                'tuile_title' => $category['name'],
                'tuile_type' => '2',
            ));
            $last_id = $wpdb->insert_id;
            foreach ($category['childs'] as $child) {
                $wpdb->insert($wpdb->prefix . "gestion_page_gru_tuile_ctg_correspondance", array(
                    'tuile_ctg_correspondance_child_id' => $child,
                    'tuile_ctg_correspondance_parent_id' => $last_id
                ));
            }
        }

        if ($result === false) {
            $code = 0;
            $message = "Une erreur est survenue lors de la syncrhonisation des téléservices";
        }

        echo json_encode(array(
            "code" => $code,
            "message" => $message,
        ));
        wp_die();
    }

    public function ajax_delete_request_record() {
        $api_request = new Gestion_Page_GRU_Api_Request(array('id_demande' => $_POST['id_demande']));
        $result = $api_request->delete_demande();
        $code = 0;
        $message = "Une erreur est survenue lors de la suppression de la demande";
        if (isset($result->data->id)) {
            $message = "La demande à bien été supprimée";
            $code = 1;
        }

        echo json_encode(array(
            "code" => $code,
            "message" => $message,
        ));
        wp_die();
    }

    public function ajax_delete_individual_record() {
        $api_individual = new Gestion_Page_GRU_Api_Individual(array('individual_id' => $_POST['individual_id']));
        $result = $api_individual->delete_individual_record();
        $code = 0;
        $message = "Une erreur est survenue lors de la suppression de l'individu";
        if (isset($result->data->id)) {
            $message = "L'individu à bien été supprimé";
            $code = 1;
        }

        echo json_encode(array(
            "code" => $code,
            "message" => $message,
        ));
        wp_die();
    }
    
    public function ajax_upload_gru_document() {       
        $context = Admin_Gestion_Page_GRU_Tools::get_context();
        $tmp_name = $_FILES["file"]["tmp_name"];
        $name = $_FILES["file"]["name"];
        $plugin_base_name = explode('/', plugin_basename(__FILE__))[0];
        $absolute_base_name = explode($plugin_base_name, plugin_dir_path(__FILE__))[0] . $plugin_base_name;
        $uniqid = uniqid();
        move_uploaded_file($tmp_name, "$absolute_base_name/assets/data/gru_upload/" . $uniqid);        
        
        $user_wp = wp_get_current_user();
        $id_individu = get_user_meta($user_wp->data->ID, 'gru_id')[0];
        $id_entite = (isset($context['id_entite'])) ? $context['id_entite'] : $id_individu;
        $type_entite = (isset($context['type_entite'])) ? $context['type_entite'] : 'CDE_INDIVIDUS';        
        $api_document = new Gestion_Page_GRU_Api_Document(array(
            'parent_type' => $type_entite,
            'parent_id' => $id_entite,
            'nom_doc' => $name,
            'type_piece_id' => $_POST['type_piece'],
            'filename' => $name,
            'file' => base64_encode(file_get_contents("$absolute_base_name/assets/data/gru_upload/" . $uniqid))
        ));
        
        $result = $api_document->create_document();
        if (isset($result->data->id)) {
            $return = array(
                "code" => 1,
                "message" => "Le document à bien été ajouté",
            );
        } else {
            $return = array(
                "code" => 0,
                "message" => "Erreur lors de l'ajout du document"
            );
        }

        echo json_encode($return);
        wp_die();
    }

    public function ajax_create_document() {
        $api_document = new Gestion_Page_GRU_Api_Document(array('parent_id' => $_POST['parent_id'],'parent_type' => $_POST['parent_type']));
        $result = $api_document->create_document();
        echo json_encode($result);
        wp_die();
    }
    
    public function ajax_download_document() {            
        $plugin_base_name = explode('/', plugin_basename(__FILE__))[0];
        $plugin_dir_path = explode($plugin_base_name, plugin_dir_path(__FILE__))[0];
        $path_download = plugins_url("$plugin_base_name");
        
        $api_document = new Gestion_Page_GRU_Api_Document(array('doc_id' => $_POST['doc_id']));
        $infos_document = $api_document->get_infos_document();
        if(isset($infos_document->data->id)) {
            $path_file = "/assets/data/export/" . uniqid();
            file_put_contents($plugin_dir_path . $plugin_base_name . $path_file, base64_decode($infos_document->data->file));
            $return = array(
                'href' => $path_download . $path_file,
                'filename' => $infos_document->data->filename
            );
        }
        echo json_encode($return);
        wp_die();
    }

    public function ajax_download_document_message() {
        $plugin_base_name = explode('/', plugin_basename(__FILE__))[0];
        $plugin_dir_path = explode($plugin_base_name, plugin_dir_path(__FILE__))[0];
        $path_download = plugins_url("$plugin_base_name");

        $api_message = new Gestion_Page_GRU_Api_Message(array('parent_id'   => $_POST['parent_id'],
                                                              'message_id'  => $_POST['message_id'],
                                                              'document_id' => $_POST['document_id']));
        $infos_document = $api_message->get_document_message();
        error_log("ajax_download_document_message:: infos_document = " . var_export($infos_document, true));
        if(isset($infos_document->data->docContent)) {
            $path_file = "/assets/data/export/" . uniqid();
            file_put_contents($plugin_dir_path . $plugin_base_name . $path_file, base64_decode($infos_document->data->docContent));
            $return = array(
                'href' => $path_download . $path_file,
                'filename' => $infos_document->data->docName
            );
            echo json_encode($return);
        } else {
            echo json_encode("Téléchargement du fichier a échoué.");
        }
        wp_die();
    }

    public function ajax_delete_document() {
        
        $api_document = new Gestion_Page_GRU_Api_Document(array('doc_id' => $_POST['doc_id']));
        $result = $api_document->delete_document();
        $code = 0;
        $message = "Une erreur est survenue lors de la suppression du document";
        if (isset($result->data->id)) {
            $message = "Le document à bien été supprimée";
            $code = 1;
        }

        echo json_encode(array(
            "code" => $code,
            "message" => $message,
        ));
        wp_die();
    }
    
    public function ajax_show_document() {        
        $api_document = new Gestion_Page_GRU_Api_Document(array(
            'doc_id' => $_POST['doc_id']
        ));

        $result = $api_document->get_infos_document();
        $user_wp = wp_get_current_user();
        $id_individu = get_user_meta($user_wp->data->ID, 'gru_id')[0];
        $upload_dir = __DIR__ . "/../../assets/data/visualiser/$id_individu";
        
        if(!is_dir($upload_dir)) {
            mkdir($upload_dir);
        } else {
            $files = glob($upload_dir.'/*');
            foreach ($files as $file) {
                if (is_file($file)) {                    
                    unlink($file);
                }
            }
        }
        
        $file_name = $result->data->filename;
        file_put_contents("$upload_dir/$file_name", base64_decode($result->data->file));
        $plugin_base_name = explode('/', plugin_basename(__FILE__))[0];
        $path_file = plugins_url("$plugin_base_name/assets/data/visualiser/$id_individu/");
        
        echo json_encode(array(
            'url_visu' => $path_file . $result->data->filename,
        ));
        
        wp_die();
    }
}

new Gestion_Page_GRU_Api();
