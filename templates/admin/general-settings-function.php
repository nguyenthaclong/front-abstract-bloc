<div id="gestion-page-gru-content" class="admin-gru-content">
    <div class="col-lg-12">
        <div id="gestion-gru-ajax-msg" class="row">
            <div class="updated notice" style="display:none;">
                <p class="msg"></p>
            </div>
            <div class="error notice" style="display:none;">
                <p class="msg"></p>
            </div>
            <div class="infos notice" style="display:none;">
                <p class="msg"></p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h1>Gestion page GRU - Gestion des fonctions</h1>            
            </div>
        </div>       
        <br/>
        <div id="function-order-container" class="row mt-2">
            <div class="col-lg-6">              
                <h3 class="ml-3"> Arborescence des pages</h3>
                <div class="row">       
                    <div id="parent-function-order" class="col-lg-11 function-order-content box-shadow-gru">        
                        <input type="hidden" id="current_function"/>
                        <div class="loader" style="display:none;">
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                        <div id="parent-function-order-content">
                                 <?php do_action("get_all_pages"); ?> 
                        </div>
                        <div class="row save-list-function">
                            <div class="col-lg-4">                          
                                <button id="save-function" class="gestion-page-gru-button"><i class="fa fa-save float-left"></i> Enregistrer </button>       
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-1 function-order-content-icon">
                        <i class="fa fa-chevron-circle-right"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <h3 class="ml-3"> Fonctions </h3>
                <div class="row">
                    <div id="infos-function-order-content" class="col-lg-11 function-order-content box-shadow-gru">
                        <div class="col-lg-12">
                            <div id="page_active_libelle" class="row  border-bottom hidden"> 
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <?php do_action("get_all_functions"); ?>   
                                </div>
                                <div id="add_function" class="col-lg-3 hidden">
                                    <button id="btn_add_function" class="gestion-page-gru-button">
                                        <i class="fa fa-check-circle float-left"></i>Ajouter à la page 
                                    </button>   
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">                                    
                                    <ol id="selectable-pages-for-function">
                                         <?php do_action("gestion_page_gru_get_function_content"); ?>   
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>     
</div>