<div id="gestion-page-gru-content" class="admin-gru-content">
    <div class="col-lg-12">
        <div id="gestion-gru-ajax-msg" class="row">
            <div class="updated notice" style="display:none;">
                <p class="msg"></p>
            </div>
            <div class="error notice" style="display:none;">
                <p class="msg"></p>
            </div>
            <div class="infos notice" style="display:none;">
                <p class="msg"></p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h1>Gestion page GRU - Gestion des menus</h1>            
            </div>
        </div>       
        <br/>
        <div id="menu-order-container" class="row mt-2">
            <div class="col-lg-6">              
                <h3 class="ml-3"> Arborescence des menus</h3>
                <div class="row">       
                    <div id="parent-menu-order" class="col-lg-11 menu-order-content box-shadow-gru">        
                        <input type="hidden" id="current_menu"/>
                        <div class="loader" style="display:none;">
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                        <div id="parent-menu-order-content">
                            <?php do_action("gestion_page_gru_get_menus_content"); ?>
                        </div>
                        <div class="row save-list-menu">
                            <div class="col-lg-4">                          
                                <button id="save-menu" class="gestion-page-gru-button"><i class="fa fa-save float-left"></i> Enregistrer </button>       
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-1 menu-order-content-icon">
                        <i class="fa fa-chevron-circle-right"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <h3 class="ml-3"> Informations </h3>
                <div class="row">
                    <div id="infos-menu-order-content" class="col-lg-11 menu-order-content box-shadow-gru">
                        <div class="col-lg-12">
                            <div class="row  border-bottom">                                
                                <h4>Menu</h4>
                            </div>
                            <div class="row">
                                <div class="col-lg-2">Libellé</div>
                                <div class="col-lg-10">                                    
                                    <input type="text" class="highligth-target-off" id="update_menu_libelle" name="update_menu_libelle"  placeholder="Libellé du menu ..." disabled/>
                                    <input type="hidden"  class="highligth-target-off"  id="update_menu_id" name="update_menu_id"  />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    Page(s) où afficher le menu  
                                </div>
                                <div class="col-lg-3">
                                    <button id="check_all" class="gestion-page-gru-button">
                                        <i class="fa fa-check-circle float-left"></i> Tous cocher 
                                    </button>   
                                </div>
                                <div class="col-lg-3">
                                    <button id="uncheck_all" class="gestion-page-gru-button">
                                        <i class="fa fa-times-circle float-left"></i> Tous décocher 
                                    </button>   
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">                                    
                                    <ol id="selectable-pages-for-menu">
                                        <?php
                                        $pages = get_pages();
                                        foreach ($pages as $page) {
                                            echo '<li data-id="' . $page->ID . '"><i class="fa fa-chevron-circle-right"></i>' . $page->post_title . '</li>';
                                        }
                                        ?>
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="row  border-bottom">       
                                <h4>Sous-menu</h4>
                            </div>
                            <div class="row">
                                <div class="col-lg-2">Libellé</div>
                                <div class="col-lg-10">                                    
                                    <input type="text" id="update_sub_menu_libelle" class="highligth-target-off" name="update_sub_menu_libelle"  placeholder="Libellé du menu ..." disabled />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2">Page cible</div>
                                <div class="col-lg-10">
                                    <select id="select-page-gru" class="gestion-page-gru-select highligth-target-off" name="page-dropdown" disabled>
                                        <option value="">...</option>
                                        <?php
                                        foreach ($pages as $page) {
                                            echo '<option value="' . $page->ID . '" data-link="' . get_page_link($page->ID) . '">' . $page->post_title . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>     
</div>