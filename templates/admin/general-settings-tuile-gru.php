<div id="gestion-page-gru-content" class="admin-gru-content">
    <div class="col-lg-12">
        <div id="gestion-gru-ajax-msg" class="row">
            <div class="updated notice" style="display:none;">
                <p class="msg"></p>
            </div>
            <div class="error notice" style="display:none;">
                <p class="msg"></p>
            </div>
            <div class="infos notice" style="display:none;">
                <p class="msg"></p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h1>Gestion page GRU - Gestion des tuiles GRU</h1>            
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-3">
                        <button id="listing_teleservices" class="gestion-page-gru-button" name="listing_teleservices"><i class="fas fa-sync-alt"></i> Synchroniser les téléservices</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="tuile-container" class="row mt-2">
            <div class="col-lg-12">              
                <div class="row">
                    <div id="tuile-list" class="col-lg-12">      
                        <?php do_action("gestion_page_gru_get_tuile_list_management"); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>   