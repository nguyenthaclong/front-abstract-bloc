<div id="gestion-page-gru-content" class="admin-gru-content">
    <input type="hidden" id="grid-value" name="grid-value" />
    <div class="col-lg-12">
        <div id="gestion-gru-ajax-msg" class="row">
            <div class="updated notice" style="display:none;">
                <p class="msg"></p>
            </div>
            <div class="error notice" style="display:none;">
                <p class="msg"></p>
            </div>
            <div class="infos notice" style="display:none;">
                <p class="msg"></p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h1>Configuration générale - Gestion grille GRU</h1>            
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-3">
                        <button id="add-new-page" class="gestion-page-gru-button "><i class="fa fa-plus float-left"></i> Créer une nouvelle page </button>
                    </div>
                     <div class="col-lg-3">
                        <button id="remove-page" class="gestion-page-gru-button "><i class="fa fa-trash float-left"></i> Supprimer une page </button>
                    </div>
                    <div class="col-lg-3">
                        <button id="import-gru-config" class="gestion-page-gru-button "><i class="fa fa-upload float-left"></i> Importer une configuration</button>
                    </div>
                    <div class="col-lg-3">
                        <button id="export-gru-config" class="gestion-page-gru-button "><i class="fa fa-download float-left"></i> Exporter la configuration du module</button>
                    </div>
                </div>               
                <br/>     
                <br/>
                <div class="row">
                    <div class="col-lg-7">
                        <h3>Grille de la page</h3>
                    </div>
                </div>                
                <div class="row mt-2">
                    <div class="col-lg-2">
                        <label class="ml-3"> Choisir la page à modifier </label>
                    </div>
                    <div class="col-lg-5">
                        <select id="select-page-gru" class="gestion-page-gru-select" name="page-dropdown">
                            <option value="">...</option>
                            <?php
                            $pages = get_pages(array('sort_column' => 'post_title'));
                            foreach ($pages as $page) {
                                $page_link = get_page_link($page->ID);
                                echo '<option value="'.$page->ID.'" data-link="'.$page_link.'">'.$page->post_title.' : '.str_replace(get_home_url(), "", $page_link).'</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-2">
                        <button id="show-page" class="gestion-page-gru-button "><i class="fa fa-search float-left"></i> Voir la page </button>
                    </div>   
                    <div class="col-lg-2">
                        <button type="submit" id="save-page-grid" name="save-page-grid" class="gestion-page-gru-button"><i class="fa fa-save float-left"></i> Enregistrer </button>
                    </div>
                </div>                  
                <br/>     
                <br/>
                <div class="row gestion-page-gru-tabs">
                    <ul class="col-lg-12">
                        <li><a href="#tabs-columns-bootstrap">Éléments grille</a></li>
                        <li><a href="#tabs-gru-blocks">Éléments GRU</a></li>
                        <li><a href="#tabs-cde-functions">Fonctions CDE</a></li>
                    </ul>
                    <div id="tabs-columns-bootstrap">
                        <?php do_action("gestion_page_gru_get_bootstrap_element_list"); ?>
                    </div>
                    <div id="tabs-gru-blocks">
                        <?php do_action("gestion_page_gru_get_tuile_list"); ?>
                    </div>
                    <div id="tabs-cde-functions">
                        <?php do_action("gestion_page_gru_get_cde_function_list"); ?>
                    </div>
                </div>
                <div class="row" style="position: relative;">
                    <div id="cursor-position-drag"></div>
                    <div class="loader" style="display:none;">
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                    <div id="grid-visual" class="col-lg-12">
                        <div class="col-lg-1"><div class="lib-col">Colonne 1</div><div class="content-background"></div></div>
                        <div class="col-lg-1"><div class="lib-col">Colonne 2</div><div class="content-background"></div></div>
                        <div class="col-lg-1"><div class="lib-col">Colonne 3</div><div class="content-background"></div></div>
                        <div class="col-lg-1"><div class="lib-col">Colonne 4</div><div class="content-background"></div></div>
                        <div class="col-lg-1"><div class="lib-col">Colonne 5</div><div class="content-background"></div></div>
                        <div class="col-lg-1"><div class="lib-col">Colonne 6</div><div class="content-background"></div></div>
                        <div class="col-lg-1"><div class="lib-col">Colonne 7</div><div class="content-background"></div></div>
                        <div class="col-lg-1"><div class="lib-col">Colonne 8</div><div class="content-background"></div></div>
                        <div class="col-lg-1"><div class="lib-col">Colonne 9</div><div class="content-background"></div></div>
                        <div class="col-lg-1"><div class="lib-col">Colonne 10</div><div class="content-background"></div></div>
                        <div class="col-lg-1"><div class="lib-col">Colonne 11</div><div class="content-background"></div></div>
                        <div class="col-lg-1"><div class="lib-col">Colonne 12</div><div class="content-background"></div></div>
                    </div>
                    <div id="grid-ghostbar"></div>
                    <div id="grid-page-gru" class="col-lg-12 box-shadow-gru"></div>
                </div>        
            </div>    
        </div>
    </div>     
</div>