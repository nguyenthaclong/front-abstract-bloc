<?php
$pages = get_pages(array('sort_column' => 'post_title'));
$select_opt = "";
foreach ($pages as $page) {
    $page_link = get_page_link($page->ID);
    $select_opt .= '<option value="' . $page->ID . '" data-link="' . $page_link . '"' . $selected . '>' . $page->post_title . ' : ' . str_replace(get_home_url(), "", $page_link) . '</option>';
}
$select_crm_link_forgotten_password = $select_crm_link_create_account = $select_opt;
?>
<div id="gestion-page-gru-content" class="admin-gru-content">
    <form method="post" action="<?php echo $_SERVER["REQUEST_URI"]; ?>">
        <div class="row">
            <div class="col-lg-6">   
                <div class="row mb-2">
                    <div class="col-lg-12">
                        <h3>Configuration GRU (SugarCRM)</h3>          
                    </div>                                
                </div>                   
                <div class="row mb-2">
                    <div class="col-lg-4">                        
                        <label for="gru_url">URL de GRU</label>
                    </div>
                    <div class="col-lg-6">                        
                        <input type="text" name="gru_url" id="gru_url" value="<?php echo $dev_options['gru_url'] ?>" />
                    </div>
                </div>           
                <div class="row mb-2">
                    <div class="col-lg-4">                        
                        <label for="crm_client_id">Client ID</label>
                    </div>
                    <div class="col-lg-6">                                               
                        <input type="text" name="crm_client_id" id="crm_client_id" value="<?php echo $dev_options['crm_client_id'] ?>" />
                    </div>
                </div>             
                <div class="row mb-2">
                    <div class="col-lg-4">                        
                        <label for="crm_client_secret">Client secret</label>
                    </div>
                    <div class="col-lg-6">                        
                        <input type="text" name="crm_client_secret" id="crm_client_secret" value="<?php echo $dev_options['crm_client_secret'] ?>" />
                    </div>
                </div>  

                <div class="row mb-2">
                    <div class="offset-lg-4 col-lg-6">   
                        <input type="button" id="check_connexion_response_button" name="check_connexion_response_button" class="button-primary" value="Tester la connexion GRU" />           
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-lg-10" id="check_connexion_response" style="display:none">   
                        <div class="row" id="check_connexion_response_loading">
                            <div class="offset-lg-1 col-lg-2 fa-3x">
                                <i class="fas fa-cog fa-spin">
                                </i>
                            </div>
                            <div class="col-lg-8">
                                <p class="">Test de la connexion en cours ...</p>
                            </div>
                        </div>
                        <div class="row" id="check_connexion_response_infos">
                            <div class="error notice" style="display:none">
                                <p class="msg"></p>
                            </div> 
                            <div class="updated notice" style="display:none">
                                <p class="msg"></p>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>    
            <div class="col-lg-6">
                <div class="row mb-2">
                    <div class="col-lg-12">
                        <h3>Correspondance des liens</h3>                    
                    </div>                                
                </div>    
                <div class="row mb-2">
                    <div class="col-lg-12">
                        <h4>Gestion du compte</h4>                    
                    </div>                                
                </div> 
                <?php
                echo Admin_Gestion_Page_GRU_Tools::get_row_select_option_link('crm_link_connexion', $link_options['crm_link_connexion'], 'Page de connexion', $select_opt) .
                Admin_Gestion_Page_GRU_Tools::get_row_select_option_link('crm_link_my_account', $link_options['crm_link_my_account'], 'Page mon compte', $select_opt) .
                Admin_Gestion_Page_GRU_Tools::get_row_select_option_link('crm_update_my_account', $link_options['crm_update_my_account'], 'Mise à jour du Compte', $select_opt) .
                Admin_Gestion_Page_GRU_Tools::get_row_select_option_link('crm_link_forgotten_password', $link_options['crm_link_forgotten_password'], 'Mot de passe oublié', $select_opt) .
                Admin_Gestion_Page_GRU_Tools::get_row_select_option_link('crm_link_create_account', $link_options['crm_link_create_account'], 'Pas encore de compte', $select_opt);
                ?>    
                <div class="row mb-2">
                    <div class="col-lg-12">
                        <h4>Gestion des foyers</h4>                    
                    </div>                                
                </div>   
                <?php
                echo Admin_Gestion_Page_GRU_Tools::get_row_select_option_link('crm_link_home_foyer', $link_options['crm_link_home_foyer'], 'Tableau de bord foyer', $select_opt) .
                Admin_Gestion_Page_GRU_Tools::get_row_select_option_link('crm_link_create_foyer', $link_options['crm_link_create_foyer'], 'Création Foyer', $select_opt) .
                Admin_Gestion_Page_GRU_Tools::get_row_select_option_link('crm_link_update_foyer', $link_options['crm_link_update_foyer'], 'Modification Foyer', $select_opt);
                ?>  
                <div class="row mb-2">
                    <div class="col-lg-12">
                        <h4>Gestion des associations</h4>                    
                    </div>                                
                </div>   
                <?php
                echo Admin_Gestion_Page_GRU_Tools::get_row_select_option_link('crm_link_home_association', $link_options['crm_link_home_association'], 'Tableau de bord Association', $select_opt) .
                Admin_Gestion_Page_GRU_Tools::get_row_select_option_link('crm_link_create_association', $link_options['crm_link_create_association'], 'Création Association', $select_opt) .
                Admin_Gestion_Page_GRU_Tools::get_row_select_option_link('crm_link_update_association', $link_options['crm_link_update_association'], 'Modification Association', $select_opt);
                ?>
                <div class="row mb-2">
                    <div class="col-lg-12">
                        <h4>Gestion des sociétés</h4>                    
                    </div>                                
                </div>   
                <?php
                echo Admin_Gestion_Page_GRU_Tools::get_row_select_option_link('crm_link_home_societe', $link_options['crm_link_home_societe'], 'Tableau de bord Société', $select_opt) .
                Admin_Gestion_Page_GRU_Tools::get_row_select_option_link('crm_link_create_societe', $link_options['crm_link_create_societe'], 'Création Société', $select_opt) .
                Admin_Gestion_Page_GRU_Tools::get_row_select_option_link('crm_link_update_societe', $link_options['crm_link_update_societe'], 'Modification Société', $select_opt);
                ?> 
                <div class="row mb-2">
                    <div class="col-lg-12">
                        <h4>Gestion des demandes</h4>                    
                    </div>                                
                </div>   
                <?php
                echo Admin_Gestion_Page_GRU_Tools::get_row_select_option_link('crm_link_show_request', $link_options['crm_link_show_request'], 'Page visualisation demande', $select_opt) .
                Admin_Gestion_Page_GRU_Tools::get_row_select_option_link('crm_link_show_request_chat', $link_options['crm_link_show_request_chat'], 'Page fil de discussion demande', $select_opt) .
                Admin_Gestion_Page_GRU_Tools::get_row_select_option_link('crm_link_create_request', $link_options['crm_link_create_request'], 'Page création demande', $select_opt) .
                Admin_Gestion_Page_GRU_Tools::get_row_select_option_link('crm_link_update_request', $link_options['crm_link_update_request'], 'Page mise à jour demande', $select_opt);
                ?>
                <div class="row mb-2">
                    <div class="col-lg-12">
                        <h4>Gestion des individus</h4>                    
                    </div>                                
                </div>   
                <?php
                echo Admin_Gestion_Page_GRU_Tools::get_row_select_option_link('crm_link_create_individual', $link_options['crm_link_create_individual'], 'Page de création d\'un individu', $select_opt) .
                Admin_Gestion_Page_GRU_Tools::get_row_select_option_link('crm_link_update_individual', $link_options['crm_link_update_individual'], 'Page mise à jour d\'un individu', $select_opt);
                ?>                
                <div class="row mb-2">
                    <div class="col-lg-12">
                        <h4>Gestion des documents</h4>                    
                    </div>                                
                </div>  

                <?php
                echo Admin_Gestion_Page_GRU_Tools::get_row_select_option_link('crm_link_show_document', $link_options['crm_link_show_document'], 'Page de visualisation de document', $select_opt);
                ?>
            </div>                    
        </div>
        <div class="row mt-4">
            <div class="offset-lg-6 col-lg-5">                
                <input type="submit" name="update_gru_parameters" class="button-primary" value="Sauvegarder" />
            </div>
        </div>
    </form>
</div>

<hr>
