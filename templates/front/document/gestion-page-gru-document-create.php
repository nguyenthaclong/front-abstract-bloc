<?php
/**
 * Classe qui génère le HTML permettant l'affichage du shortcode [document_ajouter]
 * @package
 * @author
 * @version
 * @abstract
 * @copyright
 */
class document_ajouter_HTML {
	
	var $liste_types_piece;
	var $infos_coffre;
    
	/**
	 * Contructeur de la classe creer_entite_HTML
	 * @param array $array_display
	 * @param type $code_entite
	 * @param type $label_entite
	 */
	function __construct ( $liste_types_piece, $infos_coffre ) {		
		$this->liste_types_piece = $liste_types_piece;
		$this->infos_coffre = $infos_coffre;
	}
	
	/**
	 * Fonction qui génère le HTML
	 * @return string
	 */
	function genere_HTML ( ) {

		# refaire le formulaire à la main
		# input text nom du document
		# construire une liste déroulante sur la liste des types
		# bouton input
		# quid des valeurs max etc ?
		# implémenter le traitement de la validation dans le shortcode
		# implémenter le WS dans l"API

		$liste_types = $this->liste_types_piece;
		$infos_coffre = $this->infos_coffre;

		$types_mime_autorises = array (
			'0'	=> 'application/pdf',
			'1'	=> 'image/png',
			'2'	=> 'image/jpeg',
			'3'	=> 'image/jpg',
			'4'	=> 'image/gif',
			'5'	=> 'application/force-download',
		);

		$html_genere = '<script>
			
			msgAjouterDocument = "Tous les champs sont obligatoires pour ajouter un fichier dans le porte-documents sécurisé";
			msgFileSize = "Le fichier dépasse la taille autorisée";
			msgFileType = "Ce type de fichier n\'est pas autorisé";
			
			function checkAjouterDocument ( ) {

				if ( ! checkChamps ( ) ) {
					alert ( msgAjouterDocument );
					return false;
				} else if ( ! checkFileSize ( ) ) {
					alert ( msgFileSize );
					return false;
				} else if ( ! checkFileType ( ) ) {
					alert ( msgFileType );
					return false;
				} else {
					return true;
				}
			}

			function checkFileSize ( ) {
			
				var taille_max_document = document.getElementsByName ( "MAX_FILE_SIZE" )[0].value;

				var fichier = document.getElementById ( "file_ajouter_document" );

				var taille_document = fichier.files[0].size;

				if ( taille_document > taille_max_document ) {
					fichier.style.borderColor="#FF0000";	
					return true;
				} else {
					fichier.style.borderColor="#008000";
					return true;
				}
			}

			function checkFileType ( ) {

				var types_mime_autorises = [ "unknown/pdf", "application/pdf", "application/force-download", "application/zip", "image/png", "image/jpeg", "image/jpg", "image/gif" ];	
				var fichier = document.getElementById ( "file_ajouter_document" );
				var type_mime_fichier = fichier.files[0].type;
				
				if ( -1 == types_mime_autorises.indexOf ( type_mime_fichier ) ) {
					fichier.style.borderColor="#FF0000";	
					return false;
				} else {
					fichier.style.borderColor="#008000";	
					return true;
				}
			}
	
			function checkChamps ( ) {
			
				tabChampsVides = Array();
				tabChampsVides.length = 0;
				var liste_champs = ["id_type_piece", "file_ajouter_document"];
				var result = true;

				for ( var i = 0; i < liste_champs.length; i++ ) {

					var obj = document.getElementById ( liste_champs[i] );
					if ( obj ){
						switch ( obj.type ){
							case "select-one":
								if ( obj.selectedIndex == 0 || obj.options[ obj.selectedIndex ].text == "" ){
									obj.style.borderColor="#FF0000";
									result = false;
								} else {
									obj.style.borderColor="#008000";
								}
								break;
							case "text":
								if ( obj.value == "" ) {
									obj.style.borderColor="#FF0000";
									result = false;
								} else {
									obj.style.borderColor="#008000";
								}
								break;
							case "file":
								if ( 0 == obj.files.length ) {
									obj.style.border="red solid 1px";
									result = false;
                                                                } else {
									obj.style.border="green solid 1px";
                                                                }
                                                                break;	
							default:
						}
					}
				}
				return result;
			}
		</script>';

		$html_genere .= '<form name="ajouter_document" id="ajouter_document" method="post" action="" enctype="multipart/form-data">';
		$html_genere .= '<input type="hidden" name="MAX_FILE_SIZE" value="'. intval ( $infos_coffre['taille_max_fichier'] ) * 1024 * 1024 .'">';
		$html_genere .= '<label for="file_ajouter_document">Sélectionnez un fichier *</label><br>';
		$html_genere .= '<input type="file" name="file_ajouter_document" id="file_ajouter_document">';
		
		$html_genere .= '<label for="id_type_piece">Catégorie de la pièce *</label>';
		$html_genere .= '<select name="id_type_piece" id="id_type_piece">';
		$html_genere .= '<option value="">Choisir une catégorie</option>';
		
		foreach ( $liste_types as $type ) {

				$html_genere .= '<option value="'.$type['id'].'">'.$type['nom'].'</option>';
		}
		$html_genere .= '</select>';
		
		$html_genere .= '<input type="submit" name="submit_ajouter_document" value="Ajouter" onclick="return checkAjouterDocument();">';
		$html_genere .= '</form>';
		
		return $html_genere;
	}
}
