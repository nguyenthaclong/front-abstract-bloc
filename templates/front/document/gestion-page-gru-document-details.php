<?php

class document_details_HTML {

    var $infos_document;

    /**
     * Constructeur de la classe liste_champ_HTML
     * @param array $array_display
     * @param type $ligne_code
     */
    function __construct($infos_document) {
        $this->infos_document = $infos_document; // la liste des documents du coffre
        //$this->thumbnails    = $array_thumbnails; // les miniatures des documents (si on les affiche d'une manière ou d'une autre sur la liste)
    }

    /**
     * Fonction qui génère le HTML de la classe liste_champ_HTML
     * @return string
     */
    function genere_HTML() {
        $infos_document = $this->infos_document;
        $html_genere .= '<table class="table-details-documents">';
        $html_genere .= '<tr><td><img alt="Image non disponible pour ce type de fichier" src="data:' . $infos_document->data->liste_mimes . ';base64,' . $infos_document->data->file . '" title="' . $infos_document->data->filename . '"></td>';
        $html_genere .= '<td><h3>' . $infos_document->data->nom_doc . '</h3>';
        $html_genere .= '<div>Catégorie du document : ' . $infos_document->data->type_piece_name . '</div>';
        $html_genere .= '<div>Ajouté le : ' . date("d/m/Y H:i", strtotime($infos_document->data->date_depot)) . '</div>';
        $html_genere .= '<div>Valide jusqu\'au : ' . date("d/m/Y H:i", strtotime($infos_document->data->date_peremption)) . '</div>';
        $html_genere .= '<div>Taille du fichier : ' . $infos_document->data->file_size . ' Octets</div>';
        $html_genere .= '<div>Type de fichier : ' . $infos_document->data->type_mime . '</div>';
        $html_genere .= '</td></tr>';
        $html_genere .=' </table>';

        return $html_genere;
    }

}
