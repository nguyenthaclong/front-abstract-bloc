<?php

class documents_liste_HTML {

    var $liste_documents;
    var $type_entite;
    var $tab_fichiers;

    /**
     * Constructeur de la classe liste_champ_HTML
     * @param array $array_display
     * @param type $ligne_code
     */
    function __construct($liste_documents, $type_entite, $tab_fichiers) {

        $this->liste_documents = $liste_documents; // la liste des documents du coffre
        //$this->thumbnails    = $array_thumbnails; // les miniatures des documents (si on les affiche d'une manière ou d'une autre sur la liste)
        $this->type_entite = $type_entite;
        $this->tab_fichiers = $tab_fichiers;
    }

    /**
     * Function qui génère le HTML de la classe liste_champ_HTML
     * @return string
     */
    function genere_HTML() {
        $type_entite = $this->type_entite;
        $tab_pieces = $this->liste_documents;

        $permalink = get_permalink();
        
        $context = Admin_Gestion_Page_GRU_Tools::get_context();
        $user_wp = wp_get_current_user();
        $id_individu = get_user_meta($user_wp->data->ID, 'gru_id')[0];
        $id_entite = ($type_entite !== "CDE_INDIVIDUS") ? $context['id_entite'] : $id_individu;

        $tbody = "";
        $link_options = get_option(Admin_Gestion_Page_GRU_Tools::links_pages_option);
        foreach ($tab_pieces as $piece) {
            $tbody .= <<<HTML
                <tr id="{$piece->id}">
                    <td>{$piece->nom_doc}</td>
                    <td>{$piece->type_piece_name}</td>
                    <td>{$piece->date_depot}</td>
                    <td>{$piece->date_peremption}</td>
                    <td>                                    
                        <span class="gru-icon-button" onclick="$.grufront.show_document('{$piece->id}')"><i class="fa fa-search" title="Voir le document"></i></span>
                        <span class="gru-icon-button" onclick="$.grufront.download_document('{$piece->id}')"><i class="fa fa-download" title="Télécharger"></i></span>
                        <span class="gru-icon-button" onclick="$.grufront.validation_suppression_document('{$piece->id}','{$piece->nom_doc}')"> <i class="fa fa-trash" title="Supprimer le document"></i></span>
                    </td>
                </tr>
HTML;
        }

        $html_add_document = "";
        $api_document = new Gestion_Page_GRU_Api_Document(array(
            'parent_type' => $type_entite,
            'parent_id' => $id_entite
        ));
        $result = $api_document->get_list_type_pieces();
        $infos_portedocs = $api_document->get_infos_portedocs();

        if (isset($infos_portedocs->data->liste_mimes)) {
            $select_pieces = "<select id='list_piece' class='list_piece'><option id='null' value=''>Sélectionnez un type de piece</option>";
            foreach ($result->data as $piece) {
                $select_pieces .= "<option value='{$piece->id}'>{$piece->nom}</option>";
            }
            $select_pieces .= "</select>";

            $accept = implode(",", $infos_portedocs->data->liste_mimes);
            $html_add_document = <<<HTML
                        <span class="add_document"><i class="fa fa-plus ml-2 mr-2"></i>Ajouter un document</span>
                        <div class="add_document_popup" style="display:none;">
                            <div class="row mb-3 mt-4">
                                <div class="col-lg-3">
                                    <label> Type de pièce </label>
                                </div>
                                <div class="col-lg-9">
                                    $select_pieces
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3">
                                </div>
                                <div class="col-lg-9">
                                    <input type="file" id="upload_document" class="upload_document float-left" accept="$accept"/>
                                </div>
                            </div>
                        </div>
HTML;
        } else {
            Admin_Gestion_Page_GRU_Tools::set_gru_msg("Impossible de récupérer les types de documents acceptés", 'error');
        }

        $html_genere = <<<HTML
            <form name="form_coffre_fort" id="form_coffre_fort" method="post" action="">
                <input type="hidden" name="uuid_document" value="0" id="uuid_document">
                <table class="table-liste-documents table table-striped">
                    <thead>
                        <tr>
                            <th>Nom</th>
                            <th>Type de Pièce</th>
                            <th>Date d'ajout</th>
                            <th>Valable jusqu'au</th>
                            <th></th>
                        </tr>
                    </thead>    
                    <tbody>$tbody</tbody>
                </table>
            </form>
            <div class="row">
                <div class="col-lg-12">
                    $html_add_document
                </div>
            </div>
HTML;

        $html_genere .= $create_new;
        return $html_genere;
    }

}
