<?php

/**
 * Classe qui génère le HTML permettant l'affichage des infos du coffre fort
 */
class infos_coffre_HTML {

    var $infos_coffre;

    /**
     * Constructeur de la classe liste_champ_HTML
     * @param array $array_display
     * @param type $ligne_code
     */
    function __construct($infos_coffre) {
        $this->infos_coffre = $infos_coffre; 
    }

    /**
     * Fonction qui génère le HTML de la classe liste_champ_HTML
     * @return string
     */
    function genere_HTML() {
        $infos_coffre = $this->infos_coffre;
        $infos_coffre['pourcentage_utilisation'] = round(( floatval(str_replace(',', '.', $infos_coffre->data->taille_coffre)) / floatval($infos_coffre->data->taille_max_coffre) ) * 100, 0);
        $html_genere = $infos_coffre;
        foreach ($infos_coffre as $nom => $info) {
            $html_genere = str_replace('{' . $nom . '}', $info, $html_genere);
        }
        return $html_genere;
    }

}
