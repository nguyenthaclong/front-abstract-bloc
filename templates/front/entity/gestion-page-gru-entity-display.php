<?php

class entity_display_HTML {

    var $infosEntite;

    function __construct($infosEntite) {
        $this->infosEntite = $infosEntite;
    }

    function genere_HTML() {
        $context = Admin_Gestion_Page_GRU_Tools::get_context();
        switch ($context['type_entite']) {
            case "CDE_FOYERS":
                $type_entite = "Foyer";
                break;
            case "CDE_ASSOCIATIONS":
                $type_entite = "Association";
                break;
            case "CDE_SOCIETES":
                $type_entite = "Société";
                break;
            case "CDE_COMMUNES":
                $type_entite = "Commune";
                break;
            case "CDE_COLLEGES":
                $type_entite = "Collège";
                break;
            case "CDE_INTERCOMMUNALITES":
                $type_entite = "Intercommunalité";
                break;
            case "CDE_AUTREORGPUBS":
                $type_entite = "Autre Organisme Public";
                break;
        }
        $address = stripslashes("{$this->infosEntite->rue1} <br/> {$this->infosEntite->code_postal}  {$this->infosEntite->ville}  {$this->infosEntite->pays}");
        $html = <<<HTML
            <div class="element-gru element-gru-notif box-shadow-gru">
                <div class="element-gru-header">
                    <div class="title">
                        <span class="gestion-page-gru-element-title">$type_entite</span>
                    </div>
                </div>
                <div class="grid-element-gru-content">
                    <div class="multi-content">
                        <i class="fa fa-home"></i>
                    </div>
                    <div class="desc-plus">$address</div>
                </div>
            </div>
HTML;
        
        return $html;
    }

}
