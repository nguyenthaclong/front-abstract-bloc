<?php

class entity_create_HTML {

    private $params;
    private $fields;

    /**
     * Contructeur de la classe creer_entite_HTML
     * @param array $array_display
     * @param type $code_entite
     * @param type $label_entite
     */
    function __construct($params, $type_entite) {
        // On récupère ce qu'on a passé en paramètre au préalable
        $this->params = $params;
        $this->fields = $type_entite;
    }

    /**
     * Fonction qui génère le HTML
     * @return string
     */
    function genere_HTML() {

        $url_js = explode('/', plugin_dir_path(__DIR__))[0] . "/wp-content/plugins/gestion-page-gru/assets/js/front/verif_adresse.js";
        $bootstrap_js = "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js";
        wp_register_script('verif_adresse', $url_js);
        wp_enqueue_script('verif_adresse');
        wp_register_script('bootstrap_js', $bootstrap_js);
        wp_enqueue_script('bootstrap_js');
        
        $context = Admin_Gestion_Page_GRU_Tools::get_context();
        
        $page_id = get_the_ID();
        $link_options = get_option(Admin_Gestion_Page_GRU_Tools::links_pages_option);
        switch ($context['type_entite']) {
            case "CDE_FOYERS":
                $title = ($page_id == $link_options['crm_link_update_foyer']) ? "MODIFIER UNE ENTITÉE" : "CRÉER UNE ENTITÉE";
                break;
            case "CDE_ASSOCIATIONS":
                $title = ($page_id == $link_options['crm_link_update_association']) ? "MODIFIER UNE ENTITÉE" : "CRÉER UNE ENTITÉE";
                break;
            case "CDE_SOCIETES":
                $title = ($page_id == $link_options['crm_link_update_societe']) ? "MODIFIER UNE ENTITÉE" : "CRÉER UNE ENTITÉE";
                break;
        }

        $html_genere = <<<HTML
            <script type="text/javascript">
                var fieldRequired = Array();
                var fieldDescription = Array();

                function formCheck( formobj ){
                        var alertMsg = "Les champs suivants doivent être remplis :";
                        var l_Msg = alertMsg.length;
                        for ( var i = 0; i < fieldRequired.length; i++ ){
                                var obj = document.getElementById ( fieldRequired[i] );
                                if ( obj ){
                                        switch ( obj.type ){
                                                case "select-one":
                                                        if ( obj.selectedIndex == -1 || obj.options[ obj.selectedIndex ].text == "" ){
                                                                alertMsg += " - " + fieldDescription[i] + "";
                                                        }
                                                        break;
                                                case "select-multiple":
                                                        if ( obj.selectedIndex == -1 ){
                                                                alertMsg += " - " + fieldDescription[i] + "";
                                                        }
                                                        break;
                                                case "text":
                                                case "textarea":
                                                if ( obj.value == "" || obj.value == null ){
                                                        alertMsg += " - " + fieldDescription[i] + "";
                                                }
                                                break;
                                                default:
                                        }
                        if ( obj.type == undefined ){
                                                var blnchecked = false;
                                                for ( var j = 0; j < obj.length; j++ ){
                                                        if ( obj[j].checked ){
                                                                blnchecked = true;
                                                        }
                                                }
                                                if ( !blnchecked ){
                                                        alertMsg += " - " + fieldDescription[i] + "";
                                                }
                                        }
                                }
                        }
                        if ( alertMsg.length == l_Msg ){
                                return true;
                        } else {
                                alert ( alertMsg );
                                return false;
                        }
                }

                function addRequired ( name, label ) {
                        fieldRequired.push ( name );
                        fieldDescription.push ( label );
                }
            </script>
                


            <!-- MODALE DE GESTION DES ADRESSES -->
            <div id="verifAdresseModal" class="modal" tabindex="-1" role="dialog" aria-labelledby="verifAdresseModal" aria-hidden="true" >
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button class="close" type="button" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 id="titre_modal" class="modal-title">TITRE</h4>
                        </div>
                        <div class="modal-body">
                            <span id="error_form" class="error">&nbsp;</span>
                            <span id="process_form">&nbsp;</span>
                            <form id="verif_adresse" action="#" method="get" role="form">
                                <input type="hidden" value="" id="adresse_id">
                                <div id="corp_modal">
                                    &nbsp;
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <input id="close_modal" class="button" type="button" value="Annuler" title="Annuler" onclick="jQuery('#verifAdresseModal').hide();">
                            <input id="submit_modal" class="button" type="button" value="Sauvegarder" title="Sélectionner" onclick="save_modal();">
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mt-4">
                <div class="col-lg-12">
                    <h3>$title</h3>                
                </div>
            </div>            
            <div class="row">
                <form id='creer_entite' class='col-lg-12 creer_entite' name='creer_entite' method='POST' action=''>
                    <input type='hidden' id='type_entite' name='type_entite' value='{$this->params['type']}'>
                    <input type='hidden' id='sauver_entite' name='sauver_entite' value='sauver'>
HTML;


        // Pour chacun des champs de l'entité
        foreach ($this->fields as $field) {
            $html_genere .= Admin_Gestion_Page_GRU_Tools::displayChamp($field);
        }

        $url_retour = get_permalink(wp_get_post_parent_id(get_the_ID()));

        if (isset($context['retour_link'])) {
            $referrer_location = $context['retour_link'];
        } else {
            $referrer_location = '';
        }
        $html_genere .= <<<HTML
                    <br/>
                    <a href="$referrer_location" class="button btn-secondaire" >Annuler</a>
                    <input type='button' class='button' name='submit_fin' id='submit_fin' value='Valider' onclick='if (formCheck(this)) { document.getElementById("creer_entite").submit(); document.getElementById("submit_fin").disabled=true;}'>
                </form>      
        </div>
HTML;

        return $html_genere;
    }

}
