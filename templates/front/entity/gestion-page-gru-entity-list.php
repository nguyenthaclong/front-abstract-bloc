<?php

# Classe qui génère le HTML pour le shortcode cde_entite_liste_instances

class entity_list_HTML {

    private $liste_entites;
    private $type_entite;
    private $type;
    private $nb_max;

    public function __construct($liste_entites, $params) {
        # La liste des entités liées à l'individu connecté, retournée par le WS cde_liaisons_objets
        $this->liste_entites = $liste_entites->relations;

        $this->type_entite = $liste_entites->objet_type;

        $this->type = $params['type_entite'];
        $this->nb_max = $params['nb_max'];
    }

    public function genere_HTML() {
        $link_options = get_option(Admin_Gestion_Page_GRU_Tools::links_pages_option);
        $url_new = $label_new = $url_home = $url_update = "";
        switch ($this->type) {
            case 'CDE_FOYERS':
                $label_new = "Créer un Foyer";
                $url_new = get_page_link($link_options['crm_link_create_foyer']);         
                $url_home = get_page_link($link_options['crm_link_home_foyer']);       
                $url_update = get_page_link($link_options['crm_link_update_foyer']);
                break;

            case 'CDE_ASSOCIATIONS':
                $label_new = "Créer une Association";
                $url_new = get_page_link($link_options['crm_link_create_association']);         
                $url_home = get_page_link($link_options['crm_link_home_association']);       
                $url_update = get_page_link($link_options['crm_link_update_association']);
                break;

            case 'CDE_SOCIETES':
                $label_new = "Créer une Société";
                $url_new = get_page_link($link_options['crm_link_create_societe']);         
                $url_home = get_page_link($link_options['crm_link_home_societe']);       
                $url_update = get_page_link($link_options['crm_link_update_societe']);
                break;

            case 'CDE_COLLEGES':
                $label_new = "Créer un Collège";
                $url_new = get_page_link($link_options['crm_link_create_college']);         
                $url_home = get_page_link($link_options['crm_link_home_college']);       
                $url_update = get_page_link($link_options['crm_link_update_college']);
                break;

            case 'CDE_COMMUNES':
                $label_new = "Créer une Commune";
                $url_new = get_page_link($link_options['crm_link_create_commune']);         
                $url_home = get_page_link($link_options['crm_link_home_commune']);       
                $url_update = get_page_link($link_options['crm_link_update_commune']);
                break;
            case 'CDE_INTERCOMMUNALITES':
                $label_new = "Créer une Intercommunalité";
                $url_new = get_page_link($link_options['crm_link_create_intercommunalite']);         
                $url_home = get_page_link($link_options['crm_link_home_intercommunalite']);       
                $url_update = get_page_link($link_options['crm_link_update_intercommunalite']);
                break;

            case 'CDE_AUTREORGPUBS':
                $label_new = "Créer un Autre Organisme Public";
                $url_new = get_page_link($link_options['crm_link_create_autreorgpubs']);         
                $url_home = get_page_link($link_options['crm_link_home_autreorgpubs']);       
                $url_update = get_page_link($link_options['crm_link_update_autreorgpubs']);
                break;
        }


        $create_new = ($url_new == "" ) ? "" : <<<HTML
            <div class="row">
                <div class="col-lg-12">
                    <a href="$url_new" ><i class="fa fa-plus ml-2 mr-2"></i>$label_new</a>
                </div>
            </div>
HTML;
        $entete = ($this->liste_entites == NULL) ? "<tr class='no-content'><th>Aucune données à afficher</th><th></th></tr>" : "<tr><th>Libellé</th><th></th></tr>";
        $tbody = "";
        $retour_url = get_permalink();
        Admin_Gestion_Page_GRU_Tools::add_to_context(array("retour_link" => $retour_url));
        if (is_array($this->liste_entites)) {
            foreach ($this->liste_entites as $key => $entite) {
                if ('1' === $entite->droit_connexion) {
                    $context = json_encode(array('type_entite' => $this->type_entite, 'id_entite' => $entite->objet_id));
                    $tbody .= <<<HTML
                        <tr id="{$entite->objet_id}">
                            <td>{$entite->objet_nom}</td>
                            <td>
                                <span class="gru-icon-button" onclick='$.grufront.set_context($context, function() { window.location = "$url_home"})'><i class="fa fa-search" title="Voir l'entité"></i></span>
                                <span class="gru-icon-button" onclick='$.grufront.set_context($context, function() { window.location = "$url_update"})'><i class="fa fa-pen" title="Modifier l'entité"></i></span>
                                <span class="gru-icon-button" onclick="$.grufront.entity_delete_validation('{$entite->objet_id}','{$this->type_entite}','{$entite->objet_nom}')"> <i class="fa fa-trash" title="Supprimer l'entité"></i></span>
                            </td>
                        </tr>
HTML;
                }
            }
        }



        $html_genere = <<<HTML
            <table class="liste_entites table table-striped">
                <thead>
                    $entete
                </thead>
                <tbody>
                    $tbody
                </tbody>
            </table>
            
            $create_new
HTML;
        return $html_genere;
    }

}
