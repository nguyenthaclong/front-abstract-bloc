<?php

class request_form_display_step_HTML {

    var $mode;
    var $nom_demande;
    var $numero_demande;
    var $nom_demandeur;
    var $nom_entite;
    var $nom_beneficiaire;
    var $nom_service;
    var $brouillon;
    var $nom_etape;
    var $numero_etape;
    var $nombre_etapes;
    var $texte_aide_etape;
    var $lien_externe_etape;
    var $label_lien_externe_etape;
    var $formulaire_etape;
    var $footer_html;

    function __construct($step, $mode) {
        $infos_etape = $step->data;
        $this->mode = $mode;

        $this->nom_demande = $infos_etape->nom_demande;
        $this->numero_demande = $infos_etape->num_demande;
        $this->nom_demandeur = $infos_etape->nom_demandeur;
        $this->nom_entite = $infos_etape->nom_entite;
        $this->nom_beneficiaire = $infos_etape->nom_beneficiaire;
        $this->nom_service = $infos_etape->nom_service;
        $this->brouillon = $infos_etape->brouillon;

        $this->nom_etape = $infos_etape->nom_etape;
        $this->numero_etape = $infos_etape->num_etape;
        $this->nombre_etapes = $infos_etape->nb_etapes;
        $this->texte_aide_etape = $infos_etape->texte_aide;
        $this->lien_externe = $infos_etape->lien_externe;
        $this->label_lien_externe = $infos_etape->label_lien_externe;

        $this->formulaire_etape = $infos_etape->form_html;
        $this->footer_html = $infos_etape->footer_html;
    }

    function genere_HTML() {
        $context = Admin_Gestion_Page_GRU_Tools::get_context();
        if (isset($context['retour_link'])) {
            $referrer_location = $context['retour_link'];
        } else {
            $referrer_location = '';
        }

        $nom_demande = mb_strtoupper($this->nom_demande, 'UTF-8');
        $html_genere = <<<HTML
        <div class="container_formulaire">
            <section class="panel">
                <div class="panel-body">
                <h4 class="return_link">
                    <span class="gru-icon-button" onclick="$.grufront.history_back('$referrer_location')">
                        <i class="fas fa-long-arrow-alt-left"></i>&nbsp;&nbsp;  Retour
                    </span>
                </h4>
                <br/>
                <div class="body_form">
                    <div class="head_form">
                        <h3 class="nom_demande">$nom_demande</h3>
HTML;

        switch ($this->mode) {
            case "creation" :
                $html_genere .= '<p>Création de la demande N°' . $this->numero_demande;
                break;
            case "modification":
                $html_genere .= '<p>Modification de la demande N°' . $this->numero_demande;
                break;
            default:
                $html_genere .= '<p>Visualisation de la demande N°' . $this->numero_demande;
                break;
        }
        
        $html_genere .= ('oui' == $this->brouillon) ?  ' (Brouillon) </p>' : '</p>';
        $html_genere .= <<<HTML
                        <p>Demandeur : {$this->nom_demandeur}</p>
                        <p>Entité : {$this->nom_entite}</p>
                        <p>Bénéficiaire : {$this->nom_beneficiaire}</p>
                        </div>
                    </div>
                </div>
            </section>
            <div class="panel-group" id="accordion">
                <section class="panel">
                    <div class="panel-body">
                        <div>
                            <div class="panel panel-default" id="step_container">
                                <h4>Étape - {$this->nom_etape} </h4>
HTML;
        

        if ('' != $this->texte_aide_etape) {
            $html_genere .= '<p>' . $this->texte_aide_etape . '</p>';
        }

        if ('' != $this->lien_externe) {
            if ('' != $this->label_lien_externe) {
                $html_genere .= '<p><a target="_blank" href="' . $this->lien_externe . '">' . $this->label_lien_externe . '</a>';
            } else {
                $html_genere .= '<p><a target="_blank" href="' . $this->lien_externe . '">Informations complémentaires </a>';
            }
        }
        $form_step = json_encode($this->formulaire_etape);
        $html_genere .= <<<HTML
                <script type="text/javascript">
                    window.addEventListener("DOMContentLoaded", (event) => {
                        $("#content-step-form").html($form_step);
                    });
                </script>
                <form action="" method="POST" name="FormDmd" id="FormDmd" enctype="multipart/form-data">
                    <input type="hidden" name="save" value="1">
                    <input type="hidden" name="cde_etapes_nb" value="{$this->nombre_etapes}">
                    <div id="content-step-form"></div>
HTML;

        if (1 == $this->numero_etape) {
                $html_genere .= '<input id="default-next-1" class="btn btn-info" type="button" value="SUIVANT" onclick="cde_soumettre_formulaire(\'next\',\'FormDmd\');">';
        } else {

            $html_genere .= '<input id="default-next-1" class="btn btn-info" type="button" value="SUIVANT" onclick="cde_soumettre_formulaire(\'next\',\'FormDmd\');">';
            $html_genere .= '<input id="default-next-1" class="btn btn-info" type="button" value="PRÉCÉDENT" onclick="cde_soumettre_formulaire(\'prev\',\'FormDmd\');">';
        }
        $html_genere . <<<HTML
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
HTML;
        
        if (!empty($this->footer_html)) {
            $html_genere .= '<div>' . $this->footer_html . '</div>';
        }

        return $html_genere;
    }

}
