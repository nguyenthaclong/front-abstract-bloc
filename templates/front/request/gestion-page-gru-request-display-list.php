<?php

class request_display_list_HTML {

    var $liste_demandes;
    var $params;

    function __construct($liste_demandes, $params) {
        $this->liste_demandes = $liste_demandes;
        $this->params = $params;
    }

    function genere_HTML() {
               
        $html_genere = '';
        $context      = Admin_Gestion_Page_GRU_Tools::get_context();
        $link_options = get_option(Admin_Gestion_Page_GRU_Tools::links_pages_option);
        $display_url = get_page_link($link_options['crm_link_show_request']);
        $display_url_chat = get_page_link($link_options['crm_link_show_request_chat']);
        $update_url = get_page_link($link_options['crm_link_update_request']);
        $liste_demandes = array();
        foreach ($this->liste_demandes as $demande) {
            if(isset($this->params['brouillon']) && $this->params['brouillon'] != $demande->brouillon )  {
                continue;
            }
            
            array_push($liste_demandes, $demande);            
        }
        
        
        $tbody = "";
        if (0 < count($liste_demandes)) {
            foreach ($liste_demandes as $demande) {
                
                $icon_provenance = '';
                if ('guichet' === $demande->provenance) {
                    $icon_provenance = '<i class="fa fa-comment-o" title="Guichet"></i>';
                } else {
                    $icon_provenance = '<i class="fa fa-laptop" title="Internet"></i>';
                }


                $date_creation = "";
                if (isset($demande->date_creation) && !empty($demande->date_creation)) {
                    $date = new DateTime($demande->date_creation);
                    if ($date) {
                        $date_creation = $date->format("d/m/Y H:i");
                    }else {
                        $date_creation = $demande->date_creation;
                    }
                }

                $libelle = "N°" . $demande->num_demande . ' - ' . mb_strtoupper($demande->nom_demande);
                $avancement = $demande->pourcentage_avancement . "%";
                $commentaire = $demande->commentaire_usager;

                $actions = <<<HTML
                    <span class="gru-icon-button" onclick='$.grufront.add_to_context({"type_entite" : "{$demande->entite_type}", "id_demande" : "{$demande->demande_id}", "retour_link" : window.location.href}, function() { window.location = "$display_url";})'> <i class="fa fa-search" title="Voir la demande"></i></span>
HTML;
                
                if ((isset($demande->editable) && !empty($demande->editable) && 1 == $demande->editable) || $demande->brouillon == "oui") {                   
                    $actions .= <<<HTML
                        <span class="gru-icon-button" onclick='$.grufront.set_context({"type_entite" : "{$demande->entite_type}", "id_demande" : "{$demande->demande_id}", "retour_link" : window.location.href}, function() { window.location = "$update_url";})'> <i class="fa fa-pencil-alt" title="Modifier la demande"></i></span>                
HTML;
                }


                if($demande->brouillon == "oui") {
                        $libelle .= ' (brouillon)';
                        $actions .= <<<HTML
                            <span class="gru-icon-button" onclick="$.grufront.validation_suppression_brouillon('{$demande->demande_id}','$libelle')"> <i class="fa fa-trash" title="Supprimer la demande"></i></span>
HTML;
                }

                if($demande->having_discussion && isset($context["id_entite"]) && isset($context["type_entite"])) {
                    $actions .= <<<HTML
                    <span class="gru-icon-button" onclick='$.grufront.add_to_context({"type_entite" : "{$context["type_entite"]}",
                                                                                  "id_entite" : "{$context["id_entite"]}",
                                                                                  "id_demande" : "{$demande->demande_id}", 
                                                                                  "retour_link" : window.location.href}, 
                                                                                  function() { window.location = "$display_url_chat";})'> 
                    <i class="fa fa-comment" title="Voir le fil de discussion"></i>
                    </span>
HTML;
                }
                

                $progress_bar = Admin_Gestion_Page_GRU_Tools::get_progress_bar($avancement);
                $tbody .= <<<HTML
                    <tr id="{$demande->demande_id}">
                        <td>$icon_provenance</td>
                        <td>$libelle</td>
                        <td>$date_creation</td>
                        <td>{$demande->statut_name}</td>    
                        <td>                        
                           $progress_bar                    
                        </td>
                        <td>$commentaire</td>
                        <td>$actions</td>
                    </tr>
HTML;
            }
        } else {
            $tbody = '<tr class="no-content"><td colspan=6><h5>Vous n\'avez aucune demande</h5></td></th>';
        }

        $html_genere .= <<<HTML
            <table class="liste_demandes table table-striped">
                <thead>
                <tr>
                    <th> </th>
                    <th> Libellé </th>
                    <th> Date création </th>
                    <th> Status </th>
                    <th> Avancement </th>
                    <th> Commentaire </th>
                    <th> </th>
                </tr>
                </thead>
                <tbody>
                    $tbody
                </tbody>
            </table>
HTML;

        return $html_genere;
    }

}
