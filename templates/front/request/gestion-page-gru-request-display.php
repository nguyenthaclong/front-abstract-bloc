<?php

class request_display_HTML {

    var $nom_demande;
    var $numero_demande;
    var $nom_demandeur;
    var $nom_entite;
    var $nom_beneficiaire;
    var $nom_service;
    var $brouillon;
    var $etapes;
    var $nom_etape;
    var $numero_etape;
    var $nombre_etapes;
    var $texte_aide_etape;
    var $lien_externe_etape;
    var $formulaire_etape;
    var $footer_html;

    function __construct($demande) {

        $this->nom_demande = $demande->nom_demande;
        $this->numero_demande = $demande->num_demande;
        $this->nom_demandeur = $demande->nom_demandeur;
        $this->nom_entite = $demande->nom_entite;
        $this->nom_beneficiaire = $demande->nom_beneficiaire;
        $this->nom_service = $demande->nom_service;
        $this->brouillon = $demande->brouillon;

        $this->etapes = $demande->etapes;
        $this->nombre_etapes = $demande->nb_etapes;

        $this->formulaire_etape = $demande->etape->str_html;
    }

    function genere_HTML() {
        $html_genere = '';
        $context = Admin_Gestion_Page_GRU_Tools::get_context();
        $request_api = new Gestion_Page_GRU_Api_Request($context);
        $cur_demande = $request_api->get_demande();

        if (isset($context['retour_link'])) {
            $referrer_location =  $context['retour_link'];
        } else {
            $referrer_location = '';
        }
        # Utiliser pour les URL	

        $link_options = get_option(Admin_Gestion_Page_GRU_Tools::links_pages_option);
        $update_url = get_page_link($link_options['crm_link_update_request']);
        # SECTION CONTENANT LES META INFORMATIONS DE LA DEMANDE

        if (isset($context['id_etape'])) {
            $recap = 'Récapitulatif de la demande : ';
        } else {
            $recap = '';
        }
        $nom_demande = mb_strtoupper($this->nom_demande, 'UTF-8');
        $html_genere .= <<<HTML
    <div class="container_formulaire">
        <section class="panel">
            <div class="panel-body">
                <h4 class="return_link">
                    <span class="gru-icon-button" onclick="$.grufront.history_back('$referrer_location')">
                        <i class="fas fa-long-arrow-alt-left"></i>&nbsp;&nbsp;  Retour
                    </span>
                </h4><br/>
                <div class="body_form">
                    <div class="head_form">
                        <h3 class="nom_demande">$recap $nom_demande </h3>
                        <p>Visualisation de la demande N° {$this->numero_demande}
            
HTML;
        if ('oui' == $this->brouillon) {
            $html_genere .= ' (Brouillon)';
        }

        if ((isset($cur_demande->data->editable) && '0' == $cur_demande->data->editable) || 'oui' == $this->brouillon){
            $html_genere .= <<<HTML
                <span class="gru-icon-button" onclick='$.grufront.set_context($context, function() { window.location = "$update_url";})'> Modifier la demande</span>
HTML;
        }

        $html_genere .= <<<HTML
                        </p>
                        <p>Demandeur : {$this->nom_demandeur} </p>
                        <p>Entité : {$this->nom_entite} </p>
HTML;

        if (!empty($this->nom_beneficiaire)) {
            $html_genere .= '<p>Bénéficiaire : ' . $this->nom_beneficiaire . '</p>';
        } else {
            $html_genere .= '<p>Bénéficiaire : ' . $this->nom_entite . '</p>';
        }
        $html_genere .= <<<HTML
                    </div>
                </div>
            </div>
        </section>
        <div class="panel-group" id="accordion">
HTML;

        # SECTION CONTENANT LES ETAPES ET LES VALEURS DE LA DEMANDE
        $first_tab = 1;
        $num_etape = 0;
        $nb_etapes = $this->nombre_etapes;
        foreach ($this->etapes as $etape_id => $etape) {

            $num_etape += 1;
            $str_num_etape = 'Étape - ';

            if ((1 == $first_tab)) {
                $link = '<a class="accordion-toggle" href="#' . $etape_id . '" data-parent="#accordion" data-toggle="collapse">' . $str_num_etape . $etape->label . '</a>';
                $step = '<div id="' . $etape_id . '" class="panel-collapse collapse in" style="">';
            } else {
                $link = '<a class="accordion-toggle collapsed" href="#' . $etape_id . '" data-parent="#accordion" data-toggle="collapse">' . $str_num_etape . $etape->label . '</a>';
                $step = '<div id="' . $etape_id . '" class="panel-collapse collapse" style="">';
            }

            $html_genere .= <<<HTML
            <div class="panel panel-default" id="step_container">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        $link
                    </h4>
                </div>
                $step
                <div class="panel-body">
                    {$etape->str_html}
                </div>
HTML;
            if ($nb_etapes == 1) {
                $html_genere .= <<<HTML
                    &nbsp;<br>
                    <div class="border-top">
                    &nbsp;<br>
                    <form action="" method="POST" id="transmit">
                        <input type="hidden" value="{$cur_demande->id}" name="id_demande">
                        <input type="hidden" value="Transmit" name="action">                        
                        <button id="default-next-1" class="btn btn-info" type="button" onclick="confirmTransmit();">TERMINER</button>                    
HTML;
                if ($this->brouillon == 'oui' || (isset($cur_demande->data->editable) && '0' == $cur_demande->data->editable)) {
                    $html_genere .= <<<HTML
                        <button id="default-next-1" class="btn btn-info" type="button" onclick='$.grufront.set_context($context, function() { window.location = "$update_url";})'>MODIFIER</button>
                   </form>
HTML;
                }
                $html_genere .= '</div>';

            }
            $html_genere .= <<<HTML
                    </div>
                </div>
HTML;
            $nb_etapes --;
        }

        $html_genere .= '</div>';

        $confirmBox = <<<EOQ
        <script language="javascript">
        function confirmTransmit() {        
            $.confirm({
                title: 'Transmettre la demande',
                boxWidth: '400px',
                useBootstrap: false,
                container: "#gestion-page-gru-content",
                scrollToPreviousElement: false,
                scrollToPreviousElementAnimate: false,
                content: 'Voulez-vous transmettre votre demande maintenant ?',
                buttons: {
                    Oui: {
                        action: function () {
                            document.getElementById('transmit').submit();
                        }
                    }, Non: {
                    }
                }
            });
         }
         </script>
EOQ;

        return $html_genere.$confirmBox;
    }

}
