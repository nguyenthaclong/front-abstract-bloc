<?php

class individual_display_HTML {

    var $infosIndividu;
    var $colonnes;

    function __construct($infosIndividu) {

        // toutes les infos de l'individu retournées par le web service
        $this->infosIndividu = $infosIndividu;
        // le HTML des shortcodes imbriqués : {nomDeLaColonne1} [nomDeLaColonne2} ...
    }

    function genere_HTML() {

        $html = "";

        // on parcourt le tableau contenant les infos de l'individu	
        foreach ($this->infosIndividu as $colonneName => $colonneValue) {
            // dans le HTML des shortcodes imbriqués on remplace chaque occurrence de colonne {nomDeLaColonne} par sa valeur du tableau retourné par le web service 
            $html .= $colonneName . ': ' . $colonneValue . "<br/>";
        }
        return $html;
    }

}
