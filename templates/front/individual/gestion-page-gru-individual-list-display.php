<?php

class individual_list_display_HTML {

    var $liste_individus;
    function __construct($liste_individus) {
        $this->liste_individus = $liste_individus;
    }

    /**
     * Function qui génère le HTML de la classe liste_individus_HTML
     * @return type
     */
    function genere_HTML() {
        // Récupération de la liste des individus à afficher (tableau)
        $current_user = wp_get_current_user();
        $link_options = get_option(Admin_Gestion_Page_GRU_Tools::links_pages_option);
        $create_url = get_page_link($link_options['crm_link_create_individual']);
        $update_url = get_page_link($link_options['crm_link_update_individual']);
        $retour_url = get_permalink();
        Admin_Gestion_Page_GRU_Tools::add_to_context(array("retour_link" => $retour_url));
        $tbody = '';
        foreach ($this->liste_individus as $individual) {
            $context =  json_encode(array('individual_id' => $individual->id));
            $tbody .= <<<HTML
                <tr id="{$individual->id}">
                    <td>{$individual->nom_usage}</td>
                    <td>{$individual->prenom} {$individual->prenom2} {$individual->prenom3}</td>
                    <td>{$individual->date_naissance}</td>
                    <td>{$individual->email}</td>
                    <td>  
HTML;

            $tbody .=<<<HTML
                        <span class="gru-icon-button" onclick='$.grufront.add_to_context($context, function() { window.location = "$update_url";})'> <i class="fa fa-pencil-alt" title="Modifier l'individu"></i></span>
HTML;

          $user_context   = Admin_Gestion_Page_GRU_Tools::get_user_context();
          $current_id_gru = $user_context->get_gru_id();
          if($current_id_gru !== $individual->id) {
             $tbody .=<<<HTML
                        <span class="gru-icon-button" onclick="$.grufront.validation_suppression_individu('$individual->id','{$individual->nom_usage} {$individual->prenom} {$individual->prenom2} {$individual->prenom3}')"> <i class="fa fa-trash" title="Supprimer l'individu"></i></span>

HTML;
          }
                      
            $tbody .=<<<HTML
                    </td>
                </tr>      
HTML;
        }

        $html_genere = <<<HTML
            <table class="table-liste-individus table table-striped">
                <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Prénom</th>
                        <th>Date de naissance</th>
                        <th>Email</th>
                        <th></th>
                    <tr>
                </thead>    
                <tbody>
                    $tbody
                </tbody>
            </table>
            <div class="row">
                <div class="col-lg-12">
                    <a href="$create_url"><i class="fa fa-plus ml-2 mr-2"></i>Ajouter un individu</a>
                </div>
            </div>
HTML;
        return $html_genere;
    }

}
