<?php

/**
 * Classe qui génère le HTML permettant l'affiche du shortcode [entite_creer_individu]
 * @package
 * @author
 * @version
 * @abstract
 * @copyright
 */
class individual_create_HTML {

    var $array_display;
    var $code_entite;

    /**
     * Contructeur de la classe creer_entite_HTML
     * @param array $array_display
     * @param type $code_entite
     */
    function __construct($array_display, $code_entite) {
        $this->array_display = $array_display;
        $this->code_entite = $code_entite;
    }

    /**
     * Fonction qui génère le HTML
     * @return string
     */
    function genere_HTML() {

        $page_id = get_the_ID();
        $link_options = get_option(Admin_Gestion_Page_GRU_Tools::links_pages_option);
        $context = Admin_Gestion_Page_GRU_Tools::get_context();
        $title = ($page_id == $link_options['crm_link_update_individual']) ? "MODIFIER UN INDIVIDU" : "CRÉER UN INDIVIDU";
        $html_genere .= '<SCRIPT>
			var fieldRequired = Array();
			var fieldDescription = Array();
		
			function dateCheck ( ) {
        var date_a_verifier = document.getElementById ( "date_naissance" ).value;
        var nom_usage_verif = document.getElementById ( "nom_usage" ).value;
        var prenom_verif = document.getElementById ( "prenom" ).value;
        var format = /^(\d{1,2}\/){2}\d{4}$/;
                                
        if(nom_usage_verif == "" || prenom_verif == ""){
            alert ( "Merci de renseigner les champs obligatoires" );
            jQuery(document).ready(function(){
                var champ_date = jQuery("#nom_usage").offset();
                jQuery("html, body").animate({scrollTop: champ_date.top - 130 }, "slow");
            });
            return false;
        }

				if ( ! format.test ( date_a_verifier ) ) {
					alert("La date renseignée ne respecte pas le format." );
					jQuery(document).ready(function(){
						var champ_date = jQuery("#date_naissance").offset();
						jQuery("html, body").animate({scrollTop: champ_date.top - 130 }, "slow");
					});
					
					return false;
				} else {
					var tab_date  = date_a_verifier.split ( "/" );
					tab_date[1] -=1;
					var ma_date = new Date( tab_date[2], tab_date[1], tab_date[0] );

					if ( ma_date.getFullYear() == tab_date[2] && ma_date.getMonth() == tab_date[1] && ma_date.getDate() == tab_date[0] ) {
						return true;
					} else {
						alert ( "La date renseignée n\'est pas valide." );
						jQuery(document).ready(function(){
							var champ_date = jQuery("#date_naissance").offset();
							jQuery("html, body").animate({scrollTop: champ_date.top - 130 }, "slow");
						});
				
						return false;
					}
				}
			}
	
			function formCheck( formobj ){
				var alertMsg = "Les champs suivants doivent être remplis :\n";
				var l_Msg = alertMsg.length;
				for ( var i = 0; i < fieldRequired.length; i++ ){
					var obj = document.getElementById ( fieldRequired[i] );
					if ( obj ){
						switch ( obj.type ){
							case "select-one":
								if ( obj.selectedIndex == -1 || obj.options[ obj.selectedIndex ].text == "" ){
									alertMsg += " - " + fieldDescription[i] + "\n";
								}
								break;
							case "select-multiple":
								if ( obj.selectedIndex == -1 ){
									alertMsg += " - " + fieldDescription[i] + "\n";
								}
								break;
							case "text":
							case "textarea":
							if ( obj.value == "" || obj.value == null ){
								alertMsg += " - " + fieldDescription[i] + "\n";
							}
							break;
							default:
						}
                        if ( obj.type == undefined ){
							var blnchecked = false;
							for ( var j = 0; j < obj.length; j++ ){
								if ( obj[j].checked ){
									blnchecked = true;
								}
							}
							if ( !blnchecked ){
								alertMsg += " - " + fieldDescription[i] + "\n";
							}
						}
					}
				}
				if ( alertMsg.length == l_Msg ){
					return true;
				} else {
					alert ( alertMsg );
					return false;
				}
			}
			
			function addRequired ( name, label ) {
				fieldRequired.push ( name );
				fieldDescription.push ( label );
			}

			</SCRIPT>';


        // Création du haut du formulaire
        // Les deux champs cachés permettent de conserver le type d'entité concerné et la variable qui permet de détecter la validation finale du form

        $html_genere .= <<<HTML
            <h3>$title</h3>             
            <form id='creer_individu' class='creer_individu' name='creer_individu' method='POST' action=''>
                <input type='hidden' id='code_entite' name='code_entite' value='{$this->code_entite}'>
                <input type='hidden' id='sauver_individu' name='sauver_individu' value='sauver'>
HTML;
        if ($context['type_entite'] == "CDE_FOYERS") {
            $html_genere .= <<<HTML
                    <h4>Étape - 1/3 - Type de l'individu</h4>             
                <div class="form_group">
                    <span class="valeur_champ">
                        <select class="form-control" id="type" title="type" name="type">
                            <option value="adulte">Adulte</option>
                            <option value="enfant">Enfant</option>
                        </select>
                    </span>
                </div>
                <br/>
                <h4>Étape - 2/3 - Informations de l'individu</h4>
HTML;
        }
        // Pour chacun des champs de l'entité
        foreach ($this->array_display as $champ) {
            $html_genere .= Admin_Gestion_Page_GRU_Tools::displayChamp($champ);
        }

        $html_genere .= "<br>";
        $url_retour = get_permalink(wp_get_post_parent_id(get_the_ID()));
        if (isset($context['retour_link'])) {
            $referrer_location = $context['retour_link'];
        } else {
            $referrer_location = '';
        }

        $rest_api_individual = new Gestion_Page_GRU_Api_Individual(array('entite_type' => $context['type_entite'], 'entite_id' => $context['id_entite']));
        $list_individu = $rest_api_individual->get_individual_list();

        switch ($context['type_entite']) {
            case "CDE_FOYERS":
                $html_genere .= <<<HTML
                    <br />
                    <h4>Étape - 3/3 - Liaisons de l'individu</h4>
HTML;
                foreach ($list_individu as $individus => $value) {
                    foreach ($value as $key => $valeur) {
                        $html_genere .= <<<HTML
                           <p>Veuillez définir les liens de parenté avec {$valeur->objet_nom}</p>
                           <div class="form_group dotted_group liaison_individu">
                               <span class="valeur_champ">
                                   <label class="label_champ">Lien de parenté</label>
                                   <p> {$valeur->objet_nom} est la ou le </p>
                                   <select class="role_1_{$valeur->objet_id}" id="role_1_{$valeur->objet_id}" name="role_1_{$valeur->objet_id}">
                                       <option value=""> </option>
                                       <option value="pere">Père</option>
                                       <option value="mere">Mère</option>
                                   </select>
                                   <p> de l'individu à créer qui est sa ou son </p>
                                   <select class="role_2_{$valeur->objet_id}" id="role_2_{$valeur->objet_id}" name="role_2_{$valeur->objet_id}">
                                       <option value=""> </option>
                                       <option value="fils">Fils</option>
                                       <option value="fille">Fille</option>
                                   </select>
                                   <div style="clear:both;"></div>
                               </span>
                           </div>
                           <br />
HTML;
                    }
                }
                break;
        }

        
        $html_genere .= <<<HTML
                 <span class="gru-icon-button" onclick="$.grufront.history_back('$referrer_location')" class="button btn-secondaire" >Annuler</span> 
                <input type='button' name='submit_fin' class='button' id='submit_fin' value='Valider' onclick='if (formCheck(this) && dateCheck() ) { document.getElementById("creer_individu").submit(); document.getElementById("submit_fin").disabled=true; }'>
            </form>
HTML;

        return $html_genere;
    }

}
