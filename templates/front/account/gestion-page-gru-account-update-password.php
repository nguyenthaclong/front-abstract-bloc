<?php

class account_update_password_HTML {

    function genere_HTML() {
        $url_retour = get_permalink(wp_get_post_parent_id(get_the_ID()));
        $html_genere = <<<HTML
            <form method="post" action="" name="modifier_mot_de_passe" id="modifier_mot_de_passe">
                <div>
                    <label for="pwd">Veuillez saisir votre mot de passe actuel *</label>
                    <input type="password" name="pwd" id="pwd">
                <div
                <div class="infos-password">Votre mot de passe doit avoir une longueur minimale de 8 caractères et comporter au moins 1 chiffre, 1 lettre en minuscule et 1 lettre en majuscule</div>
                <hr>
                <div>
                    <label for="new-pwd-1">Nouveau mot de passe *</label><input type="password" name="new-pwd-1" id="new-pwd-1">
                <div>
                <div>
                    <label for="new-pwd-2">Répéter le nouveau mot de passe *</label><input type="password" name="new-pwd-2" id="new-pwd-2">
                <div>
                <a href="$url_retour" class="button btn-secondaire" >Annuler</a>
                <input type="submit" class="button" name="modifier_mot_de_passe" value="Valider" onclick="return checkForm()">
            </form>                
            <script type="text/javascript">
                var msgBlank ="";
                var msgMail = "";		
                var msgPassword = "";		

                function checkForm ( ) {

                        if ( ! checkBlank ( ) ) {
                                alert ( msgBlank );
                                return false;
                        } else {
                                if ( ! checkPassword ( ) ) {
                                        alert ( msgPassword );
                                        return false;
                                } else {
                                        return true;
                                }
                        }
                }

                function checkPassForm ( ) {

                        if ( ! checkPassword ( ) ) {
                                alert ( msgPassword );
                                return false;
                        } else {
                                return true;
                        }
                }

                function checkBlank ( ) {

                        var result = true;

                        var pwd = document.getElementById ( "pwd" );
                        var new_pwd_1 = document.getElementById ( "new-pwd-1" );
                        var new_pwd_2 = document.getElementById ( "new-pwd-2" );
                                if ( "" === pwd.value ) {
                                        result = false;
                                        pwd.style.borderColor="#FF0000";
                                } else  {
                                        pwd.style.borderColor="#008000";

                                }
                                if ( "" === new_pwd_1.value ) {
                                        result = false;
                                        new_pwd_1.style.borderColor="#FF0000";
                                } else {
                                        new_pwd_1.style.borderColor="#008000";
                                }

                                if ( "" === new_pwd_2.value ) {
                                        result = false;
                                        new_pwd_2.style.borderColor="#FF0000";
                                } else {
                                        new_pwd_2.style.borderColor="#008000";	
                                }
                                if ( false === result ) {
                                        msgBlank = "Tous les champs sont obligatoires.";
                                }

                        return result;
                }

                function checkMail ( ) {

                        var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                        var mail = document.getElementById ( "email" );

                        if ( ! regex.test ( mail.value ) ) {
                                msgMail = "L\'adresse mail renseignée n\'est pas valide.";
                                mail.style.borderColor="#FF0000";
                                return false;
                        } else {
                                mail.style.borderColor="#008000";
                                return true;
                        }
                }

                function checkPassword ( ) {

                        var pwd1 = document.getElementById ( "new-pwd-1" );
                        var pwd2 = document.getElementById ( "new-pwd-2" );

                        if ( pwd1.value !== pwd2.value ) {
                                msgPassword = "Les deux mots de passe ne sont pas identiques.";
                                pwd1.style.borderColor="#FF0000";
                                pwd2.style.borderColor="#FF0000";
                                return false;
                        } else {

                                // 8 caractères minimum, 1 chiffre, 1 minuscule, 1 capitale
                                var regex = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/;
                                if ( ! regex.test ( pwd1.value ) ) {
                                        msgPassword = "Le mot de passe renseignée ne respecte pas les règles de sécurité.";
                                        pwd1.style.borderColor="#FF0000";
                                        pwd2.style.borderColor="#FF0000";
                                        return false;

                                } else  {
                                        pwd1.style.borderColor="#008000";
                                        pwd2.style.borderColor="#008000";
                                        return true;
                                }
                        }
                }
            </script>
                
HTML;
        return $html_genere;
    
    }

}
