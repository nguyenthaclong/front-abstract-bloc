<?php

class account_validation_HTML {

	# Spécial Nanterre

	function genere_HTML ( ) {


		$code_activation = '';
		$email = '';
		$email2 = '';
	
		$html_genere .='<SCRIPT>
			
			var tabChampsVides = Array();
			var msgChampsVides = "";
			var msgDateNaissance = "";
			var msgMail = "";
			var msgCode = "";

			function checkForm ( ) {

				var result = false;

				if ( ! checkBlank() ) {
					alert ( msgChampsVides );
				} else {
					if ( ! checkCode() ) {
						alert ( msgCode );
					} else {
						if ( ! checkMail() ) {
							alert ( msgMail );
						} else {
							return true;	
						}
					}
				}
				return result;
			}
	
			function checkBlank ( ) {
			
				tabChampsVides.length = 0;
				msgChampsVides = "Les champs suivants sont obligatoires :\n";
				var liste_champs = ["code_activation", "email", "email2"];
				var result = true;

				for ( var i = 0; i < liste_champs.length; i++ ) {

					var obj = document.getElementById ( liste_champs[i] );
					if ( obj ){
						switch ( obj.type ){
							case "select-one":
								if ( obj.selectedIndex == -1 || obj.options[ obj.selectedIndex ].text == "" ){
									if ( -1 == tabChampsVides.indexOf ( obj.previousSibling.innerHTML.slice ( 0, -2 ) ) ) {
										tabChampsVides[ i ] =  obj.previousSibling.innerHTML.slice ( 0, -2 );
										obj.style.borderColor="#FF0000";
									}
									result = false;
								} else {

									obj.style.borderColor="#008000";
									if ( -1 != tabChampsVides.indexOf ( obj.previousSibling.innerHTML.slice ( 0, -2 ) ) ) {
										tabChampsVides.splice ( i, 1 );
									}	
								}
								break;
							case "text":
								if ( obj.value == "" ) {
									if ( -1 == tabChampsVides.indexOf ( obj.previousSibling.innerHTML.slice ( 0, -2 ) ) ) {
										tabChampsVides[ i ] =  obj.previousSibling.innerHTML.slice ( 0, -2 );
										obj.style.borderColor="#FF0000";

									}
									result = false;
								} else {
									obj.style.borderColor="#008000";
									if ( -1 != tabChampsVides.indexOf ( obj.previousSibling.innerHTML.slice ( 0, -2 ) ) ) {
										tabChampsVides.splice ( i, 1 );
									}	
								}
								break;
							case "password":
								if ( obj.value == "" ) {
									if ( -1 == tabChampsVides.indexOf ( obj.previousSibling.innerHTML.slice ( 0, -2 ) ) ) {
                                                                                tabChampsVides[ i ] =  obj.previousSibling.innerHTML.slice ( 0, -2 );
										obj.style.borderColor="#FF0000";

                                                                        }
                                                                        result = false;
                                                                } else {
									obj.style.borderColor="#008000";
                                                                        if ( -1 != tabChampsVides.indexOf ( obj.previousSibling.innerHTML.slice ( 0, -2 ) ) ) {
                                                                                tabChampsVides.splice ( i, 1 );
                                                                        }
                                                                }
                                                                break;	
							default:
						}
					}
				}

				for ( var j = 0; j < tabChampsVides.length; j++ ) {
					if ( typeof tabChampsVides[j] !== "undefined" ) {
						msgChampsVides += " - " + tabChampsVides[j] + "\n";
					}
				}
				return result;
			}
			
			function checkCode ( ) {
			
				var code = document.getElementById ( "code_activation" );
				if ( 10 != code.value.length ) {
					msgCode = "Le code d\activation doit contenir 10 caractères";
					code.style.borderColor="#FF0000";
					return false;
				} else {
					code.style.borderColor="#008000";
					return true;
				}
			}

			function checkMail ( ) {
				
				var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				var email1 = document.getElementById ( "email" );
				var email2 = document.getElementById ( "email2" );
				
				if ( email.value === email2.value ) {
					if ( ! regex.test ( email.value ) ) {
						msgMail = "L\'adresse mail renseignée n\'est pas valide.";
						email1.style.borderColor="#FF0000";
						email2.style.borderColor="#FF0000";	
						return false;
					} else {
						return true;
					}
				} else {
					msgMail = "Les deux adresses ne sont pas identiques.";
					email1.style.borderColor="#FF0000";
					email2.style.borderColor="#FF0000";
					return false;
				}
			}
		</SCRIPT>';
	
		if ( 'POST' == $_SERVER['REQUEST_METHOD'] && 'Valider' == $_POST['submit_valider_compte'] ) { # le formulaire a été validé

			# Récupération des valeurs saisies si elles existent dans le $_POST
			if ( isset ( $_POST['code_activation'] ) && ! empty ( $_POST['code_activation'] ) ) {
				$code_activation = $_POST['code_activation'];
			}
			if ( isset ( $_POST['email'] ) && ! empty ( $_POST['email'] ) ) {
				$email = $_POST['email'];
			}
			if ( isset ( $_POST['email2'] ) && ! empty ( $_POST['email2'] ) ) {
				$email2 = $_POST['email2'];
			}
		}	

		$html_genere .= '<form method="post" id="valider_compte" class="valider_compte" name="valider_compte" action="">';
		$html_genere .= '<div><label for="code_activation">Code d\'activation *</label><input type="text" id="code_activation" name="code_activation" value="'.$code_activation.'"></div>';
		$html_genere .= '<div><label for="email">Adresse mail *</label><input type="text" id="email" name="email" value="'.$email.'"></div>';
		$html_genere .= '<div><label for="email2">Répéter l\'adresse mail *</label><input type="text" id="email2" name="email2" value="'.$email2.'"></div>';
		//$html_genere .= '<div><label for="pwd1">Mot de passe *</label>';
		//$html_genere .= '<div class="infos-password">Votre mot de passe doit avoir une longueur minimale de 8 caractères et comporter au moins 1 chiffre, 1 lettre en minuscule et 1 lettre en majuscule</div>';
		//$html_genere .= '<input type="password" id="pwd1" name="pwd1"></div>';
		//$html_genere .= '<div><label for="pwd1">Répéter le mot de passe *</label><input type="password" id="pwd2" name="pwd2"></div>';
		
		# Captcha
		if ( function_exists ( 'cptch_check_custom_form' ) && cptch_check_custom_form ( ) !== true ) {
			$html_genere .= '<div><label>Veuillez compléter la formule de sécurité ci-dessous *</label></div>';
		}

		if ( function_exists ( 'cptch_display_captcha_custom' ) ) {
			$html_genere .= "<input type='hidden' name='cntctfrm_contact_action' value='true' />";
			$html_genere .= cptch_display_captcha_custom();
			$html_genere .= '<br><br>';
		};
		$html_genere .= '<input type="submit" name="submit_valider_compte" value="Valider" onclick="return checkForm();">';
		$html_genere .= '</form>';
		
		return $html_genere;
	}
}

