<?php

class account_lost_password_HTML {

	//function __construct ( $infos_compte ) {
	//	$this->infos_compte = $infos_compte;
	//}

	function genere_HTML ( ) {

		$html_genere = '<script>

			var msgBlank ="";
			var msgMail = "";		
			var msgPassword = "";		

			function checkForm ( ) {

				if ( ! checkBlank ( ) ) {
					alert ( msgBlank );
					return false;
				} else {
					if ( ! checkMail ( ) ) {
						alert ( msgMail );
						return false;
					} else {
						return true;
					}
				}
			}

			function checkPassForm ( ) {

				if ( ! checkPassword ( ) ) {
					alert ( msgPassword );
					return false;
				} else {
					return true;
				}
			}

			function checkBlank ( ) {
				
				var result = true;

				var mail = document.getElementById ( "email" );
					if ( "" === mail.value ) {
						msgBlank = "Veuillez saisir l\'adresse mail de votre compte.";
						result = false;
						mail.style.borderColor="#FF0000";
					}
				return result;
			}

			function checkMail ( ) {
				
				var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				var mail = document.getElementById ( "email" );
			
				if ( ! regex.test ( mail.value ) ) {
					msgMail = "L\'adresse mail renseignée n\'est pas valide.";
					mail.style.borderColor="#FF0000";
					return false;
				} else {
					mail.style.borderColor="#008000";
					return true;
				}
			}
			function checkPassword ( ) {

				var mdp1 = document.getElementById ( "pwd1" );
				var mdp2 = document.getElementById ( "pwd2" );
				if ( "" == mdp1.value || "" == mdp2.value ) {

					msgPassword = "Veuillez saisir votre nouveau mot de passe dans les deux champs.";
					if ( "" == mdp1.value ) { mdp1.style.borderColor="#FF0000"; } else { mdp1.style.borderColor="#008000"; }
					if ( "" == mdp2.value ) { mdp2.style.borderColor="#FF0000"; } else { mdp2.style.borderColor="#008000"; }
					return false;	
				} else {
					if ( mdp1.value !== mdp2.value ) {
						msgPassword = "Les deux mots de passe ne sont pas identiques.";
						mdp1.style.borderColor="#FF0000";	
						mdp2.style.borderColor="#FF0000";
						return false;					
					} else {
						mdp1.style.borderColor="#008000";
						mdp2.style.borderColor="#008000";
						return true;
					}
				}
			}
		';		
		$html_genere .= '</script>';

		if ( empty ( $_GET['data'] ) ) {

			if ( ! empty ( $_POST['email'] ) ) {
				$email = $_POST['email'];
			} else {
				$email = '';
			}

			$html_genere .= '<form method="post" action="" name="mot_de_passe_oublie" id="mot_de_passe_oublie">';
			$html_genere .= '<div><label for="email">Veuillez saisir l\'adresse mail de votre compte *</label><input type="text" name="email" value="'.$email.'" id="email" /></div>';
			$html_genere .= '<input type="submit" class="button" name="mot_de_passe_oublie" value="Valider" onclick="return checkForm()">';
			$html_genere .= '</form>';
		} else {

			$html_genere .= '<form method="post" action="" name="modifier_mot_de_passe" id="modifier_mot_de_passe">';
			$html_genere .= '<div class="infos-password">Votre mot de passe doit avoir une longueur minimale de 8 caractères et comporter au moins 1 chiffre, 1 lettre en minuscule et 1 lettre en majuscule</div>';
			$html_genere .= '<div><label for="pwd1">Mot de passe *</label><input type="password" name="pwd1" id="pwd1" /></div>';
			$html_genere .= '<div><label for="pwd2">Répéter le mot de passe *</label><input type="password" name="pwd2" id="pwd2" /></div>';
			$html_genere .= '<input type="submit" class="button" name="modifier_mot_de_passe" value="Valider" onclick="return checkPassForm()" />';
			$html_genere .= '</form>';

		}
		return $html_genere;
	}

}
