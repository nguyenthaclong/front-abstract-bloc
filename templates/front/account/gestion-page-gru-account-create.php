<?php

class account_create_HTML {

    function genere_HTML() {

        $html_genere = "";
        $civilite = '';
        $prenom = '';
        $nom = '';
        $date_naissance = '';
        $sexe = '';
        $email = '';
        $email2 = '';

        if ('POST' == $_SERVER['REQUEST_METHOD'] && 'Valider' == $_POST['creation_compte']) { # le formulaire a été validé
            # Récupération des valeurs saisies si elles existent dans le $_POST
            if (isset($_POST['civilite']) && !empty($_POST['civilite'])) {
                $civilite = $_POST['civilite'];
            }
            if (isset($_POST['prenom']) && !empty($_POST['prenom'])) {
                $prenom = $_POST['prenom'];
            }
            if (isset($_POST['nom']) && !empty($_POST['nom'])) {
                $nom = $_POST['nom'];
            }
            if (isset($_POST['date_naissance']) && !empty($_POST['date_naissance'])) {
                $date_naissance = $_POST['date_naissance'];
            }
            if (isset($_POST['sexe']) && !empty($_POST['sexe'])) {
                $sexe = $_POST['sexe'];
            }
            if (isset($_POST['email']) && !empty($_POST['email'])) {
                $email = $_POST['email'];
            }
            if (isset($_POST['email2']) && !empty($_POST['email2'])) {
                $email2 = $_POST['email2'];
            }
        }

        $select_situtation_civilite = str_replace('value="' . $civilite . '"', 'value="' . $civilite . '" selected', <<<HTML
            <select class="form-control" id="civilite" name="civilite" required>
                <option value=""></option>
                <option value="M.">Monsieur</option>
                <option value="Mme">Madame</option>
            </select>         
HTML
        );

        $select_sexe = str_replace('value="' . $sexe . '"', 'value="' . $sexe . '" selected', <<<HTML
            <select name="sexe" id="sexe" required>
                <option value=""></option>
                <option value="F">Féminin</option>
                <option value="M">Masculin</option>
            </select>         
HTML
        );
        $home_url = home_url();

        // L'input avec la class "champ_sup_hp" sert de honeypot. Si renseigné, pas de création de compte.
        $html_genere .= <<<HTML
            <h3>CRÉER VOTRE COMPTE PERSONNEL</h3>
            <form id="creer_compte" class="creer_compte" name="creer_compte" method="POST" action="">
                <div>
                    <label for="civilite">Civilité *</label>
                    $select_situtation_civilite
                </div>
                <div>
                    <label for="prenom">Prénom *</label>
                    <input type="text" id="prenom" name="prenom" value="$prenom" required>
                </div>
                <div>
                    <label for="nom">Nom *</label>
                    <input type="text" id="nom_usage" name="nom_usage" value="$nom " required>
                </div>
                <div>
                    <label for="date_naissance">Date de naissance (Au format JJ/MM/AAAA) *</label>
                    <input type="text" id="date_naissance" name="date_naissance" value="$date_naissance" size="10" maxlength="10" data-mask="99/99/9999" required>
                </div>
                <div>
                    <label for="sexe">Sexe *</label>
                    $select_sexe
                </div>
                <div class="champ_sup_hp">
                    <label for="champ_sup_hp">Champ supplémentaire HP *</label>
                    <input type="text" autocomplete="off" id="champ_sup_hp" name="champ_sup_hp" value="">
                </div>
                <div>
                    <label for="email">Adresse mail *</label>
                    <input type="text" id="email" name="email" value="$email" required>
                </div>
                <div>
                    <label for="email2">Répéter l'adresse mail *</label>
                    <input type="text" id="email2" name="email2" value="$email2" required>
                </div>
                <div>
                    <label for="pwd1">Mot de passe *</label>
                    <div class="infos-password">
                        Votre mot de passe doit avoir une longueur minimale de 8 caractères et comporter au moins 1 chiffre, 1 lettre en minuscule et 1 lettre en majuscule 
                    </div>
                    <input type="password" id="pwd1" name="pwd1" required>
                </div>
                <div>
                    <label for="pwd1">Répéter le mot de passe *</label>
                    <input type="password" id="pwd2" name="pwd2" required>
                </div>
                <div>
                    <label for="mentions-legales">Mentions légales *</label>
                    <input type="checkbox" id="mentions_legales" name="mentions_legales" value="1">
                    <span class="conditions-generales">
                        J'ai lu et j'accepte les <a href="$home_url/cgu">Conditions Générales d'Utilisation</a>
                        et la <a href="$home_url/cnil">Charte Informatique et Libertés</a>.
                    </span>
                </div>
                <br>
HTML;


        # Captcha
        if (function_exists('cptch_check_custom_form') && cptch_check_custom_form() !== true) {
            $html_genere .= '<div><label>Veuillez compléter la formule de sécurité ci-dessous *</label></div>';
        }

        if (function_exists('cptch_display_captcha_custom')) {
            $html_genere .= "<input type='hidden' name='cntctfrm_contact_action' value='true' />";
            $html_genere .= cptch_display_captcha_custom();
            $html_genere .= '<BR>';
        }


        $html_genere .= <<<HTML
            <a href="$home_url" class="button btn-secondaire">Annuler</a>
            <input type="submit" value="Valider" class="button" name="creation_compte" id="submit_creation_compte" onclick="return checkForm();">
        </form>   
        
HTML;

        # fin 1.2
        return $html_genere;
    }
}
