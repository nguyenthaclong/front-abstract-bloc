<?php

class account_activation_HTML {

    var $infos_compte;

    function __construct($infos_compte) {
        $this->infos_compte = $infos_compte;
    }

    function genere_HTML() {

        $html_genere = '<script>

			var msgBlank ="";
			var msgPassword = "";		

			function checkForm ( ) {

				if ( ! checkBlank ( ) ) {
					alert ( msgBlank );
					return false;
				} else {
					if ( ! checkPassword ( ) ) {
						alert ( msgPassword );
						return false;
					} else {
						return true;
					}
				}
			}

			function checkBlank ( ) {
				
				var result = true;		
				var liste_champs = [ "pwd1", "pwd2" ];
			
				for ( var i = 0; i < liste_champs.length; i++ ) {
					var obj = document.getElementById ( liste_champs[i] );
					if ( "" === obj.value ) {
						msgBlank = "Les champs de mot de passe ne peuvent pas être vides.";
						result = false;
						obj.style.borderColor="#FF0000";
					}
				}
				return result;
			}

			function checkPassword ( ) {
				
				var mdp1 = document.getElementById ( "pwd1" );
				var mdp2 = document.getElementById ( "pwd2" );
				
				if ( mdp1.value !== mdp2.value ) {
					msgPassword = "Les deux mots de passe ne sont pas identiques.";
					mdp1.style.borderColor="#FF0000";	
					mdp2.style.borderColor="#FF0000";
					return false;					
				} else {
					return true;
				}
			}
		';
        $html_genere .= '</script>';

        $html_genere .= '<div>Veuillez remplir le formulaire suivant pour terminer l\'activation de votre compte </div>';
        $html_genere .= '<form method="post" action="" name="activation_compte" id="activation_compte">';
        $html_genere .= '<div><label for="email">Adresse mail *</label><input type="text" name="email" value="' . $this->infos_compte['email'] . '" id="email" disabled class="input-disabled"><div>';
        $html_genere .= '<div><label for="pwd1">Mot de passe *</label><input type="password" name="pwd1" id="pwd1"></div>';
        $html_genere .= '<div><label for="pwd2">Répéter le mot de passe *</label><input type="password" name="pwd2" id="pwd2"></div>';
        $html_genere .= '<input type="submit" class="button" name="activation_compte" value="Valider" onclick="return checkForm()">';
        $html_genere .= '</form>';

        return $html_genere;
    }

}
