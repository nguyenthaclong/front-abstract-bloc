<?php

/**
 * Classe qui génère le HTML permettant l'affiche du shortcode [entite_creer_individu]
 */
class account_update_HTML {

    private $fields_data;

    /**
     * Contructeur de la classe creer_entite_HTML
     * @param array $array_display
     * @param type $code_entite
     * @param type $label_entite
     */
    function __construct($fields) {
        $this->fields_data = $fields;
    }

    /**
     * Fonction qui génère le HTML
     * @return string
     */
    function genere_HTML() {
        $url_retour = get_permalink(wp_get_post_parent_id(get_the_ID()));
        $select_situtation_civilite = str_replace('value="' . $this->fields_data['salutation'] . '"', 'value="' . $this->fields_data['salutation'] . '" selected', <<<HTML
            <select class="form-control" id="salutation" name="salutation" required>
                <option value=""></option>
                <option value="M.">Monsieur</option>
                <option value="Mme">Madame</option>
            </select>         
HTML
        );

        $select_situtation_familiale = str_replace('value="' . $this->fields_data['situation_familiale'] . '"', 'value="' . $this->fields_data['situation_familiale'] . '" selected', <<<HTML
            <select class="form-control" id="situation_familiale" name="situation_familiale">
                <option value=""></option>
                <option value="celibataire">Célibataire</option>
                <option value="concubinage">Concubinage</option>
                <option value="pacse">Pacsé</option><option value="marie">Marié</option>
                <option value="divorce">Divorcé</option>
                <option value="veuf">Veuf</option>
            </select>           
HTML
        );


        $context = Admin_Gestion_Page_GRU_Tools::get_context();
        if (isset($context['retour_link'])) {
            $referrer_location = $context['retour_link'];
        } else {
            $referrer_location = get_home_url();
        }
 
        $html_genere = <<<HTML
            <h3>MODIFIER MON COMPTE</h3>
            <p><em>Les champs marqués d’un astérisque (*) sont obligatoires.</em></p>
            <form id="modifier_compte" class="modifier_compte" name="modifier_compte" method="POST" action="">
                <div class="form_group">
                    <label class="label_champ" for="salutation">Civilité</label>
                    <span class="valeur_champ">
                        $select_situtation_civilite
                    </span>
                </div> 
                <div class="form_group">
                    <label class="label_champ" for="prenom">Prénom *</label>
                    <span class="valeur_champ">
                        <input class="form-control" id="prenom" name="prenom" title="prenom" type="text"  value="{$this->fields_data['prenom']}" required>
                    </span>                
                </div>
                <div class="form_group">
                    <label class="label_champ" for="prenom2">Prénom 2</label>
                    <span class="valeur_champ">
                        <input class="form-control" id="prenom2" name="prenom2" type="text"  value="{$this->fields_data['prenom2']}">
                    </span>                
                </div>
                <div class="form_group">
                    <label class="label_champ" for="prenom3">Prénom 3</label>
                    <span class="valeur_champ">
                        <input class="form-control" id="prenom3" name="prenom3" type="text"  value="{$this->fields_data['prenom3']}">
                    </span>                
                </div>
                <div class="form_group">
                    <label class="label_champ" for="nom_usage">Nom d'usage *</label>
                    <span class="valeur_champ">
                        <input class="form-control" id="nom_usage" name="nom_usage" type="text"  value="{$this->fields_data['nom_usage']}" required>
                    </span>                
                </div>
                <div class="form_group">
                    <label class="label_champ" for="nom_famille">Nom de famille</label>
                    <span class="valeur_champ">
                        <input class="form-control" id="nom_famille" name="nom_famille" type="text"  value="{$this->fields_data['nom_famille']}">
                    </span>                
                </div> 
                <div class="form_group">
                    <label class="label_champ" for="date_naissance">Date de naissance - (Format : JJ/MM/AAAA) *</label>
                    <span class="valeur_champ">
                        <input class="form-control" id="date_naissance" name="date_naissance" type="text"  value="{$this->fields_data['date_naissance']}" data-mask="99/99/9999" required>
                    </span>                
                </div>  
                <div class="form_group">
                    <label class="label_champ" for="situation_familiale">Situation Familiale</label>
                    <span class="valeur_champ">
                        $select_situtation_familiale
                    </span>
                </div>   
                <div class="form_group">
                    <label class="label_champ" for="mobile">Téléphone Portable</label>
                    <span class="valeur_champ">
                        <input class="form-control" id="mobile" name="mobile" type="text"  value="{$this->fields_data['mobile']}">
                    </span>                
                </div>
                <div class="form_group">
                    <label class="label_champ" for="email">Adresse email *</label>
                    <span class="valeur_champ">
                        <input class="form-control" id="email" name="email" type="text"  value="{$this->fields_data['email']}" required>
                    </span>                
                </div>
                <a href="$referrer_location" class="button btn-secondaire" >Annuler</a>
                <input type="submit" name="modifier_compte" value="Valider" class="button" >
            </form>    
HTML;

        return $html_genere;
    }

}
