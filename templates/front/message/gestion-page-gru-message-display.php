<?php

class message_display_messages_demande_HTML {

    private $liste_messages;
    private $infos_demande;
    private $params;

    function __construct($messages, $params) {
        $this->infos_demande = $messages->demand;
        $this->liste_messages = $messages->messages;
        $this->params = $params;
    }

    function genere_HTML() {

        $html_genere = '';
        $context = Admin_Gestion_Page_GRU_Tools::get_context();
        if (isset($context['retour_link'])) {
            $referrer_location =  $context['retour_link'];
        } else {
            $referrer_location = '';
        }
        $link_options = get_option(Admin_Gestion_Page_GRU_Tools::links_pages_option);
        $update_url = get_page_link($link_options['crm_link_update_request']);

        # SECTION CONTENANT LES META INFORMATIONS DE LA DEMANDE
        $nom_demande = mb_strtoupper($this->infos_demande->libelle, 'UTF-8');
        $html_genere .= <<<HTML
    <div class="container_formulaire">
        <section class="panel">
            <div class="panel-body">
                <h4 class="return_link">
                    <span class="gru-icon-button" onclick="$.grufront.history_back('$referrer_location')">
                        <i class="fas fa-long-arrow-alt-left"></i>&nbsp;&nbsp;  Retour
                    </span>
                </h4><br/>
                <div class="body_form">
                    <div class="head_form">
                        <h3 class="nom_demande"> $nom_demande </h3>
                        <p>Fil de discussion de la demande N° {$this->infos_demande->numero}
            
HTML;
        if ('oui' == $this->brouillon) {
            $html_genere .= ' (Brouillon)';
        }

        $html_genere .= <<<HTML
                        </p>
                        <p>Demandeur : {$this->infos_demande->demandeur} </p>
                        <p>Entité : {$this->infos_demande->entite_nom} </p>
HTML;

        if (!empty($this->infos_demande->beneficiaire)) {
            $html_genere .= '<p>Bénéficiaire : ' . $this->infos_demande->beneficiaire . '</p>';
        } else {
            $html_genere .= '<p>Bénéficiaire : ' . $this->infos_demande->entite_nom . '</p>';
        }
        $html_genere .= <<<HTML
                    </div>
                </div>
            </div>
        </section>
        <div class="panel-group" id="accordion">
HTML;

        #SECTION DES MESSAGES
        $html_genere .= <<<HTML
            <div class="panel panel-default" id="step_container">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Fil de discussion
                    </h4>
                </div>                
                <div class="panel-body">
HTML;
        $allowedFiles = ["application/pdf","image/png","image/jpeg","image/gif"];
        $mimeAccepted = implode(',', $allowedFiles);
        $mimeAcceptedJs = implode(',', array_map(function ($item) { return '"'.$item.'"';}, $allowedFiles));
        $html_genere .= '<script language="javascript">

                                    function cde_fd_checkMessage() {
                                      const response = document.getElementsByName("reponse")[0];
                                      if (response.value.trim() === "") {
                                        alert("Votre message n\'a pas de contenu.");
                                        response.focus();
                                        return false;
                                      }
                                      const mimeAccepted = [' . $mimeAcceptedJs . '];
                                      const upload = document.getElementById("piece_jointe");
                                      if (upload.files.length === 1) {
                                        
                                        if (mimeAccepted.indexOf(upload.files[0].type) === -1) {
                                            alert("Seuls les fichiers PDF ou les images peuvent être attachés.");
                                            upload.value = "";
                                            return false;
                                        }  
                                      
                                        if (upload.files[0].size > 3*1024*1024) {
                                            alert("Votre fichier est trop volumineux (3Mo maximum).");
                                            upload.value = "";
                                            return false;
                                        }  
                                      }
                                      return true;
                                    }
                                </script>';

        $lastStatusMessage = '';
        foreach ($this->liste_messages as $message) {
            // Création de l'objet date pour la date de création du message et formattage pour enlever l'heure
            $date     = date_create_from_format('Y-m-d H:i:s', $message->date_entered);
            $datetime = $date->format('Y-m-d');

            // Récupération de la date pour formattage de la date
            $date_heure_message = preg_split('/ /', $message->date_entered, -1, PREG_SPLIT_NO_EMPTY);
            $heure_message      = substr($date_heure_message[1], 0, 5);

            // Génération des blocs de messages contenus dans la liste
            $rest_params = get_option(Gestion_Page_GRU_Api::rest_param_option);
            $url_cde = $rest_params['gru_url'];

            // secure and format content before display in textarea
            $messageContent = nl2br(htmlspecialchars(stripslashes(html_entity_decode($message->message, ENT_QUOTES))));

            // generate file link
            $html_genere_file_link = '';
            if ($message->filename !== '') {
                $idDemande = $message->parent_id;
                $idFileStored = $message->file_base64;
                $html_genere_file_link = '<a style="display:none;"/>
                                        <a href="#" 
                                        onclick="$.grufront.download_document_message(\''.$idDemande.'\', \''.$message->id.'\',\''.$idFileStored.'\');">'.
                                        $message->file_name . '</a>';
            }

            if ($message->flag_type == 1) {
                $html_genere .= '<div id="bloc-usager" class="panel panel-default sb2">
                                    <div class="panel-body sb2">
                                        <p class="entete-usager">' . $message->created_by . ', le ' . strftime("%d/%m/%Y", strtotime($datetime)) . ' à ' . $heure_message . ' </p>
                                        <p class="mess-usager">' . $messageContent . '</p>';
                $html_genere .= $html_genere_file_link. '</div></div><br />';
            } else {
                $html_genere .= '<div id="bloc-agent" class="panel panel-default sb1">
                                    <div class="panel-body sb1">
                                        <p class="entete-agent">Agent, le ' . strftime("%d/%m/%Y", strtotime($datetime)) . ' à ' . $heure_message . ' </p>
                                        <p class="mess-agent">' . $messageContent . '</p>';
                $html_genere .= $html_genere_file_link.'</div></div>';
            }
            $lastStatusMessage = $message->flag_statut;
        }

        if ($lastStatusMessage == 1) {
            $html_genere .= '<div class="panel panel-default sb1">
                                <div style="background-color: #fcf8e3; border-color: #faf2cc; color: #8a6d3b; text-align: center;" class="alert alert-dark" role="alert">
                                  Cette discussion est clôturée.
                                </div>
                             </div>';
        } else {
            $html_genere .= '<form method="post" enctype="multipart/form-data" onsubmit="return cde_fd_checkMessage();">
                            <div>
                                <label for="piece_jointe">Joindre un fichier (PDF ou images) :</label>
                                <input type="file" id="piece_jointe" name="piece_jointe" accept="'.$mimeAccepted.'">
                            </div>
                            <div>
                                <label for="reponse">Répondre :</label>
                                <textarea style="min-width: 80%;max-width: 80%;" name="reponse" cols="40" rows="5"></textarea>
                            </div>
                            <br>
                            <div>
                                <input type="submit" class="button" value="Envoyer">
                                <input id="id_demande" name="id_demande" type="hidden" value="' . $this->infos_demande->id . '"/>
                                <input id="nom_demande" name="nom_demande" type="hidden" value="' . htmlspecialchars($nom_demande) . '"/>
                            </div>
                        </form>';
        }



        $html_genere .= <<<HTML
                </div>
            </div>
        </div>
    </div>
HTML;

        return $html_genere;
    }

}
