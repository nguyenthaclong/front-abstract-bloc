<?php

class message_display_list_thread_HTML
{

    private $liste_fils;
    private $params;

    function __construct($list, $params) {
        $this->liste_fils = $list;
        $this->params = $params;
    }

    function genere_HTML() {

        $html_genere  = '';
        $context      = Admin_Gestion_Page_GRU_Tools::get_context();
        $link_options = get_option(Admin_Gestion_Page_GRU_Tools::links_pages_option);
        $display_url  = get_page_link($link_options['crm_link_show_request_chat']);

        #
        # Afficher liste des fils existants pour l'entité en cours
        #
        $tbody = "";
        foreach ($this->liste_fils as $fil) {
            $date_creation = "";
            if (isset($fil->created_date) && !empty($fil->created_date)) {
                $date = new DateTime($fil->created_date);
                if ($date) {
                    $date_creation = $date->format("d/m/Y H:i");
                }else {
                    $date_creation = $fil->created_date;
                }
            }

            $libelle = "N° {$fil->demande_numero} - ".mb_strtoupper($fil->demande_name);
            $actions = <<<HTML
                <span class="gru-icon-button" onclick='$.grufront.add_to_context({"type_entite" : "{$context["type_entite"]}",
                                                                                  "id_entite" : "{$context["id_entite"]}",
                                                                                  "id_demande" : "{$fil->demande_id}", 
                                                                                  "retour_link" : window.location.href}, 
                                                                                  function() { window.location = "$display_url";})'> 
                    <i class="fa fa-search" title="Ouvrir"></i>
                </span>
HTML;
            $tbody .= <<<HTML
                <tr id="Fil_{$fil->demande_id}">
                    <td>$libelle</td>
                    <td>$date_creation</td>
                    <td>{$fil->count_messages}</td>    
                    <td>$actions</td>
                </tr>
HTML;
        }
        if ($tbody === "") {
            $tbody = '<tr class="no-content"><td colspan=6><h5>Aucun fil de discussion.</h5></td></th>';
        }

        $html_genere .= <<<HTML
            <table class="liste_demandes table table-striped">
                <thead>
                <tr>
                    <th> Demande concernée </th>
                    <th> Discussion démarré le </th>
                    <th> Nombre de messages </th>
                    <th> </th>
                </tr>
                </thead>
                <tbody>
                    $tbody
                </tbody>
            </table>
HTML;
        return $html_genere;
    }

}