<?php

class message_display_list_non_lu_HTML {

    private $liste_messages;
    private $params;

    function __construct($messages, $params) {
        $this->liste_messages = $messages;
        $this->params = $params;
    }

    function genere_HTML() {

        $html_genere = '';
        $context = Admin_Gestion_Page_GRU_Tools::get_context();
        $link_options = get_option(Admin_Gestion_Page_GRU_Tools::links_pages_option);
        $display_url = get_page_link($link_options['crm_link_show_request_chat']);

        #
        # Afficher non lus liste
        #
        $tbody = "";
        foreach ($this->liste_messages as $id_message => $message) {
            $date_creation = "";
            if (isset($message->created_date) && !empty($message->created_date)) {
                $date = new DateTime($message->created_date);
                if ($date) {
                    $date_creation = $date->format("d/m/Y H:i");
                }else {
                    $date_creation = $message->created_date;
                }
            }

            $libelle = "N° {$message->demande_numero} - ".mb_strtoupper($message->demande_name);
            $short_content = substr($message->message, 0, 50) . '...';
            $actions = <<<HTML
                <span class="gru-icon-button" onclick='$.grufront.add_to_context({"type_entite" : "{$context["type_entite"]}",
                                                                                  "id_entite" : "{$context["id_entite"]}",
                                                                                  "id_demande" : "{$message->demande_id}", 
                                                                                  "retour_link" : window.location.href}, 
                                                                                  function() { window.location = "$display_url";})'> 
                    <i class="fa fa-search" title="Ouvrir"></i>
                </span>
HTML;
            $tbody .= <<<HTML
                <tr id="{$id_message}">
                    <td>$libelle</td>
                    <td>$date_creation</td>
                    <td>{$short_content}</td>    
                    <td>$actions</td>
                </tr>
HTML;
        }

        if ($tbody === "") {
            $tbody = '<tr class="no-content"><td colspan=6><h5>Aucun message non lu.</h5></td></th>';
        }

        $html_genere .= <<<HTML
            <table class="liste_demandes table table-striped">
                <thead>
                <tr>
                    <th> Demande concernée </th>
                    <th> Message créé le </th>
                    <th> Aperçu de message </th>
                    <th> </th>
                </tr>
                </thead>
                <tbody>
                    $tbody
                </tbody>
            </table>
HTML;
        return $html_genere;
    }

}
