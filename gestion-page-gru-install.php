<?php

/**
 * 2019 Communauté CAPDEMAT
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to communaute-capdemat.fr so we can send you a copy immediately.
 *
 *  @author   Communauté CAPDEMAT <contact@communaute-capdemat.fr>
 *  @copyright 2019 Communauté CAPDEMAT
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * 
 *  GestionPageGRU_Install
 */
class Gestion_Page_GRU_Install {
 
    /**
     * Fonction d'installation du module 
     * @global type $wpdb
     */
    public static function install() {
        global $wpdb; 

        $wpdb->query(<<<SQL
            CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}gestion_page_gru_tuile` (
                `tuile_id` int(11) NOT NULL AUTO_INCREMENT,
                `tuile_type` int(2) NOT NULL COMMENT '0: Bloc de type text (affichage), 1: Bloc service, 2: Bloc de type categorie',
                `tuile_title` VARCHAR(250) NOT NULL COMMENT 'Titre affiché en FO',
                `tuile_description` TEXT NOT NULL COMMENT 'Nom de la tuile visible en FO ou pour le type commentaire le texte affiché',
                `tuile_description_plus` TEXT NULL DEFAULT '' COMMENT 'Texte supplémentaire affiché au centre de la tuile (surtout utilisé pour les types 3)',
                `tuile_icon` VARCHAR(250) NULL DEFAULT '' COMMENT 'Icone visible en FO (référence fontawesome)',
                `tuile_image` VARCHAR(250) NULL DEFAULT '' COMMENT 'Image visible en FO',
                `tuile_teleservice_id` VARCHAR(255) NULL DEFAULT '' COMMENT 'Id du teleservice',
                `tuile_function_id` int(11) NULL COMMENT 'Id du teleservice',
                `tuile_page_id` VARCHAR(250) NULL COMMENT 'Id page pour une tuile type lien',
                `tuile_type_beneficiaire` VARCHAR(100) NULL Comment 'Type de bénéficiaire',
                PRIMARY KEY (`tuile_id`),
                FOREIGN KEY (`tuile_function_id`) REFERENCES {$wpdb->prefix}gestion_page_gru_function(`function_id`),
                FOREIGN KEY (`tuile_page_id`) REFERENCES {$wpdb->prefix}wp_posts(`ID`)
            ) ENGINE=MyISAM  DEFAULT CHARSET=utf8
SQL
        );

        $wpdb->query(<<<SQL
            CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}gestion_page_gru_tuile_style` (
                `tuile_style_id` int(11) NOT NULL AUTO_INCREMENT,
                `tuile_style_tuile_id` int(11) NOT NULL,
                `tuile_style_css_libelle` VARCHAR(250) NOT NULL,
                `tuile_style_css_value` VARCHAR(250) NOT NULL,
                PRIMARY KEY (`tuile_style_id`),
                FOREIGN KEY (`tuile_style_tuile_id`) REFERENCES {$wpdb->prefix}gestion_page_gru_tuile(`tuile_id`)
            ) ENGINE=MyISAM  DEFAULT CHARSET=utf8
SQL
        );

        $wpdb->query(<<<SQL
            CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}gestion_page_gru_tuile_ctg_correspondance` (
                `tuile_ctg_correspondance_id` int(11) NOT NULL AUTO_INCREMENT,
                `tuile_ctg_correspondance_child_id` int(11) NOT NULL COMMENT 'ID de la tuile catégorie parent',
                `tuile_ctg_correspondance_parent_id` int(11) NOT NULL COMMENT 'ID de la page liée',
                FOREIGN KEY (`tuile_ctg_correspondance_child_id`) REFERENCES {$wpdb->prefix}gestion_page_gru_tuile(`tuile_id`),
                FOREIGN KEY (`tuile_ctg_correspondance_parent_id`) REFERENCES {$wpdb->prefix}gestion_page_gru_tuile(`tuile_id`),
                PRIMARY KEY (`tuile_ctg_correspondance_id`)
            ) ENGINE=MyISAM  DEFAULT CHARSET=utf8
SQL
        );

        $wpdb->query(<<<SQL
            CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}gestion_page_gru_page_correspondance_grid` (
                `page_correspondance_grid_id` bigint(20) NOT NULL,
                `page_correspondance_grid` text DEFAULT NULL,            
                FOREIGN KEY (`page_correspondance_grid_id`) REFERENCES {$wpdb->prefix}posts(`ID`)
            ) ENGINE=MyISAM  DEFAULT CHARSET=utf8
SQL
        );

        $wpdb->query(<<<SQL
            CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}gestion_page_gru_page_menu` (
                `page_menu_id` int(11) NOT NULL AUTO_INCREMENT,
                `page_menu_libelle` varchar(200) NOT NULL,
                `page_menu_content` text DEFAULT NULL,
                `page_menu_is_default` int(1) DEFAULT 0 NOT NULL COMMENT '1: Menu par défaut du site, il ne doit y en avoir qu''un seul',
                PRIMARY KEY (`page_menu_id`)
            ) ENGINE=MyISAM  DEFAULT CHARSET=utf8
SQL
        );

        $wpdb->query(<<<SQL
            CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}gestion_page_gru_page_correspondance_menu` (
                `page_correspondance_menu_page_id` bigint(20) NULL,
                `page_correspondance_menu_menu_id` int(11) NOT NULL,
                FOREIGN KEY (`page_correspondance_menu_page_id`) REFERENCES {$wpdb->prefix}posts(`ID`),
                FOREIGN KEY (`page_correspondance_menu_menu_id`) REFERENCES {$wpdb->prefix}gestion_page_gru_page_menu(`page_menu_id`)
            ) ENGINE=MyISAM  DEFAULT CHARSET=utf8
SQL
        );

        $wpdb->query(<<<SQL
            CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}gestion_page_gru_function` (
                `function_id` int(11) NOT NULL AUTO_INCREMENT,
                `function_libelle` varchar(100) NOT NULL,
                `function_name` varchar(50) NOT NULL,
                `function_need_get_params` TINYINT(1) NOT NULL DEFAULT '0',
                `function_params_canvas` text DEFAULT NULL,
                PRIMARY KEY (`function_id`)
            ) ENGINE=MyISAM  DEFAULT CHARSET=utf8
SQL
        );


        self::init_general_settings();
    }

    /**
     * initGeneralSettings
     */
    public static function init_general_settings() {
        //insertion en bdd des shortcodes par défaut
        global $wpdb;

        $result = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}gestion_page_gru_function`");
        if (count($result) == 0) {
            $wp_gestion_page_gru_function = array(
                
                //-----------------------------------------------------------------------------------------------------------------------------------------
                //--------------------------------------------------------- FUNCTION COMPTE ---------------------------------------------------------------
                array('function_libelle' => 'Compte - Formulaire de connexion', 'function_name' => 'cde_formulaire_connexion', 'function_params_canvas' => NULL),
                array('function_libelle' => 'Compte - Formulaire de création de compte', 'function_name' => 'cde_formulaire_creation_compte', 'function_params_canvas' => NULL),
                array('function_libelle' => 'Compte - Formulaire de modification de compte', 'function_name' => 'cde_formulaire_modification_compte', 'function_params_canvas' => NULL),
                array('function_libelle' => 'Compte - Formulaire modification de mot de passe', 'function_name' => 'cde_modification_mot_de_passe', 'function_params_canvas' => NULL),
                array('function_libelle' => 'Compte - formulaire mot de passe oublié', 'function_name' => 'cde_mot_de_passe_oublie', 'function_params_canvas' => NULL),
                array('function_libelle' => 'Compte - Activation de compte', 'function_name' => 'cde_activation_compte', 'function_params_canvas' => NULL),
                array('function_libelle' => 'Compte - Bouton créer mon compte', 'function_name' => 'cde_lien_creation_compte', 'function_params_canvas' => NULL),
                
                //-----------------------------------------------------------------------------------------------------------------------------------------
                //--------------------------------------------------------- FUNCTION ENTITE ---------------------------------------------------------------
                array('function_libelle' => 'Entité - Formulaire de création d\'entité', 'function_name' => 'cde_entite_creer', 'function_params_canvas' => 'a:1:{s:11:"type_entite";a:7:{i:0;s:16:"CDE_AUTREORGPUBS";i:1;s:12:"CDE_COLLEGES";i:2;s:10:"CDE_FOYERS";i:3;s:16:"CDE_ASSOCIATIONS";i:4;s:12:"CDE_COMMUNES";i:5;s:12:"CDE_SOCIETES";i:6;s:21:"CDE_INTERCOMMUNALITES";}}'),
                array('function_libelle' => 'Entité - Formulaire de modification de l\'entité ', 'function_name' => 'cde_entite_modifier', 'function_need_get_params' => '1', 'function_params_canvas' => 'a:1:{s:11:"type_entite";a:7:{i:0;s:16:"CDE_AUTREORGPUBS";i:1;s:12:"CDE_COLLEGES";i:2;s:10:"CDE_FOYERS";i:3;s:16:"CDE_ASSOCIATIONS";i:4;s:12:"CDE_COMMUNES";i:5;s:12:"CDE_SOCIETES";i:6;s:21:"CDE_INTERCOMMUNALITES";}}'),
                array('function_libelle' => 'Entité - Informations de l\'entité', 'function_name' => 'cde_entite_infos', 'function_need_get_params' => '1', 'function_params_canvas' => NULL),
                array('function_libelle' => 'Entité - Liste des intances d\'une entité', 'function_name' => 'cde_entite_liste_instances', 'function_params_canvas' => 'a:1:{s:11:"type_entite";a:7:{i:0;s:16:"CDE_AUTREORGPUBS";i:1;s:12:"CDE_COLLEGES";i:2;s:10:"CDE_FOYERS";i:3;s:16:"CDE_ASSOCIATIONS";i:4;s:12:"CDE_COMMUNES";i:5;s:12:"CDE_SOCIETES";i:6;s:21:"CDE_INTERCOMMUNALITES";}}'),

                //-----------------------------------------------------------------------------------------------------------------------------------------
                //--------------------------------------------------------- FUNCTION INDIVIDU -------------------------------------------------------------
                array('function_libelle' => 'Individu - Formulaire de création d\'un individu', 'function_name' => 'cde_entite_creer_individu', 'function_need_get_params' => '1', 'function_params_canvas' => 'a:1:{s:11:"type_entite";a:7:{i:0;s:10:"CDE_FOYERS";i:1;s:16:"CDE_ASSOCIATIONS";i:2;s:16:"CDE_AUTREORGPUBS";i:3;s:12:"CDE_COLLEGES";i:4;s:12:"CDE_COMMUNES";i:5;s:12:"CDE_SOCIETES";i:6;s:21:"CDE_INTERCOMMUNALITES";}}'),
                array('function_libelle' => 'Individu - Formulaire de modification d\'un individu', 'function_name' => 'cde_entite_modifier_individu', 'function_need_get_params' => '1', 'function_params_canvas' => 'a:1:{s:11:"type_entite";a:7:{i:0;s:10:"CDE_FOYERS";i:1;s:16:"CDE_ASSOCIATIONS";i:2;s:16:"CDE_AUTREORGPUBS";i:3;s:12:"CDE_COLLEGES";i:4;s:12:"CDE_COMMUNES";i:5;s:12:"CDE_SOCIETES";i:6;s:21:"CDE_INTERCOMMUNALITES";}}'),
                array('function_libelle' => 'Individu - Informations de l\'individu ', 'function_name' => 'cde_individu_infos', 'function_params_canvas' => NULL),
                array('function_libelle' => 'Individu - Liste des individus d\'une entité ', 'function_name' => 'cde_entite_liste_individus', 'function_need_get_params' => '1', 'function_params_canvas' => NULL),

                //-----------------------------------------------------------------------------------------------------------------------------------------
                //--------------------------------------------------------- FUNCTION DEMANDE --------------------------------------------------------------
                array('function_libelle' => 'Demande - Formulaire de création de demande', 'function_name' => 'cde_entite_creer_demande', 'function_need_get_params' => '1', 'function_params_canvas' => NULL),
                array('function_libelle' => 'Demande - Formulaire de modification de demande', 'function_name' => 'cde_entite_modifier_demande', 'function_need_get_params' => '1', 'function_params_canvas' => NULL),
                array('function_libelle' => 'Demande - Informations d\'une demande', 'function_name' => 'cde_entite_visualiser_demande', 'function_need_get_params' => '1', 'function_params_canvas' => NULL),
                array('function_libelle' => 'Demande - Liste des demandes d\'une entité ', 'function_name' => 'cde_entite_liste_demandes', 'function_need_get_params' => '1', 'function_params_canvas' => 'a:2:{s:11:"type_entite";a:8:{i:0;s:12:"CDE_SOCIETES";i:1;s:16:"CDE_ASSOCIATIONS";i:2;s:12:"CDE_COLLEGES";i:3;s:12:"CDE_COMMUNES";i:4;s:12:"CDE_SOCIETES";i:5;s:10:"CDE_FOYERS";i:6;s:21:"CDE_INTERCOMMUNALITES";i:7;s:16:"CDE_AUTREORGPUBS";}s:9:"brouillon";a:2:{i:0;s:3:"oui";i:1;s:3:"non";}}'),

                //-----------------------------------------------------------------------------------------------------------------------------------------
                //--------------------------------------------------------- FUNCTION DOCUMENT -------------------------------------------------------------                            
                
                array('function_libelle' => 'Document - Liste des documents', 'function_name' => 'cde_entite_liste_documents', 'function_params_canvas' => NULL),
                array('function_libelle' => 'Document - Coffre-fort Infos', 'function_name' => 'cde_coffre_fort_infos', 'function_params_canvas' => NULL),
                array('function_libelle' => 'Document - Coffre-fort documents details', 'function_name' => 'cde_coffre_fort_document_details', 'function_params_canvas' => NULL),
                array('function_libelle' => 'Document - Erreur Download', 'function_name' => 'cde_coffre_fort_erreur_download', 'function_params_canvas' => NULL),

                // array('function_libelle' => 'cde_entite_creer_liaisons_individus', 'function_name' => 'cde_entite_creer_liaisons_individus', 'function_need_get_params' => '1', 'function_params_canvas' => 'a:1:{s:4:"type";s:10:"CDE_FOYERS";}'),
                // array('function_libelle' => 'cde_entite_modifier_liaisons_individus', 'function_name' => 'cde_entite_modifier_liaisons_individus', 'function_need_get_params' => '1', 'function_params_canvas' => 'a:1:{s:4:"type";s:10:"CDE_FOYERS";}'),

                //-----------------------------------------------------------------------------------------------------------------------------------------
                //--------------------------------------------------------- FUNCTION MESSAGES (FIL DE DISCUSSION) --------------------------------------------------------------
                array('function_libelle' => 'Fil de discussion - Messages non lus concernant les demandes d\'une entité', 'function_name' => 'cde_entite_liste_messages_recents', 'function_need_get_params' => '1', 'function_params_canvas' => 'a:2:{s:11:"type_entite";a:8:{i:0;s:12:"CDE_SOCIETES";i:1;s:16:"CDE_ASSOCIATIONS";i:2;s:12:"CDE_COLLEGES";i:3;s:12:"CDE_COMMUNES";i:4;s:12:"CDE_SOCIETES";i:5;s:10:"CDE_FOYERS";i:6;s:21:"CDE_INTERCOMMUNALITES";i:7;s:16:"CDE_AUTREORGPUBS";}s:9:"brouillon";a:2:{i:0;s:3:"oui";i:1;s:3:"non";}}'),
                array('function_libelle' => 'Fil de discussion - Fil de discussion d\'une demande', 'function_name' => 'cde_entite_visualiser_messages_demande', 'function_need_get_params' => '1', 'function_params_canvas' => NULL),
                array('function_libelle' => 'Fil de discussion - Liste des fils de discussion d\'une entité', 'function_name' => 'cde_entite_liste_fils_discussion', 'function_need_get_params' => '1', 'function_params_canvas' => 'a:2:{s:11:"type_entite";a:8:{i:0;s:12:"CDE_SOCIETES";i:1;s:16:"CDE_ASSOCIATIONS";i:2;s:12:"CDE_COLLEGES";i:3;s:12:"CDE_COMMUNES";i:4;s:12:"CDE_SOCIETES";i:5;s:10:"CDE_FOYERS";i:6;s:21:"CDE_INTERCOMMUNALITES";i:7;s:16:"CDE_AUTREORGPUBS";}s:9:"brouillon";a:2:{i:0;s:3:"oui";i:1;s:3:"non";}}'),

                //-----------------------------------------------------------------------------------------------------------------------------------------
            );
            
            foreach ($wp_gestion_page_gru_function as $function) {
                $wpdb->insert("{$wpdb->prefix}gestion_page_gru_function", $function);
            }
        }             
        
        try {
            // TODO RECUPERATION TELESERVICE AVEC WEB SERVICE
        } catch (Exception $ex) {
            
        }
    }

    /**
     * deactivation
     */
    public static function deactivation() {
        
    }

}

new Gestion_Page_GRU_Install();
