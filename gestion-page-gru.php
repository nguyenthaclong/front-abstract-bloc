<?php

/**
 * Plugin Name: front-abstract-tuile
 * Version: 3.0.0
 * Author: Communauté CAPDEMAT
 * Author URI: https://www.communaute-capdemat.fr/
 *
 * @package GestionPageGRU
 */

require plugin_dir_path( __FILE__ ) . 'vendor/autoload.php';

class Gestion_Page_GRU {

    public function __construct() {
        remove_filter('authenticate', 'wp_authenticate_username_password', 20);
        add_filter('authenticate', array($this, 'authenticate_mail'), 20, 3);
        include_once plugin_dir_path(__FILE__) . 'gestion-page-gru-install.php';
        include_once plugin_dir_path(__FILE__) . 'includes/admin/gestion-page-gru-tools.class.php';
        include_once plugin_dir_path(__FILE__) . 'includes/admin/gestion-page-gru-user-context.class.php';

        if (is_admin()) {
            include_once plugin_dir_path(__FILE__) . 'includes/admin/gestion-page-gru-general.class.php';
            include_once plugin_dir_path(__FILE__) . 'includes/admin/gestion-page-gru-grid.class.php';
            include_once plugin_dir_path(__FILE__) . 'includes/admin/gestion-page-gru-menu.class.php';
            include_once plugin_dir_path(__FILE__) . 'includes/admin/gestion-page-gru-tuile.class.php';
            include_once plugin_dir_path(__FILE__) . 'includes/admin/gestion-page-gru-import-export.class.php';
        }

        include_once plugin_dir_path(__FILE__) . 'includes/gestion-page-gru-tuile-dynamic-style.class.php';
        include_once plugin_dir_path(__FILE__) . 'includes/cde-functions/gestion-page-gru-functions.php';
        include_once plugin_dir_path(__FILE__) . 'includes/cde-functions/gestion-page-gru-functions-account.php';
        include_once plugin_dir_path(__FILE__) . 'includes/cde-functions/gestion-page-gru-functions-entity.php';
        include_once plugin_dir_path(__FILE__) . 'includes/cde-functions/gestion-page-gru-functions-individual.php';
        include_once plugin_dir_path(__FILE__) . 'includes/cde-functions/gestion-page-gru-functions-request.php';
        include_once plugin_dir_path(__FILE__) . 'includes/cde-functions/gestion-page-gru-functions-document.php';
        include_once plugin_dir_path(__FILE__) . 'includes/cde-functions/gestion-page-gru-functions-message.php';
        include_once plugin_dir_path(__FILE__) . 'includes/api/gestion-page-gru-api.class.php';
        include_once plugin_dir_path(__FILE__) . 'includes/api/gestion-page-gru-api-account.class.php';
        include_once plugin_dir_path(__FILE__) . 'includes/api/gestion-page-gru-api-entity.class.php';
        include_once plugin_dir_path(__FILE__) . 'includes/api/gestion-page-gru-api-individual.class.php';
        include_once plugin_dir_path(__FILE__) . 'includes/api/gestion-page-gru-api-request.class.php';
        include_once plugin_dir_path(__FILE__) . 'includes/api/gestion-page-gru-api-service.class.php';
        include_once plugin_dir_path(__FILE__) . 'includes/api/gestion-page-gru-api-document.class.php';
        include_once plugin_dir_path(__FILE__) . 'includes/api/gestion-page-gru-api-message.class.php';
        include_once plugin_dir_path(__FILE__) . 'includes/gestion-page-gru-menu-generation.class.php';
        include_once plugin_dir_path(__FILE__) . 'includes/gestion-page-gru-grid-generation.class.php';

        $plugin_base_name = explode('/', plugin_basename(__FILE__))[0];
        wp_register_style('gestion-page-gru', plugins_url("$plugin_base_name/assets/css/gestion-page-gru.css"));
        wp_register_style('fil-de-discussion', plugins_url("$plugin_base_name/assets/css/fil-de-discussion.css"));
        wp_register_style('gestion-page-gru-bootstrap-reboot', plugins_url("$plugin_base_name/assets/css/bootstrap-reboot.min.css"));
        wp_register_style('gestion-page-gru-bootstrap', plugins_url("$plugin_base_name/assets/css/bootstrap.min.css"));
        wp_register_style('gestion-page-gru-bootstrap-grid', plugins_url("$plugin_base_name/assets/css/bootstrap-grid.min.css"));
        wp_register_style('gestion-page-gru-fontawesome', plugins_url("$plugin_base_name/assets/fonts/css/all.min.css"));
        wp_enqueue_style('gestion-page-gru');
        wp_enqueue_style('fil-de-discussion');
        wp_enqueue_style('gestion-page-gru-bootstrap-reboot');
        wp_enqueue_style('gestion-page-gru-bootstrap');
        wp_enqueue_style('gestion-page-gru-bootstrap-grid');
        wp_enqueue_style('gestion-page-gru-fontawesome');

        register_activation_hook(__FILE__, array('Gestion_Page_GRU_Install', 'install'));
        register_deactivation_hook(__FILE__, array('Gestion_Page_GRU_Install', 'deactivation'));

        load_theme_textdomain('gestionpagegru', plugin_dir_path(__FILE__) . '/languages');
    }

    public function authenticate_mail($user, $email, $password) {

        // Vérifier la validité des accès.
        if (empty($email) || empty($password) || !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return new WP_Error('authentication_failed', '');
        }

        // Vérifier si l'utilisateur est présent coté SuiteCRM (Récupérer l'access token).
        $api = new Gestion_Page_GRU_Api();
        $access_token = $api->get_access_token_usager($email, $password);
        $user_context = Admin_Gestion_Page_GRU_Tools::get_user_context();

        // Si pas d'access token.
        if ($access_token === null) {
            // C'est peut être l'admin.
            $user = (filter_var($email, FILTER_VALIDATE_EMAIL)) ?
                $user_context->get_user_by('email', $email) : $user_context->get_user_by('login', $email);

            // Si pas d'ultilisateur (donc pas d'admin).
            if (!$user) {
                return new WP_Error('authentication_failed', '');
            }

            // Vérifier le mot de passe de l'utilisateur WP.
            if (!wp_check_password($password, $user->user_pass, $user->ID)) {
                return new WP_Error('authentication_failed', '');
            }
        } else {
            // Récupérer le gru_id à partir de l'access token.
            $rest_api_comptes = new Gestion_Page_GRU_Api_Account();
            $account_id_data = $rest_api_comptes->get_account_id();
            $gru_id = isset($account_id_data->data->id) ? $account_id_data->data->id : null;

            // Récupérer l'utilisateur à partir du gru_id.
            $user = $user_context->get_user_by('login', $gru_id);

            // Si pas d'utilisateur, en créer un.
            if (!$user) {
                // Créer un utilisateur seulement avec le gru_id car les autres détails sont gérés par SuiteCRM.
                $user_id = $user_context->insert_user(['user_login' => $gru_id]);

                $user_context->add_user_meta($user_id, 'gru_id', $gru_id);

                // Récupérer l'utilisateur WP à partir du gru_id.
                $user = $user_context->get_user_by('login', $gru_id);
            }

            // Init usager et access token.
            $api->init_usager($user);
        }

        return $user;
    }
}

function capdemat_session_start() {
    if (!session_id()) {
        @session_start();
    }
}
add_action('init', 'capdemat_session_start', 1);

new Gestion_Page_GRU();

add_filter('show_admin_bar', '__return_false');
add_filter('login_redirect', 'custom_login_redirect', 10, 3);

function custom_login_redirect($redirect_to, $requested_redirect_to, $user) {
    $link_options = get_option(Admin_Gestion_Page_GRU_Tools::links_pages_option);
    $link_connexion = get_page_link($link_options['crm_link_connexion']);
    $admin_url = get_admin_url();
    
    if ($admin_url != $redirect_to) {
        if (is_wp_error($user)) {
            Admin_Gestion_Page_GRU_Tools::set_gru_msg("Login / mot de passe incorrect, veuillez réessayer", "error");
            wp_redirect($link_connexion);
            exit();
        } else {
            $allowed_roles = array('editor', 'administrator', 'author');
            if (array_intersect($allowed_roles, $user->roles)) {
                wp_redirect(admin_url());
                exit();
            }
            $link_my_account = get_page_link($link_options['crm_link_my_account']);
            wp_redirect($link_my_account);
            exit();
        }
    }

    return $redirect_to;
}
