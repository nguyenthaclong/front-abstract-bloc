<?php

namespace CDE\Application\Actors\Admin\UseCases\CreateTuile\Repositories\Core\TuileLink;

use CDE\Application\Actors\Admin\UseCases\CreateTuile\Repositories\Core\AbstractCreateTuileRepository;
use CDE\Entities\Tuiles\Core\TuileLink\TuileLink;
use CDE\Entities\Tuiles\TuileInterface;

final class CreateTuileLinkRepository extends AbstractCreateTuileRepository
{
    public function persistSpecificData(TuileInterface $tuile)
    {
        # add specific data
        /** @var TuileLink $tuile */
        $resultOpDb = $this->db->update(self::TUILE_ATTRIBUTES_TABLE, ['value'    => $tuile->getLink()],
                                                                      ['tuile_id' => $tuile->getId(),
                                                                       'name'     => 'link']);
        if (false === $resultOpDb) {
            throw new \RuntimeException(self::DB_EXCEPTION_MESSAGE, 500);
        } elseif (0 === $resultOpDb) {

            # nothing is updated, we must insert
            $this->db->insert(self::TUILE_ATTRIBUTES_TABLE, ['tuile_id' => $tuile->getId(),
                                                             'name'     => 'link',
                                                             'value'    => $tuile->getLink()]);
        }
        return $tuile;
    }

}