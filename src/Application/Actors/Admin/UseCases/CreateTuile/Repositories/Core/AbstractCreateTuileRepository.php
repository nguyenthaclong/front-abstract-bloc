<?php
namespace CDE\Application\Actors\Admin\UseCases\CreateTuile\Repositories\Core;

use CDE\Actors\Admin\UseCases\CreateTuile\Boundaries\RepositoryInterface;
use CDE\Entities\Tuiles\Core\TuileLink\TuileLink;
use CDE\Entities\Tuiles\TuileInterface;
use InvalidArgumentException;
use ReflectionClass;
use ReflectionProperty;

abstract class AbstractCreateTuileRepository implements RepositoryInterface
{
    const TUILE_TABLE            = 'tuiles';
    const TUILE_ATTRIBUTES_TABLE = 'tuiles_attributes';
    const DB_EXCEPTION_MESSAGE   = 'Persist in database has failed.';

    /** @var wpdb  */
    protected $db;

    public function __construct($db)
    {
        $this->db = $db;
    }


    abstract public function persistSpecificData(TuileInterface $tuile);


    public function persist(TuileInterface $tuile): TuileInterface
    {
        /** @var TuileLink $tuile */
        $id = $tuile->getId();
        $prepareData = ['title'       => $tuile->getTitle(),
                        'sub_title'   => $tuile->getSubTitle(),
                        'description' => $tuile->getDescription(),
                        'content'     => $tuile->getContent()];
        if (empty($id)) {
            $resultOpDb = $this->db->insert(self::TUILE_TABLE, $prepareData);
            $id = $this->db->insert_id;
        } else {
            $resultOpDb = $this->db->update(self::TUILE_TABLE, $prepareData, ['id' => $id]);
        }

        # throw error if any
        if (false === $resultOpDb) {
            throw new \RuntimeException(self::DB_EXCEPTION_MESSAGE, 500);
        }

        #update id
        $tuile->setId($id);

        # add specific data
        $this->persistSpecificData($tuile);

        # add style data
        $styleTuile = $tuile->getStyle();
        try {
            $reflect = new ReflectionClass($styleTuile);
            $props   = $reflect->getProperties(ReflectionProperty::IS_PUBLIC);
            foreach ($props as $prop) {
                $nameProp = $prop->getName();

                # make update first
                $resultOpDb = $this->db->update(self::TUILE_ATTRIBUTES_TABLE, ['value'    => $prop->getValue($styleTuile)],
                                                                              ['tuile_id' => $id,
                                                                               'name'     => $nameProp]);
                # throw error if any
                if (false === $resultOpDb) {
                    throw new \RuntimeException(self::DB_EXCEPTION_MESSAGE, 500);
                } elseif (0 === $resultOpDb) {

                    # nothing is updated, we must insert
                    $resultOpDb = $this->db->insert(self::TUILE_ATTRIBUTES_TABLE, ['tuile_id' => $id,
                                                                                    'name'     => $nameProp,
                                                                                    'value'    => $prop->getValue($styleTuile)]);
                }

                # check error again
                if (false === $resultOpDb) {
                    throw new \RuntimeException(self::DB_EXCEPTION_MESSAGE, 500);
                }
            }
        } catch (\Exception $e) {
            throw new InvalidArgumentException($e->getMessage(), 500);
        }

        return $tuile;
    }

}