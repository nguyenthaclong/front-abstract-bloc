<?php

namespace CDE\Application\Actors\Admin\UseCases\CreateTuile\Presenters;

use CDE\Actors\BoundaryDataFactory;
use CDE\Actors\BoundaryDataInterface;
use CDE\Application\Actors\Admin\Controller\Boundaries\CreateTuilePresenterInterface;

class CreateTuilePresenter implements CreateTuilePresenterInterface
{

    /**
     * @var CreateTuileViewInterface
     */
    private $view;

    /** @var string */
    private $tuileType;

    public function __construct(CreateTuileViewInterface $view, string $tuileType)
    {
        $this->view      = $view;
        $this->tuileType = $tuileType;
    }

    /**
     * @inheritDoc
     */
    public function presentCreateTuileInputForm() : string
    {
        # print out
        return $this->view->showCreateTuileInputForm();
    }

    /**
     * @inheritDoc
     */
    public function presentCreateTuileDone(BoundaryDataInterface $responseData): string
    {
        $viewData = new CreateTuileViewData();
        $factory  = new BoundaryDataFactory();
        $viewData->data = $factory->getInstanceResponseData('AdminCreateTuile', $this->tuileType);
        $this->view->showCreateTuileResponse($viewData);
    }

    /**
     * @inheritDoc
     */
    public function presentCreateTuileInvalidArgumentException(): string
    {
        $this->view->showCreateTuileInputDataInvalid();
    }

    /**
     * @inheritDoc
     */
    public function presentCreateTuileRuntimeException(): string
    {
        $this->view->showCreateTuileRunTimeError();
    }
}