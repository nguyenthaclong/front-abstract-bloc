<?php
namespace CDE\Application\Actors\Admin\UseCases\CreateTuile\Presenters;

interface CreateTuileViewInterface {
    
    public function showCreateTuileInputForm() : string;

    public function showCreateTuileResponse(CreateTuileViewData $viewData): string;

    public function showCreateTuileInputDataInvalid() : string;

    public function showCreateTuileRunTimeError() : string;

}