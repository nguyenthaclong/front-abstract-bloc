<?php
namespace CDE\Application\Actors\Admin\UseCases\CreateTuile\Presenters;


use CDE\Actors\BoundaryDataInterface;

class CreateTuileViewData
{
    /** @var BoundaryDataInterface */
    public $data;

}