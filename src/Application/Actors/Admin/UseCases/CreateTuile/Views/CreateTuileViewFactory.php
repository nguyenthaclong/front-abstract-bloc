<?php
namespace CDE\Application\Actors\Admin\UseCases\CreateTuile\Views;

use CDE\Application\Actors\Admin\UseCases\CreateTuile\Presenters\CreateTuileViewInterface;

class CreateTuileViewFactory
{
    const NAMESPACE_VIEW = "CDE\\Application\\Actors\\Admin\\UseCases\\CreateTuile\\Views";
    const CORE_IDENTIFIER       = "Core";
    const EXTENSION_IDENTIFIER  = "Extension";

    /**
     * @param string $typeTuile
     * @return CreateTuileViewInterface
     * @throws \InvalidArgumentException
     */
    public static function getView(string $typeTuile)
    {
        $dirCore = __DIR__."/".self::CORE_IDENTIFIER."/";
        $dirExt  = __DIR__."/".self::EXTENSION_IDENTIFIER."/";

        $relPathClassFile = '%1$s/Create%1$sView.php';
        $relativePath = sprintf($relPathClassFile, $typeTuile);

        $relPathClassNameSpace = '%1$s\\Create%1$sView';
        $relativeNameSpace = sprintf($relPathClassNameSpace, $typeTuile);

        $pathCoreClass = $dirCore. $relativePath;
        $pathExtClass  = $dirExt. $relativePath;

        if (file_exists($pathCoreClass)) {
            $className = self::NAMESPACE_VIEW . "\\".self::CORE_IDENTIFIER."\\".$relativeNameSpace;
            return new $className();
        } elseif (file_exists($pathExtClass)) {
            $className = self::NAMESPACE_VIEW . "\\".self::EXTENSION_IDENTIFIER."\\".$relativeNameSpace;
            return new $className();
        } else {
            throw new \InvalidArgumentException("Class Create[$typeTuile]View not found.");
        }
    }
}