<?php
namespace CDE\Application\Actors\Admin\UseCases\CreateTuile\Views;

use Admin_Gestion_Page_GRU_Tools;
use CDE\Actors\AbstractDataStructure;
use CDE\Application\Actors\Admin\UseCases\CreateTuile\Presenters\CreateTuileViewData;
use CDE\Application\Actors\Admin\UseCases\CreateTuile\Presenters\CreateTuileViewInterface;

abstract class AbstractCreateTuileView implements CreateTuileViewInterface
{

    abstract protected function showTuilePreVisual() : string;

    abstract protected function showTuileSpecificInputForm() : string;

    private function showTuileBasicInputForm(CreateTuileViewData $viewData = null)
    {
        $title = $description = "";
        if ($viewData !== null) {
            /** @var AbstractDataStructure $dataForView */
            $dataForView = $viewData->data;
            $title       = $dataForView->title;
            $description = $dataForView->description;
        }

        return <<<HTML
<input type="hidden" name="tuile_id" id="tuile_id" value=""/>
                   <input type="hidden" name="tuile_type" id="tuile_type" value=""/>
                   <input type="hidden" name="tuile_icon" id="tuile_icon" value="" />
                   <input type="hidden" name="tuile_image_sup" id="tuile_image_sup" value="" />
                   <input type="hidden" name="tuile_teleservice_id" id="tuile_teleservice_id" />
                    <div class="row">
                        <div class="col-lg-12">
                           <h5>Formulaire d'édition</h5>
                        </div>
                    </div>
                   <div class="row">
                       <div class="col-lg-3">
                           Nom de la tuile
                       </div>
                       <div class="col-lg-9">
                           <input type="text" id="tuile_title" name="tuile_title" value="$title" />
                       </div> 
                   </div>      
                   <div class="row">
                       <div class="col-lg-3">
                           Description de la tuile
                       </div>
                       <div class="col-lg-9">
                           <textarea rows='5' id="tuile_description" name="tuile_description">$description</textarea>
                       </div>
                   </div>   
                    <div class="row" id="content-upload-image"> 
                       <div class="col-lg-3">
                           Image de fond
                           <i class="fa fa-trash" id="remove_image" title="Retirer l'image" />
                       </div>
                       <div class="col-lg-9">
                           <input type="file" id="tuile_image" name="tuile_image" accept="image/x-png,image/gif,image/jpeg"/>
                       </div> 
                       <div id="gestion-gru-ajax-msg-upload" class="col-lg-12">
                           <div class="updated notice" style="display:none">
                               <p class="msg"></p>
                           </div>
                           <div class="error notice" style="display:none;">
                               <p class="msg"></p>
                           </div>
                           <div class="infos notice" style="display:none;">
                               <p class="msg"></p>
                           </div>
                       </div>
                   </div>      
                   <div class="row" id="show_icon_list_content" > 
                       <div class="col-lg-3 highligth-target-off">
                           Icone de la tuile
                           <i class="fa fa-trash" id="remove_icon" title="Retirer l'icone" />
                       </div> 
                       <div class="col-lg-9">
                           <button id="show_icon_list" class="gestion-page-gru-button"> Afficher formulaire de sélection d'icones </button>
                       </div>  
                       <br/>   
                   </div>
HTML;

    }


    private function showTuileStyleInputForm(CreateTuileViewData $viewData = null)
    {
        # prepare font list
        $Admin_Gestion_Page_GRU_Tools = new Admin_Gestion_Page_GRU_Tools();
        $fontFamilies = $Admin_Gestion_Page_GRU_Tools->get_list_font_family();
        $fontFamiliesOptions = implode("",array_map(function ($font) {
            return "<option value='$font' style='font-family: $font'>$font</option>";
        }, $fontFamilies));

        # init ours variables
        $titleFontSize = $bodyFontSize = $titleColor = $bodyColor = "";
        $titleBgColor = $bodyBgColor = $iconFontSize = $iconColor = "";
        if ($viewData !== null) {
            /** @var AbstractDataStructure $dataForView */
            $dataForView = $viewData->data;
            $titleFontSize = $dataForView->style_title_font_size;
            $bodyFontSize  = $dataForView->style_body_font_size;
            $titleColor    = $dataForView->style_title_color;
            $bodyColor     = $dataForView->style_body_color;
            $titleBgColor  = $dataForView->style_title_background_color;
            $bodyBgColor   = $dataForView->style_body_background_color;
            $iconFontSize  = $dataForView->style_icon_font_size;
            $iconColor     = $dataForView->style_icon_color;
        }

        return <<<HTML
<div id="select-page-tuile-style-content">
                        <div class="row"> 
                           <div class="col-lg-3">
                               Police du titre
                           </div>
                           <div class="col-lg-9">                                    
                               <select id="title-font-family" name="font-family">
                                    $fontFamiliesOptions
                               </select>
                           </div>
                       </div>
                       <div class="row"> 
                           <div class="col-lg-3">
                               Police de la desc.
                           </div>
                           <div class="col-lg-9">                                    
                               <select id="body-font-family" name="font-family">
                                    $fontFamiliesOptions
                               </select>
                           </div>
                       </div>
                       <div class="row"> 
                           <div class="col-lg-3">
                               Taille titre (px)
                           </div>
                           <div class="col-lg-3">
                               <input id="title-font-size" type="text" value="$titleFontSize"/>
                           </div>
                           <div class="col-lg-3">
                               Taille desc. (px)
                           </div>
                           <div class="col-lg-3">
                               <input id="body-font-size" type="text" value="$bodyFontSize"/>
                           </div>
                       </div>
                       <div class="row"> 
                           <div class="col-lg-3">
                               Couleur texte titre
                           </div>
                           <div class="col-lg-3">
                               <input id="title-color" value="$titleColor" type="text" autocomplete="off" class="color-picker gestion-page-gru-select"/>
                           </div>
                           <div class="col-lg-3">
                               Couleur texte desc.
                           </div>
                           <div class="col-lg-3">
                               <input id="body-color" value="$bodyColor" type="text" autocomplete="off" class="color-picker gestion-page-gru-select"/>
                           </div>
                       </div>
                       <div class="row"> 
                           <div class="col-lg-3">
                               Couleur fond titre
                           </div>
                           <div class="col-lg-3">
                               <input id="title-background-color" value="$titleBgColor" type="text" autocomplete="off" class="color-picker gestion-page-gru-select"/>
                           </div>
                           <div class="col-lg-3">
                               Couleur fond desc.
                           </div>
                           <div class="col-lg-3">
                               <input id="body-background-color" value="$bodyBgColor" type="text" autocomplete="off" class="color-picker gestion-page-gru-select"/>
                           </div>
                       </div>
                       <div class="row">
                           <div class="col-lg-3">
                               Taille icône (px)
                           </div>
                           <div class="col-lg-3">
                               <input id="icon-font-size" value="$iconFontSize" type="text" />
                           </div>
                           <div class="col-lg-3">
                               Couleur icône
                           </div>
                           <div class="col-lg-3">
                               <input id="icon-color" value="$iconColor" type="text" class="color-picker gestion-page-gru-select"/>
                           </div>
                       </div>
                   </div>
HTML;
    }


    public function showCreateTuileInputForm(): string
    {
        return <<<HTML
            <div id="infos-tuile-content" class="row">
                <div id="tuile-visuel" class="col-lg-6">
                    <div class="row">
                        {$this->showTuilePreVisual()}
                    </div>
                </div>               
                <div id="infos-tuile-form" class="col-lg-6">
                   {$this->showTuileBasicInputForm()}       
                   
                   # specific tuile form here             
                   {$this->showTuileSpecificInputForm()}                
                   
                   {$this->showTuileStyleInputForm()} 
               </div> 
            </div>
HTML;
    }

    public function showCreateTuileResponse(CreateTuileViewData $viewData): string
    {
        return <<<HTML
            <div id="infos-tuile-content" class="row">
                <div id="tuile-visuel" class="col-lg-6">
                    <div class="row">
                        {$this->showTuilePreVisual()}
                    </div>
                </div>               
                <div id="infos-tuile-form" class="col-lg-6">
                   {$this->showTuileBasicInputForm($viewData)}       
                   
                   # specific tuile form here             
                   {$this->showTuileSpecificInputForm()}                
                   
                   {$this->showTuileStyleInputForm($viewData)} 
               </div> 
            </div>
HTML;
    }




    public function showCreateTuileInputDataInvalid(): string
    {
        // TODO: Implement showCreateTuileInputDataInvalid() method.
    }

    public function showCreateTuileRunTimeError(): string
    {
        // TODO: Implement showCreateTuileRunTimeError() method.
    }
}