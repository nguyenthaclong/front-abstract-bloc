<?php
namespace CDE\Application\Actors\Admin\UseCases\CreateTuile\Views\Core\TuileLink;

use CDE\Application\Actors\Admin\UseCases\CreateTuile\Views\AbstractCreateTuileView;

final class CreateTuileLinkView extends AbstractCreateTuileView
{

    protected function showTuilePreVisual(): string
    {
        return <<<EOQ
        <div class="element-gru box-shadow-gru" data-tuile-id="" name="">
            <div class="element-gru-header">
                <div class="title">
                <span class="gestion-page-gru-element-title">Tuile type lien</span>
                </div>
            </div>
            <div class="grid-element-gru-content">                
                <p>Contenu...</p>
            </div>
        </div>
EOQ;
    }

    protected function showTuileSpecificInputForm(): string
    {
        $display_external = "display:none;";

        $pages = get_pages(array('sort_column' => 'post_title'));
        $select_pages = "<select id='tuile_page_id' name='tuile_page_id' class='gestion-page-gru-select'><option value='-1'>...</option>";
        foreach ($pages as $page) {
            $page_link = get_page_link($page->ID);
            $select_pages .= '<option value="'.$page->ID.'">'.$page->post_title.' : '.str_replace(get_home_url(), "", $page_link).'</option>';
        }
        $select_pages .= "</select>";
        return <<<HTML
                <div class="row"> 
                    <div class="col-lg-3">
                        Page liée
                    </div>
                    <div class="col-lg-3 mt-1">
                        <input type="checkbox" id="switch_external_link" />
                        Lien externe
                    </div>
                    <div class="col-lg-6 content-external-link" style="$display_external">
                        <input type="text" id="tuile_page_id_external" name="tuile_page_id_external" value="" />
                    </div>
                    <div class="col-lg-6 content-internal-link" style="">
                        $select_pages
                    </div>
                </div>    
                <div class="row"> 
                    <div class="col-lg-3">
                        Texte au survol
                    </div>
                    <div class="col-lg-9">
                        <textarea rows='5' id="tuile_description_plus" name="tuile_description_plus"></textarea>
                    </div>
                </div>                
HTML;
    }
}