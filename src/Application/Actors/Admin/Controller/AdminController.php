<?php

namespace CDE\Application\Actors\Admin\Controller;


use CDE\Actors\Admin\UseCases\CreateTuile\Boundaries\RequesterInterface;
use CDE\Actors\BoundaryDataInterface;
use CDE\Application\Actors\Admin\Controller\Boundaries\CreateTuilePresenterInterface;
use InvalidArgumentException;
use RuntimeException;

class AdminController
{
    public function actionCreateTuile(BoundaryDataInterface $requestData,
                                      RequesterInterface $requester,
                                      CreateTuilePresenterInterface $presenter) : string
    {
        try {
            $responseData = $requester->doCreateTuile($requestData);
            return $presenter->presentCreateTuileDone($responseData);
        } catch (InvalidArgumentException $e) {
            $presenter->presentCreateTuileInvalidArgumentException();
        } catch (RuntimeException $e) {
            $presenter->presentCreateTuileRuntimeException();
        }
    }
}