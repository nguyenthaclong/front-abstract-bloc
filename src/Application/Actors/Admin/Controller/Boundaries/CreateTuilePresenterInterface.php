<?php
namespace CDE\Application\Actors\Admin\Controller\Boundaries;

use CDE\Actors\BoundaryDataInterface;

interface CreateTuilePresenterInterface {

    /**
     * @return string
     */
    public function presentCreateTuileInputForm() : string;

    /**
     * @param BoundaryDataInterface $responseData
     * @return string
     */
    public function presentCreateTuileDone(BoundaryDataInterface $responseData) : string;

    /**
     * @return string
     */
    public function presentCreateTuileInvalidArgumentException() : string;

    /**
     * @return string
     */
    public function presentCreateTuileRuntimeException() : string;

}