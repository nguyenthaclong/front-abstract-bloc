<?php

namespace CDE\TestApi;

use Faker\Factory as FakerFactory;
use ReflectionClass;
use ReflectionProperty;
use CDE\Actors\BoundaryDataFactory;
use CDE\Actors\BoundaryDataInterface;


/**
 * Class DataModelHelper
 * Use to decouple data structure from tests
 */
class DataModelHelper {

    /**
     * @var BoundaryDataFactory
     */
    private $dataStructureFactory;

    /**
     * @var string
     */
    private $useCase;

    public function __construct(BoundaryDataFactory $dataFactory, string $useCase)
    {
        $this->dataStructureFactory = $dataFactory;
        $this->useCase = $useCase;
    }

    /**
     * @param string $className
     * @return BoundaryDataInterface
     */
    public function createFakeResponseData(string $className): BoundaryDataInterface
    {
        $responseData = $this->dataStructureFactory->getInstanceResponseData($this->useCase, $className);
        $structure    = $this->getPropertiesBoundaryData($responseData);
        return $this->generateFakeData($responseData, $structure);
    }

    /**
     * @param string $className
     * @return BoundaryDataInterface
     */
    public function createFakeRequestData(string $className): BoundaryDataInterface
    {
        $requestData = $this->dataStructureFactory->getInstanceRequestData($this->useCase, $className);
        $structure    = $this->getPropertiesBoundaryData($requestData);
        return $this->generateFakeData($requestData, $structure);
    }


    /**
     * @param BoundaryDataInterface $boundaryData
     * @param array $structure
     * @return BoundaryDataInterface
     */
    protected function generateFakeData(BoundaryDataInterface $boundaryData, array $structure)
    {
        $faker = FakerFactory::create();
        $dateFields = $boundaryData->getDateFields();
        foreach ($structure as $propName => $propOptions) {
            switch ($propOptions['type']) {
                case 'bool':
                    $boundaryData->$propName = $faker->boolean;
                    break;
                case 'int':
                    $boundaryData->$propName = $faker->numberBetween(0,2);
                    break;
                case 'string':
                    if (array_key_exists($propName, $dateFields)) {
                        $boundaryData->$propName = $faker->date($dateFields[$propName]);
                    } elseif (strpos($propName, 'password') !== false) {
                        $boundaryData->$propName = $faker->password(6,10);
                    } elseif (strpos($propName, 'email') !== false) {
                        $boundaryData->$propName = $faker->email;
                    } elseif (strpos($propName, 'address') !== false) {
                        $boundaryData->$propName = $faker->streetAddress;
                    } elseif (strpos($propName, 'postalCode') !== false) {
                        $boundaryData->$propName = $faker->postcode;
                    } elseif (strpos($propName, 'postalCity') !== false) {
                        $boundaryData->$propName = $faker->city;
                    } elseif (strpos($propName, 'postalCountry') !== false) {
                        $boundaryData->$propName = $faker->country;
                    } else {
                        $boundaryData->$propName = $faker->userName;
                    }
                    break;
                case 'array':
                    $boundaryData->$propName = $faker->shuffleArray();
                    break;
                default:
                    $boundaryData->$propName = $faker->text(20);
                    break;
            }
        }
        return $boundaryData;
    }

    /**
     * @param BoundaryDataInterface $boundaryData
     * @return array
     */
    protected function getPropertiesBoundaryData(BoundaryDataInterface $boundaryData)
    {
        try {
            $reflect = new ReflectionClass($boundaryData);
            $props   = $reflect->getProperties(ReflectionProperty::IS_PUBLIC);
            $output = [];
            foreach ($props as $prop) {
                $name       = $prop->getName();
                $isOptional = !in_array($prop->getName(), $boundaryData->getMandatoryFields());
                $typeProp   = $this->getPropertyTypeFromDocComment($prop->getDocComment());
                $output[$name] = ['optional'=> $isOptional, 'type' => $typeProp];
            }
            return $output;
        } catch (\Exception $e) {
            return ['ReflexionError' => $e->getMessage()];
        }
    }

    /**
     * Parse doc comment to get type of property
     * @param string $docComment
     * @return string
     */
    protected function getPropertyTypeFromDocComment(string $docComment) {
        $defaultType = 'string';
        $matches = [];

        preg_match('/\*\s+@var\s+(\S+)\s+.*/m', $docComment, $matches);
        if (isset($matches[1]) && !empty($matches[1])) {
            return $matches[1];
        } else {
            return $defaultType;
        }
    }
}