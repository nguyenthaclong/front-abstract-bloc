<?php
namespace CDE\Entities\Tuiles;

use CDE\Entities\Tuiles\TuileInterface;
use CDE\Entities\Tuiles\Core\TuileLink\TuileLink;

class TuileFactory
{
    /**
     * @param string $type
     * @return TuileInterface
     */
    public static function createTuile(string $type) : TuileInterface {
        switch ($type) {
            case 'TuileLink':
                return new TuileLink();
            default:
                throw new \InvalidArgumentException("Undefined tuile type", 404);
        }
    }

}