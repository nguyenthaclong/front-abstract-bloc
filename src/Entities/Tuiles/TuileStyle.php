<?php
namespace CDE\Entities\Tuiles;


class TuileStyle
{
    /** @var string */
    public $style_title_background_color;

    /** @var string */
    public $style_title_color;

    /** @var string */
    public $style_title_font_family;

    /** @var string */
    public $style_title_font_size;

    /** @var string */
    public $style_body_background_color;

    /** @var string */
    public $style_body_color;

    /** @var string */
    public $style_body_font_family;

    /** @var string */
    public $style_body_font_size;

    /** @var string */
    public $style_icon_color;

    /** @var string */
    public $style_icon_font_size;

    /** @var string */
    public $style_logoImage;

    /** @var string */
    public $style_bgImage;
}