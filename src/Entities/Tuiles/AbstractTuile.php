<?php
namespace CDE\Entities\Tuiles;

use CDE\Actors\BoundaryDataInterface;
use InvalidArgumentException;
use ReflectionClass;
use ReflectionProperty;

abstract class AbstractTuile implements TuileInterface
{
    /** @var int */
    private $id;

    /** @var string */
    private $title;

    /** @var string */
    private $subTitle;

    /** @var string */
    private $description;

    /** @var string */
    private $content;

    /** @var TuileStyle */
    private $style;

    /** @var int */
    private $deleted;


    /**
     * @return string
     */
    abstract public function getContent(): string;

    /**
     * To override by sub type
     * @param BoundaryDataInterface $boundaryData
     * @return BoundaryDataInterface
     */
    abstract public function setSpecificProperties(BoundaryDataInterface $boundaryData) : BoundaryDataInterface;


    /**
     * Basic function to extend by sub Tuile type
     * @param BoundaryDataInterface $data
     */
    public function fillFromDataStructure(BoundaryDataInterface $data) {
        try {
            $reflect = new ReflectionClass($data);
            $props   = $reflect->getProperties(ReflectionProperty::IS_PUBLIC|ReflectionProperty::IS_PROTECTED|ReflectionProperty::IS_PRIVATE);
            foreach ($props as $prop) {
                $nameProp = $prop->getName();
                $setterProp = "set".ucfirst($nameProp);
                if (property_exists($this, $nameProp)) {
                    $this->$setterProp($prop->getValue($data));
                }
            }

            # again to setup tuileStyle
            $tuileStyle = new TuileStyle();
            foreach ($props as $prop) {
                $nameProp = $prop->getName();
                if (strpos($nameProp, 'style_') === 0) {
                    $tuileStyle->$nameProp = $prop->getValue($data);
                }
            }
            $this->setStyle($tuileStyle);
        } catch (\Exception $e) {
            throw new InvalidArgumentException($e->getMessage(), 500);
        }
    }


    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getSubTitle()
    {
        return $this->subTitle;
    }

    /**
     * @param string $subTitle
     */
    public function setSubTitle($subTitle)
    {
        $this->subTitle = $subTitle;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content)
    {
        $this->content = $content;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return TuileStyle
     */
    public function getStyle(): TuileStyle
    {
        return $this->style;
    }

    /**
     * @param TuileStyle $style
     */
    public function setStyle(TuileStyle $style)
    {
        $this->style = $style;
    }

    /**
     * @return int
     */
    public function getDeleted(): int
    {
        return $this->deleted;
    }

    /**
     * @param int $deleted
     */
    public function setDeleted(int $deleted)
    {
        $this->deleted = $deleted;
    }

}