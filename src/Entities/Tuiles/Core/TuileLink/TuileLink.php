<?php
namespace CDE\Entities\Tuiles\Core\TuileLink;

use CDE\Actors\BoundaryDataInterface;
use CDE\Entities\Tuiles\AbstractTuile;

class TuileLink extends AbstractTuile
{
    /** @var string */
    private $link;

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink(string $link)
    {
        $this->link = $link;
    }


    /**
     * @inheritDoc
     */
    public function getContent(): string
    {
        // @TODO
        return "Hi, i am ".__CLASS__;
    }

    /**
     * @inheritDoc
     */
    public function setSpecificProperties(BoundaryDataInterface $boundaryData) : BoundaryDataInterface
    {
        /** keep type */
        $nameClassParts = explode("\\", __CLASS__);
        $boundaryData->typeData = $nameClassParts[count($nameClassParts)-1];


        /** specific */
        $boundaryData->link = $this->getLink();
        return $boundaryData;
    }
}