<?php
namespace CDE\Actors\Admin\BoundaryData\Core\TuileLink;

use CDE\Actors\AbstractDataStructure;

final class RequestData extends AbstractDataStructure {

    # Specific data
    /** @var string */
    public $link;

     /**
     * @return bool
     */
    public function isEmpty(): bool
    {
        return (empty($this->link));
    }

    public function getMandatoryFields(): array
    {
        return [];
    }

    public function getDateFields(): array
    {
        return [];
    }

}