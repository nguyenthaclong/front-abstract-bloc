<?php
namespace CDE\Actors\Admin\BoundaryData\Core\TuileLink;

use CDE\Actors\AbstractDataStructure;

final class ResponseData extends AbstractDataStructure {

    # Specific data
    /** @var string */
    public $link;

    /**
     * @return bool
     */
    public function isEmpty(): bool
    {
        return (empty($this->id) && empty($this->title));
    }

    public function getMandatoryFields(): array
    {
        return ['id', 'title'];
    }

    public function getDateFields(): array
    {
        return [];
    }

}