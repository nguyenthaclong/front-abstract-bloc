<?php
namespace CDE\Actors\Admin\UseCases\CreateTuile\Boundaries;

use CDE\Actors\BoundaryDataInterface;

interface RequesterInterface
{
    public function doCreateTuile(BoundaryDataInterface $requestData) : BoundaryDataInterface;
}