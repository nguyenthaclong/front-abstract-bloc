<?php
namespace CDE\Actors\Admin\UseCases\CreateTuile\Boundaries;

use CDE\Entities\Tuiles\TuileInterface;

interface RepositoryInterface {
    public function persist(TuileInterface $tuile) : TuileInterface;
}