<?php
namespace CDE\Actors\Admin\UseCases\CreateTuile;

use CDE\Actors\Admin\UseCases\CreateTuile\Boundaries\RepositoryInterface;
use CDE\Actors\Admin\UseCases\CreateTuile\Boundaries\RequesterInterface;
use CDE\Actors\BoundaryDataFactory;
use CDE\Actors\BoundaryDataInterface;
use CDE\Entities\Tuiles\AbstractTuile;
use CDE\Entities\Tuiles\TuileFactory;
use CDE\Entities\Tuiles\TuileInterface;
use CDE\Entities\Tuiles\TuileStyle;

class CreateTuile implements RequesterInterface
{
    /** @var RepositoryInterface */
    private $repository;

    /**
     * @param RepositoryInterface $repository
     */
    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function doCreateTuile(BoundaryDataInterface $requestData): BoundaryDataInterface
    {
        # get input data
        /** @var RequestData $input */
        $input = $requestData;

        # converse request data to specific type request data
        $type = $input->typeData;
        $requestTyped = BoundaryDataFactory::createRequestDataFrom($type, $input->toArray());

        # use case scenario here

        # create tuile and set data
        /** @var TuileInterface $tuile */
        $tuile = TuileFactory::createTuile($type);
        $tuile->fillFromDataStructure($requestTyped);

        # if creation is done, it means all data is validated, we persist tuile
        /** @var TuileInterface $tuilePersisted */
        $tuilePersisted = $this->repository->persist($tuile);

        # prepare response data
        return BoundaryDataFactory::createResponseDataFrom($type, $tuilePersisted);

    }
}