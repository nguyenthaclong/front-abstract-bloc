<?php
namespace CDE\Actors\Admin\UseCases\CreateTuile;

use CDE\Actors\AbstractDataStructure;

final class RequestData extends AbstractDataStructure {

     /**
     * @return bool
     */
    public function isEmpty(): bool
    {
        return (empty($this->title));
    }

    public function getMandatoryFields(): array
    {
        return ['title'];
    }

    public function getDateFields(): array
    {
        return [];
    }

}