<?php
namespace CDE\Actors\Admin\UseCases\CreateTuile;

use CDE\Actors\AbstractDataStructure;

final class ResponseData extends AbstractDataStructure {

    /**
     * @return bool
     */
    public function isEmpty(): bool
    {
        return (empty($this->id) && empty($this->title));
    }

    public function getMandatoryFields(): array
    {
        return ['id', 'title'];
    }

    public function getDateFields(): array
    {
        return [];
    }

}