<?php
namespace CDE\Actors;

use ReflectionClass;
use ReflectionProperty;

abstract class AbstractDataStructure implements BoundaryDataInterface
{
    /** @var int */
    public $id;

    /** @var string */
    public $typeData;

    /** @var string */
    public $title;

    /** @var string */
    public $subTitle;

    /** @var string */
    public $description;

    /** @var string */
    public $content;

    # style data
    /** @var string */
    public $style_title_background_color;

    /** @var string */
    public $style_title_color;

    /** @var string */
    public $style_title_font_family;

    /** @var string */
    public $style_title_font_size;

    /** @var string */
    public $style_body_background_color;

    /** @var string */
    public $style_body_color;

    /** @var string */
    public $style_body_font_family;

    /** @var string */
    public $style_body_font_size;

    /** @var string */
    public $style_icon_color;

    /** @var string */
    public $style_icon_font_size;

    /** @var string */
    public $style_logoImage;

    /** @var string */
    public $style_bgImage;

    abstract public function getDateFields() : array;

    abstract public function getMandatoryFields() : array;

    abstract public function isEmpty() : bool;

    public function toArray(): array
    {
        try {
            $reflect = new ReflectionClass($this);
            $props   = $reflect->getProperties(ReflectionProperty::IS_PUBLIC);
            $output = [];
            foreach ($props as $prop) {
                $output[$prop->getName()] = $prop->getValue($this);
            }
            return $output;
        } catch (\Exception $e) {
            return [];
        }
    }
}