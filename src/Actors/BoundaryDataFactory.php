<?php
namespace CDE\Actors;

use CDE\Actors\Admin\BoundaryData\Core\TuileLink\RequestData as TuileLinkAdminRequestData;
use CDE\Actors\Admin\BoundaryData\Core\TuileLink\ResponseData as  TuileLinkAdminResponseData;
use CDE\Entities\Tuiles\AbstractTuile;
use CDE\Entities\Tuiles\Core\TuileLink\TuileLink;
use CDE\Entities\Tuiles\TuileInterface;
use CDE\Entities\Tuiles\TuileStyle;
use InvalidArgumentException;
use ReflectionClass;
use ReflectionProperty;


final class BoundaryDataFactory
{
    /**
     * @param string $type specific type to convert to
     * @param array $data source data request
     * @return BoundaryDataInterface
     */
    public static function createRequestDataFrom(string $type, array $data) : BoundaryDataInterface
    {
        $destRequest = null;
        switch ($type) {
            case 'TuileLink':
                $destRequest = new TuileLinkAdminRequestData();
                break;
            default:
                throw new InvalidArgumentException("Undefined tuile type", 404);
        }
        foreach ($data as $key => $value) {
            $destRequest->$key = $value;
        }

        return $destRequest;
    }

    public static function createResponseDataFrom(string $type, TuileInterface $tuile) : BoundaryDataInterface {
        $destResponse = null;
        switch ($type) {
            case 'TuileLink':
                $destResponse = new TuileLinkAdminResponseData();
                break;
            default:
                throw new InvalidArgumentException();
        }
        $destResponse->id          = $tuile->getId();
        $destResponse->title       = $tuile->getTitle();
        $destResponse->subTitle    = $tuile->getSubtitle();
        $destResponse->description = $tuile->getDescription();
        $destResponse->content     = $tuile->getContent();
        /** @var TuileStyle $style */
        $style = $tuile->getStyle();
        try {
            $reflect = new ReflectionClass($style);
            $props   = $reflect->getProperties(ReflectionProperty::IS_PUBLIC);
            foreach ($props as $prop) {
                $nameProp = $prop->getName();
                if (property_exists($destResponse, $nameProp)) {
                    $destResponse->$nameProp = $prop->getValue($style);
                }
            }
        } catch (\Exception $e) {
            throw new InvalidArgumentException($e->getMessage(), 500);
        }

        # set specific part
        return $tuile->setSpecificProperties($destResponse);
    }



    /**
     * @param string $useCase
     * @param string $className
     * @return BoundaryDataInterface
     */
    public function getInstanceRequestData(string $useCase, string $className)
    {
        switch ($useCase) {
            case 'AdminCreateTuile' :
                return $this->getRequestDataByClass($className);
            default:
                throw new InvalidArgumentException();
        }
    }

    /**
     * @param string $className
     * @return BoundaryDataInterface
     */
    protected function getRequestDataByClass(string $className) : BoundaryDataInterface {
        switch ($className) {
            case 'TuileLink':
                return new TuileLinkAdminRequestData();
            default:
                throw new InvalidArgumentException();
        }
    }

    /**
     * @param string $useCase
     * @param string $className
     * @return BoundaryDataInterface
     */
    public function getInstanceResponseData(string $useCase, string $className)
    {
        switch ($useCase) {
            case 'AdminCreateTuile' :
                return $this->getResponseDataByClass($className);
            default:
                throw new InvalidArgumentException();
        }
    }

    /**
     * @param string $className
     * @return BoundaryDataInterface
     */
    protected function getResponseDataByClass(string $className) : BoundaryDataInterface {
        switch ($className) {
            case 'TuileLink':
                return new TuileLinkAdminResponseData();
            default:
                throw new InvalidArgumentException();
        }
    }
}