/**
 * 1997-2018 Quadra Informatique
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 *
 *  @author    Quadra Informatique <modules@quadra-informatique.fr>
 *  @copyright 1997-2018 Quadra Informatique
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * 
 */

general_admin_settings.tuile_mangager = {
    /**
     * Fonction d'initation
     * 
     * @returns {void}
     */
    init: function () {
        this.bind_tuile_event();
    },
    /**
     * Bind l'ensemble des events liés à la structure de gestion des tuiles
     * 
     * @returns {void}
     */
    bind_tuile_event: function () {
        var that = this;
        $("#tuile-container #tuile-list .element-gru").unbind();
        $("#tuile-container #tuile-list .element-gru").on("click", function () {
            that.get_tuile_data($(this).data('tuile-id'));
        });
        $("#infos-tuile-content #show_icon_list").unbind();
        $("#infos-tuile-content #show_icon_list").on("click", function () {
            var dialog = $.dialog({
                title: 'Selectionnez une icone',
                useBootstrap: false,
                container: "#gestion-page-gru-content",
                content: function () {
                    var self = this;
                    $.ajax({
                        method: "POST",
                        url: general_admin_settings.base_url + "/wp-admin/admin-ajax.php",
                        data: {action: "get_icon_list"},
                        success: function (data) {
                            self.setContent(data);
                            $('.jconfirm-content .form-modif-tuile').unbind();
                            $('.jconfirm-content .form-modif-tuile').on('click', function () {
                                var icon_select = $(this).data('icon');
                                $('#tuile_icon').val(icon_select);
                                $("#tuile-visuel .grid-element-gru-content i").attr('class', icon_select);
                                dialog.close();
                            });
                            $("#filter_icon_list").on('keyup', function () {
                                if ($(this).val() === "") {
                                    $('.jconfirm-content .form-modif-tuile').show();
                                } else {
                                    $('.jconfirm-content .form-modif-tuile:not([data-icon*=' + $(this).val() + '])').hide();
                                    $('.jconfirm-content .form-modif-tuile[data-icon*=' + $(this).val() + ']').show();
                                }
                            });
                        }
                    });
                }
            });
        });
        $("#infos-tuile-content #select-tuile-list-content #select-list-tuile li").unbind();
        $("#infos-tuile-content #select-tuile-list-content #select-list-tuile li").on('click', function () {
            $(this).toggleClass('selected');
            var list_tuile = [];
            $.each($("#infos-tuile-content #select-tuile-list-content #select-list-tuile li.selected"), function (i, e) {
                list_tuile.push($(e).data('tuile-id'));
            });
            $(".gestion-tuile-gru-block-gru[data-tuile-id=" + $("#tuile_id").val() + "]").data('tuile-liste', list_tuile);
        });
        $("#infos-tuile-content #content-upload-image #tuile_image").unbind();
        $("#infos-tuile-content #content-upload-image #tuile_image").on('change', function () {
            var file_data = $(this).prop('files')[0];
            var form_data = new FormData();
            form_data.append('file', file_data);
            form_data.append('action', "upload_tuile_image");
            $.ajax({
                method: "POST",
                url: general_admin_settings.base_url + "/wp-admin/admin-ajax.php",
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (data) {
                    $("#gestion-gru-ajax-msg-upload div.notice").hide();
                    switch (data.code) {
                        case 0:
                            $("#gestion-gru-ajax-msg-upload div.error.notice p.msg").html(data.message);
                            $("#gestion-gru-ajax-msg-upload div.error.notice").show(200);
                            break;
                        case 1:
                            $("#gestion-gru-ajax-msg-upload div.updated.notice p.msg").html(data.message);
                            $("#gestion-gru-ajax-msg-upload div.updated.notice").show(200);
                            $("#infos-tuile-content #tuile-visuel > div > div > div").css('background-image', 'url(' + data.path_image + ')');
                            $("#tuile_image_sup").val('');
                            break;
                        case 2:
                            $("#gestion-gru-ajax-msg-upload div.update-nag.notice p.msg").html(data.message);
                            $("#gestion-gru-ajax-msg-upload div.update-nag.notice").show(200);
                            break;
                    }
                }
            });
        });
        $("#tuile-container #add_block_gru_type_text, #tuile-container #add_block_gru_type_link").unbind();
        $("#tuile-container #add_block_gru_type_text, #tuile-container #add_block_gru_type_link").on('click', function () {
            var block = ($(this).attr('id') === "add_block_gru_type_link") ? 
                $(that.create_element_type_link()) :
                $(that.create_element_type_text());
            $(this).before(block);
            block.show('drop', 200);
            that.bind_tuile_event();
        });
        
        var options = {
            color: '#RGB',
            customBG: '#222', // bg of page is dark, so if opcity close to 0 -> dark shines through
            doRender: 'div div', // tell it where to render bg-color if no input
            colorNames: {// get more colors in the other demo... will be displayed next to color patch
                '808080': 'grey',
                '00FFFF': 'cyan',
                '000000': 'black',
                '0000FF': 'blue',
                'FF00FF': 'magenta',
                '008000': 'green',
                'FF0000': 'red',
                'C0C0C0': 'silver',
                'FFFFFF': 'white',
                'FFFF00': 'yellow'
            },
            renderCallback: function ($elm, toggled) {
                if (toggled === false) {
                    $elm.trigger('change');
                }
            }
        };
        $("#select-page-tuile-style-content .color-picker").colorPicker(options);
    },
    bind_tuile_form_event: function () {
        $("#tuile_title").on('change', function () {
            var value = $(this).val();
            $("#infos-tuile-content #tuile-visuel .gestion-page-gru-element-title").html(value);
        });
        $("#tuile_description").on('change', function () {
            var value = $(this).val();
            $("#infos-tuile-content #tuile-visuel .grid-element-gru-content p").html(value);
        });
        $("#tuile_description_plus").on('change', function () {
            var value = $(this).val();
            $("#infos-tuile-content #tuile-visuel .grid-element-gru-content .desc-plus").html(value);
        });
        $("#remove_icon, #remove_image").unbind();
        $("#remove_icon").on('click', function () {
            $("#tuile_icon").val('');
            $("#infos-tuile-content #tuile-visuel .grid-element-gru-content i.fa").attr('class', 'fa');
        });
        $("#remove_image").on('click', function () {
            $("#tuile_image").val('');
            $("#tuile_image_sup").val('sup');
            $("#infos-tuile-content > #tuile-visuel > div > div > div").css('background-image', 'none');
        });
        
        $("#switch_external_link").on('change', function() {
            var row = $(this).closest('.row');
            if($(this).prop('checked')) {
                row.find('.content-internal-link').hide();
                row.find('.content-external-link').show();
            } else {
                row.find('.content-internal-link').show();
                row.find('.content-external-link').hide();                
            }
        });
        
        $("#title-font-family").on('change', function () {
            var value = $(this).val();
            $("#infos-tuile-content #tuile-visuel .gestion-page-gru-element-title").each(function () {
                this.style.setProperty('font-family', value, 'important');
            });
        });
        $("#title-font-size").on('change', function () {
            var value = $(this).val();
            $("#infos-tuile-content #tuile-visuel .gestion-page-gru-element-title").each(function () {
                this.style.setProperty('font-size', value + "px", 'important');
            });
        });
        $("#title-color").on('change', function () {
            var value = $(this).val();
            $("#infos-tuile-content #tuile-visuel .gestion-page-gru-element-title").each(function () {
                this.style.setProperty('color', value, 'important');
            });
        });
        $("#title-background-color").on('change', function () {
            var value = $(this).val();
            $("#infos-tuile-content #tuile-visuel .element-gru-header").each(function () {
                this.style.setProperty('background-color', value, 'important');
            });
        });
        $("#body-font-family").on('change', function () {
            var value = $(this).val();
            $("#infos-tuile-content #tuile-visuel .grid-element-gru-content p").each(function () {
                this.style.setProperty('font-family', value, 'important');
            });
        });
        $("#body-font-size").on('change', function () {
            var value = $(this).val();
            $("#infos-tuile-content #tuile-visuel .grid-element-gru-content p").each(function () {
                this.style.setProperty('font-size', value + "px", 'important');
            });
        });
        $("#body-color").on('change', function () {
            var value = $(this).val();
            $("#infos-tuile-content #tuile-visuel .grid-element-gru-content p").each(function () {
                this.style.setProperty('color', value, 'important');
            });
        });
        $("#body-background-color").on('change', function () {
            var value = $(this).val();
            $("#infos-tuile-content #tuile-visuel .grid-element-gru-content p").each(function () {
                this.style.setProperty('background-color', value, 'important');
            });
        });
        $("#icon-font-size").on('change', function () {
            var value = $(this).val();
            $("#infos-tuile-content #tuile-visuel .grid-element-gru-content i").each(function () {
                this.style.setProperty('font-size', value + "px", 'important');
            });
        });
        $("#icon-color").on('change', function () {
            var value = $(this).val();
            $("#infos-tuile-content #tuile-visuel .grid-element-gru-content i").each(function () {
                this.style.setProperty('color', value, 'important');
            });
        });
    },
    /**
     * Génère le DOM d'une tuile de type text
     * 
     * @returns {String}
     */
    create_element_type_text: function () {
        var tuile_id = $(".gestion-tuile-gru-block-gru.new-text").length + 1;
        var tuile_element_pattern = '<div class="element-gru element-gru-text box-shadow-gru" data-tuile-id="new_' + tuile_id + '_0" name="new_' + tuile_id + '_0">' +
                                        '<div class="row content-text">' +
                                            '<div class="col-lg-6">' +
                                                '<div class="element-gru-header">' +
                                                    '<div class="title">' +
                                                        '<span class="gestion-page-gru-element-title">Titre ...</span>' +
                                                    '</div>' +
                                                '</div>' +
                                                '<div class="grid-element-gru-content">' +
                                                    '<p> Description ... </p>' +
                                                '</div>' +
                                            '</div>' +
                                        '</div>' +
                                    '</div>';
        return tuile_element_pattern;
    },
    
    create_element_type_link: function() {
        var tuile_id = $(".gestion-tuile-gru-block-gru.new-text").length + 1;
        var tuile_element_pattern = '<div class="element-gru box-shadow-gru" data-tuile-id="new_' + tuile_id + '_4" name="new_' + tuile_id + '_4">' +
                                        '<div class="element-gru-header">' +
                                            '<div class="title">' +
                                                '<span class="gestion-page-gru-element-title">Titre ...</span>' +
                                            '</div>' +
                                        '</div>' +
                                        '<div class="grid-element-gru-content">' +
                                            '<i class=""></i>' + 
                                            '<p> Description ... </p>' +
                                        '</div>' +
                                    '</div>';
        return tuile_element_pattern;
    },
    
    prepare_save_data: function () {
        var id_tuile = $("#tuile_id").val();
        var list_child = [];
        $("#select-list-tuile li.selected").each(function () {
            list_child.push($(this).data('tuile-id'));
        });
        var data = {
            'action': 'save_tuile_content',
            'tuile_id': id_tuile,
            'tuile_title': $("#tuile_title").val(),
            'tuile_type': $("#tuile_type").val(),
            'tuile_icon': $("#tuile_icon").val(),
            'tuile_image': ($("#tuile_image").prop('files')[0] !== undefined) ? $("#tuile_image").prop('files')[0].name : "",
            'tuile_image_sup': $("#tuile_image_sup").val(),
            'tuile_list': list_child,
            'tuile_description': $("#tuile_description").val(),
            'tuile_description_plus': $("#tuile_description_plus").val(),
            'tuile_function_id': $("#tuile_function_id").val(),
            'tuile_page_id': ($("#tuile_page_id").is(':visible')) ? $("#tuile_page_id").val() :  $("#tuile_page_id_external").val()
        };
        if ($("#select-page-tuile-style-content").is(':visible')) {
            data.style = {};
            data.style['title-font-family'] = $("#select-page-tuile-style-content #title-font-family").val();
            data.style['body-font-family'] = $("#select-page-tuile-style-content #body-font-family").val();
            data.style['title-font-size'] = $("#select-page-tuile-style-content #title-font-size").val();
            data.style['body-font-size'] = $("#select-page-tuile-style-content #body-font-size").val();
            data.style['title-color'] = $("#select-page-tuile-style-content #title-color").val();
            data.style['title-background-color'] = $("#select-page-tuile-style-content #title-background-color").val();
            data.style['body-color'] = $("#select-page-tuile-style-content #body-color").val();
            data.style['body-background-color'] = $("#select-page-tuile-style-content #body-background-color").val();
            data.style['icon-color'] = $("#select-page-tuile-style-content #icon-color").val();
            data.style['icon-font-size'] = $("#select-page-tuile-style-content #icon-font-size").val();
        }

        return data;
    },
    
    get_tuile_data: function (id_tuile) {
        var that = this;
        var data = {
            'action': 'admin_create_tuile_new',
            'tuile_type': 'TuileLink'
        };
        var confirm;
        var custom_buttons = {
            formSubmit: {
                text: 'Enregistrer',
                btnClass: 'btn-blue',
                action: function () {
                    var data = that.prepare_save_data();
                    var callback = function (data) {
                        var updated_tuile = $(data.tuile_html);
                        $("#tuile-list #style-tag-" + data.lastid).remove();
                        $("#tuile-list .element-gru[data-tuile-id=" + data.lastid + "]").replaceWith(updated_tuile);
                        that.bind_tuile_event();
                    };
                    general_admin_settings.ajax_call(data, callback);
                }
            }
        };
        var parent_ctg = $(".element-gru[data-tuile-id=" + id_tuile + "]").closest('div.row');
        var cancel_button = {
            text: 'Annuler',
            action: function () {
            }
        }
        var dupplicate_button = {
            text: 'Dupliquer la tuile',
            btnClass: 'btn-blue',
            action: function () {
                var data = that.prepare_save_data();
                data.tuile_id = "duplicate";
                data.tuile_type = "3";
                var callback = function (data) {
                    var duplicated_tuile = $(data.tuile_html);
                    $("#tuile-list #type3 .content-list").append(duplicated_tuile);
                    that.bind_tuile_event();
                };
                general_admin_settings.ajax_call(data, callback);
            }
        };
        var delete_button = {
            text: 'Supprimer la tuile',
            btnClass: 'btn-red',
            action: function () {
                var data = that.prepare_save_data();
                var popupmain = this;
                $.confirm({
                    title: 'Supprimer la tuile ?',
                    boxWidth: '400px',
                    useBootstrap: false,
                    container: "#gestion-page-gru-content",
                    scrollToPreviousElement: false,
                    scrollToPreviousElementAnimate: false,
                    content: 'Vous supprimerez définitivement cet élément, êtes-vous sur ?',
                    buttons: {
                        Oui: {
                            action: function () {
                                data.tuile_delete = 'delete';
                                var callback = function (data) {
                                    $("#tuile-list #style-tag-" + data.lastid).remove();
                                    $("#tuile-list .element-gru[data-tuile-id=" + data.lastid + "]").remove();
                                    that.bind_tuile_event();
                                    popupmain.close();
                                };
                                general_admin_settings.ajax_call(data, callback);
                            }
                        }, Non: {
                        }
                    }
                });
                return false;
            } // end action
        };
        switch (parent_ctg.attr('id')) {
            case "type0" :
                custom_buttons['deleteTuile'] = delete_button;
                break;
            case "type1" :
                break;
            case "type2" :
                break;
            case "type3" :
                custom_buttons['duplicateTuile'] = dupplicate_button;
                custom_buttons['deleteTuile'] = delete_button;
                break;
            case "type4" :
                custom_buttons['deleteTuile'] = delete_button;
                break;
        }

        custom_buttons['cancelTuile'] = cancel_button;
        confirm = $.confirm({
            title: 'Edition tuile',
            boxWidth: '1500px',
            useBootstrap: false,
            container: "#gestion-page-gru-content",
            scrollToPreviousElement: false,
            scrollToPreviousElementAnimate: false,
            buttons: custom_buttons,
            content: function () {
                var self = this;
                $.ajax({
                    method: "POST",
                    url: general_admin_settings.base_url + "/wp-admin/admin-ajax.php",
                    data: data,
                    success: function (data) {
                        self.setContent(data);
                        that.bind_tuile_event();
                        that.bind_tuile_form_event();
                    }
                });
            }
        });
    }
};
