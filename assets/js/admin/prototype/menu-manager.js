/**
 * 1997-2018 Quadra Informatique
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 *
 *  @author    Quadra Informatique <modules@quadra-informatique.fr>
 *  @copyright 1997-2018 Quadra Informatique
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * 
 */
general_admin_settings.menu_manager = {
    /**
     * Array contenant les infos des menus
     * @type Array
     */
    menu_list: [],
    
    /**
     * Function d'initation
     * 
     * @returns {void}
     */
    init: function() {
        this.bind_menu_event();
    },
    
    /**
     * Bind l'ensemble des events liés à la structure de gestion du menu
     * 
     * @returns {void}
     */
    bind_menu_event : function () {
        var that = this;
        
        $("#parent-menu-order-content .fa.fa-trash").unbind();
        $("#parent-menu-order-content .fa.fa-trash").on('click',function() {
            if($(this).parent().hasClass('libelle')) {
                $(this).parent().parent().remove();
            } else {                
                if($(this).parent().hasClass('sub-libelle')) {
                    $(this).parent().next().remove();
                }
                $(this).parent().remove();
            }
        });
        
        $("#parent-menu-order-content > ul > li.libelle").unbind();
        $("#parent-menu-order-content > ul > li.libelle").on('click',function(e){
            if($(e.target).hasClass('fa-trash')) {
                return false;
            }
            
            $("#selectable-pages-for-menu li").removeClass('selected');
            if($(this).hasClass('open')) {
                $(this).removeClass('open');
                $(this).next('li.menu-content').slideUp(200);
                $("#update_menu_id").val('');
                $("#update_menu_libelle").val('');
            } else {
                $("#parent-menu-order-content ul li.libelle").removeClass('open');
                $("#parent-menu-order-content ul li.menu-content").slideUp(200);
                $(this).addClass('open');
                $(this).next('li.menu-content').slideDown(200);
                $("#update_menu_id").val($(this).data('id'));
                $("#update_menu_libelle").val($(this).data('libelle'));
                that.highligth_target("#update_menu_libelle");
                that.highligth_target("#selectable-pages-for-menu");        
                $(this).parent().find('.menu-content li.sub-libelle').removeClass('open');
                $("#parent-menu-order-content").find('i.fa-chevron-circle-down').removeClass('fa-chevron-circle-down').addClass('fa-chevron-circle-right');
                
                if($(this).data('list-page') !== "") {
                    $.each($(this).data('list-page'), function(i,e) {
                        $("#selectable-pages-for-menu li[data-id="+e+"]").addClass('selected');
                    });                    
                }
            }            
                
            if($(this).find('i.fa').hasClass('fa-chevron-circle-right')) {
                $(this).find('i.fa').removeClass('fa-chevron-circle-right');
                $(this).find('i.fa').addClass('fa-chevron-circle-down');
            } else {
                $(this).find('i.fa').removeClass('fa-chevron-circle-down');
                $(this).find('i.fa').addClass('fa-chevron-circle-right');
            }
        });
        
        $("li.menu-content > ul > li.sub-libelle").unbind();
        $("li.menu-content > ul > li.sub-libelle").on('click',function(e){
            if($(e.target).hasClass('fa-trash')) {
                return false;
            }
            if($(this).hasClass('open')) {
                $(this).removeClass('open');
                $(this).next('li.menu-content').slideUp(200);
            } else {
                $(this).addClass('open');
                $(this).next('li.menu-content').slideDown(200);
            }
                
            if($(this).find('i.fa').hasClass('fa-chevron-circle-right')) {
                $(this).find('i.fa').removeClass('fa-chevron-circle-right');
                $(this).find('i.fa').addClass('fa-chevron-circle-down');
            } else {
                $(this).find('i.fa').removeClass('fa-chevron-circle-down');
                $(this).find('i.fa').addClass('fa-chevron-circle-right');
            }
        });
        
        $("li.menu-content > ul > li.menu-content > ul > li.link").unbind();
        $("li.menu-content > ul > li.sub-libelle, li.menu-content > ul > li.menu-content > ul > li.link").on('click',function(e) {
            if($(e.target).hasClass('fa-trash')) {
                return false;
            }
            $("li.menu-content > ul > li.sub-libelle, li.menu-content > ul > li.menu-content > ul > li.link").removeClass('selected');
            $(this).addClass('selected');
            that.highligth_target("#update_sub_menu_libelle, #select-page-gru");
            $('#update_sub_menu_libelle').val($(this).data('libelle'));
            $('#select-page-gru').val($(this).data('id-page-link'));
        });
        
        $("#parent-menu-order-content > ul > li.add-element.menu").unbind();
        $("#parent-menu-order-content > ul > li.add-element.menu").on('click',function() {
            var previous = $(this).parent().prev().find('li.libelle');
            var next_id = (previous.data('menu-id') === undefined) ? 1 : parseInt(previous.data('menu-id'))+1;
            var element = that.create_element_parent(next_id);
            $(this).parent().before(element);            
            that.bind_menu_event();
        });
        
        $("li.menu-content > ul > li.add-element.menu").unbind();
        $("li.menu-content > ul > li.add-element.menu").on('click',function() {
            var previous = $(this).prev().prev('.sub-libelle');
            var next_id = (previous.data('sub-menu-id') === undefined) ? 1 : parseInt(previous.data('sub-menu-id'))+1;
            var element = that.create_element_menu(next_id);
            $(this).before(element);            
            that.bind_menu_event();
        });
        
        $("li.menu-content > ul > li.menu-content > ul > li.add-element.sub-menu").unbind();
        $("li.menu-content > ul > li.menu-content > ul > li.add-element.sub-menu").on('click',function() {
            var previous = $(this).prev('.link');
            var next_id = (previous.data('link-id') === undefined) ? 1 : parseInt(previous.data('link-id'))+1;
            var element = that.create_element_sous_menu(next_id);
            $(this).before(element);
            that.bind_menu_event();
        });
        
        var list_page_menu = function() {
            var list_page = [];
            $.each($("#selectable-pages-for-menu li.selected"), function(i,e) {
                list_page.push($(e).data('id'));
            });
            $("#parent-menu-order-content > ul > li.libelle.open").data('list-page',list_page);
        };
        
        $("#infos-menu-order-content #check_all, #infos-menu-order-content #uncheck_all").unbind();
        $("#infos-menu-order-content #check_all").on('click', function() {
            $("#selectable-pages-for-menu li").addClass('selected');          
            list_page_menu();
        });
        
        $("#infos-menu-order-content #uncheck_all").on('click', function() {   
            $("#selectable-pages-for-menu li").removeClass('selected');
            list_page_menu();
        });
        
        
        $("#selectable-pages-for-menu li").unbind();
        $("#selectable-pages-for-menu li").on('click',function() {
            $(this).toggleClass('selected');    
            list_page_menu();
                     
        });                        
        
        $("#update_menu_libelle, #update_sub_menu_libelle").unbind();
        $("#update_menu_libelle").on('keyup',function() {
            $("#parent-menu-order-content > ul > li.libelle.open").data('libelle',$(this).val()).find('span.libelle').html($(this).val());
        });
        $("#update_sub_menu_libelle").on('keyup',function() {
            $("li.sub-libelle.selected, li.link.selected").data('libelle',$(this).val()).find('span.libelle').html($(this).val());
        });
        $("#select-page-gru").unbind();
        $("#select-page-gru").on('change',function() {
            $("li.sub-libelle.selected, li.link.selected").data('id-page-link', $(this).val());
        });
    },
    
    /**
     * Mets en surbrillance les inputs à modifier
     * 
     * @param {string} selector
     * @returns {void}
     */
    highligth_target: function(selector) {
        $(selector).addClass('highligth-target');
        $(selector).prop('disabled',false);
        setTimeout(function() {                    
            $(selector).removeClass('highligth-target').addClass('highligth-target-off');
        },500);
    },
    
    /**
     * Génère le DOM d'un élément parent du menu
     * 
     * @param {string} next_id
     * @returns {String}
     */
    create_element_parent: function(next_id) {
        var parent_element_pattern = '<ul>' +
                                        '<li class="libelle border-bottom" data-list-page="[]" data-menu-id="'+next_id+'" data-libelle="Libellé menu" data-default="">' +
                                            '<i class="fa fa-chevron-circle-right"></i>'+
                                            '<span class="libelle">Libellé menu</span>'+
                                            '<i class="fa fa-trash"></i>'+
                                        '</li>' +
                                        '<li class="menu-content" style="display: none;">'+
                                            '<ul>' +                                
                                                '<li class="add-element menu"> <i class="fa fa-plus-circle"></i> Ajouter un élément </li>' +
                                            '</ul>' + 
                                        '</li>' +                                
                                    '</ul>';
        return parent_element_pattern;
    },    
    
    /**
     * Génère le DOM d'un élément du menu
     * 
     * @param {string} next_id
     * @returns {String}
     */
    create_element_menu: function(next_id) {
        var menu_element_pattern =  '<li class="sub-libelle" data-sub-menu-id="'+next_id+'" data-slug="" data-libelle="Lien menu" data-id-page-link="">' +
                                        '<i class="fa fa-chevron-circle-right"></i>' +
                                        '<span class="libelle">Lien menu</span>'+
                                        '<i class="fa fa-trash"></i>' +
                                    '</li>' +
                                    '</li>' +
                                    '<li class="menu-content" style="display: none;">' +
                                        '<ul>' +
                                            '<li class="add-element sub-menu">' +
                                                '<i class="fa fa-plus-circle"></i>'+
                                                '<span class="libelle">Ajouter un élément</span>'+
                                                '<i class="fa fa-trash"></i>' +
                                            '</li>'+
                                        '</ul>' +
                                    '</li>';
        return menu_element_pattern;
    },
    
    /**
     * Génère le DOM d'un élément enfant du menu
     * 
     * @param {string} next_id
     * @returns {String}
     */
    create_element_sous_menu: function(next_id) {
        var sous_menu_element_pattern = '<li class="link" data-link-id="'+next_id+'" data-libelle="Sous-menu" data-slug="" data-id-page-link="">' +
                                            '<i class="fa fa-link"></i>' +
                                            '<span class="libelle">Sous-menu</span>' +
                                            '<i class="fa fa-trash"></i>' +
                                        '</li>';
        return sous_menu_element_pattern;
    },
    
    /**
     * Génère la structure du menu créer avec l'IHM
     * 
     * @param {string} _menu_parent
     * @returns {Array}
     */
    generate_menu_list: function(_menu_parent) {
        var sub_menu_list = [];
        var menu_content = $(_menu_parent).next();
        $(menu_content).find('li.sub-libelle').each(function() {
            var sub_menu_content = $(this).next();
            var childs_list = [];
            $(sub_menu_content).find('li.link').each(function() {
                childs_list.push({'link-id' : $(this).data('link-id'), 'id-page-link': $(this).data('id-page-link'), 'libelle': $(this).data('libelle')});
            });            
            sub_menu_list.push({'sub-menu-id' : $(this).data('sub-menu-id'), 'libelle': $(this).data('libelle'), 'id-page-link': $(this).data('id-page-link'), childs: childs_list});
        });
        return sub_menu_list;
    },
    
    /**
     * Prépare les données à envoyer en POST pour sauvegarde
     * 
     * @returns {Array}
     */
    prepare_save_menu_list: function() {
        var that = this;
        this.menu_list = [];
        $("#parent-menu-order-content > ul > li.libelle").each(function () {
            var childs_list = that.generate_menu_list($(this));
            that.generate_menu_list("#" + $(this).attr('id'), that.grid_current_value);
            that.menu_list.push({'menu-id': $(this).data('menu-id'), 'libelle': $(this).data('libelle'), 'list-page': $(this).data('list-page'), childs : childs_list});
        });

        return this.menu_list;
    }
};