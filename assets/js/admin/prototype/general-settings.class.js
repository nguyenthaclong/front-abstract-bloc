/**
 * 1997-2018 Quadra Informatique
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 *
 *  @author    Quadra Informatique <modules@quadra-informatique.fr>
 *  @copyright 1997-2018 Quadra Informatique
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * 
 */
var $ = jQuery;
general_admin_settings = {
    /**
     * Base url utilisé pour l'ajax
     * @type String
     */
    base_url:"",
    
    /**
     * Function d'init du prototype
     * @returns {void}
     */
    init: function () {
        var get_url = window.location;
        this.base_url = get_url .protocol + "//" + get_url.host + get_url.pathname.split('/wp-admin')[0];
        if(general_admin_settings.grid_manager !== undefined) {
            general_admin_settings.grid_manager.init();
        }
        
        if(general_admin_settings.menu_manager !== undefined) {
            general_admin_settings.menu_manager.init();            
        }
        
        if(general_admin_settings.tuile_mangager !== undefined) {
            general_admin_settings.tuile_mangager.init();            
        }
        
        if(general_admin_settings.function_manager !== undefined) {
            general_admin_settings.function_manager.init();            
        }
    },
    
    /**
     * Gère lse appels ajax et les retours associés
     * 
     * @param {object} data
     * @returns {void}
     */
    ajax_call: function (data, callback) {
        var callback = callback || function() {};
        
        $.ajax({
            method: "POST",
            url: this.base_url + "/wp-admin/admin-ajax.php",
            data: data,
            dataType: 'json',
            success: function (data) {
                if (data.length !== undefined) {
                    $("#gestion-gru-ajax-msg div.notice").hide();
                    $("#gestion-gru-ajax-msg div.notice p.msg").html('');
                    $.each(data, function (i, e) {
                        switch (e.code) {
                            case 0:
                                $("#gestion-gru-ajax-msg div.error.notice p.msg").append(e.message + "<br/>");
                                $("#gestion-gru-ajax-msg div.error.notice").show(200);
                                break;
                            case 1:
                                $("#gestion-gru-ajax-msg div.updated.notice p.msg").append(e.message + "<br/>");
                                $("#gestion-gru-ajax-msg div.updated.notice").show(200);
                                break;
                            case 2:
                                $("#gestion-gru-ajax-msg div.infos.notice p.msg").append(e.message + "<br/>");
                                $("#gestion-gru-ajax-msg div.infos.notice").show(200);
                                break;
                        }
                    });
                } else {
                    $("#gestion-gru-ajax-msg div.notice").hide();
                    switch (data.code) {
                        case 0:
                            $("#gestion-gru-ajax-msg div.error.notice p.msg").html(data.message);
                            $("#gestion-gru-ajax-msg div.error.notice").show(200);
                            break;
                        case 1:
                            $("#gestion-gru-ajax-msg div.updated.notice p.msg").html(data.message);
                            $("#gestion-gru-ajax-msg div.updated.notice").show(200);
                            break;
                        case 2:
                            $("#gestion-gru-ajax-msg div.infos.notice p.msg").html(data.message);
                            $("#gestion-gru-ajax-msg div.infos.notice").show(200);
                            break;
                    }
                }
                
                setTimeout(function() {
                    $("#gestion-gru-ajax-msg div.notice").hide(200);
                },3000);
                
                callback(data);
            }
        });
    }
};
