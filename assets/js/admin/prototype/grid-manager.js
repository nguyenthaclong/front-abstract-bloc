/**
 * 1997-2018 Quadra Informatique
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 *
 *  @author    Quadra Informatique <modules@quadra-informatique.fr>
 *  @copyright 1997-2018 Quadra Informatique
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * 
 */
general_admin_settings.grid_manager = {
    /**
     * ype de l'effet associé a l'affichage des éléments
     * @type String
     */
    effect_type: 'puff',

    /**
     * Durée de l'effet définis au dessus
     * @type Number
     */
    effect_time: 200,

    /**
     * Container jquery de la grille
     * @type type
     */
    grid_container: $("#grid-page-gru"),

    /**
     * Grille actuelle
     * @type Array
     */
    grid_current_value: "",

    /**
     * Compteur d'éléments présents dans la grille 
     * @type Number
     */
    grid_increment_element: 1,

    /**
     * Compteur d'éléments présents dans la grille (utilisé pour l'enregistrement de la grille
     * @type Number
     */
    grid_nb_items: 0,

    last_edited_cde_tuile: null,

    /**
     * Function d'initation
     * 
     * @returns {void}
     */
    init: function () {
        this.bind_grid_event();
    },

    /**
     * Ajout d'un élément dans le DOM de la grille
     * 
     * @param {object} _element Element jquery
     * @param {object} _target Element jquery
     * @returns {void}
     */
    add_grid_element: function (_element, _target) {
        var target = _target || this.grid_container;
        this.grid_nb_items++;
        if (target === this.grid_container) {
            $(_element).appendTo(target).show(this.effect_type, this.effect_time);
        } else {
            $(target).find('.grid-element-content:first').append(_element);
            $(_element).show(this.effect_type, this.effect_time);
        }
        $(_element).attr('id', 'grid-element-' + this.grid_nb_items);
    },

    /**
     * Retire un élément du DOM de la grille
     * 
     * @param {object} _element Element jquery
     * @returns {void}
     */
    remove_grid_element: function (_element) {
        $(_element).remove();
    },

    create_grid_element_multi: function (_name, is_already_in_section = true) {
        var row = $(this.create_grid_element('row'));
        row.attr('id', 'grid-item-' + this.grid_increment_element);
        this.grid_increment_element++;
        var col = parseInt(_name.replace('multi-', ''));
        var nb = 12 / col;
        for (i = 0; i < nb; i++) {
            this.grid_nb_items++;
            var block = $(this.create_grid_element('col-lg-' + col, 'grid-element-' + this.grid_nb_items));
            block.show();
            row.find('.grid-element-content:first').append(block);
        }
        row.show();
        if (is_already_in_section) {
            return row;
        } else {
            var section = $(this.create_grid_element('section'));
            section.find('.grid-element-content:first').append(row);
            return section;
    }
    },

    /**
     * Génère le DOM d'un élément de la grille
     * 
     * @param {String} _name
     * @returns {String}
     */
    create_grid_element: function (_name, _id) {
        var sizeup = (_name === "row" || _name === "section") ? "" : '<div class="sizeup" style="display:none;"><i class="fa fa-chevron-right"></i></div>';
        var id = _id || "";

        var color_picker = "";
        if (_name === "section") {
            var color_picker = "<i class='edit-section fa fa-pencil-alt' title='Edition section'></i>";
        }

        var grid_element_pattern = "<div class='grid-element " + _name + "' name='" + _name + "' id='" + id + "' style='display:none;'>" +
                "<div class='row'>" +
                "<div class='col-lg-12'>" +
                "<div class='row grid-element-header'>" +
                "<div class='actions'>" +
                "<i class='delete-block fa fa-trash' title='Supprimer le block'></i>" +
                "<i class='clone-block fa fa-clone' title='Duppliquer le block'></i>" +
                color_picker +
                "</div>" +
                "<div class='title-header'>" +
                "<span class='gestion-page-gru-element-title'>" + _name + "</span>" +
                "</div>" +
                "</div>" +
                sizeup +
                "<div class='row grid-element-content'>" +
                "</div>" +
                "</div>" +
                "</div> " +
                "</div>";
        return grid_element_pattern;
    },

    /**
     * Génère le DOM d'un élément GRU de la grille
     * 
     * @param {String} _element_gru
     * @returns {String}
     */
    create_element_gru: function (_element_gru) {
        var element = $(_element_gru);

        switch (element.data('tuile-type')) {
            case 0 :
                var element_gru_pattern = "<div class='element-gru element-gru-text box-shadow-gru' name='" + element.attr('name') + "' style='display:none; background-image:url(" + element.data('tuile-image-href') + ")'>" +
                        "<div class='row content-text'>" +
                        "<div class='col-lg-6'>" +
                        "<div class='element-gru-header'>" +
                        "<div class='actions float-left'>" +
                        "<i class='delete-block fa fa-trash' title='Supprimer le block'></i>" +
                        "</div>" +
                        "<div class='title'>" +
                        "<span class='gestion-page-gru-element-title'>" + element.data('tuile-title') + "</span>" +
                        "</div>" +
                        "</div>" +
                        "<div class='grid-element-gru-content'>" +
                        "<p>" + element.data('tuile-description') + "</p>" +
                        "</div>" +
                        "</div>" +
                        "</div>" +
                        "</div>";

                break;
            default :
                var function_id = element.data('tuile-function-id');
                if (function_id !== "" && function_id != "0" && general_admin_settings.function_canvas_param[function_id].data !== false) {
                    var desc = ($(this).hasClass('gestion-page-gru-cde-function')) ? '' : "<p>" + element.data('tuile-description') + "</p>";
                    var params_canvas_html = "<div class='grid-element-gru-content row'>" +
                            "<div class='offset-lg-1 col-lg-5'>" +
                            "<i class='fa fa-" + element.data('tuile-icon') + "'></i>" +
                            "</div>" +
                            "<div class='col-lg-6 cde-function-params '>" +
                            "<div>" +
                            "<i class='fa fa-cogs'></i> Paramètres" +
                            "</div><br/>" +
                            "<button>Modifier</button>" +
                            "</div><br/>" +
                            desc +
                            "</div>";
                } else {
                    var params_canvas_html = "<div class='grid-element-gru-content'>" +
                            "<i class='fa fa-" + element.data('tuile-icon') + "'></i>" +
                            "<p>" + element.data('tuile-description') + "</p>" +
                            "</div>";
                }

                var element_gru_pattern = "<div class='element-gru box-shadow-gru' name='" + element.attr('name') + "' data-function-id='" + function_id + "' style='display:none;'>" +
                        "<div class='element-gru-header'>" +
                        "<div class='actions float-left'>" +
                        "<i class='delete-block fa fa-trash' title='Supprimer le block'></i>" +
                        "</div>" +
                        "<div class='title'>" +
                        "<span class='gestion-page-gru-element-title'>" + element.data('tuile-title') + "</span>" +
                        "</div>" +
                        "</div>" +
                        params_canvas_html +
                        "</div>";
                break;
        }
        return element_gru_pattern;
    },

    /**
     * Génère le DOM d'un élément function CDE de la grille
     * 
     * @param {String} _element_cde
     * @returns {String}
     */
    create_element_cde_function: function (_element_cde) {
        var element = $(_element_cde);
        var params_canvas = general_admin_settings.function_canvas_param[element.data('function-id')];

        var params_canvas_html = "<p> Aucun paramètres à renseigner </p>";
        if (params_canvas.data !== false || params_canvas.childs.length !== 0) {
            params_canvas_html = '<div><i class="fa fa-cogs"></i>Paramètres</div><br/><button>Modifier</button>';
        }


        var element_cde_function_pattern = "<div class='element-cde-function element-gru box-shadow-gru' name='" + element.attr('name') + "' data-function-id='" + element.data('function-id') + "'>" +
                "<div class='element-gru-header'>" +
                "<div class='actions float-left'>" +
                "<i class='delete-block fa fa-trash' title='Supprimer le block'></i>" +
                "</div>" +
                "<p class='text-center'>" + element.data('function-libelle') + "</p>" +
                "</div>" +
                "<div class='col-lg-12 content-text'>" +
                "<div class='row'>" +
                "<div class='col-lg-12 cde-function-params text-center'>" +
                params_canvas_html +
                "</div>" +
                "</div>" +
                "</div>" +
                "</div>";
        return element_cde_function_pattern;
    },

    /**
     * Créer la popup pour l'ajout d'une nouvelle page
     * 
     * @returns void
     */
    create_add_new_page_dialog: function () {

        $.confirm({
            title: 'Ajouter une nouvelle page',
            content: '<div action="" class="formName">' +
                    '<div class="form-group">' +
                    '<label>Libellé</label>' +
                    '<input id="new_page_libelle" type="text" placeholder="Libellé de la page..." class="form-control" />' +
                    '</div>' +
                    '</div>'
            ,
            boxWidth: '500px',
            useBootstrap: false,
            container: "#gestion-page-gru-content",
            scrollToPreviousElement: false,
            scrollToPreviousElementAnimate: false,
            buttons: {
                formSubmit: {
                    text: 'Créer la page',
                    btnClass: 'btn-blue',
                    action: function () {
                        var libelle = this.$content.find('#new_page_libelle').val();
                        if (libelle === "") {
                            $.confirm({
                                title: "Informations",
                                content: "Veuillez choisir renseigner un libellé avant d'ajouter une page",
                                buttons: {
                                    done: {
                                        text: "Ok"
                                    }
                                }
                            });
                            $("#new_page_libelle").focus();
                            return false;
                        }

                        var data = {
                            'action': 'add_new_page',
                            'libelle': libelle
                        };

                        var callback = function (data) {
                            $("#select-page-gru").append("<option value='" + data.id_post + "' data-link='" + data.data_link + "'>" + libelle + "</option>");
                            $("#new_page_libelle").val("");
                            $("#select-page-gru").val(data.id_post);
                            $("#grid-page-gru").html("");
                        };
                        general_admin_settings.ajax_call(data, callback);
                    }
                },
                cancel: {
                    text: 'Annuler'
                }
            }
        });
    },

    /**
     * Créer la popup pour la suppresion d'une nouvelle page
     * 
     * @returns void
     */
    remove_page_dialog: function () {
        var clone = $('#select-page-gru').clone();
        $.confirm({
            title: 'Supprimer une page',
            content: clone,
            boxWidth: '500px',
            useBootstrap: false,
            container: "#gestion-page-gru-content",
            scrollToPreviousElement: false,
            scrollToPreviousElementAnimate: false,
            buttons: {
                formSubmit: {
                    text: 'Supprimer la page',
                    btnClass: 'btn-blue',
                    action: function () {

                        var id = this.$content.find('#select-page-gru').val();
                        if (id === "") {
                            $.confirm({
                                title: "Informations",
                                content: "Veuillez selectionner une page avant de supprimer",
                                buttons: {
                                    done: {
                                        text: "Ok"
                                    }
                                }
                            });
                            return false;
                        }
                        $.confirm({
                            title: 'Supression !',
                            content: 'Etes vous sûr de vouloir supprimer cette page ?',
                            type: 'red',
                            typeAnimated: true,
                            buttons: {
                                Confirmer: {
                                    text: 'Confirmer',
                                    btnClass: 'btn-red',
                                    action: function () {
                                        var data = {
                                            'action': 'remove_page',
                                            'id-page': id
                                        };
                                        var callback = function (data) {
                                            setTimeout(function () {
                                                location.reload();
                                            }, 2000);
                                        };
                                        general_admin_settings.ajax_call(data, callback);
                                    }
                                },
                                Annuler: function () {
                                }
                            }
                        });
                    }
                },
                cancel: {
                    text: 'Annuler'
                }
            }
        });
    },

    /**
     * Crée la popup pour l'importe d'une configuration de module (NON OPERATIONNEL POUR LE MOMENT)
     * 
     * @returns void
     */
    create_import_config_dialog: function () {
        var that = this;
        $.confirm({
            title: 'Importer une configuration',
            content: '<div action="" class="formName">' +
                    '<div class="form-group">' +
                    '<label>Fichier à importer</label>' +
                    '<input id="import_file" type="file" accept=".xml" />' +
                    '</div>' +
                    '</div>',
            boxWidth: '500px',
            useBootstrap: false,
            container: "#gestion-page-gru-content",
            scrollToPreviousElement: false,
            scrollToPreviousElementAnimate: false,
            buttons: {
                formSubmit: {
                    text: 'Importer la configuration',
                    btnClass: 'btn-blue',
                    action: function () {

                        var input_file = this.$content.find('#import_file');
                        var file_data = input_file.prop('files')[0];

                        if (file_data === undefined) {
                            $.confirm({
                                title: "Informations",
                                content: "Veuillez choisir un fichier à importer",
                                buttons: {
                                    done: {
                                        text: "Ok"
                                    }
                                }
                            });
                            return false;
                        }

                        var form_data = new FormData();
                        form_data.append('file', file_data);
                        form_data.append('action', "import_configuration");
                        form_data.append('libelle', file_data.name);

                        $.ajax({
                            method: "POST",
                            url: general_admin_settings.base_url + "/wp-admin/admin-ajax.php",
                            data: form_data,
                            dataType: 'json',
                            contentType: false,
                            processData: false,
                            success: function (data) {
                                $("#gestion-gru-ajax-msg div.notice").hide();
                                switch (data.code) {
                                    case 0:
                                        $("#gestion-gru-ajax-msg div.error.notice p.msg").html(data.message);
                                        $("#gestion-gru-ajax-msg div.error.notice").show(200);
                                        break;
                                    case 1:
                                    case 2:
                                        $("#gestion-gru-ajax-msg div.infos.notice p.msg").html(data.message);
                                        $("#gestion-gru-ajax-msg div.infos.notice").show(200);

                                        $.confirm({
                                            title: 'Saisir les correpondances',
                                            content: data.html,
                                            boxWidth: '95%',
                                            useBootstrap: false,
                                            container: "#gestion-page-gru-content",
                                            scrollToPreviousElement: false,
                                            scrollToPreviousElementAnimate: false,
                                            onOpen: function () {
                                                that.bind_import_popin_event();
                                            },
                                            buttons: {
                                                formSubmit: {
                                                    text: 'Valider les correpondances',
                                                    btnClass: 'btn-blue',
                                                    action: function () {
                                                        var import_data = {'import_file': this.$content.find("#import-file").val(), 'config': [], 'homepage': '', 'grid': [], 'menu': []};

                                                        this.$content.find("#import-popup-content #config-import div.config-data").each(function () {
                                                            import_data.config.push({key: $(this).data('key'), value: $(this).find('.select-page-correspondance').val()});
                                                        });

                                                        this.$content.find("#import-popup-content #home-page").each(function () {
                                                            import_data.homepage = $(this).find('.select-page-correspondance').val();
                                                        });


                                                        this.$content.find("#import-popup-content #grid-import .toggle-import:not(.fa-rotate-180)").each(function () {
                                                            var row = $(this).closest('.grid-data');
                                                            import_data.grid.push({page_correspondance_grid_id: $(row).find('.select-page-correspondance').val(), page_correspondance_grid: $(row).data('grid')});
                                                        });

                                                        this.$content.find("#import-popup-content #menu-import > div.main-menu").each(function () {
                                                            var menu = {'menu-libelle': $(this).find('input[name=menu-libelle]').val(), 'childs': []};
                                                            if (!$(this).find('i.toggle-import.main-menu').hasClass('fa-rotate-180')) {
                                                                $(this).find('div.sub-menu').each(function () {
                                                                    if (!$(this).find('i.toggle-import.sub-menu').hasClass('fa-rotate-180')) {
                                                                        var sub_menu_childs = [];
                                                                        $(this).find('div.link').each(function () {
                                                                            if (!$(this).find('i.toggle-import.link').hasClass('fa-rotate-180')) {
                                                                                sub_menu_childs.push({
                                                                                    'link-id': $(this).attr('name'),
                                                                                    'libelle': $(this).find('input[name=link-libelle]').val(),
                                                                                    'id-page-link': $(this).find('.select-page-correspondance').val()
                                                                                });
                                                                            }
                                                                        });
                                                                        menu.childs.push({
                                                                            'sub-menu-id': $(this).attr('name'),
                                                                            'libelle': $(this).find('input[name=sub-menu-libelle]').val(),
                                                                            'id-page-link': $(this).find('.select-page-correspondance').val(),
                                                                            'childs': sub_menu_childs
                                                                        });
                                                                    }
                                                                });
                                                                import_data.menu.push(menu);
                                                            }
                                                        });

                                                        var data = {
                                                            action: "validate_import_configuration",
                                                            import_data: import_data
                                                        };

                                                        $("#gestion-gru-ajax-msg div.notice").hide(200);

                                                        $.confirm({
                                                            title: 'Attention !',
                                                            content: "L'import des données va écraser la confiugration actuelle, êtes vous sûr de vouloir continuer ?",
                                                            cancelButton: "Fermer",
                                                            icon: 'fa fa-exclamation-triangle',
                                                            confirmButtonClass: 'btn-warning',
                                                            animation: 'zoom',
                                                            buttons: {
                                                                confirm: {
                                                                    text: 'Oui, je valide l\'import',
                                                                    btnClass: 'btn-danger',
                                                                    action: function () {
                                                                        $.ajax({
                                                                            method: "POST",
                                                                            url: general_admin_settings.base_url + "/wp-admin/admin-ajax.php",
                                                                            data: data,
                                                                            dataType: 'json',
                                                                            success: function (data) {
                                                                                switch (data.code) {
                                                                                    case 0:
                                                                                        $("#gestion-gru-ajax-msg div.error.notice p.msg").html(data.message);
                                                                                        $("#gestion-gru-ajax-msg div.error.notice").show(200);
                                                                                        break;
                                                                                    case 1:
                                                                                        $("#gestion-gru-ajax-msg div.updated.notice p.msg").html(data.message);
                                                                                        $("#gestion-gru-ajax-msg div.updated.notice").show(200);
                                                                                        break;
                                                                                }

                                                                                setTimeout(function () {
                                                                                    location.reload();
                                                                                }, 3000);
                                                                            }
                                                                        });
                                                                    }
                                                                },
                                                                cancel: {
                                                                    text: "Fermer"
                                                                }
                                                            }
                                                        });
                                                    }
                                                },
                                                cancel: {
                                                    text: 'Annuler'
                                                }
                                            }
                                        });



                                        break;
                                }

                                setTimeout(function () {
                                    $("#gestion-gru-ajax-msg div.notice").hide(200);
                                }, 3000);
                            }
                        });
                    }
                },
                cancel: {
                    text: 'Annuler'
                }
            }
        });
    },

    /**
     * Crée la popup pour la gestion des paramètres liés aux sections
     * 
     * @param {Object} click_element Element target
     * @returns void
     */
    create_edit_dialog_section: function (click_element) {
        $.confirm({
            title: 'Edition section',
            content: '<div action="" class="formName">' +
                    '<div class="form-group">' +
                    '<label>Titre</label>' +
                    '<input id="section_title" type="text" placeholder="Titre de la section..." class="form-control" />' +
                    '</div>' +
                    '<div class="form-group">' +
                    '<label>Couleur de fond</label>' +
                    '<input id="section_colorpicker" type="text" placeholder="Couleur de fond de la section..." value="#FFFFFF" class="form-control" />' +
                    '</div>' +
                    '<div class="form-group">' +
                    '<label>Couleur du titre</label>' +
                    '<input id="section_title_colorpicker" type="text" placeholder="Couleur du titre..." value="#000000" class="form-control" />' +
                    '</div>' +
                    '</div>'
            ,
            boxWidth: '500px',
            useBootstrap: false,
            container: "#gestion-page-gru-content",
            scrollToPreviousElement: false,
            scrollToPreviousElementAnimate: false,
            onOpen: function () {
                var section = click_element.closest('.section');
                if (click_element.data('section_title') !== "") {
                    this.$content.find('#section_title').val(section.data('section_title'));
                }
                if (click_element.data('section_colorpicker') !== "") {
                    this.$content.find('#section_colorpicker').val(section.data('section_colorpicker'));
                }
                if (click_element.data('section_title_colorpicker') !== "") {
                    this.$content.find('#section_title_colorpicker').val(section.data('section_title_colorpicker'));
                }
                var options = {
                    color: '#RGB',
                    customBG: '#222', // bg of page is dark, so if opcity close to 0 -> dark shines through
                    doRender: 'div div', // tell it where to render bg-color if no input
                    zindex: 10000000,
                    colorNames: {// get more colors in the other demo... will be displayed next to color patch
                        '808080': 'grey',
                        '00FFFF': 'cyan',
                        '000000': 'black',
                        '0000FF': 'blue',
                        'FF00FF': 'magenta',
                        '008000': 'green',
                        'FF0000': 'red',
                        'C0C0C0': 'silver',
                        'FFFFFF': 'white',
                        'FFFF00': 'yellow'
                    }
                };
                $("#section_colorpicker, #section_title_colorpicker").colorPicker(options);

            },
            buttons: {
                formSubmit: {
                    text: 'Appliquer',
                    btnClass: 'btn-blue',
                    action: function () {
                        var section_title = this.$content.find('#section_title').val();
                        var section_colorpicker = this.$content.find('#section_colorpicker').val();
                        var section_title_colorpicker = this.$content.find('#section_title_colorpicker').val();
                        var section = click_element.closest('.section');

                        section.find('> div > div > .grid-element-header > .title-header > .gestion-page-gru-element-title').text(section_title);
                        section.find('> div > div > .grid-element-header').css('background-color', section_colorpicker)
                                .css('border-color', section_colorpicker)
                                .css('color', section_title_colorpicker);
                        section.data('section_title', section_title);
                        section.data('section_colorpicker', section_colorpicker);
                        section.data('section_title_colorpicker', section_title_colorpicker);
                    }
                },
                cancel: {
                    text: 'Annuler'
                }
            }
        });
    },

    /**
     * Crée la popup pour la gestion des paramètres liés aux tuiles qui en ont besoin
     * 
     * @param {String} cde_function_id ID de la fonction CDE
     * @returns void
     */
    create_edit_dialog_cde_function_param: function (cde_function_id, function_params) {
        var that = this;
        var function_params = function_params || {data: [], childs: []};
        var canvas_function = general_admin_settings.function_canvas_param[cde_function_id];
        if (canvas_function.data === false && canvas_function.childs === []) {
            return false;
        }

        var content_param = this.create_dynamic_cde_params_list(canvas_function, function_params);

        $.confirm({
            title: 'Gestion paramètres',
            content: '<div action="" class="formName">' +
                    content_param +
                    '</div>',
            boxWidth: '600px',
            useBootstrap: false,
            container: "#gestion-page-gru-content",
            scrollToPreviousElement: false,
            scrollToPreviousElementAnimate: false,
            onOpen: function () {

            },
            buttons: {
                formSubmit: {
                    text: 'Appliquer',
                    btnClass: 'btn-blue',
                    action: function () {
                        var dynamic_param_select = this.$content.find('select');
                        var selected_param = {'data': {}, 'childs': []};
                        $.each(dynamic_param_select, function (i, e) {
                            var key = $(e).attr('id');
                            var value = $(e).val();
                            if (value == "-1") {
                                return true;
                            }
                            selected_param.data[key] = value;
                        });

                        that.last_edited_cde_tuile.data('function-params', selected_param);
                    }
                },
                cancel: {
                    text: 'Annuler'
                }
            }
        });
    },

    /**
     * Génère le formulaire de saisi de params en fonction des canvas liés au fonction CDE
     * 
     * @returns {String}
     */
    create_dynamic_cde_params_list: function (dynamic_params, function_params) {
        var return_list = "<div class='dynamic-params'>";
        if (dynamic_params.data !== false) {
            $.each(dynamic_params.data, function (key, list) {
                return_list += "<div class='row'>" +
                        "<div class='col-lg-3'>" +
                        "<label>" + key + "</label>" +
                        "</div>" +
                        "<div class='col-lg-9'>" +
                        "<select id='" + key + "'>" +
                        "<option value='-1'> Choisir ... </option>";
                if (typeof list === "object") {
                    $.each(list, function (i, value) {
                        if (function_params.data) {
                            var is_selected = (function_params.data[key] === value) ? " selected" : "";
                            return_list += "<option value='" + value + "'" + is_selected + ">" + value + "</option>";
                        } else {
                            return_list += "<option value='" + value + "'>" + value + "</option>";

                        }
                    });
                } else {
                    return_list += "<option value='" + list + "'>" + list + "</option>";
                }
                return_list += "</select></div></div>";
            });
        }
        if (dynamic_params.childs.length > 0) {
            return_list += "<br/><br/><div class='row'>" +
                    "<div class='col-lg-12'>" +
                    "<h5>Champs à intégrer</h5>" +
                    "</div>" +
                    "</div>" +
                    "<div class='row'>";
            $.each(dynamic_params.childs, function (i, field) {
                var is_checked = (function_params.childs.indexOf(field) !== -1) ? " checked" : "";
                return_list += "<div class='col-lg-4'>" +
                        "<label>" + field + "</label>" +
                        "</div>" +
                        "<div class='col-lg-2'>" +
                        "<input type='checkbox' id='" + field + "'" + is_checked + " />" +
                        "</div>";
            });
            return_list += "</div>";
        }

        return_list += "</div>";
        return return_list;
    },

    /**
     * Bind l'ensemble des events liés à la structure de la grille
     * 
     * @returns {void}
     */
    bind_grid_event: function () {
        var that = this;
        var drag_prev_y = -1;
        var drag_element_current_position = null;
        var drag_element_current_position_inner_grid = null;
        var drag_direction = null;

        $("#grid-page-gru").sortable({
            axis: "y",
            containment: "#grid-page-gru",
            start: function (event, ui) {
                $(".gestion-page-gru-block-column:not(.section), .gestion-page-gru-block-gru").draggable().draggable("disable");
                $("#grid-page-gru, .grid-element-content").droppable().droppable("disable");
            },
            stop: function (event, ui) {
                $(".gestion-page-gru-block-column:not(.section), .gestion-page-gru-block-gru").draggable().draggable("enable");
                $("#grid-page-gru, .grid-element-content").droppable().droppable("enable");
            }
        });

        $(".gestion-page-gru-block-column").unbind('click');
        $(".gestion-page-gru-block-column").on('click', function () {
            if ($(this).attr('name').substr(0, 5) === "multi") {
                var block = that.create_grid_element_multi($(this).attr('name'), false);
            } else {
                var block = that.create_grid_element($(this).attr('name'));
            }
            that.add_grid_element($(block));
            that.bind_grid_event();
        });

        $(".gestion-page-gru-block-column:not(.section), .gestion-page-gru-block-gru, .gestion-page-gru-cde-function").draggable({
            cursor: "move",
            containment: '#gestion-page-gru-content',
            helper: "clone",
            revert: 'invalid',
            move: 10,
            zIndex: 10000,
            drag: function (event, ui) {
                if (drag_prev_y > event.pageY) {
                    drag_direction = "top";
                } else if (drag_prev_y < event.pageY) {
                    drag_direction = "bottom";
                }
                drag_prev_y = event.pageY;
                drag_element_current_position = event.pageY - $("#grid-page-gru").offset().top;
                drag_element_current_position_inner_grid = {x: event.pageX - $("#grid-page-gru").offset().left, y: event.pageY - $("#grid-page-gru").offset().top};
            }
        });

        $('.grid-element i.clone-block').unbind();
        $('.grid-element i.clone-block').on('click', function () {
            var clone = $(this).closest('.grid-element').clone();
            $(clone).find('.grid-element[id*=grid-element]').each(function () {
                that.grid_nb_items++;
                $(this).attr('id', 'grid-element-' + that.grid_nb_items);
            });

            var target = ($(this).closest('.grid-element-content').parent().length > 0) ? $(this).closest('.grid-element-content').parent() : that.grid_container;
            that.add_grid_element(clone, target);
            that.bind_grid_event();
        });

        $('.grid-element i.delete-block').unbind();
        $('.grid-element i.delete-block').on('click', function () {
            $(this).closest('.grid-element').hide('drop', {direction: "down"}, that.effect_time, function () {
                that.remove_grid_element(this);
            });
        });

        var section_parent = null;
        $(".grid-element i.edit-section").unbind();
        $(".grid-element i.edit-section").on('click', function () {
            that.create_edit_dialog_section($(this));
            section_parent = $(this).parent().parent();
        });

        $('.element-gru i.delete-block').unbind();
        $('.element-gru i.delete-block').on('click', function () {
            $(this).closest('.element-gru').hide('drop', {direction: "down"}, that.effect_time, function () {
                that.remove_grid_element(this);
            });
        });

        $('.cde-function-params button').unbind();
        $('.cde-function-params button').on('click', function () {
            var element_gru = $(this).closest('.element-gru');
            that.last_edited_cde_tuile = $(this).closest('.element-gru');
            that.create_edit_dialog_cde_function_param(element_gru.data('function-id'), element_gru.data('function-params'));
        });

        $(".grid-element:not(.section), .element-gru").draggable({
            helper: "clone",
            appendTo: "#grid-page-gru",
            revert: "invalid",
            move: 10,
            zIndex: 10000,
            cursor: "move",
            cursorAt: {top: 5, left: 5},
            start: function (event, ui) {
                $(ui.helper).addClass('helper-clone-draggable');
                $(this).css({"opacity": 0.6});
            },
            drag: function (event, ui) {
                drag_element_current_position_inner_grid = {x: event.pageX - $("#grid-page-gru").offset().left, y: event.pageY - $("#grid-page-gru").offset().top};
            }
        });

        var mouse_down = false;

        $(".grid-element").unbind('mouseenter mouseleave');
        $(".grid-element").on('mouseenter mouseleave', function (e) {
            e.stopPropagation();
            if (!mouse_down) {
                $(this).find('> .row > .col-lg-12 > .sizeup').toggle('slide', {direction: 'right'}, 100);
                $("#grid-page-gru").sortable({disabled: false});
                $(".ui-draggable.ui-draggable-handle").draggable({disabled: false});
            }
        });
        $("#grid-page-gru .grid-element .sizeup").unbind();
        $("#grid-page-gru .grid-element .sizeup").on('mousedown', function (e) {
            mouse_down = true;

            $("#grid-page-gru").sortable({disabled: true});
            $(".ui-draggable.ui-draggable-handle").draggable({disabled: true});
            var grid_element = $(this).closest('.grid-element');
            var ghostbar = $("#grid-ghostbar");
            ghostbar.show();
            ghostbar.height($(this).height());
            ghostbar.position({
                my: "left top",
                at: "right top",
                of: this,
                collision: "fit"
            });
            ghostbar.css({"margin-left": "-12px"});
            var start_width = ghostbar.width();
            var start_offset = ghostbar.offset().left;
            $(document).unbind('mousemove mouseup');
            $(document).on('mousemove', function (e) {
                if (start_offset < e.pageX) {
                    ghostbar.css({"margin-left": "-12px"});
                    ghostbar.css("width", start_width + e.pageX - ghostbar.offset().left);
                } else {
                    ghostbar.css("width", start_width + ghostbar.offset().left + ghostbar.width() - e.pageX);
                    ghostbar.css("margin-left", "-" + ghostbar.width() + "px");
                }
            });

            // ############################################################################
            // TODO IMBRIQUATION DE ROW MUTIPLE A CHECKER POUR L'AGRANDISSEMENT DE COLONNES
            $(document).on('mouseup', function (e1) {
                e.stopPropagation();
                var position_in_grid = e1.pageX - $("#grid-page-gru").offset().left;
                var cpt_col = 0;
                mouse_down = false;
                grid_element.find('> .row > .col-lg-12 > .sizeup').toggle('slide', {direction: 'right'}, 100);
                $("#grid-visual .col-lg-1").each(function () {
                    if (position_in_grid > $(this).position().left) {
                        cpt_col++;
                    } else {
                        return false;
                    }
                });

                var col_before = 0;
                var elements_before = grid_element.prevAll();
                $.each(elements_before, function (i, e) {
                    if ($(this).position().top === grid_element.position().top) {
                        var col = parseInt($(this).attr('name').replace('col-lg-', ''));
                        col_before += col;
                    }
                });

                var current_col = grid_element.attr('name');
                var new_col_size = ((cpt_col - col_before) < 0) ? cpt_col : cpt_col - col_before;
                var new_col = (new_col_size !== 0) ? 'col-lg-' + new_col_size : 'col-lg-' + 1;
                grid_element.removeClass(current_col).addClass(new_col).attr('name', new_col);
                grid_element.find('div > div > .grid-element-header .gestion-page-gru-element-title').text(new_col);
                ghostbar.width('10').hide();
                ghostbar.css({"margin-left": "0"});
                $(document).unbind('mousemove mouseup');
            });
            // ############################################################################
        });


        $("#grid-page-gru, .grid-element-content").droppable({
            hoverClass: "drop-hover",
            activeClass: "highlight",
            accepted: ".gestion-page-gru-block-column, .grid-element",
            tolerance: "pointer",
            greedy: true,
            deactivate: function () {
                $(".grid-element-content").removeClass('highlight');
            },
            over: function (event, ui) {
                if ($(event.target).attr('id') === "grid-page-gru") {
                    var cursor_position_drag = 40;
                    if ($("#grid-page-gru > div").length > 0) {
                        var grid_content_elements = $("#grid-page-gru > div");
                        grid_content_elements.each(function (i, e) {
                            var check_position_top = $(this).position().top;
                            if (drag_direction === "top") {
                                check_position_top += 50;
                            }
                            if (check_position_top < (drag_element_current_position - 10)) {
                                var section_check = ($(this).hasClass('section') && $(grid_content_elements[i + 1]).hasClass('section')) ? 8 : 0;
                                cursor_position_drag = $(this).position().top + $(this).outerHeight(true) - section_check;
                            }
                        });

                        if ($(".gestion-page-gru-tabs").css('position') === "fixed") {
                            cursor_position_drag += $(".gestion-page-gru-tabs").height();
                        }
                    }

                    $("#cursor-position-drag").css({top: cursor_position_drag}).show();
                } else {
                    $("#cursor-position-drag").hide();
                }
            },
            out: function (event, ui) {
            },
            drop: function (event, ui) {
                $("#cursor-position-drag").hide();
                if (ui.draggable.hasClass('gestion-page-gru-block-column')) {
                    if (ui.draggable.attr('name').substr(0, 5) === "multi") {
                        var is_already_in_section = $(event.target).parents(".grid-element.section").length >= 1 || $(event.target).parents(".grid-element").hasClass("row");
                        var draggable = $(that.create_grid_element_multi(ui.draggable.attr('name'), is_already_in_section));
                    } else {
                        var draggable = $(that.create_grid_element(ui.draggable.attr('name'))).show();
                    }
                    draggable.attr('id', 'grid-item-' + that.grid_nb_items);
                    that.grid_nb_items++;
                } else if (ui.draggable.hasClass('gestion-page-gru-block-gru')) {
                    var draggable = $(that.create_element_gru(ui.draggable)).show();
                } else if (ui.draggable.hasClass('gestion-page-gru-cde-function')) {
                    var draggable = $(that.create_element_cde_function(ui.draggable)).show();
                } else {
                    var draggable = ui.draggable;
                }

                // Si drop le div grille parent
                if ($(event.target).attr('id') === "grid-page-gru") {
                    if ($("#grid-page-gru > div").length > 0) {
                        var element = null;
                        var grid_content_elements = $("#grid-page-gru > div");
                        grid_content_elements.each(function (i, e) {

                            var check_position_top = $(this).position().top;
                            if (drag_direction === "top") {
                                check_position_top += 50;
                            }

                            if (check_position_top < (drag_element_current_position - 10)) {
                                element = $(this);
                            }
                        });

                        (element === null) ?
                                $(event.target).prepend(draggable) :
                                element.after(draggable);
                    } else {
                        $(event.target).append(draggable);
                    }
                } else {
                    var append = true;
                    var top_container = $(event.target).offset().top - that.grid_container.offset().top;
                    var drop_in_container_position = Math.abs(top_container - drag_element_current_position_inner_grid.y);
                    $(event.target).find('> .grid-element').each(function () {
                        if ( (drop_in_container_position < $(this).position().top) || ($(this).position().left > (drag_element_current_position_inner_grid.x) - 30)) {
                            $(this).before(draggable.css({left: 0, top: 0, 'z-index': 1}));
                            append = false;
                            return false;
                        }
                    });
                    if (append) {
                        $(this).append(draggable.css({left: 0, top: 0, 'z-index': 1}));
                    }
                }

                if (draggable !== undefined) {
                    draggable.css({"opacity": 1});
                    draggable.hide().show(that.effect_type, that.effect_time);
                }
                that.bind_grid_event();
            }
        });
    },

    /**
     * Bind l'ensemble des events liés à la popup d'import de config
     * 
     * @returns {void}
     */
    bind_import_popin_event: function () {
        $("#import-popup-content i.toggle-import").on('click', function () {
            if ($(this).hasClass('fa-rotate-180')) {
                $(this).removeClass('fa-rotate-180');
                if ($(this).hasClass('menu-import')) {
                    $(this).parent().parent().parent().find('i.toggle-import').removeClass('fa-rotate-180');
                }
            } else {
                $(this).addClass('fa-rotate-180');
                if ($(this).hasClass('menu-import')) {
                    $(this).parent().parent().parent().find('i.toggle-import').addClass('fa-rotate-180');
                }
            }
        });
    },

    /**
     * Génère l'objet json de la grille généré dans le DOM
     * 
     * @param {Object} _selector Element jquery
     * @param {Array} _grid Tableau grille enfant (utilisé en récursive)
     * @returns {void}
     */
    generate_grid_current_value: function (_selector, _grid) {
        var that = this;
        var push = ($(_selector).attr('name') === "section") ?
                {name: "grid-item-" + this.grid_increment_element, columns: $(_selector).attr('name'), items: [],
                    color: $(_selector).data('section_colorpicker'),
                    title: $(_selector).data('section_title'),
                    titlecolor: $(_selector).data('section_title_colorpicker')} :
                {name: "grid-item-" + this.grid_increment_element, columns: $(_selector).attr('name'), items: []};

        _grid.push(push);
        this.grid_increment_element++;
        var grid = _grid[_grid.length - 1].items;
        if ($(_selector).find('.grid-element').length > 0) {
            $('#' + $(_selector).attr('id') + ' > div > div > .grid-element-content > .grid-element').each(function () {
                that.generate_grid_current_value("#" + $(this).attr('id'), grid);
            });
        }

        if ($('#' + $(_selector).attr('id') + ' > div > div > .grid-element-content > .element-gru').length > 0) {
            $('#' + $(_selector).attr('id') + ' > div > div > .grid-element-content > .element-gru').each(function () {
                var key = $(this).attr('name').split('-').pop();
                var type_element = ($(this).hasClass('element-cde-function')) ? 'element-cde' : 'element-gru';
                if ($(this).data('function-id') !== undefined) {
                    var params_elem = $(this).data('function-params') || {data: []};
                    var push = {'name': type_element, 'key': key,
                        'function': {
                            'id': $(this).data('function-id'),
                            'data': params_elem.data
                        }
                    };
                } else {
                    var push = {'name': type_element, 'key': key};
                }
                grid.push(push);
            });
        }
    },

    /**
     * Prépare la grille json à envoyer en ajax avant sauvegarde
     * 
     * @returns {Array}
     */
    prepare_save_grid: function () {
        var that = this;
        this.grid_current_value = [];
        $("#grid-page-gru > .grid-element").each(function () {
            that.generate_grid_current_value("#" + $(this).attr('id'), that.grid_current_value);
        });
        return this.grid_current_value;
    },

    /**
     * Permet d'exporter la configuration du module
     * 
     * @returns void
     */
    export_config: function () {

        var data = {
            'action': 'export_module_configuration'
        };

        $.ajax({
            url: general_admin_settings.base_url + "/wp-admin/admin-ajax.php",
            type: 'POST',
            data: data,
            success: function (data) {
                $("body").append('<a id="download_link" href="' + data + '" download="export_module_gru.xml" />');
                var link = document.getElementById('download_link');
                link.click();
                $("a#download_link").remove();
            }
        });
    }
};