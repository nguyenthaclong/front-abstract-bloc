/**
 * 1997-2018 Quadra Informatique
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 *
 *  @author    Quadra Informatique <modules@quadra-informatique.fr>
 *  @copyright 1997-2018 Quadra Informatique
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * 
 */
jQuery(document).ready(function () {
    var $ = jQuery;
    general_admin_settings.init();
    $(".gestion-page-gru-button").button();
    $(".gestion-page-gru-tabs").tabs();

    $("#add-new-menu").on('click', function () {
        if ($("#new_menu_libelle").val() === "") {
            $.alert({
                title: "Informations",
                content: "Veuillez choisir renseigner un libellé avant d'ajouter un élément",
                confirm: function () {
                    $("#new_menu_libelle").focus();
                }
            });
            return false;
        }

        var data = {
            'action': 'add_new_menu',
            'libelle': $("#new_menu_libelle").val()
        };
        general_admin_settings.ajax_call(data);
    });
    
    $("#save-menu").on('click', function() {
        var data = {
            'action': 'save_menu_list',
            'data': general_admin_settings.menu_manager.prepare_save_menu_list()
        };
        general_admin_settings.ajax_call(data);
    });
});