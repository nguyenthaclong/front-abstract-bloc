/**
 * 1997-2018 Quadra Informatique
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 *
 *  @author    Quadra Informatique <modules@quadra-informatique.fr>
 *  @copyright 1997-2018 Quadra Informatique
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * 
 */
jQuery(document).ready(function () {
    var $ = jQuery;
    general_admin_settings.init();
    $(".gestion-page-gru-button").button();
    $("button#listing_teleservices").click(function () {
        $.alert({
            icon: 'fas fa-exclamation-triangle',
            type: 'orange',
            title: 'Synchronisation des Téléservices',
            boxWidth: '800px',
            useBootstrap: false,
            container: "#gestion-page-gru-content",
            scrollToPreviousElement: false,
            scrollToPreviousElementAnimate: false,
            content: 'Cette action est irréverssible et synchronisera les téléservices de votre plateforme GRU avec votre portail usagers. <br/>\n\
                      Certaines tuiles et/ou styles peuvent être supprimé ou modifié. Êtes-vous sûr de vouloir faire cela ?',
            buttons: {
                "Synchroniser les téléservices": {
                    btnClass: 'btn-orange',
                    action: function () {
                        
                        var data = {
                            action : "synchronize_services_list"
                        };
                        var callback = function() {
                            setTimeout(function() {
                                location.reload();
                            },3000);
                        };
                        general_admin_settings.ajax_call(data, callback);
                    }
                },
                "Annuler": function () {
                    this.close();
                }
            }
        });
    });
});
