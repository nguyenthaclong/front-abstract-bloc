/**
 * 1997-2018 Quadra Informatique
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 *
 *  @author    Quadra Informatique <modules@quadra-informatique.fr>
 *  @copyright 1997-2018 Quadra Informatique
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * 
 */
jQuery(document).ready(function () {
    var $ = jQuery;
    general_admin_settings.init();
    var offset_top = $('div.gestion-page-gru-tabs').offset().top;
    var width_tabs = $('div.gestion-page-gru-tabs').width();
    $(window).scroll(function () {
        if (window.pageYOffset > offset_top) {
            $('div.gestion-page-gru-tabs').css({
                'position': 'fixed',
                'width': width_tabs + "px",
                'top': 30
            });
            $("#grid-page-gru").css({
                "margin-top": $('div.gestion-page-gru-tabs').height() + "px"
            });
        } else {
            $('div.gestion-page-gru-tabs').css({
                'position': 'relative',
                'top': 0
            });
            $("#grid-page-gru").css({
                "margin-top": 0
            });
        }
    });


    $(".gestion-page-gru-button").button();
    $(".gestion-page-gru-tabs").tabs();

    $("#add-new-page").on('click', function () {
        general_admin_settings.grid_manager.create_add_new_page_dialog();
    });
    
   $("#remove-page").on('click', function () {
        general_admin_settings.grid_manager.remove_page_dialog();
    });
    
    $("#export-gru-config").on('click',function() {
        general_admin_settings.grid_manager.export_config();
    });
    
    $("#import-gru-config").on('click',function() {
        general_admin_settings.grid_manager.create_import_config_dialog();        
    });


    $("#show-page").on('click', function () {
        if ($("#select-page-gru").val() === "") {
            alert('Veuillez choisir une page');
            return false;
        }

        var win = window.open($("#select-page-gru").find(":selected").data('link'), '_blank');
        if (win) {
            win.focus();
        }
    });

    $("#save-page-grid").on('click', function (e) {
        if ($("#select-page-gru").val() === "") {
            alert('Veuillez choisir une page avant d\'enregistrer');
            return false;
        }

        var data = {
            'action': 'save_grid_page',
            'id-page': $("#select-page-gru").val(),
            'grid': general_admin_settings.grid_manager.prepare_save_grid()
        };
        general_admin_settings.ajax_call(data);
        e.preventDefault();

    });

    $("#select-page-gru").on('change', function () {
        $("#grid-page-gru").html("");
        $("#gestion-gru-ajax-msg div.notice").hide();
        $("#gestion-page-gru-content .loader").show();

        var data = {
            'action': 'get_grid_content',
            'id-page': $(this).val()
        };

        $.ajax({
            method: "POST",
            url: general_admin_settings.base_url + "/wp-admin/admin-ajax.php",
            data: data,
            dataType: 'json',
            success: function (data) {
                $("#grid-page-gru").html(data.content);
                $("#gestion-page-gru-content .loader").hide();
                general_admin_settings.grid_manager.grid_nb_items = $("#grid-page-gru .grid-element").length;
                general_admin_settings.grid_manager.bind_grid_event();
            },
            error: function (data) {
                $("#gestion-page-gru-content .loader").hide();
            }
        });
    });  
});