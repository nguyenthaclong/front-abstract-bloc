/**
 * 1997-2018 Quadra Informatique
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 *
 *  @author    Quadra Informatique <modules@quadra-informatique.fr>
 *  @copyright 1997-2018 Quadra Informatique
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * 
 */
jQuery(document).ready(function () {
    var $ = jQuery;
    general_admin_settings.init();
    $(".gestion-page-gru-button").button();
    $(".gestion-page-gru-tabs").tabs();

    $("#save-function").on('click', function () {
        var data = {
            'action': 'save_function_list',
            'data': general_admin_settings.function_manager.prepare_save_function_list()
        };
        general_admin_settings.ajax_call(data);
    });

    $("#check_connexion_response_button").on('click', function () {
        $("#check_connexion_response_loading").show();
        $("#check_connexion_response_infos div.error.notice").hide();
        $("#check_connexion_response_infos div.updated.notice").hide();
        $("#check_connexion_response").show('200', function () {
            var data = {
                'action': 'check_connexion_gru',
                'client_id' : $("#crm_client_id").val(),
                'client_secret' : $("#crm_client_secret").val()                
            };
            
            general_admin_settings.ajax_call(data, function (data) {
                $("#check_connexion_response_loading").hide(200);
                switch (data.code) {
                    case 0 :
                        $("#check_connexion_response_infos div.error.notice p.msg").html(data.message);
                        $("#check_connexion_response_infos div.error.notice").show(200);
                        break;
                    case 1:
                        $("#check_connexion_response_infos div.updated.notice p.msg").html(data.message);
                        $("#check_connexion_response_infos div.updated.notice").show(200);
                        break;
                }
            });
            
        });
    });
});