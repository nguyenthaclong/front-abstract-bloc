

// une fois que la page est chargée
$(document).ready(function () {
    //console.log($("#rue1").val());
    // on ajoute le onchange sur la rue
    $('#rue1').blur(function () {
        open_modal();
    });

    // sur le codepostal
    $('#code_postal').blur(function () {
        open_modal();
    });

    // sur la ville
    $('#ville').blur(function () {
        open_modal();
    });
});



function open_modal() {

    // Pour identifier les id des champs concernés
    var id_champ_rue = '#rue1';
    var id_champ_cp = '#code_postal';
    var id_champ_ville = '#ville';


    $('#titre_modal').html("Recherche d'adresse");
    $('#submit_modal').attr("disabled", "disabled");
    $('#corp_modal').html('');
    $('#error_form').html('');
    $('#process_form').html('Recherche en cours&nbsp;&nbsp;&nbsp;<i class="fa fa-spinner fa-spin fa-lg"></i>');


    var request_data = '';
    var request_url = location.protocol + "//api-adresse.data.gouv.fr/search/";


    var valeur_rue = $(id_champ_rue).val();

    if ('' != valeur_rue) {
        request_data += 'q=' + valeur_rue;

    } else {
        if ('' !== $(id_champ_ville).val()) {
            request_data += 'q=' + $(id_champ_ville).val();
            request_data += '&type=city';
        } else {
            return;
        }
    }

    if ('' == request_data) {
        return;
    }
    //console.log(request_data);
    if ('' != $(id_champ_cp).val()) {
        request_data += '&postcode=' + $(id_champ_cp).val();
    }

    //$('#adresse_id').val(champ_id);


    $.ajax({
        type: 'GET',
        url: request_url,
        data: request_data,
        success: function (msg) {
            var donnees = msg;
            var str_donnees = "";

            if (donnees.hasOwnProperty('features')) {
                var tableau_features = donnees['features'];
                if (0 !== tableau_features.length) {
                    for (var idx in tableau_features) {
                        str_donnees = str_donnees
                                + '<div class="radio"><label>'
                                + '<input id="adresse" type="radio" value="'
                                + tableau_features[idx]['properties']['name'] + ':'
                                + tableau_features[idx]['properties']['postcode'] + ':'
                                + tableau_features[idx]['properties']['city'] + ':'
                                + tableau_features[idx]['properties']['id']
                                + '" name="adresse"> '
                                + tableau_features[idx]['properties']['label']
                                + '</label></div>';
                    }
                    console.log(str_donnees);

                    $('#corp_modal').html(str_donnees);
                    $('#process_form').html('');
                    $('#submit_modal').removeAttr("disabled");
                } else {
                    // Aucune correspondance
                    $('#corp_modal').html('Aucune correspondance trouvée ...');
                    $('#process_form').html('');
                }


            }

        }

    });
    //$('#verifAdresseModal').modal('show');
    $('#verifAdresseModal').show();
    $('#verifAdresseModal').attr("aria-hidden", "false");

    $("button.close").css("margin-left", "0px");
    $("button.close").css("padding", "0 0 0 0");
    $('button.close').click(function () {
        $('#verifAdresseModal').hide();
        $('#verifAdresseModal').attr("aria-hidden", "true");
    });

}

function save_modal( ) {

    //var frm = $ ( '#save_field' );
    //var champ_id = '#' + $ ( '#field_record' ).val();

    var select_value = $('input[name=adresse]:checked', '#verif_adresse').val();

    var array_values = select_value.split(':');
    champ_id = $('#adresse_id').val();

    $('#rue1').val(array_values[0]);
    $('#code_postal').val(array_values[1]);
    $('#ville').val(array_values[2]);
    $('#pays').val('FRANCE');

    $('#verifAdresseModal').hide();
    $('#verifAdresseModal').attr("aria-hidden", "true");

}

















