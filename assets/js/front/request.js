(function ($) {
    $.grufront.validation_suppression_brouillon = function (id, label) {
        $.alert({
            title: "Attention",
            icon: 'fa fa-exclamation-triangle warning',
            type: 'orange',
            content: "Voulez-vous supprimer le brouillon : " + label + " ?",
            boxWidth: '500px',
            useBootstrap: false,
            container: "#gestion-page-gru-content",
            scrollToPreviousElement: false,
            scrollToPreviousElementAnimate: false,
            buttons: {
                confirm: {
                    text: 'Supprimer',
                    btnClass: 'btn-orange',
                    action: function () {
                        var data = {
                            'action': 'delete_request',
                            'id_demande': id
                        };

                        ajax_call(data, function () {
                            $('tr#' + id).hide(300, function () {
                          var nb = $('.content-link-notif tr#' + id).closest('.element-gru').find('.desc-dyn').text();
                            $('.content-link-notif tr#' + id).closest('.element-gru').find('.desc-dyn').text( parseInt(nb) - 1 );
                            $('.content-link-notif tr#' + id).remove();
                            $('.element-gru-notif-content-clone tr#' + id).hide(300, function () {
                                $(this).remove();
                            });
                            });
                        });
                    }
                },
                cancel: {
                    text: 'Annuler'
                }
            }
        });
    }
})(jQuery);



