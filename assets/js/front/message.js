(function ($) {

  $.grufront.download_document_message = function (parent_id, message_id, document_id) {
    const data = {
      'action': 'download_document_message',
      'parent_id': parent_id,
      'message_id': message_id,
      'document_id': document_id,
    };

    $.ajax({
      method: "POST",
      url: base_url + "/wp-admin/admin-ajax.php",
      data: data,
      dataType: 'json',
      success: function (data) {
        if (data.href) {
          $("body").append('<a id="download_link" href="' + data.href + '" download="' + data.filename + '" />');
          var link = document.getElementById('download_link');
          link.click();
          $("a#download_link").remove();
        } else {
          $.alert({
            title: "Informations",
            content: "Téléchargement n'a pas pu aboutir.",
            confirm: {
              text : 'Fermer'
            }
          });
        }
      }
    });
  };

})(jQuery);