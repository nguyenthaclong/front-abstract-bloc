(function ($) {
    $.grufront.entity_delete_validation = function (id, type, label) {

        $.confirm({
            title: "Êtes vous sûr de vouloir supprimer l'entité ?",
            content: "<div> Vous êtes sur le point de supprimer l'entité <strong>" + label + "</strong>",
            boxWidth: '500px',
            useBootstrap: false,
            container: "#gestion-page-gru-content",
            scrollToPreviousElement: false,
            scrollToPreviousElementAnimate: false,
            buttons: {
                confirm: {
                    text: 'Supprimer',
                    btnClass: 'btn-blue',
                    action: function () {
                        var data = {
                            'action': 'delete_entity',
                            'id_entity': id,
                            'type_entity': type
                        };
                        ajax_call(data, function () {
                            var nb = $('.content-link-notif tr#' + id).closest('.element-gru').find('.desc-dyn').text();
                            $('.content-link-notif tr#' + id).closest('.element-gru').find('.desc-dyn').text( parseInt(nb) - 1 );
                            $('.content-link-notif tr#' + id).remove();
                            $('.element-gru-notif-content-clone tr#' + id).hide(300, function () {
                                $(this).remove();
                            });
                        });
                    }
                },
                cancel: {
                    text: 'Annuler'
                }
            }
        });
    }
})(jQuery);
