(function ($) {
    
    $.grufront.show_document = function(doc_id) {
        var data = {
            'action': 'show_document',
            'doc_id': doc_id
        };

        $.ajax({
            method: "POST",
            url: base_url + "/wp-admin/admin-ajax.php",
            data: data,
            dataType : 'json',
            success: function (data) {
                window.open(data.url_visu);
            }
        });
    };
    
    
    $.grufront.download_document = function (doc_id) {
        var data = {
            'action': 'download_document',
            'doc_id': doc_id
        };

        $.ajax({
            method: "POST",
            url: base_url + "/wp-admin/admin-ajax.php",
            data: data,
            dataType: 'json',
            success: function (data) {
                $("body").append('<a id="download_link" href="' + data.href + '" download="' + data.filename + '" />');
                var link = document.getElementById('download_link');
                link.click();
                $("a#download_link").remove();
            }
        });
    };

    $.grufront.validation_suppression_document = function validation_suppression_document(id, label) {

        $.alert({
            title: "Attention",
            icon: 'fa fa-exclamation-triangle warning',
            type: 'orange',
            content: "Voulez-vous supprimer le document : " + label + " ?",
            boxWidth: '500px',
            useBootstrap: false,
            container: "#gestion-page-gru-content",
            scrollToPreviousElement: false,
            scrollToPreviousElementAnimate: false,
            buttons: {
                confirm: {
                    text: 'Supprimer',
                    btnClass: 'btn-orange',
                    action: function () {
                        var data = {
                            'action': 'delete_document',
                            'doc_id': id
                        };

                        ajax_call(data, function () {
                 var nb = $('.content-link-notif tr#' + id).closest('.element-gru').find('.desc-dyn').text();
                            $('.content-link-notif tr#' + id).closest('.element-gru').find('.desc-dyn').text( parseInt(nb) - 1 );
                            $('.content-link-notif tr#' + id).remove();
                            $('.element-gru-notif-content-clone tr#' + id).hide(300, function () {
                                $(this).remove();
                            });
                        });
                    }
                },
                cancel: {
                    text: 'Annuler'
                }
            }
        });
    };

})(jQuery);

