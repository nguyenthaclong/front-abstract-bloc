/**
 * 1997-2018 Quadra Informatique
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 *
 *  @author    Quadra Informatique <modules@quadra-informatique.fr>
 *  @copyright 1997-2018 Quadra Informatique
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * 
 */

var get_url = window.location;
var path_name = get_url.pathname.split('/');

(function ($) {
    $.grufront = {};
    $(document).ready(function () {

        var bind_event_details_list = function () {
            $(".element-gru-notif-content-clone .add_document").unbind();
            $(".element-gru-notif-content-clone .add_document").on('click', function () {
                $.confirm({
                    title: "Ajouter un document",
                    content: $(this).next('.add_document_popup').clone().show(),
                    boxWidth: '600px',
                    useBootstrap: false,
                    container: "#gestion-page-gru-content",
                    scrollToPreviousElement: false,
                    scrollToPreviousElementAnimate: false,
                    buttons: {
                        confirm: {
                            text: 'Ajouter le document',
                            btnClass: 'btn-blue upload_confirm',
                            action: function () {
                                var file_data = this.$content.find('#upload_document').prop('files')[0];
                                var form_data = new FormData();
                                form_data.append('file', file_data);
                                form_data.append('type_piece', this.$content.find('#list_piece').val());
                                form_data.append('action', "upload_gru_document");
                                $.ajax({
                                    method: "POST",
                                    url: base_url + "/wp-admin/admin-ajax.php",
                                    data: form_data,
                                    dataType: 'json',
                                    contentType: false,
                                    processData: false,
                                    success: function (data) {
                                        $("#gestion-gru-ajax-msg div.notice").hide();
                                        switch (data.code) {
                                            case 0:
                                                $("#gestion-gru-ajax-msg div.error.notice p.msg").html(data.message);
                                                $("#gestion-gru-ajax-msg div.error.notice").show(200);
                                                break;
                                            case 1:
                                                $("#gestion-gru-ajax-msg div.updated.notice p.msg").html(data.message);
                                                $("#gestion-gru-ajax-msg div.updated.notice").show(200);
                                                break;
                                        }
                                        setTimeout(function () {
                                            location.reload();
                                        }, 1500);
                                    }
                                });
                            }
                        },
                        cancel: {
                            text: 'Annuler'
                        }
                    }
                });
            });
        }

        $("#gestion-page-gru-content.nav-menu-gru .gestion-page-gru-ctg-menu").on('mouseenter', function () {
            if ($(this).find('.menu-content').find('ul.ctg-childs li').length > 0) {
                $("#gestion-page-gru-content.nav-menu-gru .gestion-page-gru-ctg-menu .menu-content").stop(true, true).slideUp();
                $(this).find('.menu-content').slideDown(100);
            }
        });

        $("#gestion-page-gru-content.nav-menu-gru .gestion-page-gru-ctg-menu").on('mouseleave', function () {
            if ($(this).find('.menu-content').find('ul.ctg-childs li').length > 0) {
                $(this).find('.menu-content').slideUp(100);
            }
        });

        $('.element-gru.element-ctg').on('click', function () {
            var that = $(this);
            if ($(this).find('.content-link-ctg').is(':visible')) {
                return false;
            }

            $('.element-gru.element-ctg').each(function () {
                if ($(this) !== that) {
                    $(this).slideUp(200);
                }
            });

            $('.element-gru.element-ctg .content-link-ctg').slideUp(200);
            $(this).find('.content-link-ctg').slideDown(200).position({
                my: "center top",
                at: "center center",
                of: $(this).closest(".row")
            });

            $(window).on('scroll', function () {
                that.find('.content-link-ctg').slideDown(200).position({
                    my: "center top",
                    at: "center center",
                    of: that.closest(".row")
                });
            });
        });

        $('.element-gru.element-ctg .content-link-ctg').on('mouseleave', function () {
            $(window).unbind('scroll');
            $(this).slideUp(200);
        });

        $('.element-gru.element-ctg .content-link-ctg a').on('click', function () {
            document.location = $(this).attr('href');
        });


        $('.element-gru.element-gru-notif').on('click', function () {
            var current = $('.element-gru-notif-content-clone');
            var clone = $(this).find('.content-link-notif').clone();
            $('.element-gru-notif-content-clone').slideUp(200, function () {
                $(this).remove();
            });
            if (current.length > 0 && current.attr('name') === clone.attr('name')) {
                return false;
            }
            $(this).closest('.row').after(clone);
            clone.addClass('element-gru-notif-content-clone');
            clone.slideDown(200);

            bind_event_details_list();
        });



        $(".element-gru .desc-dyn").each(function () {
            var element_gru = $(this).closest('.element-gru');
            if ($(element_gru).find('.content-link-notif table tbody h3').length > 0) {
                $(this).text(0);
            } else {
                $(this).text($(element_gru).find('.content-link-notif table tbody tr:not(.no-content)').length);
            }
        });


        var offset_top = $('.main_menu').offset().top;
        var width_tabs = $('.main_menu').width();
        $(window).scroll(function () {
            if (window.pageYOffset > offset_top) {
                $('.main_menu').css({
                    'position': 'fixed',
                    'width': width_tabs + "px"
                });
            } else {
                $('.main_menu').css({
                    'position': 'relative',
                    'top': 0,
                    '-webkit-transition': 'top 0.2s', /* For Safari 3.1 to 6.0 */
                    'transition': 'top 0.2s'
                });
                $("body").css({
                    "margin-top": 0
                });
            }
        });
    });

    $.grufront.set_context = function (context, callback, action) {
        var callback = callback || function () {
            location.reload();
        };
        var action = action || "set_context";
        var data = {
            'action': action,
            'data': context
        };

        $.ajax({
            method: "POST",
            url: base_url + "/wp-admin/admin-ajax.php",
            data: data,
            success: function (data) {
                callback();
            }
        });
    };

    $.grufront.add_to_context = function (context, callback) {
        $.grufront.set_context(context, callback, 'add_to_context');
    };

    $.grufront.history_back = function (url) {
        if (typeof url !== 'undefined') {
            window.location = url;
        } else {
            window.location = document.referrer;
        }
    };

    $("#creer_individu select#type").on('change', function () {
        switch ($(this).val()) {
            case 'enfant' :
                $("#creer_individu #profession").closest('.form_group').hide();
                $("#creer_individu #tel_bureau").closest('.form_group').hide();
                $("#creer_individu #email").closest('.form_group').hide();
                $("#creer_individu #situation_familiale").closest('.form_group').hide();
                $("#creer_individu #mobile").closest('.form_group').hide();
                break;
            case "adulte" :
                $("#creer_individu #profession").closest('.form_group').show();
                $("#creer_individu #tel_bureau").closest('.form_group').show();
                $("#creer_individu #email").closest('.form_group').show();
                $("#creer_individu #situation_familiale").closest('.form_group').show();
                $("#creer_individu #mobile").closest('.form_group').show();
                break;
        }
    });
})(jQuery);



function ajax_call(data, callback) {
    var callback = callback || function () {
    };
    $.ajax({
        method: "POST",
        url: base_url + "/wp-admin/admin-ajax.php",
        data: data,
        dataType: 'json',
        success: function (data) {
            if (data.length !== undefined) {
                $("#gestion-gru-ajax-msg div.notice").hide();
                $("#gestion-gru-ajax-msg div.notice p.msg").html('');
                $.each(data, function (i, e) {
                    switch (e.code) {
                        case 0:
                            $("#gestion-gru-ajax-msg div.error.notice p.msg").append(e.message + "<br/>");
                            $("#gestion-gru-ajax-msg div.error.notice").show(200);
                            break;
                        case 1:
                            $("#gestion-gru-ajax-msg div.updated.notice p.msg").append(e.message + "<br/>");
                            $("#gestion-gru-ajax-msg div.updated.notice").show(200);
                            break;
                        case 2:
                            $("#gestion-gru-ajax-msg div.infos.notice p.msg").append(e.message + "<br/>");
                            $("#gestion-gru-ajax-msg div.infos.notice").show(200);
                            break;
                    }
                });
            } else {
                $("#gestion-gru-ajax-msg div.notice").hide();
                switch (data.code) {
                    case 0:
                        $("#gestion-gru-ajax-msg div.error.notice p.msg").html(data.message);
                        $("#gestion-gru-ajax-msg div.error.notice").show(200);
                        break;
                    case 1:
                        $("#gestion-gru-ajax-msg div.updated.notice p.msg").html(data.message);
                        $("#gestion-gru-ajax-msg div.updated.notice").show(200);
                        break;
                    case 2:
                        $("#gestion-gru-ajax-msg div.infos.notice p.msg").html(data.message);
                        $("#gestion-gru-ajax-msg div.infos.notice").show(200);
                        break;
                }
            }

            setTimeout(function () {
                $("#gestion-gru-ajax-msg div.notice").hide(200);
            }, 3000);
            callback(data);
        }
    });
}
